<?php /* Template name: Page Builder */ ?>

<?php if ( have_rows('builder') ): while ( have_rows('builder') ) : the_row();

  if ( get_row_layout() == 'page_header' ):

    get_template_part( 'template-parts/intro-image', 'Page Header' );
    
  elseif ( get_row_layout() == 'logo_grid' ):
      
    get_template_part( 'template-parts/client-logos', 'Logo Grid' );

  elseif ( get_row_layout() == 'logo_linked' ):

    get_template_part( 'template-parts/client-logos-linked', 'Logo Grid Linked' );
    
  elseif ( get_row_layout() == 'services_slides' ):

    get_template_part( 'template-parts/service-tabs', 'Services Slides' );

  elseif ( get_row_layout() == 'testimonials' ):

    get_template_part( 'template-parts/testimonials', 'Testimonials' );

  elseif ( get_row_layout() == 'form' ):

    get_template_part( 'template-parts/form', 'Form' );

  elseif ( get_row_layout() == 'text_blocks' ):

    get_template_part( 'template-parts/text-blocks', 'Text Blocks' );

  elseif ( get_row_layout() == 'solutions' ):

    get_template_part( 'template-parts/solutions', 'Solutions' );

  elseif ( get_row_layout() == 'team' ):

    get_template_part( 'template-parts/team', 'Team' );

  elseif ( get_row_layout() == 'products_list' ):

    get_template_part( 'template-parts/products', 'Products' );

  elseif ( get_row_layout() == 'accordion' ):

    get_template_part( 'template-parts/faqs', 'FAQs' );

  elseif ( get_row_layout() == 'features' ):

    get_template_part( 'template-parts/features-alt', 'Features' );

  elseif ( get_row_layout() == 'hero' ):

    get_template_part( 'template-parts/hero', 'Hero' );
    
  elseif ( get_row_layout() == 'other' ):
      
      // get_template_part( 'components/pageHeader', 'Page Header'); 
      
  elseif ( get_row_layout() == 'pricing' ):
    
    get_template_part( 'template-parts/pricing-builder', 'Pricing' );
    
  elseif ( get_row_layout() == 'video' ):
    
    get_template_part( 'template-parts/product-video', 'Video' );
    
  endif;

endwhile; else: endif;