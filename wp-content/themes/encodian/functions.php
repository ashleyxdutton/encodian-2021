<?php
/*
 * bgnBlankWp Includes
 * The $bgnBlankWp_includes array determines the code library included into the site.
 */

$bgnBlankWp_includes = [
  'inc/functions-root.php',
  'inc/functions-admin.php',
  'inc/functions-email.php',
  'inc/functions-theme.php',
  'inc/functions-helper.php',
  'inc/functions-optimisation.php',
  'inc/register-products.php',
  'inc/register-industries.php',
  'inc/register-policies.php',
  'inc/register-forms.php',
  'inc/load-svg.php'
];

foreach ($bgnBlankWp_includes as $file) {

  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'bgn-blank-wp'), $file), E_USER_ERROR);
  }

  require_once $filepath;

}

unset($file, $filepath);