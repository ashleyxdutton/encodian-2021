<?php
/*
  Template Name: Flowr & Vectr Pages
  Template Post Type: products
*/ ?>

<div class="sidebar pos--abs">
  <div class="container container--l align--center">
    <div class="sidebar__content pos--rel">
      <img class="sidebar__icon style-svg" src="<?= get_field('icon'); ?>" alt="">
      <p class="sidebar__title fs--18 fw--700" style="color: <?= the_field('colour'); ?>"><?= the_field('sidebar_title'); ?></p>
      <p class="sidebar__text fs--18"><?= the_field('sidebar_text'); ?></p>
      <a class="sidebar__button button-outline pos--rel" href="<?= get_field('sidebar_button')['url']; ?>" style="color: <?= the_field('colour'); ?>; border-color: <?= the_field('colour'); ?>;" data-to="form">
        <span><?= get_field('sidebar_button')['title']; ?></span>
        <div class="pos--abs" style="background: <?= the_field('colour'); ?>;"></div>
      </a>
    </div>
  </div>
</div>

<a class="fixedBtn pos--fix z--9" href="#" data-to="form">
  <img class="fixedBtn__icon style-svg pos--abs" src="<?= get_field('icon'); ?>" alt="">
  <p class="fixedBtn__label fs--16 fw--700 fc--white"><?= get_field('sidebar_button')['title']; ?></p>
</a>

<nav class="quicklinks pos--abs z--9" data-quicklinks>
  <div class="container container--l flex flex--x-end">
    <ul class="quicklinks__list flex flex--inline">
      <li class="quicklinks__link fc--white active" data-quicklink="overview">Overview</li>
      <li class="quicklinks__link fc--white" data-quicklink="benefits">Features</li>
      <li class="quicklinks__link fc--white" data-quicklink="pricing">Pricing</li>
      <li class="quicklinks__link fc--white" data-quicklink="faqs">FAQs</li>
    </ul>
  </div>
</nav>

<section class="productIntro" style="background-color: <?= the_field('colour'); ?>" data-inview data-section="overview">
  <div class="container container--l">

    <div class="productIntro__content o--0">
      <h1 class="productIntro__heading" data-split-lines><?= the_field('heading'); ?></h1>
      <div class="sidebar sidebar--intro pos--abs z--2">
        <div class="container container--l align--center">
          <div class="sidebar__content pos--rel">
            <img class="sidebar__icon style-svg" src="<?= get_field('icon'); ?>" alt="">
            <p class="sidebar__title fs--18 fw--700 fc--white"><?= the_field('sidebar_title'); ?></p>
            <p class="sidebar__text fs--18"><?= the_field('sidebar_text'); ?></p>
            <a class="sidebar__button button-outline" href="<?= get_field('sidebar_button')['url']; ?>" data-to="form"><?= get_field('sidebar_button')['title']; ?></a>
          </div>
        </div>
      </div>
      <p class="productIntro__text fs--24"><?= the_field('text'); ?></p>
      <div class="productIntro__powered flex flex--y-center">
        <p class="fs--18">Powered by</p>
        <a href="<?= the_field('powered_by_link'); ?>" target="_blank"><img src="<?= the_field('powered_by_logo'); ?>" alt=""></a>
      </div>
      <div class="productIntro__statistic pos--rel">
        <span class="ff--head fs--80 fw--900 fc--white" data-count="<?= the_field('statistics_number'); ?>"><?= the_field('statistics_number'); ?></span>
        <p class="fs--24 fc--white"><?= the_field('statistics_label'); ?></p>
        <?= file_get_contents('' . get_site_url() . '/wp-content/themes/encodian/src/svgs/statistic-sketch.svg'); ?>
      </div>    
    </div>
  </div>
</section>

<?php get_part('benefit-logos'); ?>

<?php get_part('features'); ?>

<?php get_part('pricing'); ?>

<?= get_template_part( 'template-parts/form' ); ?>

<?php get_part('faqs'); ?>