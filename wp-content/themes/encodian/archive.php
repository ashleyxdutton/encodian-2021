<?php /* Archive Template */ ?>

<?php if (have_posts()) : ?>

  <section class="container spacing"> 
    <div class="row">

      <h1><?php single_cat_title(); ?></h1>

      <?php while (have_posts()) : the_post();

        get_part( 'article');

      endwhile; ?>

    </div>
  </section>

<?php else :

  get_part( 'error');

endif;
