<?php /*
Template Name: Policy Pages
Template Post Type: policies, page
*/ ?>

<section class="post post--basic">
  <div class="container container--s">
    <h1><?= get_the_title(); ?></h1>
    <?php the_content(); ?>
  </div>
</section>