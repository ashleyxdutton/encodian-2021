<section class="imageIntro <?= get_sub_field('image') ? '' : 'imageIntro--noImage' ?>" data-inview>
  <div class="container container--l flex">

    <?php if ( get_sub_field('image') ): ?>
      
      <div class="imageIntro__imageHolder pos--rel">
        <img class="imageIntro__image pos--abs" src="<?= get_sub_field('image')['url']; ?>" alt="<?= get_sub_field('image')['alt']; ?>">
        <?= load_svg('services-mask'); ?>
      </div>

    <?php endif; ?>

    <div class="imageIntro__content">
      <h1 class="imageIntro__heading" data-split-lines><?= get_sub_field('heading'); ?></h1>
      <p class="imageIntro__text fs--24" data-inview><?= get_sub_field('text'); ?></p>
    </div>

  </div>
</section>