const mix = require('laravel-mix')
const src = `src`
const dist = `dist/`
const url = `http://localhost:8000`

mix
.options({
  processCssUrls: false
})
.webpackConfig({
  module: {
    rules: [{
      test: /\.scss/,
      enforce: "pre",
      loader: 'import-glob-loader'
    }]
  }
})
.js(`${src}/js/main.js`, `${dist}/main.bundle.js`)
.sass(`${src}/scss/main.scss`, `/main.css`)
.setPublicPath(dist)
.version() 

.browserSync({
  proxy: url,
  browser: `Google Chrome`,
  notify: true,
  open: false,
  files: [
    `${dist}/main.css`,
    `${dist}/main.js`,
    `**/*.php`
  ]
})
 