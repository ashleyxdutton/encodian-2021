<?php
/*
  Template Name: Filer Page
  Template Post Type: products
*/
?>

<div class="sidebar pos--abs z--2">
  <div class="container container--l align--center">
    <div class="sidebar__content pos--rel">
      <img class="sidebar__icon style-svg" src="<?= get_field('icon'); ?>" alt="">
      <p class="sidebar__title fs--18 fw--700" style="color: <?= the_field('colour'); ?>"><?= the_field('sidebar_title'); ?></p>
      <p class="sidebar__text fs--18"><?= the_field('sidebar_text'); ?></p>
      <a class="sidebar__button button-outline pos--rel" href="#" data-to="form" style="color: <?= the_field('colour'); ?>; border-color: <?= the_field('colour'); ?>;">
        <span><?= get_field('sidebar_button')['title']; ?></span>
        <div class="pos--abs" style="background: <?= the_field('colour'); ?>;"></div>
      </a>
    </div>
  </div>
</div>

<a class="fixedBtn pos--fix z--9" href="#" data-to="form">
  <img class="fixedBtn__icon style-svg pos--abs" src="<?= get_field('icon'); ?>" alt="">
  <p class="fixedBtn__label fs--16 fw--700 fc--white"><?= get_field('sidebar_button')['title']; ?></p>
</a>

<nav class="quicklinks pos--abs z--9" data-quicklinks>
  <div class="container container--l flex flex--x-end">
    <ul class="quicklinks__list flex flex--inline">
      <li class="quicklinks__link fc--white active" data-quicklink="overview">Overview</li>
      <li class="quicklinks__link fc--white" data-quicklink="benefits">Benefits</li>
      <li class="quicklinks__link fc--white" data-quicklink="features">Features</li>
      <li class="quicklinks__link fc--white" data-quicklink="testimonials">Testimonials</li>
    </ul>
  </div>
</nav>

<section class="productIntro productIntro--filer pos--rel z--3" style="background-color: <?= the_field('colour'); ?>" data-inview data-section="overview">
  <div class="container container--l">

    <div class="productIntro__content o--0">
      <h1 class="productIntro__heading" data-split-lines><?= the_field('heading'); ?></h1>
      <div class="sidebar sidebar--intro pos--abs z--2">
      <div class="container container--l align--center">
          <div class="sidebar__content pos--rel">
            <img class="sidebar__icon style-svg" src="<?= get_field('icon'); ?>" alt="">
            <p class="sidebar__title fs--18 fw--700 fc--white"><?= the_field('sidebar_title'); ?></p>
            <p class="sidebar__text fs--18"><?= the_field('sidebar_text'); ?></p>
            <a class="sidebar__button button-outline" href="#" data-to="form"><?= get_field('sidebar_button')['title']; ?></a>
          </div>
        </div>
      </div>
      <p class="productIntro__text fs--24"><?= the_field('text'); ?></p>
    </div>

  </div>
</section>

<?php get_part('benefit-logos'); ?>

<?php get_part('benefits'); ?>

<?php get_part('features-alt'); ?>

<section class="testimonials testimonials--product" style="background-color: <?= the_field('colour'); ?>" data-section="testimonials">
  <div class="container container--l">
    <div class="testimonials__header" data-inview>
      <h2 class="testimonials__heading" data-split-lines><?= the_field('testimonial_heading'); ?></h2>
      <p class="testimonials__text"><?= the_field('testimonial_text'); ?></p>
    </div>

    <?php $testimonials = get_field('add_testimonials'); ?>

    <div class="testimonials__holder pos--rel" data-inview="testimonials">

      <ul class="testimonials__slider pos--rel z--2" data-testimonials>
        <?php foreach( $testimonials as $testimonial): ?>
          <li class="testimonials__item">
            <div class="testimonials__author pos--rel flex flex--x-center">
              <img class="testimonials__image" src="<?= $testimonial['image']['url'] ?>" alt="">
              <p class="testimonials__name fs--16 fw--700"><?= $testimonial['author'] ?></p>
              <p class="testimonials__company fs--16"><?= $testimonial['company_title'] ?></p>
            </div>
            <p class="testimonials__quote fs--24"><?= $testimonial['quote'] ?></p>
          </li>
        <?php endforeach; ?>
      </ul>

      <div class="testimonials__bg pos--abs">
        <?= file_get_contents('' . get_site_url() . '/wp-content/themes/encodian/src/svgs/testimonials-clip.svg'); ?>
        <?= file_get_contents('' . get_site_url() . '/wp-content/themes/encodian/src/svgs/testimonials-bg.svg'); ?>
      </div>

    </div>

  </div>
</section>

<?= get_template_part( 'template-parts/form' ); ?>