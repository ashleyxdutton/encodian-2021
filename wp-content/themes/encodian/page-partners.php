<section class="introBasic" data-inview>
  <div class="introBasic__content o--0 z--3">
    <h1 class="introBasic__heading" data-split-lines><?= the_field('intro_heading'); ?></h1>
    <p class="introBasic__text fs--24"><?= the_field('intro_text'); ?></p>
  </div>
</section>

<section class="partners">
  <div class="container container--m">

    <?php $partners = get_field('partners'); ?>

    <ul class="partners__list flex">
      <?php foreach( $partners as $partner ): ?>
        <li class="partners__item flex flex--y-center align--center">
          <div class="partners__logo flex flex--y-center flex--x-center">
            <img src="<?= $partner["logo"]["url"]; ?>" alt="<?= $partner["logo"]["alt"]; ?>">
          </div>
          <p class="partners__text"><?= $partner["text"]; ?></p>
          <a class="partners__button button" href="<?= $partner["button"]["url"]; ?>" target="<?= $partner["button"]["target"]; ?>"><?= $partner["button"]["title"]; ?></a>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
</section>

<section class="clientLogos" data-inview="client">
  <?php $logos = get_field('resellers_logos'); ?>
  <div class="container container--l">
    <h2 class="clientLogos__heading align--center" data-split-lines><?= the_field('resellers_heading'); ?></h2>
    <p class="clientLogos__text fs--24"><?= the_field('resellers_text'); ?></p>
    <ul class="clientLogos__list flex">
      <?php foreach( $logos as $logo): ?>
        <li class="clientLogos__item flex flex--x-center flex--y-center">
          <a href="<?= $logo['logo_link']; ?>" target="_blank"><img src="<?= $logo['logo']["url"]; ?>" alt="<?= $logo['logo']["alt"]; ?>"></a>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
</section>

<?= get_template_part( 'template-parts/form' ); ?>