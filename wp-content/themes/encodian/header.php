<header class="header pos--fix z--max">
  <div class="container container--l flex flex--x-between flex--y-center">

    <a class="header__logo z--9" href="/">
      <?= load_svg('logo'); ?>
    </a>

    <nav class="header__nav flex flex--y-end">

      <?php wp_nav_menu( array(
        'theme_location' => 'header-menu'
      ) ); ?>

      <!-- <a class="header__account flex flex--y-center" href="https://account.encodian.com/" target="_blank">
        <p class="fs--16 fw--500">My Account</p>
        <?= load_svg('account'); ?>
      </a> -->

    </nav>

    <a class="header__menuButton pos--rel" href="">
      <span></span>
    </a>

  </div>
</header>