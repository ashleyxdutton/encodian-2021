<?php

  $category = get_category(get_query_var( 'cat' ));

  $posts = new WP_Query([
    'post_type'=>'post',
    'orderby'=>'date',
    'order' => 'DESC',
    'cat' => $category->cat_ID,
  ]);

?>

<section class="posts posts--search pos--rel">
  <div class="container container--m">

    <h2 class="posts__title">Category: <?= $category->name ?></h2>
    <p class="posts__results"><?= $posts->found_posts; ?> Results</p>

    <div class="posts__filters flex flex--x-between">

      <?php
        $cat_args = array( 'orderby' => 'name', 'order' => 'ASC' );
        $categories = get_categories($cat_args);
      ?>

      <select class="posts__select" name="filterCategory" id="" onChange="window.location.href=this.value">
        <option value="#">Filter by category</option>
        <?php foreach($categories as $category): ?>
          <option value="/category/<?= $category->slug; ?>"><?= $category->name; ?></option>
        <?php endforeach; ?>
      </select>

      <?php get_search_form(); ?> 
      
      <script>
        document.querySelector('.searchform #s').placeholder = "Keyword search";
      </script>

    </div>

    <ul class="posts__list flex flex--x-between">

      <?php if($posts->have_posts()): while($posts->have_posts()): $posts->the_post(); ?>

        <li class="posts__item">
          <a class="posts__imageHolder pos--rel" href="<?= get_the_permalink() ?>">
            <?= get_the_post_thumbnail(); ?>
            <?php if (get_field('show_video_button')): ?>
              <svg class="posts__play pos--abs center--xy" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><circle cx="50" cy="50" r="50" style="fill:#331f45"/><path d="M61.32,45.65a5,5,0,0,1,1.89,6.82,5.09,5.09,0,0,1-1.89,1.88L46.47,62.77a5,5,0,0,1-6.82-1.89A5,5,0,0,1,39,58.42V41.58a5,5,0,0,1,7.47-4.35Z" style="fill:#fff"/></svg>
            <?php endif; ?>
          </a>
          <a class="posts__heading" href="<?= get_the_permalink() ?>"><h2 class="fs--30"><?= get_the_title(); ?></h2></a>
          <p class="posts__excerpt fs--18"><?= get_the_excerpt(); ?></p>
          <a class="posts__button button-outline button-outline--black" href="<?= get_the_permalink() ?>">Read article</a>
        </li>

      <?php endwhile; ?>

    </ul>

  </div>

</section>

<?php
 
else: ?>

<section class="search">
  <div class="container container--m">
    <h2 class="search__heading fs--25 fw--700 lh--130">Sorry, no posts matched your criteria.</h2>
  </div>
</section>

<?php endif; ?>