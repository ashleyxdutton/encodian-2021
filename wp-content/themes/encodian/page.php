<?php
/*
 * Default Page Template
 */
if (have_posts()): while (have_posts()): the_post();

  get_part('content');

endwhile; endif;
