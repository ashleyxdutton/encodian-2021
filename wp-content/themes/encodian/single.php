<?php
/*
 * Single Post Template
 */
if (have_posts()): while (have_posts()): the_post(); ?>

<?php if (!get_field('hide_product_link')): ?>

<?php
  if (get_field('related_product')):
    $id = get_field('related_product');
  else:
    $id = 31;
  endif;
?>

<a class="fixedPromo pos--fix z--9 align--center" href="<?= get_the_permalink($id); ?>?form">
  <img class="fixedPromo__icon" src="<?= get_field('icon', $id); ?>" alt="">
  <p class="fixedPromo__text fs--14"><?= the_field('sidebar_text', $id); ?></p>
  <p class="fixedPromo__link fs--12 fw--700"><?= get_field('sidebar_button', $id)['title']; ?></p>
</a>

<?php endif; ?>

<section class="introPost introPost--single pos--rel">
  <div class="container container--l">

    <div class="introPost__header flex flex--x-between flex--y-start pos--rel z--2">
      <div class="introPost__imageHolder pos--rel">
        <?= get_the_post_thumbnail(); ?>
      </div>

      <a class="introPost__back pos--abs arrowBtn arrowBtn--previous" href="#"></a>

      <div class="introPost__content">
        <div class="introPost__meta flex">
          <p class="introPost__author fs--16">By <?= get_the_author(); ?></p>
          <p class="introPost__date fs--16"><?= get_the_date('jS F Y'); ?></p>
        </div>
        <h1 class="introPost__heading"><?= get_the_title(); ?></h1>
        <div class="introPost__share flex flex--y-center">
          <p class="fs--24">Share</p>
          <a class="introPost__shareLink introPost__shareLink--twitter" href="https://twitter.com/intent/tweet?url=<?= get_the_permalink(); ?>&text=&via=encodian" target="_blank" rel="nofollow">
            <?= file_get_contents('' . get_site_url() . '/wp-content/themes/encodian/src/svgs/icon-twitter.svg'); ?>
          </a>
          <a class="introPost__shareLink introPost__shareLink--linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?= get_the_permalink(); ?>" target="_blank" rel="nofollow">
            <?= file_get_contents('' . get_site_url() . '/wp-content/themes/encodian/src/svgs/icon-linkedin.svg'); ?>
          </a>
        </div>
      </div>
    </div>

  </div>

  <div class="introPost__bg introPost__bg--<?= get_field('header_colour'); ?> pos--abs"></div>

</section>

<section class="post">
  <div class="container container--s">
    <?php the_content(); ?>

    <?php if (get_field('video_url')): ?>
      <iframe class="post__video" width="560" height="315" src="https://www.youtube.com/embed/<?= get_field('video_url'); ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <?php endif; ?>

  </div>
</section>

<section class="nextPrev">
  <div class="container flex flex--x-between">

    <?php $prev = get_previous_post(); ?>
    <?php $next = get_next_post(); ?>

    <a class="nextPrev__link align--left" href="<?= get_permalink($prev->ID); ?>">
      <p class="fs--12">Prev post</p>
      <h2 class="fs--16"><?= get_the_title($prev->ID); ?></h2>
    </a>

    <a class="nextPrev__link align--right" href="<?= get_permalink($next->ID); ?>">
      <p class="fs--12">Next post</p>
      <h2 class="fs--16"><?= get_the_title($next->ID); ?></h2>
    </a>

  </div>
</section>

<?php if ( comments_open() || get_comments_number() ) :
  comments_template();
endif; ?>

<?php endwhile; endif; ?>

<section class="relatedPosts">
  <div class="container container--m">
    <h2 class="relatedPosts__heading">Related posts</h2>
    <ul class="posts__list flex flex--x-between">

      <?php

      $currentID = get_the_ID();

      $args = array(
        'post_type' => 'post',
        'posts_per_page' => 2,
        'post__not_in' => array( $currentID )
      );

      $posts = new WP_Query( $args );

      if ( $posts->have_posts() ) : while ( $posts->have_posts() ) : $posts->the_post(); ?>

      <li class="posts__item">
        <a class="posts__imageHolder pos--rel" href="<?= get_the_permalink() ?>">
          <?= get_the_post_thumbnail(); ?>
          <?php if (get_field('show_video_button')): ?>
            <svg class="posts__play pos--abs center--xy" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><circle cx="50" cy="50" r="50" style="fill:#331f45"/><path d="M61.32,45.65a5,5,0,0,1,1.89,6.82,5.09,5.09,0,0,1-1.89,1.88L46.47,62.77a5,5,0,0,1-6.82-1.89A5,5,0,0,1,39,58.42V41.58a5,5,0,0,1,7.47-4.35Z" style="fill:#fff"/></svg>
          <?php endif; ?>
        </a>
        <a class="posts__heading" href="<?= get_the_permalink() ?>"><h2 class="fs--30"><?= get_the_title(); ?></h2></a>
        <p class="posts__excerpt fs--18"><?= get_the_excerpt(); ?></p>
        <a class="posts__button button-outline button-outline--black" href="<?= get_the_permalink() ?>">Read article</a>
      </li>

      <?php endwhile; endif; wp_reset_query(); ?>
      
    </ul>
  </div>
</section>