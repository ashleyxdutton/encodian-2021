<?php /* Index Template */ ?>

<?php
  $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
  $class = "posts--page";
  if ($paged === 1):
    $class = "";
?>

<section class="introPost pos--rel">
  <div class="container container--l">

    <div class="introPost__subscribe pos--rel z--2">
      <p>Subscribe to get the latest news from Encodian</p>
      <form
        data-process
        data-validate
        data-subject="Newsletter Signup | Encodian"
        data-recipient="hello@encodian.com"
        data-footer="This e-mail was sent from the Newsletter Signup form on the Encodian website"
        data-restricted="mailer.com"
        data-success="<h2>Thank you for subscribing.</h2>">
        <input type="email" name="EmailAddress" placeholder="* Email address" required autocomplete="off">
        <input type="text" name="sfu" class="sfu">
        <input type="submit" value="">
      </form>
    </div>

    <?php

    $args =  array(
      'post_type' => 'post',
      'posts_per_page' => 1
    );

    $featured = new WP_Query( $args );

    if ( $featured->have_posts() ) : while ( $featured->have_posts() ) : $featured->the_post(); ?>

    <div class="introPost__header flex flex--x-between flex--y-start pos--rel z--2">
      <a class="introPost__imageHolder pos--rel" href="<?= get_the_permalink() ?>">
        <?= get_the_post_thumbnail(); ?>
      </a>
      <div class="introPost__content">
        <p class="introPost__date fs--18"><?= get_the_date('jS F Y'); ?></p>
        <a class="introPost__heading" href="<?= get_the_permalink() ?>"><h1><?= get_the_title(); ?></h1></a>
        <a class="introPost__button button-outline button-outline--black" href="<?= get_the_permalink() ?>">Read article</a>
      </div>
    </div>

    <?php endwhile; endif; ?>

  </div>

  <div class="introPost__bg pos--abs"></div>

</section>

<?php endif; ?>

<section class="posts pos--rel <?= $class; ?>">
  <div class="container container--m">

    <div class="posts__filters flex flex--x-between">

      <?php
        $cat_args = array( 'orderby' => 'name', 'order' => 'ASC' );
        $categories = get_categories($cat_args);
      ?>

      <select class="posts__select" name="filterCategory" id="" onChange="window.location.href=this.value">
        <option value="#">Filter by category</option>
        <?php foreach($categories as $category): ?>
          <option value="/category/<?= $category->slug; ?>"><?= $category->name; ?></option>
        <?php endforeach; ?>
      </select>

      <?php get_search_form(); ?> 
      
      <script>
        document.querySelector('.searchform #s').placeholder = "Keyword search";
      </script>

    </div>

    <ul class="posts__list flex flex--x-between">

      <?php

      $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

      $args =  array(
        'post_type' => 'post',
        'paged' => $paged
      );

      $posts = new WP_Query( $args );

      if ( $posts->have_posts() ) : while ( $posts->have_posts() ) : $posts->the_post(); ?>

      <li class="posts__item">
        <a class="posts__imageHolder pos--rel" href="<?= get_the_permalink() ?>">
          <?= get_the_post_thumbnail(); ?>
          <?php if (get_field('show_video_button')): ?>
            <svg class="posts__play pos--abs center--xy" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><circle cx="50" cy="50" r="50" style="fill:#331f45"/><path d="M61.32,45.65a5,5,0,0,1,1.89,6.82,5.09,5.09,0,0,1-1.89,1.88L46.47,62.77a5,5,0,0,1-6.82-1.89A5,5,0,0,1,39,58.42V41.58a5,5,0,0,1,7.47-4.35Z" style="fill:#fff"/></svg>
          <?php endif; ?>
        </a>
        <a class="posts__heading" href="<?= get_the_permalink() ?>"><h2 class="fs--30"><?= get_the_title(); ?></h2></a>
        <p class="posts__excerpt fs--18"><?= get_the_excerpt(); ?></p>
        <a class="posts__button button-outline button-outline--black" href="<?= get_the_permalink() ?>">Read article</a>
      </li>

      <?php endwhile; endif; ?>
      
    </ul>

    <div class="posts__pagination flex flex--x-center">
      <?= paginate_links( array(
        'base' => str_replace( 99999, '%#%', esc_url( get_pagenum_link( 99999 ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $posts->max_num_pages,
        'prev_next' => true
      ) ); ?>
    </div>
  
  </div>
</section>