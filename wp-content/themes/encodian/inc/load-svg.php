<?php
 
/**
* Load an inline SVG.
*
* @param string $filename The filename of the SVG you want to load.
*
* @return string The content of the SVG you want to load.
*/
function load_svg( $filename ) {
 
    // Add the path to your SVG directory inside your theme.
    $svg_path = '/src/svgs/';

    if ( file_exists( '/var/www/html/wp-content/themes/encodian' . $svg_path . $filename . '.svg' ) ) {

        return file_get_contents( '/var/www/html/wp-content/themes/encodian' . $svg_path . $filename . '.svg' );

    } else if ( file_exists( '' . get_site_url() . '/wp-content/themes/encodian/src/svgs/' . $filename . '.svg' ) ) {
 
        // Load and return the contents of the file
        
    } else {
        
        // Return a blank string if we can't find the file.
        return file_get_contents( '' . get_site_url() . '/wp-content/themes/encodian/src/svgs/' . $filename . '.svg' );

    }
 
}