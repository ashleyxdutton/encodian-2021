<?php 
/**
 * Process Mail Functions
 * https://developer.wordpress.org/reference/functions/wp_mail/
 * https://github.com/andrimaruf/wp_mail/blob/master/functions.php
 */
add_action('wp_ajax_process_form', 'process_form');
add_action('wp_ajax_nopriv_process_form', 'process_form');

function process_form() {

	// Check if permissions correct to send mail
	if ( !check_ajax_referer( 'ajax-nonce', 'nonce', false ) ) {
		die('Permission denied');
	}

	// Validate if sfu is empty to proceed
	if ( isset($_POST['sfu']) && $_POST['sfu'] == '' ) {

		$data = array();

		foreach( $_POST as $name => $value ) {
			if ( $name !== 'sfu' && $name !== 'recipient' && $name !== 'action' && $name !== 'nonce' && $name !== 'form_id' && $name !== 'subject' && $name !== 'footer' ) {
				array_push($data, $name, $value);
			}
		}

		$form_id = isset($_POST['form_id']) ? $_POST['form_id'] : 0;

		if ( $_POST['subject'] ):
			
			$subject = $_POST['subject'];
			$footer = $_POST['footer'];
			$recipient_name = 'Encodian';
			$recipient_email = $_POST['recipient'];

		elseif ( $form_id == 2 ):
			$subject = 'Filer Demo Request | Encodian';
			$footer = 'This e-mail was sent from the Filer form on the Encodian website';
			$recipient_name 	= 'Encodian';
			$recipient_email 	= 'hello@encodian.com';
		elseif ( $form_id == 1 ):
			$subject = 'Newsletter Signup | Encodian';
			$footer = 'This e-mail was sent from the Newsletter Signup form on the Encodian website';
			$recipient_name 	= 'Encodian';
			$recipient_email 	= 'hello@encodian.com';
		else:
			$subject = 'General Enquiry | Encodian';
			$footer = 'This e-mail was sent from the Contact Form on the Encodian website';
			$recipient_name 	= 'Encodian';
			$recipient_email 	= 'hello@encodian.com';
		endif;

		$sender_name 			= 'No Reply';
		$sender_email 		= 'noreply@encodian-website.azurewebsites.net';
		
		// Build Headers
		$headers =  'Organization: Encodian' . "\r\n";
		$headers .= 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'X-Priority: 2' . "\r\n";
  	$headers .= 'X-Mailer: PHP' . phpversion() . "\r\n";
		// $headers .= 'To: ' . $recipient_name . ' <' . $recipient_email  . '>' . "\r\n";
		// $headers .= 'From: ' . $sender_name . ' <' . $sender_email . '>' . "\r\n";

		send_form_notification( $recipient_email, $subject, $headers, $data, $footer );

		die();

	}
}

function send_form_notification( $recipient_email, $subject, $headers, $data, $footer ) {

	$fields = '';

	// foreach ( $data as $key => $row ) {
	// 	if ( !empty( $row['value'] ) ) {
	// 		$fields .= '<p><strong>' . $row['label'] . ':</strong> ' . $row['value'] . '</p>';
	// 	}
	// }

	// foreach ( $data as $key => $row ) {
	// 	if ( !empty( $row['value'] ) ) {
	// 		$fields .= '<p><strong>' . $row['label'] . ':</strong> ' . $row['value'] . '</p>';
	// 	}
	// }

	foreach ( $data as $key => $row ) {
		if( $key % 2 == 0 ) {
			$fields .= '<p><strong>' . preg_replace('/(?<!\ )[A-Z]/', ' $0', $row) .  ':</strong></p>';
		} else {
			$fields .= '<p>' . $row .  '</p>';
		}
	}

	$search = array('*|SUBJECT|*', '*|FIELDS|*', '*|FOOTER|*');
	$replace = array($subject, $fields, $footer);

	$template = file_get_contents(get_template_directory() . '/template-emails/mail-notification.html');
	$template = str_replace( $search, $replace, $template );

	$result = wp_mail( $recipient_email, $subject, $template, $headers );
	return $result;

}