<?php
/*
 * Remove Query Strings
 */
function _remove_script_version( $src ){

 	$parts = explode( '?', $src );
 	return $parts[0];

}

add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );

/**
 * Remove unwanted WP items
 */
function theme_reset() {
	
  remove_action('wp_head', 'rsd_link');
  remove_action('wp_head', 'feed_links_extra');
  remove_action('wp_head', 'feed_links');
  remove_action('wp_head', 'wlwmanifest_link');
  remove_action('wp_head', 'index_rel_link');
  remove_action('wp_head', 'parent_post_rel_link');
  remove_action('wp_head', 'start_post_rel_link');
  remove_action('wp_head', 'wp_shortlink_wp_head');
  remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
  remove_action('wp_head', 'wp_generator');
  remove_action('admin_print_styles', 'print_emoji_styles' );
  remove_action('wp_head', 'print_emoji_detection_script', 7 );
  remove_action('admin_print_scripts', 'print_emoji_detection_script' );
  remove_action('wp_print_styles', 'print_emoji_styles' );
  remove_filter('wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter('the_content_feed', 'wp_staticize_emoji' );
  remove_filter('comment_text_rss', 'wp_staticize_emoji' );

  add_filter('emoji_svg_url', '__return_false' );

  // Stop WP auto linking media images
  update_option('image_default_link_type', 'none');

  wp_deregister_script( 'wp-embed' );
}

add_action('init', 'theme_reset');

/**
 * Remove comments
 */
// function remove_admin_menus() {

//   remove_menu_page( 'edit-comments.php' );
// }

// add_action('admin_menu', 'remove_admin_menus' );

function remove_comment_support() {

	remove_post_type_support( 'post', 'comments' );
	remove_post_type_support( 'page', 'comments' );
}

add_action('init', 'remove_comment_support', 100);

function admin_bar_render() {

	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('comments');
}

add_action('wp_before_admin_bar_render', 'admin_bar_render' );
