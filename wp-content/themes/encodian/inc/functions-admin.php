<?php

/**
 * Replace WP login logo with clients
 */
function bgn_include_client_logo_and_branding() {
  echo '<style type="text/css">
    .login {
      background: #F5F5F6;
    }
    .login h1 a {
      background-image: url("' . get_template_directory_uri() . '/src/svgs/logo-colour.svg");
      background-size: 250px auto;
      height: 58px;
      width: 250px;
      margin: 0 auto 40px;
    }
    .login form {
      border: 0;
      box-shadow: none;
      border-radius: 6px;
      -webkit-box-shadow: 0 6px 10px rgba(0, 0, 0, 0.15);
      box-shadow: 0 6px 10px rgba(0, 0, 0, 0.15);
    }
    .login #nav a,
    .login #backtoblog a,
    .login #nav a:hover,
    .login #backtoblog a:hover,
    .login .mysp-login-extras a,
    .login .mysp-login-extras a:hover {
      text-shadow: none;
    }
  </style>';
}
add_action( 'login_head', 'bgn_include_client_logo_and_branding' );

/**
 * WP Login Logo Link URL
 */
function bgn_login_logo_url( $url ) {
  return get_bloginfo( 'url' ) . '/';
}
add_filter( 'login_headerurl', 'bgn_login_logo_url' );

/**
 * Custom credit added to admin footer
 */
function bgn_add_credit_to_admin_footer() {
  echo '<strong>' . get_bloginfo( 'name' ) . ' –</strong> A website by <a href="https://bgn.agency" target="_blank">BGN</a>.';
}
add_filter( 'admin_footer_text', 'bgn_add_credit_to_admin_footer' );

/**
 * Custom credit added to admin footer
 */
function bgn_version_removal() {
  return '';
}
add_filter( 'the_generator', 'bgn_version_removal' );

/*
 * Remove Welcome Panel
 */
remove_action('welcome_panel', 'wp_welcome_panel');

/**
 * Remove default widgets from dashboard 
 */
function remove_dashboard_widgets() {

  global $wp_meta_boxes;

  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_site_health']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
  
}

add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );

/**
 * Add custom support widget to admin dashboard
 */  
function my_custom_dashboard_widgets() {

  global $wp_meta_boxes;
  
  wp_add_dashboard_widget('custom_help_widget', 'BGN Support', 'custom_dashboard_help');

}

add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
 
function custom_dashboard_help() {
  ?>
    <p>If you need assistance with your website please contact us:</p>
    <p><strong>Client services:</strong> <a href="mailto:paul@bgn.agency">paul@bgn.agency</a>.
    <br/>
    <strong>Support:</strong> <a href="mailto:info@bgn.agency">info@bgn.agency</a>.</p>
  <?php
}


/*
* ACF Layout Thumbnails
*/

add_filter('acfe/flexible/thumbnail/layout=hero', 'acf_layout_thumbnail', 10, 3);

function acf_layout_thumbnail($thumbnail, $field, $layout){

  return 'https://encodian.bgn.agency/wp-content/uploads/2022/02/iStock-1173805290-scaled.jpg';

}

// Remove visual editor tab from Gravity Forms notification editor screen

add_action( 'admin_init', 'disable_tinymce_for_notifications');
function disable_tinymce_for_notifications() {
    if ( ( RGForms::is_gravity_page() && rgget( 'page' ) === 'gf_edit_forms' && rgget( 'view' ) === 'settings' ) && rgget( 'subview' ) === 'notification' ) {
        add_filter( 'user_can_richedit', '__return_false' );
    }
}