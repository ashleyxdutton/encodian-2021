<?php

/*
 * Register Custom Post Type
 */

function register_industries() {

  $labels = array(
    'name'                => _x( 'Industries', 'Post Type General Name', 'text_domain' ),
    'singular_name'       => _x( 'Industry', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'           => __( 'Industries', 'text_domain' ),
    'parent_item_colon'   => __( 'Parent Industry:', 'text_domain' ),
    'all_items'           => __( 'All Industries', 'text_domain' ),
    'view_item'           => __( 'View Industry', 'text_domain' ),
    'add_new_item'        => __( 'Add New Industry', 'text_domain' ),
    'add_new'             => __( 'Add New', 'text_domain' ),
    'edit_item'           => __( 'Edit Industry', 'text_domain' ),
    'update_item'         => __( 'Update Industry', 'text_domain' ),
    'search_items'        => __( 'Search Industries', 'text_domain' ),
    'not_found'           => __( 'No industries found', 'text_domain' ),
    'not_found_in_trash'  => __( 'No industries found in Trash', 'text_domain' ),
  );
  $args = array(
    'label'               => __( 'industries', 'text_domain' ),
    'description'         => __( 'Industries information pages', 'text_domain' ),
    'labels'              => $labels,
    'supports'            => array( 'title', 'revisions', 'page-attributes', 'excerpt', 'thumbnail' ),
    'taxonomies'          => array( ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => 'dashicons-index-card',
    'can_export'          => true,
    'has_archive'         => false,
    'exclude_from_search' => true,
    'publicly_queryable'  => true,
    'capability_type'     => 'page',
    'rewrite' => array('slug' => 'industries', 'with_front' => false)
  );
  register_post_type( 'industries', $args );
}
add_action( 'init', 'register_industries', 0 );
