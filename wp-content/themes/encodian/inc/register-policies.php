<?php

/*
 * Register Custom Post Type
 */

function register_policies() {

  $labels = array(
    'name'                => _x( 'Policies', 'Post Type General Name', 'text_domain' ),
    'singular_name'       => _x( 'Policy', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'           => __( 'Policies', 'text_domain' ),
    'parent_item_colon'   => __( 'Parent Policy:', 'text_domain' ),
    'all_items'           => __( 'All Policies', 'text_domain' ),
    'view_item'           => __( 'View Policy', 'text_domain' ),
    'add_new_item'        => __( 'Add New Policy', 'text_domain' ),
    'add_new'             => __( 'Add New', 'text_domain' ),
    'edit_item'           => __( 'Edit Policy', 'text_domain' ),
    'update_item'         => __( 'Update Policy', 'text_domain' ),
    'search_items'        => __( 'Search Policies', 'text_domain' ),
    'not_found'           => __( 'No policies found', 'text_domain' ),
    'not_found_in_trash'  => __( 'No policies found in Trash', 'text_domain' ),
  );
  $args = array(
    'label'               => __( 'policies', 'text_domain' ),
    'description'         => __( 'Policies information pages', 'text_domain' ),
    'labels'              => $labels,
    'supports'            => array( 'title', 'editor', 'revisions', 'page-attributes', 'excerpt', 'thumbnail' ),
    'taxonomies'          => array( ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => 'dashicons-text-page',
    'can_export'          => true,
    'has_archive'         => false,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'page',
    'rewrite' => array('slug' => 'policies', 'with_front' => false)
  );
  register_post_type( 'policies', $args );
}
add_action( 'init', 'register_policies', 0 );
