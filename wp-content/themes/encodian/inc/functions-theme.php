<?php
/*
 * Register Scripts
 */
function register_load_scripts(){

  wp_register_script( 'mainScript', get_template_directory_uri().'/dist/main.bundle.js','','', true );
  wp_register_script( 'manifest', get_template_directory_uri().'/dist/manifest.bundle.js','','', true );
  wp_register_script( 'vendors', get_template_directory_uri().'/dist/vendors.bundle.js','','', true );

  /* Localize site directory data and nonces to javascript */
  wp_localize_script( 'mainScript', 'WP', array(
		'ajax'            => admin_url('admin-ajax.php'),     // WP.ajax
    'nonce'           => wp_create_nonce( "ajax-nonce" ), // WP.nonce
    'directory_uri'   => get_template_directory_uri()     // WP.directory_uri
	));

  wp_enqueue_script( ['mainScript', 'manifest', 'vendors'] );
}

add_action( 'wp_enqueue_scripts', 'register_load_scripts'  );

/*
 * Register Stylesheet
 */
function register_load_stylesheets() { 

  wp_register_style( 'styles', bgn_version( 'main.css' ), '', '', 'screen', false );
  
  wp_enqueue_style( 'styles' );

}

add_action( 'wp_enqueue_scripts', 'register_load_stylesheets'  );

/**
 * Add async to wp_enqueue_script
 * https://matthewhorne.me/defer-async-wordpress-scripts/
 */
function add_async_attribute($tag, $handle) {

  // add script handles to the array below
  $scripts_to_async = array('scripts');
  
  foreach($scripts_to_async as $async_script) {
    if ($async_script === $handle) {
      return str_replace(' src', ' async="async" src', $tag);
    }
  }

  return $tag;
}

add_filter('script_loader_tag', 'add_async_attribute', 10, 2);

/*
 * Register Menus
 */
function register_menu() {

  register_nav_menus( array(
    'header-menu' => esc_html__( 'Header' ),
    'footer-products-menu' => esc_html__( 'Footer — Products' ),
    'footer-services-menu' => esc_html__( 'Footer — Services' ),
    'footer-industries-menu' => esc_html__( 'Footer — Industries' ),
    'footer-support-menu' => esc_html__( 'Footer — Support' ),
    'footer-partners-menu' => esc_html__( 'Footer — Partners' ),
    'footer-policies-menu' => esc_html__( 'Footer — Policies' )
  ) );
  
}

add_action( 'init', 'register_menu' );

/*
 * Register Featured Image Support
 */
add_theme_support( 'post-thumbnails' );

/**
 * Enable SVG upload to media
 */
function enable_svg($mime_types){

	$mime_types['svg'] = 'image/svg+xml';
	return $mime_types;
}

add_filter('upload_mimes', 'enable_svg', 1, 1);

/**
 * Get SVG path and display
 */
function theme_svg($file){

  if (file_exists( get_stylesheet_directory_uri() . '/src/assets/icons/' . $file . '.svg')) {

    include(get_stylesheet_directory_uri() . '/src/assets/icons/' . $file . '.svg');
  }
  elseif (file_exists( get_stylesheet_directory_uri() . '/src/assets/brand/' . $file . '.svg')) {

    include(get_stylesheet_directory_uri() . '/src/assets/brand/' . $file . '.svg');
  }
  elseif (file_exists( get_template_directory() . '/src/assets/icons/' . $file . '.svg')) {

    include( get_template_directory() . '/src/assets/icons/' . $file . '.svg');
  }
  elseif (file_exists( get_template_directory() . '/src/assets/brand/' . $file . '.svg')) {

    include( get_template_directory() . '/src/assets/brand/' . $file . '.svg');
  }
}

/**
 * Detect user browser
 */
function browser_body_class($classes) {

  global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;

  if($is_lynx) $classes[] = 'lynx';
  elseif($is_gecko) $classes[] = 'gecko';
  elseif($is_opera) $classes[] = 'opera';
  elseif($is_NS4) $classes[] = 'ns4';
  elseif($is_safari) $classes[] = 'safari';
  elseif($is_chrome) $classes[] = 'chrome';
  elseif($is_IE) {
    $classes[] = 'ie';
    if(preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
    $classes[] = 'ie'.$browser_version[1];
  } 
  else $classes[] = 'unknown';
  if($is_iphone) $classes[] = 'iphone';
  if ( stristr( $_SERVER['HTTP_USER_AGENT'],"mac") ) {
    $classes[] = 'osx';
  } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"linux") ) {
    $classes[] = 'linux';
  } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"windows") ) {
    $classes[] = 'windows';
  } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"android") ) {
    $classes[] = 'android';
  }
  return $classes;
}

add_filter('body_class','browser_body_class');

/*
 * Page Slug Body Class
 */
function add_slug_body_class( $classes ) {

  global $post;

  if ( isset( $post ) ) {
    
    $classes[] = $post->post_type . '-' . $post->post_name;
  }
  return $classes;
}

add_filter( 'body_class', 'add_slug_body_class' );

/**
 * Disable Gutenberg
 * Posts and Post_types
 */
add_filter('use_block_editor_for_post', '__return_false', 10);
add_filter('use_block_editor_for_post_type', '__return_false', 10);

add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);


function my_wp_nav_menu_objects( $items, $args ) {
	
	// loop
	foreach( $items as $item ) {

		// vars
		$icon = get_field('icon', $item->object_id);
    $heading = get_field('heading', $item->object_id);
    $colour = get_field('colour', $item->object_id);
    $logo = get_field('partner_logo', $item);

    // append title with colour
		if( $colour ) {
			
			$item->title .= ' <p class="menu-heading" role="presentation" style="color: ' . $colour . '">' . $item->title . '</p>';

		}
		
		// append icon
		if( $icon ) {
			
			$item->title .= ' <img class="menu-icon" src="' . $icon . '" alt="">';

		}

    // append text
		if( $heading ) {
			
			$item->title .= ' <p class="menu-text">' . $heading . '</p>';

		}

    // append text
		if( $logo ) {
			
			$item->title .= ' <img class="menu-logo" src="' . $logo["url"] . '" alt="">';

		}
		
	}
	
	
	// return
	return $items;
	
}

function SearchFilter($query) {
  if ($query->is_search) {
      $query->set('post_type', 'post');
  }
  return $query;
}

add_filter('pre_get_posts','SearchFilter');

/**
 * Gets the path to a versioned Mix file in a theme.
 *
 * Use this function if you want to load theme dependencies. This function will cache the contents
 * of the manifest file for you. This also means that you can’t work with different mix locations.
 * For that, you’d need to use `mix_any()`.
 *
 * Inspired by <https://www.sitepoint.com/use-laravel-mix-non-laravel-projects/>.
 *
 * @since 1.0.0
 *
 * @param string $path The relative path to the file.
 * @param string $manifest_directory Optional. Custom path to manifest directory. Default 'build'.
 *
 * @return string The versioned file URL.
 */

function bgn_version( $path, $manifest_directory = 'dist' ) {
  static $manifest;
  static $manifest_path;

  if ( ! $manifest_path ) {
    $manifest_path = get_theme_file_path( $manifest_directory . '/mix-manifest.json' );
  }

  // Bailout if manifest couldn’t be found
  if ( ! file_exists( $manifest_path ) ) {
    return get_theme_file_uri( $path );
  }

  if ( ! $manifest ) {
    // @codingStandardsIgnoreLine
    $manifest = json_decode( file_get_contents( $manifest_path ), true );
  }

  // Remove manifest directory from path
  $path = str_replace( $manifest_directory, '', $path );
  // Make sure there’s a leading slash
  $path = '/' . ltrim( $path, '/' );

  // Bailout with default theme path if file could not be found in manifest
  if ( ! array_key_exists( $path, $manifest ) ) {
    return get_theme_file_uri( $path );
  }

  // Get file URL from manifest file
  $path = $manifest[ $path ];
  // Make sure there’s no leading slash
  $path = ltrim( $path, '/' );

  return get_theme_file_uri( trailingslashit( $manifest_directory ) . $path );
}