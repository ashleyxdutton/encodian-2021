<?php

/*
 * Register Custom Post Type
 */

function register_products() {

  $labels = array(
    'name'                => _x( 'Products', 'Post Type General Name', 'text_domain' ),
    'singular_name'       => _x( 'Product', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'           => __( 'Products', 'text_domain' ),
    'parent_item_colon'   => __( 'Parent Product:', 'text_domain' ),
    'all_items'           => __( 'All Products', 'text_domain' ),
    'view_item'           => __( 'View Product', 'text_domain' ),
    'add_new_item'        => __( 'Add New Product', 'text_domain' ),
    'add_new'             => __( 'Add New', 'text_domain' ),
    'edit_item'           => __( 'Edit Product', 'text_domain' ),
    'update_item'         => __( 'Update Product', 'text_domain' ),
    'search_items'        => __( 'Search Products', 'text_domain' ),
    'not_found'           => __( 'No products found', 'text_domain' ),
    'not_found_in_trash'  => __( 'No products found in Trash', 'text_domain' ),
  );
  $args = array(
    'label'               => __( 'products', 'text_domain' ),
    'description'         => __( 'Products information pages', 'text_domain' ),
    'labels'              => $labels,
    'supports'            => array( 'title', 'revisions', 'page-attributes', 'excerpt', 'thumbnail' ),
    'taxonomies'          => array( ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => 'dashicons-open-folder',
    'can_export'          => true,
    'has_archive'         => false,
    'exclude_from_search' => true,
    'publicly_queryable'  => true,
    'capability_type'     => 'page',
    'rewrite' => array('slug' => 'products', 'with_front' => false)
  );
  register_post_type( 'products', $args );
}
add_action( 'init', 'register_products', 0 );
