<?php
/*
 * Custom image sizes
 */
add_image_size( 'img-1920', 1920 );

/**
 * ACF Responsive Image Helper Function
 * NOTE https://github.com/lukemoody/acf-responsive-image-helper-function
 * Exampel use to generate html img tag 
 * image = get_field('image');
 * get_acf_image_srcset( $image, 'img-1920', '1920px' );
 */
function get_acf_image_srcset($image_id, $image_size, $max_width) {

	// check the ACF image ID is not blank
	if($image_id != '') {

		// set the default src image size
		$image_src = wp_get_attachment_image_url( $image_id, $image_size );

		// set the srcset with various image sizes
		$image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );

    // generate the markup for the responsive image
    echo '<img srcset="' . $image_srcset . '" sizes="(max-width: ' . $max_width . ') 100vw, '. $max_width . '" src="' . $image_src . '" alt="' . get_post_meta( $image_id, '_wp_attachment_image_alt', true ) . '" />';

	}
}

/**
 * Load template part while supplying data.
 *
 * @param string $slug The slug name for the generic template.
 * @param array $params An associated array of data that will be extracted into the templates scope
 * @param bool $output Whether to output component or return as string.
 * @return string
 */
function get_part( $name, $vars = array(), $output = true ) {

	if (!$output) ob_start();
  
  // If template part doesn't exist display error message
  if (!$_template_part = locate_template('template-parts/' . $name . '.php', false, false)) {
    trigger_error(sprintf(__('Error locating template part for inclusion', 'bgn-blank-wp'), $_template_part), E_USER_ERROR);
  }

  // Make variables accessible in scope
	extract($vars, EXTR_SKIP);

  // Require template part
  require($_template_part);

	if(!$output) return ob_get_clean();
}

/**
 * ACF Global Options
 */
if( function_exists('acf_add_options_page') ) {

	$option_page = acf_add_options_page(array(
		'page_title' 	=> 'Global Settings',
		'menu_title' 	=> 'Global Settings',
		'menu_slug' 	=> 'theme-global-settings',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));

}

function ab_remove_admin_bar() {
  global $wp_admin_bar;

  $wp_admin_bar->remove_menu('wp-logo');
  // $wp_admin_bar->remove_menu('dashboard');
  $wp_admin_bar->remove_menu('widgets');
  $wp_admin_bar->remove_menu('menus');
  //$wp_admin_bar->remove_menu('site-name');
  $wp_admin_bar->remove_menu('wpseo-menu');

  $wp_admin_bar->remove_menu('comments');
  // $wp_admin_bar->remove_menu('edit');
  $wp_admin_bar->remove_menu('themes');
  $wp_admin_bar->remove_menu('customize');
  $wp_admin_bar->remove_node('search');

  $wp_admin_bar->remove_menu('new-content');
  $wp_admin_bar->remove_menu('new-page');
  $wp_admin_bar->remove_menu('new-post');
  $wp_admin_bar->remove_menu('new-link');
  $wp_admin_bar->remove_menu('new-media');
  $wp_admin_bar->remove_menu('new-user');
  $wp_admin_bar->remove_menu('new-form');
  $wp_admin_bar->remove_menu('wpseo-menu');

  //$wp_admin_bar->remove_menu('edit-profile');
}
add_action('wp_before_admin_bar_render', 'ab_remove_admin_bar');