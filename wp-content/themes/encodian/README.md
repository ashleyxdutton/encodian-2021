# README

This README documents the requirements for bgn-blank-wp, created by Luke Moody (BGN)

Blank scaffold theme for bespoke WordPress developments with front-end boilerplate. Write stylesheets with Sass, automatically check your JavaScript for errors, enable synchronized browser testing, and more with a Gulp based setup.

- bgn-blank-wp
- Version 2.0.4
- https://www.bgn.agency

### Features

- Modern JavaScript
- Webpack
- BrowserSync
- Scss
- Gsap

### Requirements

Ensure all dependencies have bene installed:

- WordPress >= 4.7
- PHP >= 7.1.3
- Node.js >= 6.9.x
- Yarn

### Structure

```shell
themes/your-theme-name/   # → Theme root
├── dist/                 # → Built assets (never edit)
├── src/                  # → Includes folder
│   ├── assets/           # → Images, logo, icons etc
│   ├── fonts/            # → Font folder
│   ├── js/               # → JavaScript build file, library and compiled source file
│   └── scss/             # → Sass partials, mixins and variables
├── lib/                  # → WP specific Library files
├── node_modules/         # → Node.js packages (never edit)
├── template-parts/       # → WP specific content layout templates
├── .gitignore            # → Directories, files, or patterns you don't want to be tracked by version control
├── lib/                  # → Includes and WP functions
├── gulpfile.json         # → Manifest to define tasks
├── package.json          # → Node.js dependencies and scripts
├── style.css             # → Theme meta information
└── ...
```

### Pre-install

- Clone bgn-blank-wp files into your `theme` directory using `git clone https://bgnagency@bitbucket.org/bgnagency/bgn-blank-wp.git && rm -rf bgn-blank-wp/.git`
- Rename `bgn-blank-wp` theme folder name to a project specific reference

### How to install

- Change to the project's theme root directory.
- Install project dependencies with `yarn`
- BrowserSync - If you’re already running a local server with PHP or similar, you’ll need to use the proxy mode. Open webpack.config.js and replace `http://bgn-blank-wp:8888/` with your local development url
- Run Webpack with `yarn start` for development mode
- Compile production ready files with `yarn build`

### Who do I talk to?

- BGN
- Luke Moody - luke@bgn.agency
