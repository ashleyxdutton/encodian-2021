<footer class="footer">
  <div class="container container--l flex">

    <div class="footer__side">
      <a class="footer__logo z--9" href="/">
        <?= load_svg('logo'); ?>
      </a>
      <?php
      $affiliates = get_field('footer_affiliates', 'option');
      if ( $affiliates ): ?>
        <h2 class="footer__affiliates-header footer__heading fs--22"><?php _e('Accreditations', 'bgn'); ?></h2>
        <div class="footer__affiliates">
          <?php foreach ( $affiliates as $key => $affiliate ): ?>
            <div class="footer__affiliate-item">
              <?php if ( $affiliate['logo'] ): ?>
                <img src="<?php echo $affiliate['logo']['url']; ?>" alt="<?php echo $affiliate['company_name'] ? $affiliate['company_name'] : __('Affilifate Logo', 'bgn'); ?>">
              <?php endif; ?>
            </div>
          <?php endforeach ?>
        </div>
      <?php endif; ?>
    </div>
    
    <nav class="footer__nav">

      <div class="footer__row footer__row--products">
        <p class="footer__heading fs--22">Products</p>
        <?php wp_nav_menu( array(
          'theme_location' => 'footer-products-menu'
        ) ); ?>
      </div>

      <div class="footer__row footer__row--main flex">
      
        <div class="footer__column footer__column--services">
          <p class="footer__heading fs--22">Services</p>
          <?php wp_nav_menu( array(
            'theme_location' => 'footer-services-menu'
          ) ); ?>
        </div>

        <div class="footer__column footer__column--industries">
          <p class="footer__heading fs--22">Industries</p>
          <?php wp_nav_menu( array(
            'theme_location' => 'footer-industries-menu'
          ) ); ?>
        </div>

        <div class="footer__column footer__column--support">
          <p class="footer__heading fs--22">Support</p>
          <?php wp_nav_menu( array(
            'theme_location' => 'footer-support-menu'
          ) ); ?>
        </div>
        
      </div>

      <div class="footer__row footer__row--partners">
        <p class="footer__heading fs--22">Partners & Technology</p>
        <?php wp_nav_menu( array( 
          'theme_location' => 'footer-partners-menu'
        ) ); ?>
      </div>

      <div class="footer__row footer__row--end flex flex--y-center">
        <?php wp_nav_menu( array( 
          'theme_location' => 'footer-policies-menu'
        ) ); ?>
        <p class="footer__copyright fs--14 fc--purple">© <?= date("Y"); ?> Encodian Solutions Ltd</p>
      </div>

    </nav>

  </div>

  <script type="text/javascript">
    _linkedin_partner_id = "4477249";
    window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
    window._linkedin_data_partner_ids.push(_linkedin_partner_id);
    </script><script type="text/javascript">
    (function(l) {
    if (!l){window.lintrk = function(a,b){window.lintrk.q.push([a,b])};
    window.lintrk.q=[]}
    var s = document.getElementsByTagName("script")[0];
    var b = document.createElement("script");
    b.type = "text/javascript";b.async = true;
    b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
    s.parentNode.insertBefore(b, s);})(window.lintrk);
  </script>
  <noscript>
    <img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=4477249&fmt=gif" />
  </noscript>

</footer>

<a class="support" href="https://support.encodian.com/hc/en-gb" target="_blank"><span>Support</span></a>

<?php wp_footer(); ?>