import { Loader } from '@googlemaps/js-api-loader'

const initGoogleMaps = () => {

  if (!document.querySelector('#map')) return

  const loader = new Loader({
    apiKey: 'AIzaSyCis2bi6bO3oQGb21qjXS4gk1LaMicboI4',
    version: 'weekly'
  })

  loader.load().then(() => {
    map = new google.maps.Map(document.getElementById('map'), {
      center: { lat: 52.372082, lng: -1.797967 },
      zoom: 14,
      disableDefaultUI: true
    })
  })


}

export default initGoogleMaps  