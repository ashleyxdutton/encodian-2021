import gsap from 'gsap'
import { ScrollToPlugin } from 'gsap/ScrollToPlugin'

gsap.registerPlugin(ScrollToPlugin)

const scrollToSection = () => {

  const trigger = document.querySelectorAll('[data-to]')

  trigger.forEach(el => {
    el.addEventListener('click', (e) => {
      e.preventDefault()

      gsap.to(window, { duration: 1, scrollTo: '#' + el.dataset.to })

    })
  })

}

export default scrollToSection