import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'
import { ScrollToPlugin } from 'gsap/ScrollToPlugin'

gsap.registerPlugin(ScrollToPlugin, ScrollTrigger)

const productSidebar = () => {

  const sidebar = document.querySelectorAll('.sidebar__content')
  const sidebarButton = document.querySelectorAll('.sidebar__button')
  const fixedButton = document.querySelector('.fixedBtn')

  const url = window.location.search
  const urlParams = new URLSearchParams(url)

  if (urlParams.has('form')) { setTimeout(() => {
    gsap.to(window, { scrollTo: { y: document.querySelector('#form'), ease: 'power1.inOut' } })
  }, 1500) }

  if (fixedButton) {
    fixedButton.addEventListener('click', (e) => {
      e.preventDefault()
      const section = document.querySelector('#' + e.currentTarget.dataset.to)
      gsap.to(window, { scrollTo: { y: section, ease: 'power1.inOut' } })
    })
  }

  if (!sidebar || window.innerWidth < 960) return
  
  sidebar.forEach(el => {

    ScrollTrigger.create({
      trigger: el,
      pin: true,
      anticipatePin: 1,
      pinSpacing: false,
      start: 'top 150px',
      end: 'bottom bottom-=258px',
      endTrigger: '.end-trigger',
      // markers: true
    })

  })

  sidebarButton.forEach(el => {
    el.addEventListener('click', (e) => {
      e.preventDefault()
      const section = document.querySelector('#' + e.currentTarget.dataset.to)
      gsap.to(window, { scrollTo: { y: section, ease: 'power1.inOut' } })
    })
  })
  
}

export default productSidebar