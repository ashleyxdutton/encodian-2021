var Flickity = require('flickity-fade')

class IntroSlider {
  constructor(el) {
    
    this.el = el
    this.arrowNext = el.parentNode.querySelector('.next')
    this.arrowPrev = el.parentNode.querySelector('.previous')
    this.dots = el.parentNode.querySelectorAll('.introSlider__dot')

    this.arrowNext.addEventListener('click', this.nextSlide.bind(this))
    this.arrowPrev.addEventListener('click', this.prevSlide.bind(this))

    this.dots[0].classList.add('active')

    this.initSlider()

  }
  initSlider() {

    this.flickity = new Flickity(this.el, {
      contain: true,
      pageDots: false,
      prevNextButtons: false,
      draggable: true,
      fade: true,
      adaptiveHeight: true
    })
 
    this.flickity.on( 'change', this.slideChanged.bind(this))

  }
  slideChanged(e) {

    if (this.flickity.selectedIndex === 0) {
      this.arrowPrev.disabled = true
      this.arrowNext.disabled = false
    } else if (this.flickity.selectedIndex === this.flickity.slides.length - 1) {
      this.arrowNext.disabled = true
      this.arrowPrev.disabled = false
    } else {
      this.arrowPrev.disabled = false
      this.arrowNext.disabled = false
    }

    this.dots.forEach(el => { el.classList.remove('active') })
    this.dots[this.flickity.selectedIndex].classList.add('active')

  }
  nextSlide(e) {

    e.preventDefault()
    this.flickity.next()

  }
  prevSlide(e) {

    e.preventDefault()
    this.flickity.previous()

  }
}

const initIntroSlider = () => {

  const carousels = document.querySelector('.introSlider__slider')

  if (carousels) new IntroSlider(carousels)

}

export default initIntroSlider