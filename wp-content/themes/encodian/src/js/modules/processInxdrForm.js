import axios from 'axios';
import Bouncer from 'formbouncerjs';
 
let formHasSubmitted = false 

var bouncer = new Bouncer('[data-validate-indxr]', {
  disableSubmit: true,
  customValidations: {
    validateDomain: (field) => {
      if (field.id === 'email_address') { 

        // Check if inputted domain contains any of those listed in rejectedDomains.
        const rejectedDomains = ['yahoo', 'gmail', 'aol', 'hotmail', 'outlook', 'mac', 'live', 'googlemail', 'icloud']
        const inputtedDomain = field.value.split("@")
        let domain = ''

        if (inputtedDomain[1]) {
          domain = inputtedDomain[1].split('.')[0]
        }
        
        if (rejectedDomains.indexOf(domain) >= 0) {
          return true
        } else {
          return false
        }

      }
    }
  },
  messages: {
		validateDomain: 'Please provide a business and not a personal email address.'
	}
})

const processIndxrForm = (e) => {
  e.preventDefault()
  const form = e.target
  const endPoint = 'https://functionsadminpaalwaysoncore5.azurewebsites.net/api/WebTrialRequestIndxr?code=zBBeQmPlYCR05/aR5HsEGn2jMU9wIyv0tFy3URFSi73C5sX0Ny0v9w=='
  const success = document.querySelector('.contactForm__success')
  const errorEl = document.querySelector('.contactForm__error')
  const existsEl = document.querySelector('.contactForm__exists')
  const responseEl = document.querySelector('.contactForm__response')
  const apiKeyElem = document.querySelector('#TrialApiKey')
  
  document.addEventListener('bouncerFormInvalid', function (event) {
    console.log(event.detail.errors)
  }, false)

  /** If validation passes, fire AJAX submission */
  document.addEventListener('bouncerFormValid', function (e) {

    if (formHasSubmitted) return false

    formHasSubmitted = true

    var firstName = document.getElementById("first_name").value
    var lastName = document.getElementById("last_name").value
    var companyName = document.getElementById("company").value
    var jobRole = document.getElementById("role").value
    var contactNumber = document.getElementById("phone_number").value
    var emailAddress = document.getElementById("email_address").value
    var mailListApproved = document.getElementById("mail_check").checked
    var gclid = document.getElementById("gclid_field").value
    var source = document.getElementById("utm_source").value
    var medium = document.getElementById("utm_medium").value
    var campaign = document.getElementById("utm_campaign").value
    var term = document.getElementById("utm_term").value

    var requestData = { Type: 'IndxrApp', FirstName: firstName, LastName: lastName, CompanyName: companyName, JobRole: jobRole, ContactNumber: contactNumber, EmailAddress: emailAddress, MailListApproved: mailListApproved, utmCampaign: campaign, utmSource: source, utmMedium: medium, utmTerm: term, gclid: gclid }

    const options = {
      method: 'POST',
      body: JSON.stringify(requestData),
      mode: 'cors',
      headers: { 
        'Access-Control-Allow-Credentials': 'true',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
      }
    }
  
    fetch(endPoint, options)
      .then(res => res.json())
      .then(res => {
        console.log(res)
        console.log('ApiKey: ' + res.ApiKey)
        console.log('Error: ' + res.Error)
        if (res.Error != null) {
          document.querySelector('.contactForm__response h3').innerHTML = res.Error
          form.style.display = 'none'
          responseEl.style.display = 'block'
        } else {
          apiKeyElem.innerHTML = res.ApiKey
          form.style.display = 'none'
          success.style.display = 'block'
        }
      })
      .catch(err => {
        console.log(err)
        console.log(`Error with message: ${err}`)
        form.style.display = 'none'
        errorEl.style.display = 'block'
      })

    form.classList.add('submitted')

  }, false)
  
  // Copy the API to the clipboard
  apiKeyElem.addEventListener('click', () => {
  
    const apiKey = apiKeyElem.innerHTML
    const tempInput = document.createElement('input')
    tempInput.value = apiKey
    document.querySelector('body').appendChild(tempInput)
    tempInput.select()
    document.execCommand('copy')
    tempInput.remove()

    document.querySelector('.tooltip').innerHTML = 'Copied!'

    setTimeout(() => {
      document.querySelector('.tooltip').innerHTML = 'Click to Copy'
    }, 3000)
  })

}; 

const initProcessIndxrForm = () => {

  const forms = document.querySelectorAll('[data-process-indxr]')

  forms.forEach(el => {
    el.addEventListener('submit', processIndxrForm, false)
  })

}

export default initProcessIndxrForm;