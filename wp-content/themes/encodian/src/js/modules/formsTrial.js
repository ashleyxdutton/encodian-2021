import axios from 'axios'
import Bouncer from 'formbouncerjs'

// Get values from the form and then remove the data attributes.

const form = document.querySelector('[data-processtrial]')

let api, type, restrictedDomains, tracking

if (form) {

  api = form.dataset.api
  type = form.dataset.type
  restrictedDomains = form.dataset.restricted.split(', ')
  tracking = form.dataset.tracking 
  
  form.removeAttribute('data-api')
  form.removeAttribute('data-type')
  form.removeAttribute('data-restricted')
  form.removeAttribute('data-tracking')

}
 
let formHasSubmitted = false 

const validation = new Bouncer('[data-validatetrial]', {
  disableSubmit: true,
  customValidations: {
    validateDomain: (field) => {
      if (field.type == 'email') {
        let domain = ''
        const inputtedDomain = field.value.split("@")
        if (inputtedDomain[1]) domain = inputtedDomain[1].split('.')[0]
        if (restrictedDomains.indexOf(domain) >= 0)
          return true
        else
          return false
      }
    }
  },
  messages: {
		validateDomain: 'Please provide a business and not a personal email address.'
	}
})

const processTrialForm = (e) => {

  e.preventDefault()
  
  const form = e.target,
        success = document.querySelector('.contactForm__success'),
        errorEl = document.querySelector('.contactForm__error'),
        existsEl = document.querySelector('.contactForm__exists'),
        responseEl = document.querySelector('.contactForm__response'),
        apiKeyEl = document.querySelector('#TrialApiKey')
  
  document.addEventListener('bouncerFormInvalid', (event) => console.log(event.detail.errors), false)
  document.addEventListener('bouncerFormValid', function (e) {

    if (formHasSubmitted) return false

    formHasSubmitted = true

    const postData = { Type: type }
    const allFields = form.querySelectorAll('input')

    // Add all of the forms fields to the postData object.
    allFields.forEach(field => {
      if (field.name != 'privacy_check' || field.name != 'MailListApproved') {
        postData[field.name] = field.value
      }
      if (field.name == 'MailListApproved' && field.value == 'on') {
        postData['MailListApproved'] = true
      } else if (field.name == 'MailListApproved' && field.value == 'off') {
        postData['MailListApproved'] = false
      }
    })

    console.log(postData)

    const options = {
      method: 'POST',  
      body: JSON.stringify(postData),
      mode: 'cors',
      headers: { 
        'Access-Control-Allow-Credentials': 'true',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
      }
    }
    fetch(api, options)
      .then(res => {
        console.log('Error code: ' + res.status)
        if (res.status == 500) throw Error(response.statusText)
        else return res.text()
      })
      .then(res => {

        console.log(res)
        console.log('ApiKey: ' + res.ApiKey)
        console.log('Error: ' + res.Error) 

        // if (res == 'Tenant already exists') {
        //   form.style.display = 'none'
        //   existsEl.style.display = 'block'
        // } else if (res.Error != null) {
        //   document.querySelector('.contactForm__response h3').innerHTML = res.Error
        //   form.style.display = 'none'
        //   responseEl.style.display = 'block'
        // } else {
        //   apiKeyEl.innerHTML = res.ApiKey
        //   form.style.display = 'none'
        //   success.style.display = 'block'
        // }

        if (res == 'Tenant already exists' || res == 'Trigr subscription already exists') {
          form.style.display = 'none'
          existsEl.style.display = 'block'

          if ( postData.Type == 'TrigrApp' ) {
            window.lintrk('track', { conversion_id: 7861449 })
          }
        } else {
          const response = JSON.parse(res)
          console.log(response)
          if (response.Error != null) {
            document.querySelector('.contactForm__response h3').innerHTML = response.Error
            form.style.display = 'none'
            responseEl.style.display = 'block'
          } else {
            apiKeyEl.innerHTML = response.ApiKey
            form.style.display = 'none' 
            success.style.display = 'block'

            if ( postData.Type == 'TrigrApp' ) { 
              window.lintrk('track', { conversion_id: 7861449 })
            }
          }
        }

        if ( tracking ) eval(tracking)
      
      })
      .catch(err => {
        console.log(`Error with message: ${err}`) 
        form.style.display = 'none'
        errorEl.style.display = 'block'
      })

    form.classList.add('submitted')

  }, false)
  
  // Copy the API to the clipboard
  apiKeyEl.addEventListener('click', () => {
  
    const apiKey = apiKeyEl.innerHTML,
          temporaryField = document.createElement('input')

    temporaryField.value = apiKey
    document.querySelector('body').appendChild(temporaryField)
    temporaryField.select()
    document.execCommand('copy')
    temporaryField.remove()
    document.querySelector('.tooltip').innerHTML = 'Copied!'
    setTimeout(() => document.querySelector('.tooltip').innerHTML = 'Click to Copy', 3000)
  })

}

const initProcessTrialForm = () => {

  const forms = document.querySelectorAll('[data-processtrial]')

  forms.forEach(el => el.addEventListener('submit', processTrialForm, false) )

}

export default initProcessTrialForm