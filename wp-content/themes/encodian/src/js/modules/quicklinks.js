import { gsap } from 'gsap'
import { ScrollToPlugin } from 'gsap/ScrollToPlugin'
import { ScrollTrigger } from 'gsap/ScrollTrigger'

gsap.registerPlugin(ScrollTrigger, ScrollToPlugin)

class Quicklinks {
  constructor(el) {

    this.el = el
    this.list = el.querySelector('.quicklinks__list')
    this.links = el.querySelectorAll('[data-quicklink]')
    this.sections = document.querySelectorAll('[data-section]')

    this.links.forEach(el => {
      el.addEventListener('click', this.scrollToSection.bind(this))
    })

    // this.pinLinks()
    this.activeStates()

    // window.addEventListener('wheel', this.moveQuicklinks.bind(this))

  }
  pinLinks() {

    // ScrollTrigger.create({
    //   trigger: this.el,
    //   pin: true,
    //   anticipatePin: 1,
    //   pinSpacing: false,
    //   start: 'top 25px',
    //   end: 'bottom bottom-=150px',
    //   endTrigger: '.faqs'
    // })

  }
  activeStates() {

    this.sections.forEach(el => {
      
      ScrollTrigger.create({
        trigger: el,
        start: 'top top',
        end: 'bottom bottom',
        // markers: true,
        onEnter: () => {
          this.links.forEach(el => {
            if (el) el.classList.remove('active')
          })
          let tg = document.querySelector('[data-quicklink="' + el.dataset.section + '"]')
          if (tg) {
            tg.classList.add('active')
          }
        },
        onEnterBack: () => {
          this.links.forEach(el => {
            if (el) el.classList.remove('active')
          })
          let tg = document.querySelector('[data-quicklink="' + el.dataset.section + '"]')
          if (tg) {
            tg.classList.add('active')
          }
        }
      })

    })

  }
  scrollToSection(e) {

    const section = document.querySelector('[data-section="' + e.currentTarget.dataset.quicklink + '"]')

    if (!section) return 

    gsap.to(window, { scrollTo: { y: section, offsetY: -10, ease: 'power1.inOut' } }) 

  }
  moveQuicklinks(e) {
    // if (e.deltaY > 0 && window.innerWidth > 960) {
    //   this.list.classList.remove('nav-revealed')
    // } else {
    //   this.list.classList.add('nav-revealed')
    // }
  }
}

const initQuicklinks = () => {

  const quicklinks = document.querySelector('[data-quicklinks]')
  
  if (!quicklinks) return

  new Quicklinks(quicklinks)

}

export default initQuicklinks