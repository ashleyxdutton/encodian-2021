import Lightense from 'lightense-images'

export default function newsLightbox() {

  window.addEventListener('load', function () {
    Lightense('.single-post .post img')
  }, false)
  
}