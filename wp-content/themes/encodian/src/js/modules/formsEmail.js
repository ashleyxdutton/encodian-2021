import axios from 'axios'
import Bouncer from 'formbouncerjs'

// Get values from the form and then remove the data attributes.

const form = document.querySelector('[data-process]')

let restrictedDomains, subject, recipient, footer, tracking

if (form) {
  
  footer = form.dataset.footer 
  subject = form.dataset.subject 
  recipient = form.dataset.recipient 
  tracking = form.dataset.tracking 

  if (form.dataset.restricted) {
    restrictedDomains = form.dataset.restricted.split(', ') 
  } else {
    restrictedDomains = ''
  }

  form.removeAttribute('data-footer') 
  form.removeAttribute('data-subject')
  form.removeAttribute('data-recipient')
  form.removeAttribute('data-restricted')
  form.removeAttribute('data-tracking')
  
}

let formHasSubmitted = false 

const validation = new Bouncer('[data-validate]', {
  disableSubmit: true,
  customValidations: {
    validateDomain: (field) => {
      if (field.type == 'email') {
        let domain = ''
        const inputtedDomain = field.value.split("@")
        if (inputtedDomain[1]) domain = inputtedDomain[1].split('.')[0]
        if (restrictedDomains.indexOf(domain) >= 0)
          return true
        else
          return false
      }
    }
  },
  messages: {
		validateDomain: 'Please provide a business and not a personal email address.'
	}
})

const processForm = (e) => {
  e.preventDefault();
  const form = e.target;

  const formData = new FormData(form);

  formData.append('action', 'process_form')
  formData.append('nonce', WP.nonce)
  formData.append('form_id', form.getAttribute('data-form-id'))
  formData.append('footer', footer)
  formData.append('subject', subject)
  formData.append('recipient', recipient)

  // for (var value of formData.values()) {
  //   console.log(value);
  // }

  document.addEventListener('bouncerFormInvalid', function (event) {
    console.log(event.detail.errors);
  }, false);


  /** If validation passes, fire AJAX submission */
  document.addEventListener('bouncerFormValid', function (e) {

    if (formHasSubmitted) return false

    formHasSubmitted = true

    form.classList.add('submitted')

    axios({
      method: 'POST',
      url: WP.ajax,
      data: formData
    })
    .then(function (response) {
      console.log('data: ' + response.data);
      console.log('status: ' + response.status);
      form.innerHTML = '<div class="success-msg">' + form.getAttribute('data-success') + '</div>'

      if ( tracking ) eval(tracking)

    },
    (error) => {
      console.log(error)
    });

  }, false);

};

const initProcessForm = () => {

  const forms = document.querySelectorAll('[data-process]');

  for (let i = 0; i < forms.length; i++) {
    forms[i].addEventListener('submit', processForm, false);
  } // END loop

};

export default initProcessForm;