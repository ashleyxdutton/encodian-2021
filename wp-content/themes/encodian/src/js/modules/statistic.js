import gsap from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'
import { CountUp } from 'countup.js'

gsap.registerPlugin(ScrollTrigger)

const statistic = () => {

  const statistic = document.querySelector('.productIntro__statistic span')
  
  if (!statistic) return

  const count = new CountUp(statistic, statistic.dataset.count)

  ScrollTrigger.create({
    trigger: '.productIntro__statistic',
    start: 'top 75%',
    end: 'top 75%',
    toggleClass: 'is-inview',
    once: true,
    // markers: true,
    onEnter: () => { count.start() }
  })

}

export default statistic