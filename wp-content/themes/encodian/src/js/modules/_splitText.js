import gsap from 'gsap'
import SplitText from '../libraries/SplitText.min.js'

gsap.registerPlugin(SplitText)

export default function initSplitText() {

  const linesToSplit = document.querySelectorAll('[data-split-lines]')

  const lineSplit = new SplitText(linesToSplit, { type: 'lines', linesClass: 'line' })

  window.addEventListener('resize', () => {
    lineSplit.split()
  })

}
