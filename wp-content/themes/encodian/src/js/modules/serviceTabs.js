import gsap from 'gsap'

class ServiceTabs {
  constructor(el) {

    this.slideHeight = 0
    this.tabs = el.querySelectorAll('.serviceTabs__tab')
    this.slides = el.querySelectorAll('.serviceTabs__slide')
    this.slideHolder = el.querySelector('.serviceTabs__slides')
    this.titles = Array.from(el.querySelectorAll('.serviceTabs__title'))

    this.tabs[0].classList.add('active')
    this.slides[0].classList.add('active')

    this.tabs.forEach(el => {
      el.addEventListener('click', this.changeTab.bind(this))
    })

    // window.addEventListener('resize', this.resize.bind(this))

    if (window.innerWidth < 960) {
      this.setupMobile()
    }

  }
  setupMobile() {

    this.titles.forEach((el, i) => {
      this.slides[i].style.height = el.offsetHeight + 'px'
      el.addEventListener('click', this.openSlide.bind(this))
    })

  }
  changeTab(e) {
    e.preventDefault()

    this.tabs.forEach(el => { el.classList.remove('active') })
    this.slides.forEach(el => { el.classList.remove('active') })

    e.currentTarget.classList.add('active')

    this.tabs.forEach((el, i) => {
      if (el === e.currentTarget) {
        this.slides[i].classList.add('active')
        this.slideHeight = this.slides[i].offsetHeight
      }
    })

    gsap.to(this.slideHolder, { duration: 0.3, height: this.slideHeight + 'px' })

  }
  openSlide(e) {

    const index = this.titles.indexOf(e.currentTarget)

    if (e.currentTarget.classList.contains('open')) {

      gsap.to(this.slides[index], { duration: 0.4, height: e.currentTarget.offsetHeight + 'px', ease: 'power1.inOut' })
      e.currentTarget.classList.remove('open')

    } else {

      gsap.to(this.slides[index], { duration: 0.4, height: 'auto', ease: 'power1.inOut' })
      e.currentTarget.classList.add('open')

    }
  }
  // resize() {
  //   if (window.innerWidth < 960) {
  //     this.titles.forEach((el, i) => {
  //       this.slides[i].style.height = el.offsetHeight + 'px'
  //     })
  //   } else {
  //     gsap.to(this.slides, { duration: 0.1, height: 'auto', ease: 'power1.inOut' })
  //   }
  // }
}

const initServiceTabs = () => {

  const section = document.querySelector('.serviceTabs')
  
  if (section) new ServiceTabs(section)

}

export default initServiceTabs