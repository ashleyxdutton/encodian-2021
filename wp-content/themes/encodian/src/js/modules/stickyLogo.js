import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'

gsap.registerPlugin(ScrollTrigger)

const stickyLogo = () => {
  
  const logo = document.querySelector('.fixedLogo')

  if (!logo) return

  ScrollTrigger.create({
    trigger: logo,
    pin: true,
    anticipatePin: 1,
    pinSpacing: false,
    start: 'top top',
    end: 'bottom bottom',
    endTrigger: '.bottom',
    // markers: true
  })
  
}

export default stickyLogo 