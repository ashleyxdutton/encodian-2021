function getParam(p) {
  var match = RegExp('[?&]' + p + '=([^&]*)').exec(window.location.search);
  return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function getExpiryRecord(value) {
  var expiryPeriod = 90 * 24 * 60 * 60 * 1000; // 90 day expiry in milliseconds
  var expiryDate = new Date().getTime() + expiryPeriod;
  return {
    value: value,
    expiryDate: expiryDate
  };
}

function addParams() {
  var gclidParam = getParam('gclid');
  var gclidFormFields = ['gclid_field'];
  var gclidRecord = null;
  var currGclidFormField;

  var utmSourceParam = getParam('utm_source');
  var utcSourceFormFields = ['utm_source'];
  var utmSourceRecord = null;
  var currUtmSourceFormField;
  
  var utmMediumParam = getParam('utm_medium');
  var utcMediumFormFields = ['utm_medium'];
  var utmMediumRecord = null;
  var currUtmMediumFormField;
  
  var utmCampaignParam = getParam('utm_campaign');
  var utcCampaignFormFields = ['utm_campaign'];
  var utmCampaignRecord = null;
  var currUtmCampaignFormField;
  
  var utmTermParam = getParam('utm_term');
  var utcTermFormFields = ['utm_term'];
  var utmTermRecord = null;
  var currUtmTermFormField;

  var gclsrcParam = getParam('gclsrc');
  var isGclsrcValid = !gclsrcParam || gclsrcParam.indexOf('aw') !== -1;

  gclidFormFields.forEach(function (field) {
    if (document.getElementById(field)) {
      currGclidFormField = document.getElementById(field);
    }
  });
  utcSourceFormFields.forEach(function (field) {
    if (document.getElementById(field)) {
      currUtmSourceFormField = document.getElementById(field);
    }
  });
  utcMediumFormFields.forEach(function (field) {
    if (document.getElementById(field)) {
      currUtmMediumFormField = document.getElementById(field);
    }
  });
  utcCampaignFormFields.forEach(function (field) {
    if (document.getElementById(field)) {
      currUtmCampaignFormField = document.getElementById(field);
    }
  });
  utcTermFormFields.forEach(function (field) {
    if (document.getElementById(field)) {
      currUtmTermFormField = document.getElementById(field);
    }
  });

  if (gclidParam && isGclsrcValid) {
    gclidRecord = getExpiryRecord(gclidParam);
    localStorage.setItem('gclid', JSON.stringify(gclidRecord));
  }
  if (utmSourceParam) {
    utmSourceRecord = getExpiryRecord(utmSourceParam);
    localStorage.setItem('utm_source', JSON.stringify(utmSourceRecord));
  }
  if (utmMediumParam) {
    utmMediumRecord = getExpiryRecord(utmMediumParam);
    localStorage.setItem('utm_medium', JSON.stringify(utmMediumRecord));
  }
  if (utmCampaignParam) {
    utmCampaignRecord = getExpiryRecord(utmCampaignParam);
    localStorage.setItem('utm_campaign', JSON.stringify(utmCampaignRecord));
  }
  if (utmTermParam) {
    utmTermRecord = getExpiryRecord(utmTermParam);
    localStorage.setItem('utm_term', JSON.stringify(utmTermRecord));
  }

  var gclid = gclidRecord || JSON.parse(localStorage.getItem('gclid'));
  var utm_source = utmSourceRecord || JSON.parse(localStorage.getItem('utm_source'));
  var utm_medium = utmMediumRecord || JSON.parse(localStorage.getItem('utm_medium'));
  var utm_campaign = utmCampaignRecord || JSON.parse(localStorage.getItem('utm_campaign'));
  var utm_term = utmTermRecord || JSON.parse(localStorage.getItem('utm_term'));
  var isGclidValid = gclid && new Date().getTime() < gclid.expiryDate;
  var isUtmSourceValid = utm_source && new Date().getTime() < utm_source.expiryDate;
  var isUtmMediumValid = utm_medium && new Date().getTime() < utm_medium.expiryDate;
  var isUtmCampaignValid = utm_campaign && new Date().getTime() < utm_campaign.expiryDate;
  var isUtmTermValid = utm_term && new Date().getTime() < utm_term.expiryDate;

  if (currGclidFormField && isGclidValid) {
    currGclidFormField.value = gclid.value;
  }
  if (currUtmSourceFormField && isUtmSourceValid) {
    currUtmSourceFormField.value = utm_source.value;
  }
  if (currUtmMediumFormField && isUtmMediumValid) {
    currUtmMediumFormField.value = utm_medium.value;
  }
  if (currUtmCampaignFormField && isUtmCampaignValid) {
    currUtmCampaignFormField.value = utm_campaign.value;
  }
  if (currUtmTermFormField && isUtmTermValid) {
    currUtmTermFormField.value = utm_term.value;
  }
}

window.addEventListener('load', addParams) 

export default function paramStorage() { }