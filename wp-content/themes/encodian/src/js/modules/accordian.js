import gsap from 'gsap'

class Accordian {
  constructor(el) {
    
    this.el = el
    this.items = Array.from(el.querySelectorAll('[data-accordian-item]'))
    this.titles = Array.from(el.querySelectorAll('[data-accordian-title]'))
    this.windowWidth = window.innerWidth

    window.addEventListener('resize', this.onResize.bind(this))

    if (el.dataset.accordian === 'mobile') {
      if (window.innerWidth <= 750) this.setup()
    } else {
      this.setup()
    }

  }
  setup() {

    this.itemClickListener = this.toggleItem.bind(this)

    document.fonts.ready.then(() => {
      this.items.forEach((el, i) => {
        el.style.height = this.titles[i].offsetHeight + 'px'
        el.addEventListener('click', this.itemClickListener)
      })
    })

  }
  toggleItem(e) {

    const index = this.items.indexOf(e.currentTarget)

    if (e.currentTarget.classList.contains('js-open')) {

      gsap.to(this.items[index], { duration: 0.4, height: this.titles[index].offsetHeight + 'px', ease: 'power1.inOut' })
      e.currentTarget.classList.remove('js-open')

    } else {

      gsap.to(this.items[index], { duration: 0.4, height: 'auto', ease: 'power1.inOut' })
      e.currentTarget.classList.add('js-open')

    }
  }
  onResize(e) {

    if (document.documentElement.clientWidth === this.windowWidth) return

    if (this.el.dataset.accordian === 'mobile' && window.innerWidth <= 750) {

      this.setup()

    } else if (this.el.dataset.accordian === 'mobile') {

      this.destroy()

    } else {

      this.items.forEach((el, i) => {
        gsap.to(el, { duration: 0.4, height: this.titles[i].offsetHeight + 'px', ease: 'power1.inOut' })
        el.classList.remove('open')
      })

    }
  }
  destroy() {

    this.items.forEach(el => {
      el.style.height = 'auto'
      el.removeEventListener('click', this.itemClickListener)
    })

  }
}

const initAccordian = () => {

  const accordians = document.querySelectorAll('[data-accordian]')
  
  if (!accordians) return

  accordians.forEach(el => {
    new Accordian(el)
  })

}

export default initAccordian