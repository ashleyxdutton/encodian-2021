import Flickity from 'flickity'


class Pricing {
  constructor(el) {

    // let check = el.querySelector('.next')

    // if ( !check ) return false

    this.slider = el.querySelector('.pricing__noslide')
    this.prices = el.querySelectorAll('.pricing__prices')
    this.dots = el.querySelectorAll('.pricing__dot')
    this.select = el.querySelector('.pricing__column select')
    this.select.addEventListener('change', this.updateCurrency.bind(this))
    this.arrowNext = el.parentNode.querySelector('.next')
    this.arrowPrev = el.parentNode.querySelector('.previous')

    const isSingle = el.classList.contains('pricing--single')

    if ( !isSingle ) {
      this.radioButtons()
      this.initSlider()
    }

  }
  initSlider() {

    this.flickity = new Flickity(this.slider, {
      contain: false,
      pageDots: false,
      prevNextButtons: false,
      draggable: true,
      initialIndex: 0,
      watchCSS: true,
      cellAlign: 'left',
      // groupCells: true,
    })

    this.dots[0].classList.add('active')
    this.arrowNext.addEventListener('click', this.nextSlide.bind(this))
    this.arrowPrev.addEventListener('click', this.prevSlide.bind(this))

    this.flickity.on( 'change', this.slideChanged.bind(this))

  }
  slideChanged(e) {

    if (this.flickity.selectedIndex === 0) {
      this.arrowPrev.disabled = true
    } else if (this.flickity.selectedIndex === this.flickity.slides.length - 1) {
      this.arrowNext.disabled = true
    } else {
      this.arrowPrev.disabled = false
      this.arrowNext.disabled = false
    }

    this.dots.forEach(el => { el.classList.remove('active') })
    this.dots[this.flickity.selectedIndex].classList.add('active')

  }
  updateCurrency(e) {

    this.prices.forEach(el => {
      el.setAttribute('data-currency', e.target.value)
    })

  }
  nextSlide(e) {

    e.preventDefault()
    this.flickity.next()

  }
  prevSlide(e) {

    e.preventDefault()
    this.flickity.previous()

  }
  radioButtons() {

    let parent = document.querySelector('[data-pricing-peroid]'),
        radios = document.querySelectorAll('[data-pricing-toggle]')

    if (!parent || !radios) return false

    radios.forEach(radio => {
      radio.addEventListener('click', e => {
        parent.removeAttribute('data-pricing-peroid')
        let period = radio.getAttribute('data-pricing-toggle')
        parent.setAttribute('data-pricing-peroid', period)
      })
    })

  }
}

const initPricing = () => {

  const section = document.querySelector('.pricing')

  if (section) new Pricing(section)





}

export default initPricing