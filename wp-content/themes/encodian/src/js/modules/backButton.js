const backButton = () => {

  const button = document.querySelector('.introPost__back')

  if (!button) return

  button.addEventListener('click', (e) => {
    e.preventDefault()

    if (window.history.length > 1 && document.referrer.includes('encodian')) {
      history.back()
    } else {
      window.location.href = '/blog';
    }

  })

}

export default backButton