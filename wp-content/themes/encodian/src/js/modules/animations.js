import gsap from 'gsap'

export default function animations() { }

const setupLogos = () => {

  gsap.set('.clientLogos__item img', { scale: 0 })

}

setupLogos() 

export function heroAnim(el) {
 
  if (!el) return

  const tl = gsap.timeline({ onComplete: () => {
    el.classList.add('complete')
  }})

  if (window.innerWidth > 960) {
    tl.to(['.header__nav .menu-item', '.header__account'], { duration: 1, opacity: 1, y: '0%', stagger: 0.04, ease: 'power2.out' })
    tl.to(['.fixedLogo__logo'], { duration: 0.8, y: '0%', opacity: 1, stagger: 0.08, ease: 'power2.out' }, '-=1.2')
    tl.to('.hero__bg', { duration: 1.0, height: 'auto', ease: 'power2.out' }, '-=1.0')
  }

}

export function clientsAnim(el) {

  if (!el) return

  const lines = el.querySelectorAll('.line')
  const logos = Array.from(el.querySelectorAll('img'))
  const randomiseLogos = logos.sort(() => Math.random() - 0.5)

  const tl = gsap.timeline({ onComplete: () => {
    el.classList.add('complete')
  }})

  tl.to(lines, { duration: 0.8, y: '0%', opacity: 1, stagger: 0.08, ease: 'power2.out' })
  tl.to(randomiseLogos, { duration: 1, scale: 1, stagger: 0.1, ease: 'back.out(1.4)' }, '-=0.7')

}

export function testimonialsAnim(el) {

  if (!el) return

  const items = el.querySelectorAll('.testimonials__item')

  gsap.to(items, { duration: 1.6, delay: 0.4, y: '0%', opacity: 1, scale: 1, stagger: 0.08, ease: 'elastic.out(1.2, 1)' })

}