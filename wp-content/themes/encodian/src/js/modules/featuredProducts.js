import gsap from 'gsap'
import Flickity from 'flickity'
import { ScrollTrigger } from 'gsap/ScrollTrigger'
import { MorphSVGPlugin } from '../libraries/MorphSVGPlugin.min.js'

gsap.registerPlugin(ScrollTrigger, MorphSVGPlugin)

class FeaturedProducts {
  constructor(el) {

    this.el = el
    this.slides = el.querySelectorAll('.featuredProducts__slide')
    this.carousel = el.querySelector('.featuredProducts__carousel')
    this.productShape = el.querySelector('.product-shapes')
    this.productShapeMobile = el.querySelector('.product-shapes-mobile')
    this.paginationItems = el.querySelectorAll('.featuredProducts__paginationItem')
    this.sketches = el.querySelectorAll('.featuredProducts__sketch')

    // Set the first slide as active.
    this.paginationItems[0].classList.add('active')
    this.sketches[0].classList.add('active')
    this.slides[0].classList.add('active')
    this.activeSlide = this.slides[0].dataset.shape

    if (window.innerWidth >= 750) {
      this.pinSection()
    } else {
      this.mobileCarousel()
    }

  }
  mobileCarousel() {

    const carousel = new Flickity(this.carousel, {
      contain: false,
      pageDots: true,
      prevNextButtons: true,
      draggable: true,
      cellAlign: 'left'
    })

    carousel.on('change', (index) => {
      gsap.to('.shapeM0', { morphSVG: '.shapeM' + index + '', duration: 0.6, ease: 'power3.inOut' })
      this.productShapeMobile.style.fill = this.slides[index].dataset.colour
    })

  }
  pinSection() {

    this.lengthNumber = 400

    ScrollTrigger.create({
      trigger: this.el,
      pin: true,
      anticipatePin: 1,
      start: 'top top-=35px',
      end: 'bottom bottom',
      end: (this.lengthNumber * this.slides.length) + 'px',
      // markers: true,
      onUpdate: ({ progress }) => {
        this.slides.forEach((el, i) => {
          if (progress >= (1 / (this.slides.length) * i) && progress < (1 / (this.slides.length) * (i + 1))) {
            this.changeSlide(el)
          }
        })
      }
    })

  }
  changeSlide(el) {

    if (el.dataset.shape === this.activeSlide) return

    this.activeSlide = el.dataset.shape

    if (window.innerWidth >= 750) {
      gsap.to('.shape0', { morphSVG: '.shape' + el.dataset.shape + '', duration: 0.6, ease: 'power3.inOut' })
      this.productShape.style.fill = el.dataset.colour
    } else {
      // gsap.to('#Vertr-M', { morphSVG: '#' + el.dataset.shape + '-M' , duration: 0.6, ease: 'power3.inOut' })
      // this.productShapeMobile.style.fill = el.dataset.colour
    }

    this.paginationItems.forEach(el => { el.classList.remove('active') })
    this.sketches.forEach(el => { el.classList.remove('active') })
    this.slides.forEach(el => { el.classList.remove('active') })

    this.slides.forEach((slide, i) => {
      if (el === this.slides[i]) {
        this.paginationItems[i].classList.add('active')
        this.sketches[i].classList.add('active')
        el.classList.add('active')
      }
    })

  }
}

const initFeaturedProducts = () => {

  const section = document.querySelector('.featuredProducts')

  if (section) new FeaturedProducts(section)

}

export default initFeaturedProducts 