class ImageModal {
  constructor(el) {

    this.items = Array.from(el.querySelectorAll('.js-modal-item'))
    this.modals = document.querySelectorAll('.featuresAlt__modal')
    this.close = document.querySelectorAll('.featuresAlt__close')

    if (window.innerWidth > 750) {

      this.items.forEach(el => {
        el.addEventListener('click', this.openModal.bind(this))
      })
  
      this.close.forEach(el => {
        el.addEventListener('click', this.closeModal.bind(this))
      })

    }

  }
  openModal(e) {
    e.preventDefault()

    const index = this.items.indexOf(e.currentTarget)

    this.modals[index].classList.add('js-open')
    document.querySelector('html').classList.add('no-scroll')

  }
  closeModal(e) {
    e.preventDefault()
    
    this.modals.forEach(el => {
      el.classList.remove('js-open')
    })

    document.querySelector('html').classList.remove('no-scroll')

  }
}

const initImageModal = () => {

  const features = document.querySelector('[data-features]')
  
  if (!features) return

  new ImageModal(features)

}

export default initImageModal