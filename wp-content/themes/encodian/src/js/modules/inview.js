import gsap from 'gsap'
import { ScrollTrigger } from "gsap/ScrollTrigger"
import * as animations from './animations.js'

gsap.registerPlugin(ScrollTrigger)

const isInview = () => {

  const sections = document.querySelectorAll('[data-inview]')

  if (!sections) return

  sections.forEach(section => {

    ScrollTrigger.create({
      trigger: section, 
      start: (section.dataset.inview === 'start') ? 'top 95%' : 'top 80%',
      end: 'bottom bottom',
      once: true,
      toggleClass: 'is-inview',
      // markers: true,
      onEnter: () => {
        switch (section.dataset.inview) { 
          case 'hero':
            animations.heroAnim(section) 
          break
          case 'client':
            animations.clientsAnim(section)
          break
          case 'testimonials':
            animations.testimonialsAnim(section)
          break
        }
      }
    })
    
  })

  document.fonts.ready.then(() => {
    ScrollTrigger.refresh()
  })

}

export default isInview