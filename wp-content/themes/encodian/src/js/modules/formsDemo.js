import axios from 'axios'
import Bouncer from 'formbouncerjs'

// Get values from the form and then remove the data attributes.

const form = document.querySelector('[data-demo]')

let api, restrictedDomains, subject, recipient, footer, tracking

if (form) {
  
  api = form.dataset.api
  footer = form.dataset.footer 
  subject = form.dataset.subject 
  recipient = form.dataset.recipient 
  restrictedDomains = form.dataset.restricted.split(', ') 
  tracking = form.dataset.tracking

  form.removeAttribute('data-api')
  form.removeAttribute('data-footer')
  form.removeAttribute('data-subject')
  form.removeAttribute('data-recipient')
  form.removeAttribute('data-restricted')
  form.removeAttribute('data-tracking')
  
}

let formHasSubmitted = false 

const validation = new Bouncer('[data-validatedemo]', {
  disableSubmit: true,
  customValidations: {
    validateDomain: (field) => {
      if (field.type == 'email') {
        let domain = ''
        const inputtedDomain = field.value.split("@")
        if (inputtedDomain[1]) domain = inputtedDomain[1].split('.')[0]
        if (restrictedDomains.indexOf(domain) >= 0)
          return true
        else
          return false
      }
    }
  },
  messages: {
		validateDomain: 'Please provide a business and not a personal email address.'
	}
})

const processDemoForm = (e) => {
  e.preventDefault()

  const form = e.target,
        formData = new FormData(form),
        success = document.querySelector('.contactForm__success'),
        errorEl = document.querySelector('.contactForm__error')

  formData.append('nonce', WP.nonce)
  formData.append('action', 'process_form')
  formData.append('footer', footer)
  formData.append('subject', subject)
  formData.append('recipient', recipient)

  document.addEventListener('bouncerFormInvalid', (event) => {
    console.log(event.detail.errors)
  }, false)

  document.addEventListener('bouncerFormValid', (e) => {

    if (formHasSubmitted) return false

    formHasSubmitted = true

    // Send submission to email via AJAX
    axios({
      method: 'POST',
      url: WP.ajax,
      data: formData
    })
    .then(function (response) {
      console.log('data: ' + response.data)
      console.log('status: ' + response.status)
    },
    (error) => { 
      console.log(error)
    })

    // Create object with form data and post to API.
    const postData = { }
    const allFields = form.querySelectorAll('input')

    // Add all of the forms fields to the postData object.
    allFields.forEach(field => {
      if (field.name != 'privacy_check' || field.name != 'MailListApproved') {
        postData[field.name] = field.value
      }
      if (field.name == 'MailListApproved' && field.value == 'on') {
        postData['MailListApproved'] = true
      } else if (field.name == 'MailListApproved' && field.value == 'off') {
        postData['MailListApproved'] = false
      }
    })

    const options = { 
      method: 'POST',
      body: JSON.stringify(postData),
      mode: 'cors',
      headers: { 
        'Access-Control-Allow-Credentials': 'true', 
        'Content-Type': 'application/json',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
      }
    }

    fetch(api, options)
      .then(res => {
        form.style.display = 'none'
        success.style.display = 'block'

        if ( tracking ) eval(tracking)

      })
      .catch(err => { 
        console.log(`Error with message: ${err}`)
        form.style.display = 'none'
        errorEl.style.display = 'block'
      })

      form.classList.add('submitted')

  }, false)
  
}

const initProcessDemoForm = () => {

  const forms = document.querySelectorAll('[data-demo]')

  for (let i = 0; i < forms.length; i++) {
    forms[i].addEventListener('submit', processDemoForm, false)
  }

}

export default initProcessDemoForm