import Flickity from 'flickity'

export default function initTestimonialCarousel() {

  const carousels = document.querySelectorAll('[data-testimonials]')

  if (!carousels) return

  carousels.forEach(el => {

    const flickity = new Flickity(el, {
      contain: true,
      pageDots: false,
      prevNextButtons: true,
      imagesLoaded: true,
      draggable: true,
      cellAlign: 'left'
    })
    
  })

}
