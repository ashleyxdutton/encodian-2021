import gsap from 'gsap';
import { ScrollTrigger } from "gsap/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger);

const logoColour = () => {

  const sectionsLight = document.querySelectorAll('[data-light]')
  const logo = document.querySelector('.fixedLogo')

  if ( !logo ) return

  sectionsLight.forEach(el => { 

    ScrollTrigger.create({
      trigger: el,
      start: 'top top+=100px',  
      bottom: 'bottom bottom',
      // markers: true,
      onToggle: ({progress, direction, isActive}) => {
        if (isActive) logo.classList.add('logo-yellow')
        else logo.classList.remove('logo-yellow')
      },
    })
    
  })
  
}

export default logoColour