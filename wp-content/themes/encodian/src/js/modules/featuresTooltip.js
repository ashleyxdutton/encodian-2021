const featuresTooltip = () => {

  const buttons = document.querySelectorAll('.open-tooltip')

  if ( !buttons.length ) return

  buttons.forEach(button => {
    button.addEventListener('click', () => {
      if ( button.classList.contains('js-open') ) {
        button.classList.remove('js-open')
      } else {
        button.classList.add('js-open')
      }
    })
  })

  window.addEventListener('click', (e) => {
    if ( e.target.classList[0] != 'pricing__productText' ) {
      buttons.forEach(button => button.classList.remove('js-open'))
    }
  })

}

export default featuresTooltip  