class Header {
  constructor(el) {

    this.el = el
    this.nav = el.querySelector('.header__nav')
    this.menuButton = el.querySelector('.header__menuButton')
    this.productsItem = el.querySelector('.products-item a')
    this.industryItem = el.querySelector('.industry-item a')
    this.aboutItem = el.querySelector('.about-item a')
    this.dropdowns = el.querySelectorAll('.dropdown a:not(.sub-menu a)')

    this.dropdowns.forEach(dropdown => {
      dropdown.classList.add('menu-accordian')
      dropdown.addEventListener('click', this.toggleSubmenu.bind(this))
    })

    // if ( this.productsItem ) {
    //   this.productsItem.classList.add('menu-accordian')
    //   this.productsItem.addEventListener('click', this.toggleSubmenu.bind(this))
    // }

    // if ( this.aboutItem ) {
    //   this.aboutItem.classList.add('menu-accordian')
    //   this.aboutItem.addEventListener('click', this.toggleSubmenu.bind(this))
    // } 

    // if ( this.industryItem ) {
    //   this.industryItem.classList.add('menu-accordian')
    //   this.industryItem.addEventListener('click', this.toggleSubmenu.bind(this))
    // }

    this.menuButton.addEventListener('click', this.toggleNav.bind(this))

    if (window.innerWidth < 960) setTimeout(() => this.productsItem.parentNode.classList.add('toggle'), 500)
    
    window.addEventListener('resize', this.calcSubmenuHeight.bind(this))

    this.calcSubmenuHeight()

    window.onscroll = function(e) {
      if (this.oldScroll > this.scrollY || window.pageYOffset === 0) {
        el.classList.remove('hidden')
      } else {
        el.classList.add('hidden')
      }
      if (this.scrollY >= 200) {
        el.classList.add('white')
      } else {
        el.classList.remove('white') 
      }
      this.oldScroll = this.scrollY;
    } 

  }
  toggleNav(e) {
    e.preventDefault()
    this.nav.classList.toggle('js-open')
    this.menuButton.classList.toggle('js-open')
    document.querySelector('html').classList.toggle('no-scroll')
  }
  toggleSubmenu(e) {
    e.preventDefault()
    e.currentTarget.parentNode.classList.toggle('toggle')
  }
  calcSubmenuHeight(e) {
    const productMenu = document.querySelector('.products-item')

    if ( productMenu ) {
      productMenu.style.height = 'auto'
    }
    // productMenu.style.height = productMenu.offsetHeight + 'px'
  }
}

const initHeader = () => {

  new Header(document.querySelector('.header'))

}

export default initHeader