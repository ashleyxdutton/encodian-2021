import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'

gsap.registerPlugin(ScrollTrigger)

const pinImage = () => {

  const image = document.querySelector('.solutions__imageHolder')

  if (!image) return

  if (window.innerWidth > 960) {

    ScrollTrigger.create({ 
      trigger: image,
      pin: true,
      anticipatePin: 1,
      pinSpacing: false,
      start: 'top 50px',
      end: 'bottom bottom-=250px',
      endTrigger: '.end-trigger',
      // markers: true
    })

  }
  
}

export default pinImage