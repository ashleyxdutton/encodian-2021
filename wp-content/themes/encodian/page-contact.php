<section class="contact" data-inview>
  <div class="container container--l flex">

    <div class="contact__mapHolder pos--rel">
      <h1 class="contact__mapHeading" role="presentation"><?= the_field('contact_heading'); ?></h1>
      <iframe class="contact__map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d19487.266725901503!2d-1.797967!3d52.372082!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4870b840afe8c50b%3A0x96ae69f69fe8da15!2sBlythe%20Valley%20Business%20Park!5e0!3m2!1sen!2sus!4v1647861972503!5m2!1sen!2sus" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
      <!-- <div class="contact__map" id="map"></div> -->
      <?= file_get_contents('' . get_site_url() . '/wp-content/themes/encodian/src/svgs/map-mask.svg'); ?>
      <?= file_get_contents('' . get_site_url() . '/wp-content/themes/encodian/src/svgs/map-mask-m.svg'); ?>
    </div>

    <div class="contact__content">
      <h1 class="contact__heading"><?= the_field('contact_heading'); ?></h1>
      <div class="contact__address ff--head fs--30 lh--100 ls--t10"><?= the_field('contact_address'); ?></div>
      <p class="ff--head fs--30 lh--100 ls--t10">Call</p>
      <a class="contact__number ff--head fs--30 lh--100 ls--t10" href="tel:<?= the_field('contact_number'); ?>"><?= the_field('contact_number'); ?></a>
    </div>
    
  </div>
</section>

<?= get_template_part( 'template-parts/form' ); ?>