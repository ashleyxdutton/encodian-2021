(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _scss_main_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../scss/main.scss */ "./src/scss/main.scss");
/* harmony import */ var _scss_main_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_scss_main_scss__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_polyfill__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-polyfill */ "./node_modules/babel-polyfill/lib/index.js");
/* harmony import */ var babel_polyfill__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_polyfill__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var custom_event_polyfill_polyfill_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! custom-event-polyfill/polyfill.js */ "./node_modules/custom-event-polyfill/polyfill.js");
/* harmony import */ var custom_event_polyfill_polyfill_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(custom_event_polyfill_polyfill_js__WEBPACK_IMPORTED_MODULE_2__);
var _this = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }

// Import Scss DO NO DELETE
 // Import external dependencies


 // Bundle module files on build
// https://medium.com/@svinkle/getting-started-with-webpack-and-es6-modules-c465d053d988
// Issue with below is order of load is set by alphabetical naming of files...?

var requiredModules = __webpack_require__("./src/js/modules sync recursive \\.(js)$/");

requiredModules.keys().map(function (key) {
  _newArrowCheck(this, _this);

  requiredModules(key)["default"]();
}.bind(undefined));

/***/ }),

/***/ "./src/js/libraries/MorphSVGPlugin.min.js":
/*!************************************************!*\
  !*** ./src/js/libraries/MorphSVGPlugin.min.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*!
 * MorphSVGPlugin 3.3.4
 * https://greensock.com
 * 
 * @license Copyright 2020, GreenSock. All rights reserved.
 * Subject to the terms at https://greensock.com/standard-license or for Club GreenSock members, the agreement issued with that membership.
 * @author: Jack Doyle, jack@greensock.com
 */
!function (t, e) {
  "object" == ( false ? undefined : _typeof(exports)) && "undefined" != typeof module ? e(exports) :  true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [exports], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(this, function (t) {
  "use strict";

  function m(t) {
    return "string" == typeof t;
  }

  var M = /[achlmqstvz]|(-?\d*\.?\d*(?:e[\-+]?\d+)?)[0-9]/gi,
      A = /(?:(-)?\d*\.?\d*(?:e[\-+]?\d+)?)[0-9]/gi,
      b = /[\+\-]?\d*\.?\d+e[\+\-]?\d+/gi,
      n = /(^[#\.][a-z]|[a-y][a-z])/i,
      D = Math.PI / 180,
      E = Math.sin,
      k = Math.cos,
      Z = Math.abs,
      $ = Math.sqrt,
      h = function _isNumber(t) {
    return "number" == typeof t;
  },
      s = function _round(t) {
    return Math.round(1e5 * t) / 1e5 || 0;
  };

  function reverseSegment(t) {
    var e,
        r = 0;

    for (t.reverse(); r < t.length; r += 2) {
      e = t[r], t[r] = t[r + 1], t[r + 1] = e;
    }

    t.reversed = !t.reversed;
  }

  var z = {
    rect: "rx,ry,x,y,width,height",
    circle: "r,cx,cy",
    ellipse: "rx,ry,cx,cy",
    line: "x1,x2,y1,y2"
  };

  function convertToPath(t, e) {
    var r,
        n,
        o,
        i,
        a,
        h,
        s,
        l,
        g,
        c,
        p,
        u,
        f,
        d,
        _,
        P,
        m,
        v,
        y,
        w,
        x,
        T,
        M = t.tagName.toLowerCase(),
        b = .552284749831;

    return "path" !== M && t.getBBox ? (h = function _createPath(t, e) {
      var r,
          n = document.createElementNS("http://www.w3.org/2000/svg", "path"),
          o = [].slice.call(t.attributes),
          i = o.length;

      for (e = "," + e + ","; -1 < --i;) {
        r = o[i].nodeName.toLowerCase(), e.indexOf("," + r + ",") < 0 && n.setAttributeNS(null, r, o[i].nodeValue);
      }

      return n;
    }(t, "x,y,width,height,cx,cy,rx,ry,r,x1,x2,y1,y2,points"), T = function _attrToObj(t, e) {
      for (var r = e ? e.split(",") : [], n = {}, o = r.length; -1 < --o;) {
        n[r[o]] = +t.getAttribute(r[o]) || 0;
      }

      return n;
    }(t, z[M]), "rect" === M ? (i = T.rx, a = T.ry || i, n = T.x, o = T.y, c = T.width - 2 * i, p = T.height - 2 * a, r = i || a ? "M" + (P = (d = (f = n + i) + c) + i) + "," + (v = o + a) + " V" + (y = v + p) + " C" + [P, w = y + a * b, _ = d + i * b, x = y + a, d, x, d - (d - f) / 3, x, f + (d - f) / 3, x, f, x, u = n + i * (1 - b), x, n, w, n, y, n, y - (y - v) / 3, n, v + (y - v) / 3, n, v, n, m = o + a * (1 - b), u, o, f, o, f + (d - f) / 3, o, d - (d - f) / 3, o, d, o, _, o, P, m, P, v].join(",") + "z" : "M" + (n + c) + "," + o + " v" + p + " h" + -c + " v" + -p + " h" + c + "z") : "circle" === M || "ellipse" === M ? (l = "circle" === M ? (i = a = T.r) * b : (i = T.rx, (a = T.ry) * b), r = "M" + ((n = T.cx) + i) + "," + (o = T.cy) + " C" + [n + i, o + l, n + (s = i * b), o + a, n, o + a, n - s, o + a, n - i, o + l, n - i, o, n - i, o - l, n - s, o - a, n, o - a, n + s, o - a, n + i, o - l, n + i, o].join(",") + "z") : "line" === M ? r = "M" + T.x1 + "," + T.y1 + " L" + T.x2 + "," + T.y2 : "polyline" !== M && "polygon" !== M || (r = "M" + (n = (g = (t.getAttribute("points") + "").match(A) || []).shift()) + "," + (o = g.shift()) + " L" + g.join(","), "polygon" === M && (r += "," + n + "," + o + "z")), h.setAttribute("d", rawPathToString(h._gsRawPath = stringToRawPath(r))), e && t.parentNode && (t.parentNode.insertBefore(h, t), t.parentNode.removeChild(t)), h) : t;
  }

  function arcToSegment(t, e, r, n, o, i, a, h, s) {
    if (t !== h || e !== s) {
      r = Z(r), n = Z(n);

      var l = o % 360 * D,
          g = k(l),
          c = E(l),
          p = Math.PI,
          u = 2 * p,
          f = (t - h) / 2,
          d = (e - s) / 2,
          _ = g * f + c * d,
          P = -c * f + g * d,
          m = _ * _,
          v = P * P,
          y = m / (r * r) + v / (n * n);

      1 < y && (r = $(y) * r, n = $(y) * n);
      var w = r * r,
          x = n * n,
          T = (w * x - w * v - x * m) / (w * v + x * m);
      T < 0 && (T = 0);
      var M = (i === a ? -1 : 1) * $(T),
          b = r * P / n * M,
          S = -n * _ / r * M,
          N = g * b - c * S + (t + h) / 2,
          R = c * b + g * S + (e + s) / 2,
          A = (_ - b) / r,
          z = (P - S) / n,
          O = (-_ - b) / r,
          L = (-P - S) / n,
          Y = A * A + z * z,
          C = (z < 0 ? -1 : 1) * Math.acos(A / $(Y)),
          F = (A * L - z * O < 0 ? -1 : 1) * Math.acos((A * O + z * L) / $(Y * (O * O + L * L)));
      isNaN(F) && (F = p), !a && 0 < F ? F -= u : a && F < 0 && (F += u), C %= u, F %= u;
      var X,
          I = Math.ceil(Z(F) / (u / 4)),
          V = [],
          j = F / I,
          G = 4 / 3 * E(j / 2) / (1 + k(j / 2)),
          U = g * r,
          q = c * r,
          H = c * -n,
          B = g * n;

      for (X = 0; X < I; X++) {
        _ = k(o = C + X * j), P = E(o), A = k(o += j), z = E(o), V.push(_ - G * P, P + G * _, A + G * z, z - G * A, A, z);
      }

      for (X = 0; X < V.length; X += 2) {
        _ = V[X], P = V[X + 1], V[X] = _ * U + P * H + N, V[X + 1] = _ * q + P * B + R;
      }

      return V[X - 2] = h, V[X - 1] = s, V;
    }
  }

  function stringToRawPath(t) {
    function tc(t, e, r, n) {
      g = (r - t) / 3, c = (n - e) / 3, h.push(t + g, e + c, r - g, n - c, r, n);
    }

    var e,
        r,
        n,
        o,
        i,
        a,
        h,
        s,
        l,
        g,
        c,
        p,
        u,
        f,
        d,
        _ = (t + "").replace(b, function (t) {
      var e = +t;
      return e < 1e-4 && -1e-4 < e ? 0 : e;
    }).match(M) || [],
        P = [],
        m = 0,
        v = 0,
        y = _.length,
        w = 0,
        x = "ERROR: malformed path: " + t;

    if (!t || !isNaN(_[0]) || isNaN(_[1])) return console.log(x), P;

    for (e = 0; e < y; e++) {
      if (u = i, isNaN(_[e]) ? a = (i = _[e].toUpperCase()) !== _[e] : e--, n = +_[e + 1], o = +_[e + 2], a && (n += m, o += v), e || (s = n, l = o), "M" === i) h && (h.length < 8 ? --P.length : w += h.length), m = s = n, v = l = o, h = [n, o], P.push(h), e += 2, i = "L";else if ("C" === i) a || (m = v = 0), (h = h || [0, 0]).push(n, o, m + 1 * _[e + 3], v + 1 * _[e + 4], m += 1 * _[e + 5], v += 1 * _[e + 6]), e += 6;else if ("S" === i) g = m, c = v, "C" !== u && "S" !== u || (g += m - h[h.length - 4], c += v - h[h.length - 3]), a || (m = v = 0), h.push(g, c, n, o, m += 1 * _[e + 3], v += 1 * _[e + 4]), e += 4;else if ("Q" === i) g = m + 2 / 3 * (n - m), c = v + 2 / 3 * (o - v), a || (m = v = 0), m += 1 * _[e + 3], v += 1 * _[e + 4], h.push(g, c, m + 2 / 3 * (n - m), v + 2 / 3 * (o - v), m, v), e += 4;else if ("T" === i) g = m - h[h.length - 4], c = v - h[h.length - 3], h.push(m + g, v + c, n + 2 / 3 * (m + 1.5 * g - n), o + 2 / 3 * (v + 1.5 * c - o), m = n, v = o), e += 2;else if ("H" === i) tc(m, v, m = n, v), e += 1;else if ("V" === i) tc(m, v, m, v = n + (a ? v - m : 0)), e += 1;else if ("L" === i || "Z" === i) "Z" === i && (n = s, o = l, h.closed = !0), ("L" === i || .5 < Z(m - n) || .5 < Z(v - o)) && (tc(m, v, n, o), "L" === i && (e += 2)), m = n, v = o;else if ("A" === i) {
        if (f = _[e + 4], d = _[e + 5], g = _[e + 6], c = _[e + 7], r = 7, 1 < f.length && (f.length < 3 ? (c = g, g = d, r--) : (c = d, g = f.substr(2), r -= 2), d = f.charAt(1), f = f.charAt(0)), p = arcToSegment(m, v, +_[e + 1], +_[e + 2], +_[e + 3], +f, +d, (a ? m : 0) + 1 * g, (a ? v : 0) + 1 * c), e += r, p) for (r = 0; r < p.length; r++) {
          h.push(p[r]);
        }
        m = h[h.length - 2], v = h[h.length - 1];
      } else console.log(x);
    }

    return (e = h.length) < 6 ? (P.pop(), e = 0) : h[0] === h[e - 2] && h[1] === h[e - 1] && (h.closed = !0), P.totalPoints = w + e, P;
  }

  function rawPathToString(t) {
    h(t[0]) && (t = [t]);
    var e,
        r,
        n,
        o,
        i = "",
        a = t.length;

    for (r = 0; r < a; r++) {
      for (o = t[r], i += "M" + s(o[0]) + "," + s(o[1]) + " C", e = o.length, n = 2; n < e; n++) {
        i += s(o[n++]) + "," + s(o[n++]) + " " + s(o[n++]) + "," + s(o[n++]) + " " + s(o[n++]) + "," + s(o[n]) + " ";
      }

      o.closed && (i += "z");
    }

    return i;
  }

  function y() {
    return r || "undefined" != typeof window && (r = window.gsap) && r.registerPlugin && r;
  }

  function L(t) {
    return console && console.warn(t);
  }

  function N(t) {
    var e,
        r = t.length,
        n = 0,
        o = 0;

    for (e = 0; e < r; e++) {
      n += t[e++], o += t[e];
    }

    return [n / (r / 2), o / (r / 2)];
  }

  function O(t) {
    var e,
        r,
        n,
        o = t.length,
        i = t[0],
        a = i,
        h = t[1],
        s = h;

    for (n = 6; n < o; n += 6) {
      i < (e = t[n]) ? i = e : e < a && (a = e), h < (r = t[n + 1]) ? h = r : r < s && (s = r);
    }

    return t.centerX = (i + a) / 2, t.centerY = (h + s) / 2, t.size = (i - a) * (h - s);
  }

  function P(t, e) {
    void 0 === e && (e = 3);

    for (var r, n, o, i, a, h, s, l, g, c, p, u, f, d, _, P, m = t.length, v = t[0][0], y = v, w = t[0][1], x = w, T = 1 / e; -1 < --m;) {
      for (r = (a = t[m]).length, i = 6; i < r; i += 6) {
        for (g = a[i], c = a[i + 1], p = a[i + 2] - g, d = a[i + 3] - c, u = a[i + 4] - g, _ = a[i + 5] - c, f = a[i + 6] - g, P = a[i + 7] - c, h = e; -1 < --h;) {
          v < (n = ((s = T * h) * s * f + 3 * (l = 1 - s) * (s * u + l * p)) * s + g) ? v = n : n < y && (y = n), w < (o = (s * s * P + 3 * l * (s * _ + l * d)) * s + c) ? w = o : o < x && (x = o);
        }
      }
    }

    return t.centerX = (v + y) / 2, t.centerY = (w + x) / 2, t.left = y, t.width = v - y, t.top = x, t.height = w - x, t.size = (v - y) * (w - x);
  }

  function Q(t, e) {
    return e.length - t.length;
  }

  function R(t, e) {
    var r = t.size || O(t),
        n = e.size || O(e);
    return Math.abs(n - r) < (r + n) / 20 ? e.centerX - t.centerX || e.centerY - t.centerY : n - r;
  }

  function S(t, e) {
    var r,
        n,
        o = t.slice(0),
        i = t.length,
        a = i - 2;

    for (e |= 0, r = 0; r < i; r++) {
      n = (r + e) % a, t[r++] = o[n], t[r] = o[1 + n];
    }
  }

  function T(t, e, r, n, o) {
    var i,
        a,
        h,
        s,
        l = t.length,
        g = 0,
        c = l - 2;

    for (r *= 6, a = 0; a < l; a += 6) {
      s = t[i = (a + r) % c] - (e[a] - n), h = t[1 + i] - (e[a + 1] - o), g += v(h * h + s * s);
    }

    return g;
  }

  function U(t, e, r) {
    var n,
        o,
        i,
        a = t.length,
        h = N(t),
        s = N(e),
        l = s[0] - h[0],
        g = s[1] - h[1],
        c = T(t, e, 0, l, g),
        p = 0;

    for (i = 6; i < a; i += 6) {
      (o = T(t, e, i / 6, l, g)) < c && (c = o, p = i);
    }

    if (r) for (reverseSegment(n = t.slice(0)), i = 6; i < a; i += 6) {
      (o = T(n, e, i / 6, l, g)) < c && (c = o, p = -i);
    }
    return p / 6;
  }

  function V(t, e, r) {
    for (var n, o, i, a, h, s, l = t.length, g = 1e20, c = 0, p = 0; -1 < --l;) {
      for (s = (n = t[l]).length, h = 0; h < s; h += 6) {
        o = n[h] - e, i = n[h + 1] - r, (a = v(o * o + i * i)) < g && (g = a, c = n[h], p = n[h + 1]);
      }
    }

    return [c, p];
  }

  function W(t, e, r, n, o, i) {
    var a,
        h,
        s,
        l,
        g = e.length,
        c = 0,
        p = Math.min(t.size || O(t), e[r].size || O(e[r])) * n,
        u = 1e20,
        f = t.centerX + o,
        d = t.centerY + i;

    for (a = r; a < g && !((e[a].size || O(e[a])) < p); a++) {
      h = e[a].centerX - f, s = e[a].centerY - d, (l = v(h * h + s * s)) < u && (c = a, u = l);
    }

    return l = e[c], e.splice(c, 1), l;
  }

  function X(t, e) {
    var r,
        n,
        o,
        i,
        a,
        h,
        s,
        l,
        g,
        c,
        p,
        u,
        f,
        d,
        _ = 0,
        P = t.length,
        m = e / ((P - 2) / 6);

    for (f = 2; f < P; f += 6) {
      for (_ += m; .999999 < _;) {
        r = t[f - 2], n = t[f - 1], o = t[f], i = t[f + 1], a = t[f + 2], h = t[f + 3], s = t[f + 4], l = t[f + 5], g = r + (o - r) * (d = 1 / ((Math.floor(_) || 1) + 1)), g += ((p = o + (a - o) * d) - g) * d, p += (a + (s - a) * d - p) * d, c = n + (i - n) * d, c += ((u = i + (h - i) * d) - c) * d, u += (h + (l - h) * d - u) * d, t.splice(f, 4, r + (o - r) * d, n + (i - n) * d, g, c, g + (p - g) * d, c + (u - c) * d, p, u, a + (s - a) * d, h + (l - h) * d), f += 6, P += 6, _--;
      }
    }

    return t;
  }

  function Y(t, e, r, n, o) {
    var i,
        a,
        h,
        s,
        l,
        g,
        c,
        p = e.length - t.length,
        u = 0 < p ? e : t,
        f = 0 < p ? t : e,
        d = 0,
        _ = "complexity" === n ? Q : R,
        m = "position" === n ? 0 : "number" == typeof n ? n : .8,
        v = f.length,
        y = "object" == _typeof(r) && r.push ? r.slice(0) : [r],
        w = "reverse" === y[0] || y[0] < 0,
        x = "log" === r;

    if (f[0]) {
      if (1 < u.length && (t.sort(_), e.sort(_), u.size || P(u), f.size || P(f), g = u.centerX - f.centerX, c = u.centerY - f.centerY, _ === R)) for (v = 0; v < f.length; v++) {
        u.splice(v, 0, W(f[v], u, v, m, g, c));
      }
      if (p) for (p < 0 && (p = -p), u[0].length > f[0].length && X(f[0], (u[0].length - f[0].length) / 6 | 0), v = f.length; d < p;) {
        u[v].size || O(u[v]), s = (h = V(f, u[v].centerX, u[v].centerY))[0], l = h[1], f[v++] = [s, l, s, l, s, l, s, l], f.totalPoints += 8, d++;
      }

      for (v = 0; v < t.length; v++) {
        i = e[v], a = t[v], (p = i.length - a.length) < 0 ? X(i, -p / 6 | 0) : 0 < p && X(a, p / 6 | 0), w && !1 !== o && !a.reversed && reverseSegment(a), (r = y[v] || 0 === y[v] ? y[v] : "auto") && (a.closed || Math.abs(a[0] - a[a.length - 2]) < .5 && Math.abs(a[1] - a[a.length - 1]) < .5 ? "auto" === r || "log" === r ? (y[v] = r = U(a, i, !v || !1 === o), r < 0 && (w = !0, reverseSegment(a), r = -r), S(a, 6 * r)) : "reverse" !== r && (v && r < 0 && reverseSegment(a), S(a, 6 * (r < 0 ? -r : r))) : !w && ("auto" === r && Math.abs(i[0] - a[0]) + Math.abs(i[1] - a[1]) + Math.abs(i[i.length - 2] - a[a.length - 2]) + Math.abs(i[i.length - 1] - a[a.length - 1]) > Math.abs(i[0] - a[a.length - 2]) + Math.abs(i[1] - a[a.length - 1]) + Math.abs(i[i.length - 2] - a[0]) + Math.abs(i[i.length - 1] - a[1]) || r % 2) ? (reverseSegment(a), y[v] = -1, w = !0) : "auto" === r ? y[v] = 0 : "reverse" === r && (y[v] = -1), a.closed !== i.closed && (a.closed = i.closed = !1));
      }

      return x && L("shapeIndex:[" + y.join(",") + "]"), t.shapeIndex = y;
    }
  }

  function _(t, e) {
    var r,
        n,
        o,
        i,
        a,
        h,
        s,
        l = 0,
        g = parseFloat(t[0]),
        c = parseFloat(t[1]),
        p = g + "," + c + " ";

    for (r = .5 * e / (.5 * (o = t.length) - 1), n = 0; n < o - 2; n += 2) {
      if (l += r, h = parseFloat(t[n + 2]), s = parseFloat(t[n + 3]), .999999 < l) for (a = 1 / (Math.floor(l) + 1), i = 1; .999999 < l;) {
        p += (g + (h - g) * a * i).toFixed(2) + "," + (c + (s - c) * a * i).toFixed(2) + " ", l--, i++;
      }
      p += h + "," + s + " ", g = h, c = s;
    }

    return p;
  }

  function aa(t) {
    var e = t[0].match(G) || [],
        r = t[1].match(G) || [],
        n = r.length - e.length;
    0 < n ? t[0] = _(e, n) : t[1] = _(r, -n);
  }

  function ba(e) {
    return isNaN(e) ? aa : function (t) {
      aa(t), t[1] = function _offsetPoints(t, e) {
        if (!e) return t;
        var r,
            n,
            o,
            i = t.match(G) || [],
            a = i.length,
            h = "";

        for (r = "reverse" === e ? (n = a - 1, -2) : (n = (2 * (parseInt(e, 10) || 0) + 1 + 100 * a) % a, 2), o = 0; o < a; o += 2) {
          h += i[n - 1] + "," + i[n] + " ", n = (n + r) % a;
        }

        return h;
      }(t[1], parseInt(e, 10));
    };
  }

  function da(t, e) {
    for (var r, n, o, i, a, h, s, l, g, c, p, u, f = t.length, d = .2 * (e || 1); -1 < --f;) {
      for (p = (n = t[f]).isSmooth = n.isSmooth || [0, 0, 0, 0], u = n.smoothData = n.smoothData || [0, 0, 0, 0], p.length = 4, l = n.length - 2, s = 6; s < l; s += 6) {
        o = n[s] - n[s - 2], i = n[s + 1] - n[s - 1], a = n[s + 2] - n[s], h = n[s + 3] - n[s + 1], g = w(i, o), c = w(h, a), (r = Math.abs(g - c) < d) && (u[s - 2] = g, u[s + 2] = c, u[s - 1] = v(o * o + i * i), u[s + 3] = v(a * a + h * h)), p.push(r, r, 0, 0, r, r);
      }

      n[l] === n[0] && n[1 + l] === n[1] && (o = n[0] - n[l - 2], i = n[1] - n[l - 1], a = n[2] - n[0], h = n[3] - n[1], g = w(i, o), c = w(h, a), Math.abs(g - c) < d && (u[l - 2] = g, u[2] = c, u[l - 1] = v(o * o + i * i), u[3] = v(a * a + h * h), p[l - 2] = p[l - 1] = !0));
    }

    return t;
  }

  function ea(t) {
    var e = t.trim().split(" ");
    return {
      x: (~t.indexOf("left") ? 0 : ~t.indexOf("right") ? 100 : isNaN(parseFloat(e[0])) ? 50 : parseFloat(e[0])) / 100,
      y: (~t.indexOf("top") ? 0 : ~t.indexOf("bottom") ? 100 : isNaN(parseFloat(e[1])) ? 50 : parseFloat(e[1])) / 100
    };
  }

  function ha(t, e, r, n) {
    var o,
        i,
        a = this._origin,
        h = this._eOrigin,
        s = t[r] - a.x,
        l = t[r + 1] - a.y,
        g = v(s * s + l * l),
        c = w(l, s);
    return s = e[r] - h.x, l = e[r + 1] - h.y, i = function _shortAngle(t) {
      return t !== t % p ? t + (t < 0 ? u : -u) : t;
    }(o = w(l, s) - c), !n && F && Math.abs(i + F.ca) < f && (n = F), this._anchorPT = F = {
      _next: this._anchorPT,
      t: t,
      sa: c,
      ca: n && i * n.ca < 0 && Math.abs(i) > d ? o : i,
      sl: g,
      cl: v(s * s + l * l) - g,
      i: r
    };
  }

  function ia(t) {
    r = y(), o = o || r && r.plugins.morphSVG, r && o ? (C = r.utils.toArray, o.prototype._tweenRotation = ha, I = 1) : t && L("Please gsap.registerPlugin(MorphSVGPlugin)");
  }

  var r,
      C,
      F,
      I,
      o,
      w = Math.atan2,
      x = Math.cos,
      j = Math.sin,
      v = Math.sqrt,
      p = Math.PI,
      u = 2 * p,
      f = .3 * p,
      d = .7 * p,
      G = /[-+=\.]*\d+[\.e\-\+]*\d*[e\-\+]*\d*/gi,
      q = /(^[#\.][a-z]|[a-y][a-z])/gi,
      H = /[achlmqstvz]/gi,
      B = "Use MorphSVGPlugin.convertToPath() to convert to a path before morphing.",
      J = {
    version: "3.3.4",
    name: "morphSVG",
    register: function register(t, e) {
      r = t, o = e, ia();
    },
    init: function init(t, e, r, n, o) {
      if (I || ia(1), !e) return L("invalid shape"), !1;
      ("string" == typeof e || e.getBBox || e[0]) && (e = {
        shape: e
      });

      var i,
          a,
          h,
          s,
          l,
          g,
          c,
          p,
          u,
          f,
          d,
          _,
          m,
          v,
          y,
          w,
          x,
          T,
          M,
          b,
          S,
          N,
          R = t.nodeType ? window.getComputedStyle(t) : {},
          A = R.fill + "",
          z = !("none" === A || "0" === (A.match(G) || [])[3] || "evenodd" === R.fillRule),
          O = (e.origin || "50 50").split(",");

      if (l = "POLYLINE" === (i = (t.nodeName + "").toUpperCase()) || "POLYGON" === i, "PATH" !== i && !l && !e.prop) return L("Cannot morph a <" + i + "> element. " + B), !1;
      if (a = "PATH" === i ? "d" : "points", !e.prop && "function" != typeof t.setAttribute) return !1;
      if (s = function _parseShape(t, e, r) {
        var n, o;
        return (!("string" == typeof t) || q.test(t) || (t.match(G) || []).length < 3) && ((n = C(t)[0]) ? (o = (n.nodeName + "").toUpperCase(), e && "PATH" !== o && (n = convertToPath(n, !1), o = "PATH"), t = n.getAttribute("PATH" === o ? "d" : "points") || "", n === r && (t = n.getAttributeNS(null, "data-original") || t)) : (L("WARNING: invalid morph to: " + t), t = !1)), t;
      }(e.shape || e.d || e.points || "", "d" == a, t), l && H.test(s)) return L("A <" + i + "> cannot accept path data. " + B), !1;

      if (g = e.shapeIndex || 0 === e.shapeIndex ? e.shapeIndex : "auto", c = e.map || J.defaultMap, this._prop = e.prop, this._render = e.render || J.defaultRender, this._apply = "updateTarget" in e ? e.updateTarget : J.defaultUpdateTarget, this._rnd = Math.pow(10, isNaN(e.precision) ? 2 : +e.precision), this._tween = r, s) {
        if (this._target = t, x = "object" == _typeof(e.precompile), f = this._prop ? t[this._prop] : t.getAttribute(a), this._prop || t.getAttributeNS(null, "data-original") || t.setAttributeNS(null, "data-original", f), "d" == a || this._prop) {
          if (f = stringToRawPath(x ? e.precompile[0] : f), d = stringToRawPath(x ? e.precompile[1] : s), !x && !Y(f, d, g, c, z)) return !1;

          for ("log" !== e.precompile && !0 !== e.precompile || L('precompile:["' + rawPathToString(f) + '","' + rawPathToString(d) + '"]'), (S = "linear" !== (e.type || J.defaultType)) && (f = da(f, e.smoothTolerance), d = da(d, e.smoothTolerance), f.size || P(f), d.size || P(d), b = ea(O[0]), this._origin = f.origin = {
            x: f.left + b.x * f.width,
            y: f.top + b.y * f.height
          }, O[1] && (b = ea(O[1])), this._eOrigin = {
            x: d.left + b.x * d.width,
            y: d.top + b.y * d.height
          }), this._rawPath = t._gsRawPath = f, m = f.length; -1 < --m;) {
            for (y = f[m], w = d[m], p = y.isSmooth || [], u = w.isSmooth || [], v = y.length, _ = F = 0; _ < v; _ += 2) {
              w[_] === y[_] && w[_ + 1] === y[_ + 1] || (S ? p[_] && u[_] ? (T = y.smoothData, M = w.smoothData, N = _ + (_ === v - 4 ? 7 - v : 5), this._controlPT = {
                _next: this._controlPT,
                i: _,
                j: m,
                l1s: T[_ + 1],
                l1c: M[_ + 1] - T[_ + 1],
                l2s: T[N],
                l2c: M[N] - T[N]
              }, h = this._tweenRotation(y, w, _ + 2), this._tweenRotation(y, w, _, h), this._tweenRotation(y, w, N - 1, h), _ += 4) : this._tweenRotation(y, w, _) : (h = this.add(y, _, y[_], w[_]), h = this.add(y, _ + 1, y[_ + 1], w[_ + 1]) || h));
            }
          }
        } else h = this.add(t, "setAttribute", t.getAttribute(a) + "", s + "", n, o, 0, ba(g), a);

        S && (this.add(this._origin, "x", this._origin.x, this._eOrigin.x), h = this.add(this._origin, "y", this._origin.y, this._eOrigin.y)), h && (this._props.push("morphSVG"), h.end = s, h.endProp = a);
      }

      return 1;
    },
    render: function render(t, e) {
      for (var r, n, o, i, a, h, s, l, g, c, p, u, f = e._rawPath, d = e._controlPT, _ = e._anchorPT, P = e._rnd, m = e._target, v = e._pt; v;) {
        v.r(t, v.d), v = v._next;
      }

      if (1 === t && e._apply) for (v = e._pt; v;) {
        v.end && (e._prop ? m[e._prop] = v.end : m.setAttribute(v.endProp, v.end)), v = v._next;
      } else if (f) {
        for (; _;) {
          a = _.sa + t * _.ca, i = _.sl + t * _.cl, _.t[_.i] = e._origin.x + x(a) * i, _.t[_.i + 1] = e._origin.y + j(a) * i, _ = _._next;
        }

        for (n = t < .5 ? 2 * t * t : (4 - 2 * t) * t - 1; d;) {
          u = (h = d.i) + (h === (o = f[d.j]).length - 4 ? 7 - o.length : 5), a = w(o[u] - o[h + 1], o[u - 1] - o[h]), c = j(a), p = x(a), l = o[h + 2], g = o[h + 3], i = d.l1s + n * d.l1c, o[h] = l - p * i, o[h + 1] = g - c * i, i = d.l2s + n * d.l2c, o[u - 1] = l + p * i, o[u] = g + c * i, d = d._next;
        }

        if (m._gsRawPath = f, e._apply) {
          for (r = "", s = 0; s < f.length; s++) {
            for (i = (o = f[s]).length, r += "M" + (o[0] * P | 0) / P + " " + (o[1] * P | 0) / P + " C", h = 2; h < i; h++) {
              r += (o[h] * P | 0) / P + " ";
            }
          }

          e._prop ? m[e._prop] = r : m.setAttribute("d", r);
        }
      }
      e._render && f && e._render.call(e._tween, f, m);
    },
    kill: function kill() {
      this._pt = this._rawPath = 0;
    },
    getRawPath: function getRawPath(t) {
      var e,
          r = (t = m(t) && n.test(t) && document.querySelector(t) || t).getAttribute ? t : 0;
      return r && (t = t.getAttribute("d")) ? (r._gsPath || (r._gsPath = {}), (e = r._gsPath[t]) && !e._dirty ? e : r._gsPath[t] = stringToRawPath(t)) : t ? m(t) ? stringToRawPath(t) : h(t[0]) ? [t] : t : console.warn("Expecting a <path> element or an SVG path data string");
    },
    stringToRawPath: stringToRawPath,
    rawPathToString: rawPathToString,
    pathFilter: function _pathFilter(t, e, r, n, o) {
      var i = stringToRawPath(t[0]),
          a = stringToRawPath(t[1]);
      Y(i, a, e || 0 === e ? e : "auto", r, o) && (t[0] = rawPathToString(i), t[1] = rawPathToString(a), "log" !== n && !0 !== n || L('precompile:["' + t[0] + '","' + t[1] + '"]'));
    },
    pointsFilter: aa,
    getTotalSize: P,
    equalizeSegmentQuantity: Y,
    convertToPath: function convertToPath$1(t, e) {
      return C(t).map(function (t) {
        return convertToPath(t, !1 !== e);
      });
    },
    defaultType: "linear",
    defaultUpdateTarget: !0,
    defaultMap: "size"
  };
  y() && r.registerPlugin(J), t.MorphSVGPlugin = J, t["default"] = J;

  if (typeof window === "undefined" || window !== t) {
    Object.defineProperty(t, "__esModule", {
      value: !0
    });
  } else {
    delete t["default"];
  }
});

/***/ }),

/***/ "./src/js/libraries/SplitText.min.js":
/*!*******************************************!*\
  !*** ./src/js/libraries/SplitText.min.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*!
 * SplitText 3.3.4
 * https://greensock.com
 * 
 * @license Copyright 2020, GreenSock. All rights reserved.
 * Subject to the terms at https://greensock.com/standard-license or for Club GreenSock members, the agreement issued with that membership.
 * @author: Jack Doyle, jack@greensock.com
 */
!function (D, u) {
  "object" == ( false ? undefined : _typeof(exports)) && "undefined" != typeof module ? u(exports) :  true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [exports], __WEBPACK_AMD_DEFINE_FACTORY__ = (u),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(this, function (D) {
  "use strict";

  var b = /([\uD800-\uDBFF][\uDC00-\uDFFF](?:[\u200D\uFE0F][\uD800-\uDBFF][\uDC00-\uDFFF]){2,}|\uD83D\uDC69(?:\u200D(?:(?:\uD83D\uDC69\u200D)?\uD83D\uDC67|(?:\uD83D\uDC69\u200D)?\uD83D\uDC66)|\uD83C[\uDFFB-\uDFFF])|\uD83D\uDC69\u200D(?:\uD83D\uDC69\u200D)?\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC69\u200D(?:\uD83D\uDC69\u200D)?\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83C\uDFF3\uFE0F\u200D\uD83C\uDF08|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDD6-\uDDDD])(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]\uFE0F|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92])|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC6F\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3C-\uDD3E\uDDD6-\uDDDF])\u200D[\u2640\u2642]\uFE0F|\uD83C\uDDFD\uD83C\uDDF0|\uD83C\uDDF6\uD83C\uDDE6|\uD83C\uDDF4\uD83C\uDDF2|\uD83C\uDDE9(?:\uD83C[\uDDEA\uDDEC\uDDEF\uDDF0\uDDF2\uDDF4\uDDFF])|\uD83C\uDDF7(?:\uD83C[\uDDEA\uDDF4\uDDF8\uDDFA\uDDFC])|\uD83C\uDDE8(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDEE\uDDF0-\uDDF5\uDDF7\uDDFA-\uDDFF])|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uFE0F\u200D[\u2640\u2642]|(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642])\uFE0F|(?:\uD83D\uDC41\uFE0F\u200D\uD83D\uDDE8|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\uD83D\uDC69\u200D[\u2695\u2696\u2708]|\uD83D\uDC68(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\u200D[\u2695\u2696\u2708]))\uFE0F|\uD83C\uDDF2(?:\uD83C[\uDDE6\uDDE8-\uDDED\uDDF0-\uDDFF])|\uD83D\uDC69\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D(?:\uD83D[\uDC68\uDC69])|\uD83D[\uDC68\uDC69]))|\uD83C\uDDF1(?:\uD83C[\uDDE6-\uDDE8\uDDEE\uDDF0\uDDF7-\uDDFB\uDDFE])|\uD83C\uDDEF(?:\uD83C[\uDDEA\uDDF2\uDDF4\uDDF5])|\uD83C\uDDED(?:\uD83C[\uDDF0\uDDF2\uDDF3\uDDF7\uDDF9\uDDFA])|\uD83C\uDDEB(?:\uD83C[\uDDEE-\uDDF0\uDDF2\uDDF4\uDDF7])|[#\*0-9]\uFE0F\u20E3|\uD83C\uDDE7(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEF\uDDF1-\uDDF4\uDDF6-\uDDF9\uDDFB\uDDFC\uDDFE\uDDFF])|\uD83C\uDDE6(?:\uD83C[\uDDE8-\uDDEC\uDDEE\uDDF1\uDDF2\uDDF4\uDDF6-\uDDFA\uDDFC\uDDFD\uDDFF])|\uD83C\uDDFF(?:\uD83C[\uDDE6\uDDF2\uDDFC])|\uD83C\uDDF5(?:\uD83C[\uDDE6\uDDEA-\uDDED\uDDF0-\uDDF3\uDDF7-\uDDF9\uDDFC\uDDFE])|\uD83C\uDDFB(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDEE\uDDF3\uDDFA])|\uD83C\uDDF3(?:\uD83C[\uDDE6\uDDE8\uDDEA-\uDDEC\uDDEE\uDDF1\uDDF4\uDDF5\uDDF7\uDDFA\uDDFF])|\uD83C\uDFF4\uDB40\uDC67\uDB40\uDC62(?:\uDB40\uDC77\uDB40\uDC6C\uDB40\uDC73|\uDB40\uDC73\uDB40\uDC63\uDB40\uDC74|\uDB40\uDC65\uDB40\uDC6E\uDB40\uDC67)\uDB40\uDC7F|\uD83D\uDC68(?:\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D)?\uD83D\uDC68|(?:(?:\uD83D[\uDC68\uDC69])\u200D)?\uD83D\uDC66\u200D\uD83D\uDC66|(?:(?:\uD83D[\uDC68\uDC69])\u200D)?\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92])|(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]))|\uD83C\uDDF8(?:\uD83C[\uDDE6-\uDDEA\uDDEC-\uDDF4\uDDF7-\uDDF9\uDDFB\uDDFD-\uDDFF])|\uD83C\uDDF0(?:\uD83C[\uDDEA\uDDEC-\uDDEE\uDDF2\uDDF3\uDDF5\uDDF7\uDDFC\uDDFE\uDDFF])|\uD83C\uDDFE(?:\uD83C[\uDDEA\uDDF9])|\uD83C\uDDEE(?:\uD83C[\uDDE8-\uDDEA\uDDF1-\uDDF4\uDDF6-\uDDF9])|\uD83C\uDDF9(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDED\uDDEF-\uDDF4\uDDF7\uDDF9\uDDFB\uDDFC\uDDFF])|\uD83C\uDDEC(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEE\uDDF1-\uDDF3\uDDF5-\uDDFA\uDDFC\uDDFE])|\uD83C\uDDFA(?:\uD83C[\uDDE6\uDDEC\uDDF2\uDDF3\uDDF8\uDDFE\uDDFF])|\uD83C\uDDEA(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDED\uDDF7-\uDDFA])|\uD83C\uDDFC(?:\uD83C[\uDDEB\uDDF8])|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDD6-\uDDDD])(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u261D\u270A-\u270D]|\uD83C[\uDF85\uDFC2\uDFC7]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66\uDC67\uDC70\uDC72\uDC74-\uDC76\uDC78\uDC7C\uDC83\uDC85\uDCAA\uDD74\uDD7A\uDD90\uDD95\uDD96\uDE4C\uDE4F\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD30-\uDD36\uDDD1-\uDDD5])(?:\uD83C[\uDFFB-\uDFFF])|\uD83D\uDC68(?:\u200D(?:(?:(?:\uD83D[\uDC68\uDC69])\u200D)?\uD83D\uDC67|(?:(?:\uD83D[\uDC68\uDC69])\u200D)?\uD83D\uDC66)|\uD83C[\uDFFB-\uDFFF])|(?:[\u261D\u26F9\u270A-\u270D]|\uD83C[\uDF85\uDFC2-\uDFC4\uDFC7\uDFCA-\uDFCC]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66-\uDC69\uDC6E\uDC70-\uDC78\uDC7C\uDC81-\uDC83\uDC85-\uDC87\uDCAA\uDD74\uDD75\uDD7A\uDD90\uDD95\uDD96\uDE45-\uDE47\uDE4B-\uDE4F\uDEA3\uDEB4-\uDEB6\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD26\uDD30-\uDD39\uDD3D\uDD3E\uDDD1-\uDDDD])(?:\uD83C[\uDFFB-\uDFFF])?|(?:[\u231A\u231B\u23E9-\u23EC\u23F0\u23F3\u25FD\u25FE\u2614\u2615\u2648-\u2653\u267F\u2693\u26A1\u26AA\u26AB\u26BD\u26BE\u26C4\u26C5\u26CE\u26D4\u26EA\u26F2\u26F3\u26F5\u26FA\u26FD\u2705\u270A\u270B\u2728\u274C\u274E\u2753-\u2755\u2757\u2795-\u2797\u27B0\u27BF\u2B1B\u2B1C\u2B50\u2B55]|\uD83C[\uDC04\uDCCF\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE1A\uDE2F\uDE32-\uDE36\uDE38-\uDE3A\uDE50\uDE51\uDF00-\uDF20\uDF2D-\uDF35\uDF37-\uDF7C\uDF7E-\uDF93\uDFA0-\uDFCA\uDFCF-\uDFD3\uDFE0-\uDFF0\uDFF4\uDFF8-\uDFFF]|\uD83D[\uDC00-\uDC3E\uDC40\uDC42-\uDCFC\uDCFF-\uDD3D\uDD4B-\uDD4E\uDD50-\uDD67\uDD7A\uDD95\uDD96\uDDA4\uDDFB-\uDE4F\uDE80-\uDEC5\uDECC\uDED0-\uDED2\uDEEB\uDEEC\uDEF4-\uDEF8]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD4C\uDD50-\uDD6B\uDD80-\uDD97\uDDC0\uDDD0-\uDDE6])|(?:[#\*0-9\xA9\xAE\u203C\u2049\u2122\u2139\u2194-\u2199\u21A9\u21AA\u231A\u231B\u2328\u23CF\u23E9-\u23F3\u23F8-\u23FA\u24C2\u25AA\u25AB\u25B6\u25C0\u25FB-\u25FE\u2600-\u2604\u260E\u2611\u2614\u2615\u2618\u261D\u2620\u2622\u2623\u2626\u262A\u262E\u262F\u2638-\u263A\u2640\u2642\u2648-\u2653\u2660\u2663\u2665\u2666\u2668\u267B\u267F\u2692-\u2697\u2699\u269B\u269C\u26A0\u26A1\u26AA\u26AB\u26B0\u26B1\u26BD\u26BE\u26C4\u26C5\u26C8\u26CE\u26CF\u26D1\u26D3\u26D4\u26E9\u26EA\u26F0-\u26F5\u26F7-\u26FA\u26FD\u2702\u2705\u2708-\u270D\u270F\u2712\u2714\u2716\u271D\u2721\u2728\u2733\u2734\u2744\u2747\u274C\u274E\u2753-\u2755\u2757\u2763\u2764\u2795-\u2797\u27A1\u27B0\u27BF\u2934\u2935\u2B05-\u2B07\u2B1B\u2B1C\u2B50\u2B55\u3030\u303D\u3297\u3299]|\uD83C[\uDC04\uDCCF\uDD70\uDD71\uDD7E\uDD7F\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE02\uDE1A\uDE2F\uDE32-\uDE3A\uDE50\uDE51\uDF00-\uDF21\uDF24-\uDF93\uDF96\uDF97\uDF99-\uDF9B\uDF9E-\uDFF0\uDFF3-\uDFF5\uDFF7-\uDFFF]|\uD83D[\uDC00-\uDCFD\uDCFF-\uDD3D\uDD49-\uDD4E\uDD50-\uDD67\uDD6F\uDD70\uDD73-\uDD7A\uDD87\uDD8A-\uDD8D\uDD90\uDD95\uDD96\uDDA4\uDDA5\uDDA8\uDDB1\uDDB2\uDDBC\uDDC2-\uDDC4\uDDD1-\uDDD3\uDDDC-\uDDDE\uDDE1\uDDE3\uDDE8\uDDEF\uDDF3\uDDFA-\uDE4F\uDE80-\uDEC5\uDECB-\uDED2\uDEE0-\uDEE5\uDEE9\uDEEB\uDEEC\uDEF0\uDEF3-\uDEF8]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD4C\uDD50-\uDD6B\uDD80-\uDD97\uDDC0\uDDD0-\uDDE6])\uFE0F)/;

  function k(D) {
    return e.getComputedStyle(D);
  }

  function n(D, u) {
    var e;
    return i(D) ? D : "string" == (e = _typeof(D)) && !u && D ? E.call(Q.querySelectorAll(D), 0) : D && "object" == e && "length" in D ? E.call(D, 0) : D ? [D] : [];
  }

  function o(D) {
    return "absolute" === D.position || !0 === D.absolute;
  }

  function p(D, u) {
    for (var e, F = u.length; -1 < --F;) {
      if (e = u[F], D.substr(0, e.length) === e) return e.length;
    }
  }

  function r(D, u) {
    void 0 === D && (D = "");
    var e = ~D.indexOf("++"),
        F = 1;
    return e && (D = D.split("++").join("")), function () {
      return "<" + u + " style='position:relative;display:inline-block;'" + (D ? " class='" + D + (e ? F++ : "") + "'>" : ">");
    };
  }

  function s(D, u, e) {
    var F = D.nodeType;
    if (1 === F || 9 === F || 11 === F) for (D = D.firstChild; D; D = D.nextSibling) {
      s(D, u, e);
    } else 3 !== F && 4 !== F || (D.nodeValue = D.nodeValue.split(u).join(e));
  }

  function t(D, u) {
    for (var e = u.length; -1 < --e;) {
      D.push(u[e]);
    }
  }

  function u(D, u, e) {
    for (var F; D && D !== u;) {
      if (F = D._next || D.nextSibling) return F.textContent.charAt(0) === e;
      D = D.parentNode || D._parent;
    }
  }

  function v(D) {
    var u,
        e,
        F = n(D.childNodes),
        t = F.length;

    for (u = 0; u < t; u++) {
      (e = F[u])._isSplit ? v(e) : (u && 3 === e.previousSibling.nodeType ? e.previousSibling.nodeValue += 3 === e.nodeType ? e.nodeValue : e.firstChild.nodeValue : 3 !== e.nodeType && D.insertBefore(e.firstChild, e), D.removeChild(e));
    }
  }

  function w(D, u) {
    return parseFloat(u[D]) || 0;
  }

  function x(D, e, F, C, i, n, E) {
    var r,
        l,
        a,
        p,
        d,
        h,
        B,
        f,
        A,
        c,
        g,
        x,
        y = k(D),
        b = w("paddingLeft", y),
        _ = -999,
        S = w("borderBottomWidth", y) + w("borderTopWidth", y),
        T = w("borderLeftWidth", y) + w("borderRightWidth", y),
        N = w("paddingTop", y) + w("paddingBottom", y),
        m = w("paddingLeft", y) + w("paddingRight", y),
        L = .2 * w("fontSize", y),
        W = y.textAlign,
        H = [],
        O = [],
        V = [],
        j = e.wordDelimiter || " ",
        M = e.tag ? e.tag : e.span ? "span" : "div",
        R = e.type || e.split || "chars,words,lines",
        z = i && ~R.indexOf("lines") ? [] : null,
        P = ~R.indexOf("words"),
        q = ~R.indexOf("chars"),
        G = o(e),
        I = e.linesClass,
        J = ~(I || "").indexOf("++"),
        K = [];

    for (J && (I = I.split("++").join("")), a = (l = D.getElementsByTagName("*")).length, d = [], r = 0; r < a; r++) {
      d[r] = l[r];
    }

    if (z || G) for (r = 0; r < a; r++) {
      ((h = (p = d[r]).parentNode === D) || G || q && !P) && (x = p.offsetTop, z && h && Math.abs(x - _) > L && ("BR" !== p.nodeName || 0 === r) && (B = [], z.push(B), _ = x), G && (p._x = p.offsetLeft, p._y = x, p._w = p.offsetWidth, p._h = p.offsetHeight), z && ((p._isSplit && h || !q && h || P && h || !P && p.parentNode.parentNode === D && !p.parentNode._isSplit) && (B.push(p), p._x -= b, u(p, D, j) && (p._wordEnd = !0)), "BR" === p.nodeName && (p.nextSibling && "BR" === p.nextSibling.nodeName || 0 === r) && z.push([])));
    }

    for (r = 0; r < a; r++) {
      h = (p = d[r]).parentNode === D, "BR" !== p.nodeName ? (G && (A = p.style, P || h || (p._x += p.parentNode._x, p._y += p.parentNode._y), A.left = p._x + "px", A.top = p._y + "px", A.position = "absolute", A.display = "block", A.width = p._w + 1 + "px", A.height = p._h + "px"), !P && q ? p._isSplit ? (p._next = p.nextSibling, p.parentNode.appendChild(p)) : p.parentNode._isSplit ? (p._parent = p.parentNode, !p.previousSibling && p.firstChild && (p.firstChild._isFirst = !0), p.nextSibling && " " === p.nextSibling.textContent && !p.nextSibling.nextSibling && K.push(p.nextSibling), p._next = p.nextSibling && p.nextSibling._isFirst ? null : p.nextSibling, p.parentNode.removeChild(p), d.splice(r--, 1), a--) : h || (x = !p.nextSibling && u(p.parentNode, D, j), p.parentNode._parent && p.parentNode._parent.appendChild(p), x && p.parentNode.appendChild(Q.createTextNode(" ")), "span" === M && (p.style.display = "inline"), H.push(p)) : p.parentNode._isSplit && !p._isSplit && "" !== p.innerHTML ? O.push(p) : q && !p._isSplit && ("span" === M && (p.style.display = "inline"), H.push(p))) : z || G ? (p.parentNode && p.parentNode.removeChild(p), d.splice(r--, 1), a--) : P || D.appendChild(p);
    }

    for (r = K.length; -1 < --r;) {
      K[r].parentNode.removeChild(K[r]);
    }

    if (z) {
      for (G && (c = Q.createElement(M), D.appendChild(c), g = c.offsetWidth + "px", x = c.offsetParent === D ? 0 : D.offsetLeft, D.removeChild(c)), A = D.style.cssText, D.style.cssText = "display:none;"; D.firstChild;) {
        D.removeChild(D.firstChild);
      }

      for (f = " " === j && (!G || !P && !q), r = 0; r < z.length; r++) {
        for (B = z[r], (c = Q.createElement(M)).style.cssText = "display:block;text-align:" + W + ";position:" + (G ? "absolute;" : "relative;"), I && (c.className = I + (J ? r + 1 : "")), V.push(c), a = B.length, l = 0; l < a; l++) {
          "BR" !== B[l].nodeName && (p = B[l], c.appendChild(p), f && p._wordEnd && c.appendChild(Q.createTextNode(" ")), G && (0 === l && (c.style.top = p._y + "px", c.style.left = b + x + "px"), p.style.top = "0px", x && (p.style.left = p._x - x + "px")));
        }

        0 === a ? c.innerHTML = "&nbsp;" : P || q || (v(c), s(c, String.fromCharCode(160), " ")), G && (c.style.width = g, c.style.height = p._h + "px"), D.appendChild(c);
      }

      D.style.cssText = A;
    }

    G && (E > D.clientHeight && (D.style.height = E - N + "px", D.clientHeight < E && (D.style.height = E + S + "px")), n > D.clientWidth && (D.style.width = n - m + "px", D.clientWidth < n && (D.style.width = n + T + "px"))), t(F, H), P && t(C, O), t(i, V);
  }

  function y(D, u, e, F) {
    var t,
        C,
        i,
        n,
        E,
        r,
        l,
        a,
        d = u.tag ? u.tag : u.span ? "span" : "div",
        h = ~(u.type || u.split || "chars,words,lines").indexOf("chars"),
        B = o(u),
        f = u.wordDelimiter || " ",
        A = " " !== f ? "" : B ? "&#173; " : " ",
        c = "</" + d + ">",
        g = 1,
        x = u.specialChars ? "function" == typeof u.specialChars ? u.specialChars : p : null,
        y = Q.createElement("div"),
        v = D.parentNode;

    for (v.insertBefore(y, D), y.textContent = D.nodeValue, v.removeChild(D), l = -1 !== (t = function getText(D) {
      var u = D.nodeType,
          e = "";

      if (1 === u || 9 === u || 11 === u) {
        if ("string" == typeof D.textContent) return D.textContent;

        for (D = D.firstChild; D; D = D.nextSibling) {
          e += getText(D);
        }
      } else if (3 === u || 4 === u) return D.nodeValue;

      return e;
    }(D = y)).indexOf("<"), !1 !== u.reduceWhiteSpace && (t = t.replace(S, " ").replace(_, "")), l && (t = t.split("<").join("{{LT}}")), E = t.length, C = (" " === t.charAt(0) ? A : "") + e(), i = 0; i < E; i++) {
      if (r = t.charAt(i), x && (a = x(t.substr(i), u.specialChars))) r = t.substr(i, a || 1), C += h && " " !== r ? F() + r + "</" + d + ">" : r, i += a - 1;else if (r === f && t.charAt(i - 1) !== f && i) {
        for (C += g ? c : "", g = 0; t.charAt(i + 1) === f;) {
          C += A, i++;
        }

        i === E - 1 ? C += A : ")" !== t.charAt(i + 1) && (C += A + e(), g = 1);
      } else "{" === r && "{{LT}}" === t.substr(i, 6) ? (C += h ? F() + "{{LT}}</" + d + ">" : "{{LT}}", i += 5) : 55296 <= r.charCodeAt(0) && r.charCodeAt(0) <= 56319 || 65024 <= t.charCodeAt(i + 1) && t.charCodeAt(i + 1) <= 65039 ? (n = ((t.substr(i, 12).split(b) || [])[1] || "").length || 2, C += h && " " !== r ? F() + t.substr(i, n) + "</" + d + ">" : t.substr(i, n), i += n - 1) : C += h && " " !== r ? F() + r + "</" + d + ">" : r;
    }

    D.outerHTML = C + (g ? c : ""), l && s(v, "{{LT}}", "<");
  }

  function z(D, u, e, F) {
    var t,
        C,
        i = n(D.childNodes),
        E = i.length,
        s = o(u);

    if (3 !== D.nodeType || 1 < E) {
      for (u.absolute = !1, t = 0; t < E; t++) {
        3 === (C = i[t]).nodeType && !/\S+/.test(C.nodeValue) || (s && 3 !== C.nodeType && "inline" === k(C).display && (C.style.display = "inline-block", C.style.position = "relative"), C._isSplit = !0, z(C, u, e, F));
      }

      return u.absolute = s, void (D._isSplit = !0);
    }

    y(D, u, e, F);
  }

  var Q,
      e,
      F,
      C,
      _ = /(?:\r|\n|\t\t)/g,
      S = /(?:\s\s+)/g,
      i = Array.isArray,
      E = [].slice,
      l = ((C = SplitText.prototype).split = function split(D) {
    this.isSplit && this.revert(), this.vars = D = D || this.vars, this._originals.length = this.chars.length = this.words.length = this.lines.length = 0;

    for (var u, e, F, t = this.elements.length, C = D.tag ? D.tag : D.span ? "span" : "div", i = r(D.wordsClass, C), n = r(D.charsClass, C); -1 < --t;) {
      F = this.elements[t], this._originals[t] = F.innerHTML, u = F.clientHeight, e = F.clientWidth, z(F, D, i, n), x(F, D, this.chars, this.words, this.lines, e, u);
    }

    return this.chars.reverse(), this.words.reverse(), this.lines.reverse(), this.isSplit = !0, this;
  }, C.revert = function revert() {
    var e = this._originals;
    if (!e) throw "revert() call wasn't scoped properly.";
    return this.elements.forEach(function (D, u) {
      return D.innerHTML = e[u];
    }), this.chars = [], this.words = [], this.lines = [], this.isSplit = !1, this;
  }, SplitText.create = function create(D, u) {
    return new SplitText(D, u);
  }, SplitText);

  function SplitText(D, u) {
    F || function _initCore() {
      Q = document, e = window, F = 1;
    }(), this.elements = n(D), this.chars = [], this.words = [], this.lines = [], this._originals = [], this.vars = u || {}, this.split(u);
  }

  l.version = "3.3.4", D.SplitText = l, D["default"] = l;

  if (typeof window === "undefined" || window !== D) {
    Object.defineProperty(D, "__esModule", {
      value: !0
    });
  } else {
    delete D["default"];
  }
});

/***/ }),

/***/ "./src/js/modules sync recursive \\.(js)$/":
/*!**************************************!*\
  !*** ./src/js/modules sync \.(js)$/ ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./_splitText.js": "./src/js/modules/_splitText.js",
	"./accordian.js": "./src/js/modules/accordian.js",
	"./animations.js": "./src/js/modules/animations.js",
	"./backButton.js": "./src/js/modules/backButton.js",
	"./featuredProducts.js": "./src/js/modules/featuredProducts.js",
	"./formsDemo.js": "./src/js/modules/formsDemo.js",
	"./formsEmail.js": "./src/js/modules/formsEmail.js",
	"./formsTrial.js": "./src/js/modules/formsTrial.js",
	"./googleMap.js": "./src/js/modules/googleMap.js",
	"./header.js": "./src/js/modules/header.js",
	"./helloworld.js": "./src/js/modules/helloworld.js",
	"./imageModal.js": "./src/js/modules/imageModal.js",
	"./introSlider.js": "./src/js/modules/introSlider.js",
	"./inview.js": "./src/js/modules/inview.js",
	"./logoColour.js": "./src/js/modules/logoColour.js",
	"./newsLightbox.js": "./src/js/modules/newsLightbox.js",
	"./paramStorage.js": "./src/js/modules/paramStorage.js",
	"./pinImage.js": "./src/js/modules/pinImage.js",
	"./pricing.js": "./src/js/modules/pricing.js",
	"./processInxdrForm.js": "./src/js/modules/processInxdrForm.js",
	"./productSidebar.js": "./src/js/modules/productSidebar.js",
	"./quicklinks.js": "./src/js/modules/quicklinks.js",
	"./scrollTo.js": "./src/js/modules/scrollTo.js",
	"./serviceTabs.js": "./src/js/modules/serviceTabs.js",
	"./statistic.js": "./src/js/modules/statistic.js",
	"./stickyLogo.js": "./src/js/modules/stickyLogo.js",
	"./testimonials.js": "./src/js/modules/testimonials.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./src/js/modules sync recursive \\.(js)$/";

/***/ }),

/***/ "./src/js/modules/_splitText.js":
/*!**************************************!*\
  !*** ./src/js/modules/_splitText.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return initSplitText; });
/* harmony import */ var gsap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! gsap */ "./node_modules/gsap/index.js");
/* harmony import */ var _libraries_SplitText_min_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../libraries/SplitText.min.js */ "./src/js/libraries/SplitText.min.js");
/* harmony import */ var _libraries_SplitText_min_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_libraries_SplitText_min_js__WEBPACK_IMPORTED_MODULE_1__);
function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }



gsap__WEBPACK_IMPORTED_MODULE_0__["default"].registerPlugin(_libraries_SplitText_min_js__WEBPACK_IMPORTED_MODULE_1___default.a);
function initSplitText() {
  var _this = this;

  var linesToSplit = document.querySelectorAll('[data-split-lines]');
  var lineSplit = new _libraries_SplitText_min_js__WEBPACK_IMPORTED_MODULE_1___default.a(linesToSplit, {
    type: 'lines',
    linesClass: 'line'
  });
  window.addEventListener('resize', function () {
    _newArrowCheck(this, _this);

    lineSplit.split();
  }.bind(this));
}

/***/ }),

/***/ "./src/js/modules/accordian.js":
/*!*************************************!*\
  !*** ./src/js/modules/accordian.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var gsap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! gsap */ "./node_modules/gsap/index.js");
var _this5 = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var Accordian = /*#__PURE__*/function () {
  function Accordian(el) {
    _classCallCheck(this, Accordian);

    this.el = el;
    this.items = Array.from(el.querySelectorAll('[data-accordian-item]'));
    this.titles = Array.from(el.querySelectorAll('[data-accordian-title]'));
    this.windowWidth = window.innerWidth;
    window.addEventListener('resize', this.onResize.bind(this));

    if (el.dataset.accordian === 'mobile') {
      if (window.innerWidth <= 750) this.setup();
    } else {
      this.setup();
    }
  }

  _createClass(Accordian, [{
    key: "setup",
    value: function setup() {
      var _this = this;

      this.itemClickListener = this.toggleItem.bind(this);
      document.fonts.ready.then(function () {
        var _this2 = this;

        _newArrowCheck(this, _this);

        this.items.forEach(function (el, i) {
          _newArrowCheck(this, _this2);

          el.style.height = this.titles[i].offsetHeight + 'px';
          el.addEventListener('click', this.itemClickListener);
        }.bind(this));
      }.bind(this));
    }
  }, {
    key: "toggleItem",
    value: function toggleItem(e) {
      var index = this.items.indexOf(e.currentTarget);

      if (e.currentTarget.classList.contains('js-open')) {
        gsap__WEBPACK_IMPORTED_MODULE_0__["default"].to(this.items[index], {
          duration: 0.4,
          height: this.titles[index].offsetHeight + 'px',
          ease: 'power1.inOut'
        });
        e.currentTarget.classList.remove('js-open');
      } else {
        gsap__WEBPACK_IMPORTED_MODULE_0__["default"].to(this.items[index], {
          duration: 0.4,
          height: 'auto',
          ease: 'power1.inOut'
        });
        e.currentTarget.classList.add('js-open');
      }
    }
  }, {
    key: "onResize",
    value: function onResize(e) {
      var _this3 = this;

      if (document.documentElement.clientWidth === this.windowWidth) return;

      if (this.el.dataset.accordian === 'mobile' && window.innerWidth <= 750) {
        this.setup();
      } else if (this.el.dataset.accordian === 'mobile') {
        this.destroy();
      } else {
        this.items.forEach(function (el, i) {
          _newArrowCheck(this, _this3);

          gsap__WEBPACK_IMPORTED_MODULE_0__["default"].to(el, {
            duration: 0.4,
            height: this.titles[i].offsetHeight + 'px',
            ease: 'power1.inOut'
          });
          el.classList.remove('open');
        }.bind(this));
      }
    }
  }, {
    key: "destroy",
    value: function destroy() {
      var _this4 = this;

      this.items.forEach(function (el) {
        _newArrowCheck(this, _this4);

        el.style.height = 'auto';
        el.removeEventListener('click', this.itemClickListener);
      }.bind(this));
    }
  }]);

  return Accordian;
}();

var initAccordian = function initAccordian() {
  var _this6 = this;

  _newArrowCheck(this, _this5);

  var accordians = document.querySelectorAll('[data-accordian]');
  if (!accordians) return;
  accordians.forEach(function (el) {
    _newArrowCheck(this, _this6);

    new Accordian(el);
  }.bind(this));
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (initAccordian);

/***/ }),

/***/ "./src/js/modules/animations.js":
/*!**************************************!*\
  !*** ./src/js/modules/animations.js ***!
  \**************************************/
/*! exports provided: default, heroAnim, clientsAnim, testimonialsAnim */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return animations; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "heroAnim", function() { return heroAnim; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "clientsAnim", function() { return clientsAnim; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "testimonialsAnim", function() { return testimonialsAnim; });
/* harmony import */ var gsap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! gsap */ "./node_modules/gsap/index.js");
var _this = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }


function animations() {}

var setupLogos = function setupLogos() {
  _newArrowCheck(this, _this);

  gsap__WEBPACK_IMPORTED_MODULE_0__["default"].set('.clientLogos__item img', {
    scale: 0
  });
}.bind(undefined);

setupLogos();
function heroAnim(el) {
  var _this2 = this;

  if (!el) return;
  var tl = gsap__WEBPACK_IMPORTED_MODULE_0__["default"].timeline({
    onComplete: function onComplete() {
      _newArrowCheck(this, _this2);

      el.classList.add('complete');
    }.bind(this)
  });

  if (window.innerWidth > 960) {
    tl.to(['.header__nav .menu-item', '.header__account'], {
      duration: 1,
      opacity: 1,
      y: '0%',
      stagger: 0.04,
      ease: 'power2.out'
    });
    tl.to(['.fixedLogo__logo'], {
      duration: 0.8,
      y: '0%',
      opacity: 1,
      stagger: 0.08,
      ease: 'power2.out'
    }, '-=1.2');
    tl.to('.hero__bg', {
      duration: 1.0,
      height: 'auto',
      ease: 'power2.out'
    }, '-=1.0');
  }
}
function clientsAnim(el) {
  var _this3 = this;

  if (!el) return;
  var lines = el.querySelectorAll('.line');
  var logos = Array.from(el.querySelectorAll('img'));
  var randomiseLogos = logos.sort(function () {
    _newArrowCheck(this, _this3);

    return Math.random() - 0.5;
  }.bind(this));
  var tl = gsap__WEBPACK_IMPORTED_MODULE_0__["default"].timeline({
    onComplete: function onComplete() {
      _newArrowCheck(this, _this3);

      el.classList.add('complete');
    }.bind(this)
  });
  tl.to(lines, {
    duration: 0.8,
    y: '0%',
    opacity: 1,
    stagger: 0.08,
    ease: 'power2.out'
  });
  tl.to(randomiseLogos, {
    duration: 1,
    scale: 1,
    stagger: 0.1,
    ease: 'back.out(1.4)'
  }, '-=0.7');
}
function testimonialsAnim(el) {
  if (!el) return;
  var items = el.querySelectorAll('.testimonials__item');
  gsap__WEBPACK_IMPORTED_MODULE_0__["default"].to(items, {
    duration: 1.6,
    delay: 0.4,
    y: '0%',
    opacity: 1,
    scale: 1,
    stagger: 0.08,
    ease: 'elastic.out(1.2, 1)'
  });
}

/***/ }),

/***/ "./src/js/modules/backButton.js":
/*!**************************************!*\
  !*** ./src/js/modules/backButton.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var _this = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }

var backButton = function backButton() {
  var _this2 = this;

  _newArrowCheck(this, _this);

  var button = document.querySelector('.introPost__back');
  if (!button) return;
  button.addEventListener('click', function (e) {
    _newArrowCheck(this, _this2);

    e.preventDefault();

    if (window.history.length > 1 && document.referrer.includes('encodian')) {
      history.back();
    } else {
      window.location.href = '/blog';
    }
  }.bind(this));
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (backButton);

/***/ }),

/***/ "./src/js/modules/featuredProducts.js":
/*!********************************************!*\
  !*** ./src/js/modules/featuredProducts.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var gsap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! gsap */ "./node_modules/gsap/index.js");
/* harmony import */ var flickity__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! flickity */ "./node_modules/flickity/js/index.js");
/* harmony import */ var flickity__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(flickity__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! gsap/ScrollTrigger */ "./node_modules/gsap/ScrollTrigger.js");
/* harmony import */ var _libraries_MorphSVGPlugin_min_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../libraries/MorphSVGPlugin.min.js */ "./src/js/libraries/MorphSVGPlugin.min.js");
/* harmony import */ var _libraries_MorphSVGPlugin_min_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_libraries_MorphSVGPlugin_min_js__WEBPACK_IMPORTED_MODULE_3__);
var _this5 = undefined;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }





gsap__WEBPACK_IMPORTED_MODULE_0__["default"].registerPlugin(gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_2__["ScrollTrigger"], _libraries_MorphSVGPlugin_min_js__WEBPACK_IMPORTED_MODULE_3__["MorphSVGPlugin"]);

var FeaturedProducts = /*#__PURE__*/function () {
  function FeaturedProducts(el) {
    _classCallCheck(this, FeaturedProducts);

    this.el = el;
    this.slides = el.querySelectorAll('.featuredProducts__slide');
    this.carousel = el.querySelector('.featuredProducts__carousel');
    this.productShape = el.querySelector('.product-shapes');
    this.productShapeMobile = el.querySelector('.product-shapes-mobile');
    this.paginationItems = el.querySelectorAll('.featuredProducts__paginationItem');
    this.sketches = el.querySelectorAll('.featuredProducts__sketch'); // Set the first slide as active.

    this.paginationItems[0].classList.add('active');
    this.sketches[0].classList.add('active');
    this.slides[0].classList.add('active');
    this.activeSlide = this.slides[0].dataset.shape;

    if (window.innerWidth >= 750) {
      this.pinSection();
    } else {
      this.mobileCarousel();
    }
  }

  _createClass(FeaturedProducts, [{
    key: "mobileCarousel",
    value: function mobileCarousel() {
      var _this = this;

      var carousel = new flickity__WEBPACK_IMPORTED_MODULE_1___default.a(this.carousel, {
        contain: false,
        pageDots: true,
        prevNextButtons: true,
        draggable: true,
        cellAlign: 'left'
      });
      carousel.on('change', function (index) {
        _newArrowCheck(this, _this);

        gsap__WEBPACK_IMPORTED_MODULE_0__["default"].to('.shapeM0', {
          morphSVG: '.shapeM' + index + '',
          duration: 0.6,
          ease: 'power3.inOut'
        });
        this.productShapeMobile.style.fill = this.slides[index].dataset.colour;
      }.bind(this));
    }
  }, {
    key: "pinSection",
    value: function pinSection() {
      var _this2 = this,
          _ScrollTrigger$create;

      this.lengthNumber = 400;
      gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_2__["ScrollTrigger"].create((_ScrollTrigger$create = {
        trigger: this.el,
        pin: true,
        anticipatePin: 1,
        start: 'top top-=35px',
        end: 'bottom bottom'
      }, _defineProperty(_ScrollTrigger$create, "end", this.lengthNumber * this.slides.length + 'px'), _defineProperty(_ScrollTrigger$create, "onUpdate", function onUpdate(_ref) {
        var _this3 = this;

        var progress = _ref.progress;

        _newArrowCheck(this, _this2);

        this.slides.forEach(function (el, i) {
          _newArrowCheck(this, _this3);

          if (progress >= 1 / this.slides.length * i && progress < 1 / this.slides.length * (i + 1)) {
            this.changeSlide(el);
          }
        }.bind(this));
      }.bind(this)), _ScrollTrigger$create));
    }
  }, {
    key: "changeSlide",
    value: function changeSlide(el) {
      var _this4 = this;

      if (el.dataset.shape === this.activeSlide) return;
      this.activeSlide = el.dataset.shape;

      if (window.innerWidth >= 750) {
        gsap__WEBPACK_IMPORTED_MODULE_0__["default"].to('.shape0', {
          morphSVG: '.shape' + el.dataset.shape + '',
          duration: 0.6,
          ease: 'power3.inOut'
        });
        this.productShape.style.fill = el.dataset.colour;
      } else {// gsap.to('#Vertr-M', { morphSVG: '#' + el.dataset.shape + '-M' , duration: 0.6, ease: 'power3.inOut' })
        // this.productShapeMobile.style.fill = el.dataset.colour
      }

      this.paginationItems.forEach(function (el) {
        _newArrowCheck(this, _this4);

        el.classList.remove('active');
      }.bind(this));
      this.sketches.forEach(function (el) {
        _newArrowCheck(this, _this4);

        el.classList.remove('active');
      }.bind(this));
      this.slides.forEach(function (el) {
        _newArrowCheck(this, _this4);

        el.classList.remove('active');
      }.bind(this));
      this.slides.forEach(function (slide, i) {
        _newArrowCheck(this, _this4);

        if (el === this.slides[i]) {
          this.paginationItems[i].classList.add('active');
          this.sketches[i].classList.add('active');
          el.classList.add('active');
        }
      }.bind(this));
    }
  }]);

  return FeaturedProducts;
}();

var initFeaturedProducts = function initFeaturedProducts() {
  _newArrowCheck(this, _this5);

  var section = document.querySelector('.featuredProducts');
  if (section) new FeaturedProducts(section);
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (initFeaturedProducts);

/***/ }),

/***/ "./src/js/modules/formsDemo.js":
/*!*************************************!*\
  !*** ./src/js/modules/formsDemo.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var formbouncerjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! formbouncerjs */ "./node_modules/formbouncerjs/dist/bouncer.polyfills.min.js");
/* harmony import */ var formbouncerjs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(formbouncerjs__WEBPACK_IMPORTED_MODULE_1__);
var _this = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }


 // Get values from the form and then remove the data attributes.

var form = document.querySelector('[data-demo]');
var api, restrictedDomains, subject, recipient, footer;

if (form) {
  api = form.dataset.api;
  footer = form.dataset.footer;
  subject = form.dataset.subject;
  recipient = form.dataset.recipient;
  restrictedDomains = form.dataset.restricted.split(', ');
  form.removeAttribute('data-api');
  form.removeAttribute('data-footer');
  form.removeAttribute('data-subject');
  form.removeAttribute('data-recipient');
  form.removeAttribute('data-restricted');
}

var formHasSubmitted = false;
var validation = new formbouncerjs__WEBPACK_IMPORTED_MODULE_1___default.a('[data-validatedemo]', {
  disableSubmit: true,
  customValidations: {
    validateDomain: function validateDomain(field) {
      _newArrowCheck(this, _this);

      if (field.type == 'email') {
        var domain = '';
        var inputtedDomain = field.value.split("@");
        if (inputtedDomain[1]) domain = inputtedDomain[1].split('.')[0];
        if (restrictedDomains.indexOf(domain) >= 0) return true;else return false;
      }
    }.bind(undefined)
  },
  messages: {
    validateDomain: 'Please provide a business and not a personal email address.'
  }
});

var processDemoForm = function processDemoForm(e) {
  var _this2 = this;

  _newArrowCheck(this, _this);

  e.preventDefault();
  var form = e.target,
      formData = new FormData(form),
      success = document.querySelector('.contactForm__success'),
      errorEl = document.querySelector('.contactForm__error');
  formData.append('nonce', WP.nonce);
  formData.append('action', 'process_form');
  formData.append('footer', footer);
  formData.append('subject', subject);
  formData.append('recipient', recipient);
  document.addEventListener('bouncerFormInvalid', function (event) {
    _newArrowCheck(this, _this2);

    console.log(event.detail.errors);
  }.bind(this), false);
  document.addEventListener('bouncerFormValid', function (e) {
    var _this3 = this;

    _newArrowCheck(this, _this2);

    if (formHasSubmitted) return false;
    formHasSubmitted = true; // Send submission to email via AJAX

    axios__WEBPACK_IMPORTED_MODULE_0___default()({
      method: 'POST',
      url: WP.ajax,
      data: formData
    }).then(function (response) {
      console.log('data: ' + response.data);
      console.log('status: ' + response.status);
    }, function (error) {
      _newArrowCheck(this, _this3);

      console.log(error);
    }.bind(this)); // Create object with form data and post to API.

    var postData = {};
    var allFields = form.querySelectorAll('input'); // Add all of the forms fields to the postData object.

    allFields.forEach(function (field) {
      _newArrowCheck(this, _this3);

      if (field.name != 'privacy_check' || field.name != 'MailListApproved') {
        postData[field.name] = field.value;
      }

      if (field.name == 'MailListApproved' && field.value == 'on') {
        postData['MailListApproved'] = true;
      } else if (field.name == 'MailListApproved' && field.value == 'off') {
        postData['MailListApproved'] = false;
      }
    }.bind(this));
    var options = {
      method: 'POST',
      body: JSON.stringify(postData),
      mode: 'cors',
      headers: {
        'Access-Control-Allow-Credentials': 'true',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
      }
    };
    fetch(api, options).then(function (res) {
      _newArrowCheck(this, _this3);

      form.style.display = 'none';
      success.style.display = 'block';
    }.bind(this))["catch"](function (err) {
      _newArrowCheck(this, _this3);

      console.log("Error with message: ".concat(err));
      form.style.display = 'none';
      errorEl.style.display = 'block';
    }.bind(this));
    form.classList.add('submitted');
  }.bind(this), false);
}.bind(undefined);

var initProcessDemoForm = function initProcessDemoForm() {
  _newArrowCheck(this, _this);

  var forms = document.querySelectorAll('[data-demo]');

  for (var i = 0; i < forms.length; i++) {
    forms[i].addEventListener('submit', processDemoForm, false);
  }
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (initProcessDemoForm);

/***/ }),

/***/ "./src/js/modules/formsEmail.js":
/*!**************************************!*\
  !*** ./src/js/modules/formsEmail.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var formbouncerjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! formbouncerjs */ "./node_modules/formbouncerjs/dist/bouncer.polyfills.min.js");
/* harmony import */ var formbouncerjs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(formbouncerjs__WEBPACK_IMPORTED_MODULE_1__);
var _this = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }


 // Get values from the form and then remove the data attributes.

var form = document.querySelector('[data-process]');
var restrictedDomains, subject, recipient, footer;

if (form) {
  footer = form.dataset.footer;
  subject = form.dataset.subject;
  recipient = form.dataset.recipient;
  restrictedDomains = form.dataset.restricted.split(', ');
  form.removeAttribute('data-footer');
  form.removeAttribute('data-subject');
  form.removeAttribute('data-recipient');
  form.removeAttribute('data-restricted');
}

var formHasSubmitted = false;
var validation = new formbouncerjs__WEBPACK_IMPORTED_MODULE_1___default.a('[data-validate]', {
  disableSubmit: true,
  customValidations: {
    validateDomain: function validateDomain(field) {
      _newArrowCheck(this, _this);

      if (field.type == 'email') {
        var domain = '';
        var inputtedDomain = field.value.split("@");
        if (inputtedDomain[1]) domain = inputtedDomain[1].split('.')[0];
        if (restrictedDomains.indexOf(domain) >= 0) return true;else return false;
      }
    }.bind(undefined)
  },
  messages: {
    validateDomain: 'Please provide a business and not a personal email address.'
  }
});

var processForm = function processForm(e) {
  _newArrowCheck(this, _this);

  e.preventDefault();
  var form = e.target;
  var formData = new FormData(form);
  formData.append('action', 'process_form');
  formData.append('nonce', WP.nonce);
  formData.append('form_id', form.getAttribute('data-form-id'));
  formData.append('footer', footer);
  formData.append('subject', subject);
  formData.append('recipient', recipient); // for (var value of formData.values()) {
  //   console.log(value);
  // }

  document.addEventListener('bouncerFormInvalid', function (event) {
    console.log(event.detail.errors);
  }, false);
  /** If validation passes, fire AJAX submission */

  document.addEventListener('bouncerFormValid', function (e) {
    var _this2 = this;

    if (formHasSubmitted) return false;
    formHasSubmitted = true;
    form.classList.add('submitted');
    axios__WEBPACK_IMPORTED_MODULE_0___default()({
      method: 'POST',
      url: WP.ajax,
      data: formData
    }).then(function (response) {
      console.log('data: ' + response.data);
      console.log('status: ' + response.status);
      form.innerHTML = '<div class="success-msg">' + form.getAttribute('data-success') + '</div>';
    }, function (error) {
      _newArrowCheck(this, _this2);

      console.log(error);
    }.bind(this));
  }, false);
}.bind(undefined);

var initProcessForm = function initProcessForm() {
  _newArrowCheck(this, _this);

  var forms = document.querySelectorAll('[data-process]');

  for (var i = 0; i < forms.length; i++) {
    forms[i].addEventListener('submit', processForm, false);
  } // END loop

}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (initProcessForm);

/***/ }),

/***/ "./src/js/modules/formsTrial.js":
/*!**************************************!*\
  !*** ./src/js/modules/formsTrial.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var formbouncerjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! formbouncerjs */ "./node_modules/formbouncerjs/dist/bouncer.polyfills.min.js");
/* harmony import */ var formbouncerjs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(formbouncerjs__WEBPACK_IMPORTED_MODULE_1__);
var _this = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }


 // Get values from the form and then remove the data attributes.

var form = document.querySelector('[data-processtrial]');
var api, type, restrictedDomains;

if (form) {
  api = form.dataset.api;
  type = form.dataset.type;
  restrictedDomains = form.dataset.restricted.split(', ');
  form.removeAttribute('data-api');
  form.removeAttribute('data-type');
  form.removeAttribute('data-restricted');
}

var formHasSubmitted = false;
var validation = new formbouncerjs__WEBPACK_IMPORTED_MODULE_1___default.a('[data-validatetrial]', {
  disableSubmit: true,
  customValidations: {
    validateDomain: function validateDomain(field) {
      _newArrowCheck(this, _this);

      if (field.type == 'email') {
        var domain = '';
        var inputtedDomain = field.value.split("@");
        if (inputtedDomain[1]) domain = inputtedDomain[1].split('.')[0];
        if (restrictedDomains.indexOf(domain) >= 0) return true;else return false;
      }
    }.bind(undefined)
  },
  messages: {
    validateDomain: 'Please provide a business and not a personal email address.'
  }
});

var processTrialForm = function processTrialForm(e) {
  var _this2 = this;

  _newArrowCheck(this, _this);

  e.preventDefault();
  var form = e.target,
      success = document.querySelector('.contactForm__success'),
      errorEl = document.querySelector('.contactForm__error'),
      existsEl = document.querySelector('.contactForm__exists'),
      responseEl = document.querySelector('.contactForm__response'),
      apiKeyEl = document.querySelector('#TrialApiKey');
  document.addEventListener('bouncerFormInvalid', function (event) {
    _newArrowCheck(this, _this2);

    return console.log(event.detail.errors);
  }.bind(this), false);
  document.addEventListener('bouncerFormValid', function (e) {
    var _this3 = this;

    if (formHasSubmitted) return false;
    formHasSubmitted = true;
    var postData = {
      Type: type
    };
    var allFields = form.querySelectorAll('input'); // Add all of the forms fields to the postData object.

    allFields.forEach(function (field) {
      _newArrowCheck(this, _this3);

      if (field.name != 'privacy_check' || field.name != 'MailListApproved') {
        postData[field.name] = field.value;
      }

      if (field.name == 'MailListApproved' && field.value == 'on') {
        postData['MailListApproved'] = true;
      } else if (field.name == 'MailListApproved' && field.value == 'off') {
        postData['MailListApproved'] = false;
      }
    }.bind(this));
    console.log(postData);
    var options = {
      method: 'POST',
      body: JSON.stringify(postData),
      mode: 'cors',
      headers: {
        'Access-Control-Allow-Credentials': 'true',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
      }
    };
    fetch(api, options).then(function (res) {
      _newArrowCheck(this, _this3);

      console.log('Code: ' + res.status);
      if (res.status == 500) throw Error(response.statusText);else return res.text();
    }.bind(this)).then(function (res) {
      _newArrowCheck(this, _this3);

      console.log(res);
      console.log('ApiKey: ' + res.ApiKey);
      console.log('Error: ' + res.Error);

      if (res == 'Tenant already exists') {
        form.style.display = 'none';
        existsEl.style.display = 'block';
      } else {
        var _response = JSON.parse(res);

        console.log(_response);

        if (_response.Error != null) {
          document.querySelector('.contactForm__response h3').innerHTML = _response.Error;
          form.style.display = 'none';
          responseEl.style.display = 'block';
        } else {
          apiKeyEl.innerHTML = _response.ApiKey;
          form.style.display = 'none';
          success.style.display = 'block';
        }
      }
    }.bind(this))["catch"](function (err) {
      _newArrowCheck(this, _this3);

      console.log("Error with message: ".concat(err));
      form.style.display = 'none';
      errorEl.style.display = 'block';
    }.bind(this));
    form.classList.add('submitted');
  }, false); // Copy the API to the clipboard

  apiKeyEl.addEventListener('click', function () {
    var _this4 = this;

    _newArrowCheck(this, _this2);

    var apiKey = apiKeyEl.innerHTML,
        temporaryField = document.createElement('input');
    temporaryField.value = apiKey;
    document.querySelector('body').appendChild(temporaryField);
    temporaryField.select();
    document.execCommand('copy');
    temporaryField.remove();
    document.querySelector('.tooltip').innerHTML = 'Copied!';
    setTimeout(function () {
      _newArrowCheck(this, _this4);

      return document.querySelector('.tooltip').innerHTML = 'Click to Copy';
    }.bind(this), 3000);
  }.bind(this));
}.bind(undefined);

var initProcessTrialForm = function initProcessTrialForm() {
  var _this5 = this;

  _newArrowCheck(this, _this);

  var forms = document.querySelectorAll('[data-processtrial]');
  forms.forEach(function (el) {
    _newArrowCheck(this, _this5);

    return el.addEventListener('submit', processTrialForm, false);
  }.bind(this));
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (initProcessTrialForm);

/***/ }),

/***/ "./src/js/modules/googleMap.js":
/*!*************************************!*\
  !*** ./src/js/modules/googleMap.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _googlemaps_js_api_loader__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @googlemaps/js-api-loader */ "./node_modules/@googlemaps/js-api-loader/dist/index.esm.js");
var _this = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }



var initGoogleMaps = function initGoogleMaps() {
  var _this2 = this;

  _newArrowCheck(this, _this);

  if (!document.querySelector('#map')) return;
  var loader = new _googlemaps_js_api_loader__WEBPACK_IMPORTED_MODULE_0__["Loader"]({
    apiKey: 'AIzaSyCis2bi6bO3oQGb21qjXS4gk1LaMicboI4',
    version: 'weekly'
  });
  loader.load().then(function () {
    _newArrowCheck(this, _this2);

    map = new google.maps.Map(document.getElementById('map'), {
      center: {
        lat: 52.372082,
        lng: -1.797967
      },
      zoom: 14,
      disableDefaultUI: true
    });
  }.bind(this));
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (initGoogleMaps);

/***/ }),

/***/ "./src/js/modules/header.js":
/*!**********************************!*\
  !*** ./src/js/modules/header.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var _this2 = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Header = /*#__PURE__*/function () {
  function Header(el) {
    var _this = this;

    _classCallCheck(this, Header);

    this.el = el;
    this.nav = el.querySelector('.header__nav');
    this.menuButton = el.querySelector('.header__menuButton');
    this.productsItem = el.querySelector('.menu-item-37 a');
    this.industryItem = el.querySelector('.menu-item-2513 a');
    this.aboutItem = el.querySelector('.menu-item-52 a');
    this.productsItem.classList.add('menu-accordian');
    this.industryItem.classList.add('menu-accordian');
    this.aboutItem.classList.add('menu-accordian');
    this.menuButton.addEventListener('click', this.toggleNav.bind(this));
    this.productsItem.addEventListener('click', this.toggleSubmenu.bind(this));
    this.industryItem.addEventListener('click', this.toggleSubmenu.bind(this));
    this.aboutItem.addEventListener('click', this.toggleSubmenu.bind(this));
    if (window.innerWidth < 960) setTimeout(function () {
      _newArrowCheck(this, _this);

      return this.productsItem.parentNode.classList.add('toggle');
    }.bind(this), 500);
    window.addEventListener('resize', this.calcSubmenuHeight.bind(this));
    this.calcSubmenuHeight();

    window.onscroll = function (e) {
      if (this.oldScroll > this.scrollY || window.pageYOffset === 0) {
        el.classList.remove('hidden');
      } else {
        el.classList.add('hidden');
      }

      if (this.scrollY >= 200) {
        el.classList.add('white');
      } else {
        el.classList.remove('white');
      }

      this.oldScroll = this.scrollY;
    };
  }

  _createClass(Header, [{
    key: "toggleNav",
    value: function toggleNav(e) {
      e.preventDefault();
      this.nav.classList.toggle('js-open');
      this.menuButton.classList.toggle('js-open');
      document.querySelector('html').classList.toggle('no-scroll');
    }
  }, {
    key: "toggleSubmenu",
    value: function toggleSubmenu(e) {
      e.preventDefault();
      e.currentTarget.parentNode.classList.toggle('toggle');
    }
  }, {
    key: "calcSubmenuHeight",
    value: function calcSubmenuHeight(e) {
      var productMenu = document.querySelector('.menu-item-37');
      productMenu.style.height = 'auto'; // productMenu.style.height = productMenu.offsetHeight + 'px'
    }
  }]);

  return Header;
}();

var initHeader = function initHeader() {
  _newArrowCheck(this, _this2);

  new Header(document.querySelector('.header'));
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (initHeader);

/***/ }),

/***/ "./src/js/modules/helloworld.js":
/*!**************************************!*\
  !*** ./src/js/modules/helloworld.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var _this = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }

var helloWorld = function helloWorld() {
  _newArrowCheck(this, _this);

  console.log('%cBGN\n' + '%cBuilt by BGN Agency:\n' + '%chttps://www.bgn.agency/', 'font-family: Georgia;font-size: 3em;font-weight: bold;color: #f8bdd7;', 'font-size: 1em;color: #f8bdd7;', 'font-size: 1em;');
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (helloWorld);

/***/ }),

/***/ "./src/js/modules/imageModal.js":
/*!**************************************!*\
  !*** ./src/js/modules/imageModal.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var _this3 = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var ImageModal = /*#__PURE__*/function () {
  function ImageModal(el) {
    var _this = this;

    _classCallCheck(this, ImageModal);

    this.items = Array.from(el.querySelectorAll('.js-modal-item'));
    this.modals = document.querySelectorAll('.featuresAlt__modal');
    this.close = document.querySelectorAll('.featuresAlt__close');

    if (window.innerWidth > 750) {
      this.items.forEach(function (el) {
        _newArrowCheck(this, _this);

        el.addEventListener('click', this.openModal.bind(this));
      }.bind(this));
      this.close.forEach(function (el) {
        _newArrowCheck(this, _this);

        el.addEventListener('click', this.closeModal.bind(this));
      }.bind(this));
    }
  }

  _createClass(ImageModal, [{
    key: "openModal",
    value: function openModal(e) {
      e.preventDefault();
      var index = this.items.indexOf(e.currentTarget);
      this.modals[index].classList.add('js-open');
      document.querySelector('html').classList.add('no-scroll');
    }
  }, {
    key: "closeModal",
    value: function closeModal(e) {
      var _this2 = this;

      e.preventDefault();
      this.modals.forEach(function (el) {
        _newArrowCheck(this, _this2);

        el.classList.remove('js-open');
      }.bind(this));
      document.querySelector('html').classList.remove('no-scroll');
    }
  }]);

  return ImageModal;
}();

var initImageModal = function initImageModal() {
  _newArrowCheck(this, _this3);

  var features = document.querySelector('[data-features]');
  if (!features) return;
  new ImageModal(features);
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (initImageModal);

/***/ }),

/***/ "./src/js/modules/introSlider.js":
/*!***************************************!*\
  !*** ./src/js/modules/introSlider.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var _this2 = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Flickity = __webpack_require__(/*! flickity-fade */ "./node_modules/flickity-fade/flickity-fade.js");

var IntroSlider = /*#__PURE__*/function () {
  function IntroSlider(el) {
    _classCallCheck(this, IntroSlider);

    this.el = el;
    this.arrowNext = el.parentNode.querySelector('.next');
    this.arrowPrev = el.parentNode.querySelector('.previous');
    this.dots = el.parentNode.querySelectorAll('.introSlider__dot');
    this.arrowNext.addEventListener('click', this.nextSlide.bind(this));
    this.arrowPrev.addEventListener('click', this.prevSlide.bind(this));
    this.dots[0].classList.add('active');
    this.initSlider();
  }

  _createClass(IntroSlider, [{
    key: "initSlider",
    value: function initSlider() {
      this.flickity = new Flickity(this.el, {
        contain: true,
        pageDots: false,
        prevNextButtons: false,
        draggable: true,
        fade: true,
        adaptiveHeight: true
      });
      this.flickity.on('change', this.slideChanged.bind(this));
    }
  }, {
    key: "slideChanged",
    value: function slideChanged(e) {
      var _this = this;

      if (this.flickity.selectedIndex === 0) {
        this.arrowPrev.disabled = true;
        this.arrowNext.disabled = false;
      } else if (this.flickity.selectedIndex === this.flickity.slides.length - 1) {
        this.arrowNext.disabled = true;
        this.arrowPrev.disabled = false;
      } else {
        this.arrowPrev.disabled = false;
        this.arrowNext.disabled = false;
      }

      this.dots.forEach(function (el) {
        _newArrowCheck(this, _this);

        el.classList.remove('active');
      }.bind(this));
      this.dots[this.flickity.selectedIndex].classList.add('active');
    }
  }, {
    key: "nextSlide",
    value: function nextSlide(e) {
      e.preventDefault();
      this.flickity.next();
    }
  }, {
    key: "prevSlide",
    value: function prevSlide(e) {
      e.preventDefault();
      this.flickity.previous();
    }
  }]);

  return IntroSlider;
}();

var initIntroSlider = function initIntroSlider() {
  _newArrowCheck(this, _this2);

  var carousels = document.querySelector('.introSlider__slider');
  if (carousels) new IntroSlider(carousels);
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (initIntroSlider);

/***/ }),

/***/ "./src/js/modules/inview.js":
/*!**********************************!*\
  !*** ./src/js/modules/inview.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var gsap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! gsap */ "./node_modules/gsap/index.js");
/* harmony import */ var gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! gsap/ScrollTrigger */ "./node_modules/gsap/ScrollTrigger.js");
/* harmony import */ var _animations_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./animations.js */ "./src/js/modules/animations.js");
var _this = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }




gsap__WEBPACK_IMPORTED_MODULE_0__["default"].registerPlugin(gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__["ScrollTrigger"]);

var isInview = function isInview() {
  var _this2 = this;

  _newArrowCheck(this, _this);

  var sections = document.querySelectorAll('[data-inview]');
  if (!sections) return;
  sections.forEach(function (section) {
    var _this3 = this;

    _newArrowCheck(this, _this2);

    gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__["ScrollTrigger"].create({
      trigger: section,
      start: section.dataset.inview === 'start' ? 'top 95%' : 'top 80%',
      end: 'bottom bottom',
      once: true,
      toggleClass: 'is-inview',
      // markers: true,
      onEnter: function onEnter() {
        _newArrowCheck(this, _this3);

        switch (section.dataset.inview) {
          case 'hero':
            _animations_js__WEBPACK_IMPORTED_MODULE_2__["heroAnim"](section);
            break;

          case 'client':
            _animations_js__WEBPACK_IMPORTED_MODULE_2__["clientsAnim"](section);
            break;

          case 'testimonials':
            _animations_js__WEBPACK_IMPORTED_MODULE_2__["testimonialsAnim"](section);
            break;
        }
      }.bind(this)
    });
  }.bind(this));
  document.fonts.ready.then(function () {
    _newArrowCheck(this, _this2);

    gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__["ScrollTrigger"].refresh();
  }.bind(this));
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (isInview);

/***/ }),

/***/ "./src/js/modules/logoColour.js":
/*!**************************************!*\
  !*** ./src/js/modules/logoColour.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var gsap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! gsap */ "./node_modules/gsap/index.js");
/* harmony import */ var gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! gsap/ScrollTrigger */ "./node_modules/gsap/ScrollTrigger.js");
var _this = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }



gsap__WEBPACK_IMPORTED_MODULE_0__["default"].registerPlugin(gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__["ScrollTrigger"]);

var logoColour = function logoColour() {
  var _this2 = this;

  _newArrowCheck(this, _this);

  var sectionsLight = document.querySelectorAll('[data-light]');
  var logo = document.querySelector('.fixedLogo');

  if (sectionsLight) {
    sectionsLight.forEach(function (el) {
      var _this3 = this;

      _newArrowCheck(this, _this2);

      gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__["ScrollTrigger"].create({
        trigger: el,
        start: 'top top+=100px',
        bottom: 'bottom bottom',
        onToggle: function onToggle(_ref) {
          var progress = _ref.progress,
              direction = _ref.direction,
              isActive = _ref.isActive;

          _newArrowCheck(this, _this3);

          if (isActive) logo.classList.add('logo-yellow');else logo.classList.remove('logo-yellow');
        }.bind(this)
      });
    }.bind(this));
  }
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (logoColour);

/***/ }),

/***/ "./src/js/modules/newsLightbox.js":
/*!****************************************!*\
  !*** ./src/js/modules/newsLightbox.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return newsLightbox; });
/* harmony import */ var lightense_images__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lightense-images */ "./node_modules/lightense-images/lightense.js");
/* harmony import */ var lightense_images__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lightense_images__WEBPACK_IMPORTED_MODULE_0__);

function newsLightbox() {
  window.addEventListener('load', function () {
    lightense_images__WEBPACK_IMPORTED_MODULE_0___default()('.single-post .post img');
  }, false);
}

/***/ }),

/***/ "./src/js/modules/paramStorage.js":
/*!****************************************!*\
  !*** ./src/js/modules/paramStorage.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return paramStorage; });
function getParam(p) {
  var match = RegExp('[?&]' + p + '=([^&]*)').exec(window.location.search);
  return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function getExpiryRecord(value) {
  var expiryPeriod = 90 * 24 * 60 * 60 * 1000; // 90 day expiry in milliseconds

  var expiryDate = new Date().getTime() + expiryPeriod;
  return {
    value: value,
    expiryDate: expiryDate
  };
}

function addParams() {
  var gclidParam = getParam('gclid');
  var gclidFormFields = ['gclid_field'];
  var gclidRecord = null;
  var currGclidFormField;
  var utmSourceParam = getParam('utm_source');
  var utcSourceFormFields = ['utm_source'];
  var utmSourceRecord = null;
  var currUtmSourceFormField;
  var utmMediumParam = getParam('utm_medium');
  var utcMediumFormFields = ['utm_medium'];
  var utmMediumRecord = null;
  var currUtmMediumFormField;
  var utmCampaignParam = getParam('utm_campaign');
  var utcCampaignFormFields = ['utm_campaign'];
  var utmCampaignRecord = null;
  var currUtmCampaignFormField;
  var utmTermParam = getParam('utm_term');
  var utcTermFormFields = ['utm_term'];
  var utmTermRecord = null;
  var currUtmTermFormField;
  var gclsrcParam = getParam('gclsrc');
  var isGclsrcValid = !gclsrcParam || gclsrcParam.indexOf('aw') !== -1;
  gclidFormFields.forEach(function (field) {
    if (document.getElementById(field)) {
      currGclidFormField = document.getElementById(field);
    }
  });
  utcSourceFormFields.forEach(function (field) {
    if (document.getElementById(field)) {
      currUtmSourceFormField = document.getElementById(field);
    }
  });
  utcMediumFormFields.forEach(function (field) {
    if (document.getElementById(field)) {
      currUtmMediumFormField = document.getElementById(field);
    }
  });
  utcCampaignFormFields.forEach(function (field) {
    if (document.getElementById(field)) {
      currUtmCampaignFormField = document.getElementById(field);
    }
  });
  utcTermFormFields.forEach(function (field) {
    if (document.getElementById(field)) {
      currUtmTermFormField = document.getElementById(field);
    }
  });

  if (gclidParam && isGclsrcValid) {
    gclidRecord = getExpiryRecord(gclidParam);
    localStorage.setItem('gclid', JSON.stringify(gclidRecord));
  }

  if (utmSourceParam) {
    utmSourceRecord = getExpiryRecord(utmSourceParam);
    localStorage.setItem('utm_source', JSON.stringify(utmSourceRecord));
  }

  if (utmMediumParam) {
    utmMediumRecord = getExpiryRecord(utmMediumParam);
    localStorage.setItem('utm_medium', JSON.stringify(utmMediumRecord));
  }

  if (utmCampaignParam) {
    utmCampaignRecord = getExpiryRecord(utmCampaignParam);
    localStorage.setItem('utm_campaign', JSON.stringify(utmCampaignRecord));
  }

  if (utmTermParam) {
    utmTermRecord = getExpiryRecord(utmTermParam);
    localStorage.setItem('utm_term', JSON.stringify(utmTermRecord));
  }

  var gclid = gclidRecord || JSON.parse(localStorage.getItem('gclid'));
  var utm_source = utmSourceRecord || JSON.parse(localStorage.getItem('utm_source'));
  var utm_medium = utmMediumRecord || JSON.parse(localStorage.getItem('utm_medium'));
  var utm_campaign = utmCampaignRecord || JSON.parse(localStorage.getItem('utm_campaign'));
  var utm_term = utmTermRecord || JSON.parse(localStorage.getItem('utm_term'));
  var isGclidValid = gclid && new Date().getTime() < gclid.expiryDate;
  var isUtmSourceValid = utm_source && new Date().getTime() < utm_source.expiryDate;
  var isUtmMediumValid = utm_medium && new Date().getTime() < utm_medium.expiryDate;
  var isUtmCampaignValid = utm_campaign && new Date().getTime() < utm_campaign.expiryDate;
  var isUtmTermValid = utm_term && new Date().getTime() < utm_term.expiryDate;

  if (currGclidFormField && isGclidValid) {
    currGclidFormField.value = gclid.value;
  }

  if (currUtmSourceFormField && isUtmSourceValid) {
    currUtmSourceFormField.value = utm_source.value;
  }

  if (currUtmMediumFormField && isUtmMediumValid) {
    currUtmMediumFormField.value = utm_medium.value;
  }

  if (currUtmCampaignFormField && isUtmCampaignValid) {
    currUtmCampaignFormField.value = utm_campaign.value;
  }

  if (currUtmTermFormField && isUtmTermValid) {
    currUtmTermFormField.value = utm_term.value;
  }
}

window.addEventListener('load', addParams);
function paramStorage() {}

/***/ }),

/***/ "./src/js/modules/pinImage.js":
/*!************************************!*\
  !*** ./src/js/modules/pinImage.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var gsap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! gsap */ "./node_modules/gsap/index.js");
/* harmony import */ var gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! gsap/ScrollTrigger */ "./node_modules/gsap/ScrollTrigger.js");
var _this = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }



gsap__WEBPACK_IMPORTED_MODULE_0__["gsap"].registerPlugin(gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__["ScrollTrigger"]);

var pinImage = function pinImage() {
  _newArrowCheck(this, _this);

  var image = document.querySelector('.solutions__imageHolder');
  if (!image) return;

  if (window.innerWidth > 960) {
    gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__["ScrollTrigger"].create({
      trigger: image,
      pin: true,
      anticipatePin: 1,
      pinSpacing: false,
      start: 'top 50px',
      end: 'bottom bottom-=250px',
      endTrigger: '.end-trigger' // markers: true

    });
  }
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (pinImage);

/***/ }),

/***/ "./src/js/modules/pricing.js":
/*!***********************************!*\
  !*** ./src/js/modules/pricing.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var flickity__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! flickity */ "./node_modules/flickity/js/index.js");
/* harmony import */ var flickity__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(flickity__WEBPACK_IMPORTED_MODULE_0__);
var _this3 = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var Pricing = /*#__PURE__*/function () {
  function Pricing(el) {
    _classCallCheck(this, Pricing);

    this.slider = el.querySelector('.pricing__slider');
    this.prices = el.querySelectorAll('.pricing__prices');
    this.dots = el.querySelectorAll('.pricing__dot');
    this.select = el.querySelector('.pricing__column select');
    this.select.addEventListener('change', this.updateCurrency.bind(this));
    this.arrowNext = el.parentNode.querySelector('.next');
    this.arrowPrev = el.parentNode.querySelector('.previous');
    if (el.classList.contains('pricing--slider')) this.initSlider();
  }

  _createClass(Pricing, [{
    key: "initSlider",
    value: function initSlider() {
      this.flickity = new flickity__WEBPACK_IMPORTED_MODULE_0___default.a(this.slider, {
        contain: false,
        pageDots: false,
        prevNextButtons: false,
        draggable: true,
        cellAlign: 'left'
      });
      this.dots[0].classList.add('active');
      this.arrowNext.addEventListener('click', this.nextSlide.bind(this));
      this.arrowPrev.addEventListener('click', this.prevSlide.bind(this));
      this.flickity.on('change', this.slideChanged.bind(this));
    }
  }, {
    key: "slideChanged",
    value: function slideChanged(e) {
      var _this = this;

      if (this.flickity.selectedIndex === 0) {
        this.arrowPrev.disabled = true;
      } else if (this.flickity.selectedIndex === this.flickity.slides.length - 1) {
        this.arrowNext.disabled = true;
      } else {
        this.arrowPrev.disabled = false;
        this.arrowNext.disabled = false;
      }

      this.dots.forEach(function (el) {
        _newArrowCheck(this, _this);

        el.classList.remove('active');
      }.bind(this));
      this.dots[this.flickity.selectedIndex].classList.add('active');
    }
  }, {
    key: "updateCurrency",
    value: function updateCurrency(e) {
      var _this2 = this;

      this.prices.forEach(function (el) {
        _newArrowCheck(this, _this2);

        el.setAttribute('data-currency', e.target.value);
      }.bind(this));
    }
  }, {
    key: "nextSlide",
    value: function nextSlide(e) {
      e.preventDefault();
      this.flickity.next();
    }
  }, {
    key: "prevSlide",
    value: function prevSlide(e) {
      e.preventDefault();
      this.flickity.previous();
    }
  }]);

  return Pricing;
}();

var initPricing = function initPricing() {
  _newArrowCheck(this, _this3);

  var section = document.querySelector('.pricing');
  if (section) new Pricing(section);
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (initPricing);

/***/ }),

/***/ "./src/js/modules/processInxdrForm.js":
/*!********************************************!*\
  !*** ./src/js/modules/processInxdrForm.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var formbouncerjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! formbouncerjs */ "./node_modules/formbouncerjs/dist/bouncer.polyfills.min.js");
/* harmony import */ var formbouncerjs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(formbouncerjs__WEBPACK_IMPORTED_MODULE_1__);
var _this = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }



var formHasSubmitted = false;
var bouncer = new formbouncerjs__WEBPACK_IMPORTED_MODULE_1___default.a('[data-validate-indxr]', {
  disableSubmit: true,
  customValidations: {
    validateDomain: function validateDomain(field) {
      _newArrowCheck(this, _this);

      if (field.id === 'email_address') {
        // Check if inputted domain contains any of those listed in rejectedDomains.
        var rejectedDomains = ['yahoo', 'gmail', 'aol', 'hotmail', 'outlook', 'mac', 'live', 'googlemail', 'icloud'];
        var inputtedDomain = field.value.split("@");
        var domain = '';

        if (inputtedDomain[1]) {
          domain = inputtedDomain[1].split('.')[0];
        }

        if (rejectedDomains.indexOf(domain) >= 0) {
          return true;
        } else {
          return false;
        }
      }
    }.bind(undefined)
  },
  messages: {
    validateDomain: 'Please provide a business and not a personal email address.'
  }
});

var processIndxrForm = function processIndxrForm(e) {
  var _this3 = this;

  _newArrowCheck(this, _this);

  e.preventDefault();
  var form = e.target;
  var endPoint = 'https://functionsadminpaalwaysoncore5.azurewebsites.net/api/WebTrialRequestIndxr?code=zBBeQmPlYCR05/aR5HsEGn2jMU9wIyv0tFy3URFSi73C5sX0Ny0v9w==';
  var success = document.querySelector('.contactForm__success');
  var errorEl = document.querySelector('.contactForm__error');
  var existsEl = document.querySelector('.contactForm__exists');
  var responseEl = document.querySelector('.contactForm__response');
  var apiKeyElem = document.querySelector('#TrialApiKey');
  document.addEventListener('bouncerFormInvalid', function (event) {
    console.log(event.detail.errors);
  }, false);
  /** If validation passes, fire AJAX submission */

  document.addEventListener('bouncerFormValid', function (e) {
    var _this2 = this;

    if (formHasSubmitted) return false;
    formHasSubmitted = true;
    var firstName = document.getElementById("first_name").value;
    var lastName = document.getElementById("last_name").value;
    var companyName = document.getElementById("company").value;
    var jobRole = document.getElementById("role").value;
    var contactNumber = document.getElementById("phone_number").value;
    var emailAddress = document.getElementById("email_address").value;
    var mailListApproved = document.getElementById("mail_check").checked;
    var gclid = document.getElementById("gclid_field").value;
    var source = document.getElementById("utm_source").value;
    var medium = document.getElementById("utm_medium").value;
    var campaign = document.getElementById("utm_campaign").value;
    var term = document.getElementById("utm_term").value;
    var requestData = {
      Type: 'IndxrApp',
      FirstName: firstName,
      LastName: lastName,
      CompanyName: companyName,
      JobRole: jobRole,
      ContactNumber: contactNumber,
      EmailAddress: emailAddress,
      MailListApproved: mailListApproved,
      utmCampaign: campaign,
      utmSource: source,
      utmMedium: medium,
      utmTerm: term,
      gclid: gclid
    };
    var options = {
      method: 'POST',
      body: JSON.stringify(requestData),
      mode: 'cors',
      headers: {
        'Access-Control-Allow-Credentials': 'true',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
      }
    };
    fetch(endPoint, options).then(function (res) {
      _newArrowCheck(this, _this2);

      return res.json();
    }.bind(this)).then(function (res) {
      _newArrowCheck(this, _this2);

      console.log(res);
      console.log('ApiKey: ' + res.ApiKey);
      console.log('Error: ' + res.Error);

      if (res.Error != null) {
        document.querySelector('.contactForm__response h3').innerHTML = res.Error;
        form.style.display = 'none';
        responseEl.style.display = 'block';
      } else {
        apiKeyElem.innerHTML = res.ApiKey;
        form.style.display = 'none';
        success.style.display = 'block';
      }
    }.bind(this))["catch"](function (err) {
      _newArrowCheck(this, _this2);

      console.log(err);
      console.log("Error with message: ".concat(err));
      form.style.display = 'none';
      errorEl.style.display = 'block';
    }.bind(this));
    form.classList.add('submitted');
  }, false); // Copy the API to the clipboard

  apiKeyElem.addEventListener('click', function () {
    var _this4 = this;

    _newArrowCheck(this, _this3);

    var apiKey = apiKeyElem.innerHTML;
    var tempInput = document.createElement('input');
    tempInput.value = apiKey;
    document.querySelector('body').appendChild(tempInput);
    tempInput.select();
    document.execCommand('copy');
    tempInput.remove();
    document.querySelector('.tooltip').innerHTML = 'Copied!';
    setTimeout(function () {
      _newArrowCheck(this, _this4);

      document.querySelector('.tooltip').innerHTML = 'Click to Copy';
    }.bind(this), 3000);
  }.bind(this));
}.bind(undefined);

var initProcessIndxrForm = function initProcessIndxrForm() {
  var _this5 = this;

  _newArrowCheck(this, _this);

  var forms = document.querySelectorAll('[data-process-indxr]');
  forms.forEach(function (el) {
    _newArrowCheck(this, _this5);

    el.addEventListener('submit', processIndxrForm, false);
  }.bind(this));
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (initProcessIndxrForm);

/***/ }),

/***/ "./src/js/modules/productSidebar.js":
/*!******************************************!*\
  !*** ./src/js/modules/productSidebar.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var gsap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! gsap */ "./node_modules/gsap/index.js");
/* harmony import */ var gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! gsap/ScrollTrigger */ "./node_modules/gsap/ScrollTrigger.js");
/* harmony import */ var gsap_ScrollToPlugin__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! gsap/ScrollToPlugin */ "./node_modules/gsap/ScrollToPlugin.js");
var _this = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }




gsap__WEBPACK_IMPORTED_MODULE_0__["gsap"].registerPlugin(gsap_ScrollToPlugin__WEBPACK_IMPORTED_MODULE_2__["ScrollToPlugin"], gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__["ScrollTrigger"]);

var productSidebar = function productSidebar() {
  var _this2 = this;

  _newArrowCheck(this, _this);

  var sidebar = document.querySelectorAll('.sidebar__content');
  var sidebarButton = document.querySelectorAll('.sidebar__button');
  var fixedButton = document.querySelector('.fixedBtn');
  var url = window.location.search;
  var urlParams = new URLSearchParams(url);

  if (urlParams.has('form')) {
    setTimeout(function () {
      _newArrowCheck(this, _this2);

      gsap__WEBPACK_IMPORTED_MODULE_0__["gsap"].to(window, {
        scrollTo: {
          y: document.querySelector('#form'),
          ease: 'power1.inOut'
        }
      });
    }.bind(this), 1500);
  }

  if (fixedButton) {
    fixedButton.addEventListener('click', function (e) {
      _newArrowCheck(this, _this2);

      e.preventDefault();
      var section = document.querySelector('#' + e.currentTarget.dataset.to);
      gsap__WEBPACK_IMPORTED_MODULE_0__["gsap"].to(window, {
        scrollTo: {
          y: section,
          ease: 'power1.inOut'
        }
      });
    }.bind(this));
  }

  if (!sidebar || window.innerWidth < 960) return;
  sidebar.forEach(function (el) {
    _newArrowCheck(this, _this2);

    gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__["ScrollTrigger"].create({
      trigger: el,
      pin: true,
      anticipatePin: 1,
      pinSpacing: false,
      start: 'top 150px',
      end: 'bottom bottom-=258px',
      endTrigger: '.end-trigger' // markers: true

    });
  }.bind(this));
  sidebarButton.forEach(function (el) {
    var _this3 = this;

    _newArrowCheck(this, _this2);

    el.addEventListener('click', function (e) {
      _newArrowCheck(this, _this3);

      e.preventDefault();
      var section = document.querySelector('#' + e.currentTarget.dataset.to);
      gsap__WEBPACK_IMPORTED_MODULE_0__["gsap"].to(window, {
        scrollTo: {
          y: section,
          ease: 'power1.inOut'
        }
      });
    }.bind(this));
  }.bind(this));
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (productSidebar);

/***/ }),

/***/ "./src/js/modules/quicklinks.js":
/*!**************************************!*\
  !*** ./src/js/modules/quicklinks.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var gsap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! gsap */ "./node_modules/gsap/index.js");
/* harmony import */ var gsap_ScrollToPlugin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! gsap/ScrollToPlugin */ "./node_modules/gsap/ScrollToPlugin.js");
/* harmony import */ var gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! gsap/ScrollTrigger */ "./node_modules/gsap/ScrollTrigger.js");
var _this6 = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }




gsap__WEBPACK_IMPORTED_MODULE_0__["gsap"].registerPlugin(gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_2__["ScrollTrigger"], gsap_ScrollToPlugin__WEBPACK_IMPORTED_MODULE_1__["ScrollToPlugin"]);

var Quicklinks = /*#__PURE__*/function () {
  function Quicklinks(el) {
    var _this = this;

    _classCallCheck(this, Quicklinks);

    this.el = el;
    this.list = el.querySelector('.quicklinks__list');
    this.links = el.querySelectorAll('[data-quicklink]');
    this.sections = document.querySelectorAll('[data-section]');
    this.links.forEach(function (el) {
      _newArrowCheck(this, _this);

      el.addEventListener('click', this.scrollToSection.bind(this));
    }.bind(this));
    this.pinLinks();
    this.activeStates();
    window.addEventListener('wheel', this.moveQuicklinks.bind(this));
  }

  _createClass(Quicklinks, [{
    key: "pinLinks",
    value: function pinLinks() {// ScrollTrigger.create({
      //   trigger: this.el,
      //   pin: true,
      //   anticipatePin: 1,
      //   pinSpacing: false,
      //   start: 'top 25px',
      //   end: 'bottom bottom-=150px',
      //   endTrigger: '.faqs'
      // })
    }
  }, {
    key: "activeStates",
    value: function activeStates() {
      var _this2 = this;

      this.sections.forEach(function (el) {
        var _this3 = this;

        _newArrowCheck(this, _this2);

        gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_2__["ScrollTrigger"].create({
          trigger: el,
          start: 'top top',
          end: 'bottom bottom',
          // markers: true,
          onEnter: function onEnter() {
            var _this4 = this;

            _newArrowCheck(this, _this3);

            this.links.forEach(function (el) {
              _newArrowCheck(this, _this4);

              el.classList.remove('active');
            }.bind(this));
            document.querySelector('[data-quicklink="' + el.dataset.section + '"]').classList.add('active');
          }.bind(this),
          onEnterBack: function onEnterBack() {
            var _this5 = this;

            _newArrowCheck(this, _this3);

            this.links.forEach(function (el) {
              _newArrowCheck(this, _this5);

              el.classList.remove('active');
            }.bind(this));
            document.querySelector('[data-quicklink="' + el.dataset.section + '"]').classList.add('active');
          }.bind(this)
        });
      }.bind(this));
    }
  }, {
    key: "scrollToSection",
    value: function scrollToSection(e) {
      var section = document.querySelector('[data-section="' + e.currentTarget.dataset.quicklink + '"]');
      gsap__WEBPACK_IMPORTED_MODULE_0__["gsap"].to(window, {
        scrollTo: {
          y: section,
          offsetY: -10,
          ease: 'power1.inOut'
        }
      });
    }
  }, {
    key: "moveQuicklinks",
    value: function moveQuicklinks(e) {// if (e.deltaY > 0 && window.innerWidth > 960) {
      //   this.list.classList.remove('nav-revealed')
      // } else {
      //   this.list.classList.add('nav-revealed')
      // }
    }
  }]);

  return Quicklinks;
}();

var initQuicklinks = function initQuicklinks() {
  _newArrowCheck(this, _this6);

  var quicklinks = document.querySelector('[data-quicklinks]');
  if (!quicklinks) return;
  new Quicklinks(quicklinks);
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (initQuicklinks);

/***/ }),

/***/ "./src/js/modules/scrollTo.js":
/*!************************************!*\
  !*** ./src/js/modules/scrollTo.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var gsap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! gsap */ "./node_modules/gsap/index.js");
/* harmony import */ var gsap_ScrollToPlugin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! gsap/ScrollToPlugin */ "./node_modules/gsap/ScrollToPlugin.js");
var _this = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }



gsap__WEBPACK_IMPORTED_MODULE_0__["default"].registerPlugin(gsap_ScrollToPlugin__WEBPACK_IMPORTED_MODULE_1__["ScrollToPlugin"]);

var scrollToSection = function scrollToSection() {
  var _this2 = this;

  _newArrowCheck(this, _this);

  var trigger = document.querySelectorAll('[data-to]');
  trigger.forEach(function (el) {
    var _this3 = this;

    _newArrowCheck(this, _this2);

    el.addEventListener('click', function (e) {
      _newArrowCheck(this, _this3);

      e.preventDefault();
      gsap__WEBPACK_IMPORTED_MODULE_0__["default"].to(window, {
        duration: 1,
        scrollTo: '#' + el.dataset.to
      });
    }.bind(this));
  }.bind(this));
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (scrollToSection);

/***/ }),

/***/ "./src/js/modules/serviceTabs.js":
/*!***************************************!*\
  !*** ./src/js/modules/serviceTabs.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var gsap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! gsap */ "./node_modules/gsap/index.js");
var _this4 = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var ServiceTabs = /*#__PURE__*/function () {
  function ServiceTabs(el) {
    var _this = this;

    _classCallCheck(this, ServiceTabs);

    this.slideHeight = 0;
    this.tabs = el.querySelectorAll('.serviceTabs__tab');
    this.slides = el.querySelectorAll('.serviceTabs__slide');
    this.slideHolder = el.querySelector('.serviceTabs__slides');
    this.titles = Array.from(el.querySelectorAll('.serviceTabs__title'));
    this.tabs[0].classList.add('active');
    this.slides[0].classList.add('active');
    this.tabs.forEach(function (el) {
      _newArrowCheck(this, _this);

      el.addEventListener('click', this.changeTab.bind(this));
    }.bind(this)); // window.addEventListener('resize', this.resize.bind(this))

    if (window.innerWidth < 960) {
      this.setupMobile();
    }
  }

  _createClass(ServiceTabs, [{
    key: "setupMobile",
    value: function setupMobile() {
      var _this2 = this;

      this.titles.forEach(function (el, i) {
        _newArrowCheck(this, _this2);

        this.slides[i].style.height = el.offsetHeight + 'px';
        el.addEventListener('click', this.openSlide.bind(this));
      }.bind(this));
    }
  }, {
    key: "changeTab",
    value: function changeTab(e) {
      var _this3 = this;

      e.preventDefault();
      this.tabs.forEach(function (el) {
        _newArrowCheck(this, _this3);

        el.classList.remove('active');
      }.bind(this));
      this.slides.forEach(function (el) {
        _newArrowCheck(this, _this3);

        el.classList.remove('active');
      }.bind(this));
      e.currentTarget.classList.add('active');
      this.tabs.forEach(function (el, i) {
        _newArrowCheck(this, _this3);

        if (el === e.currentTarget) {
          this.slides[i].classList.add('active');
          this.slideHeight = this.slides[i].offsetHeight;
        }
      }.bind(this));
      gsap__WEBPACK_IMPORTED_MODULE_0__["default"].to(this.slideHolder, {
        duration: 0.3,
        height: this.slideHeight + 'px'
      });
    }
  }, {
    key: "openSlide",
    value: function openSlide(e) {
      var index = this.titles.indexOf(e.currentTarget);

      if (e.currentTarget.classList.contains('open')) {
        gsap__WEBPACK_IMPORTED_MODULE_0__["default"].to(this.slides[index], {
          duration: 0.4,
          height: e.currentTarget.offsetHeight + 'px',
          ease: 'power1.inOut'
        });
        e.currentTarget.classList.remove('open');
      } else {
        gsap__WEBPACK_IMPORTED_MODULE_0__["default"].to(this.slides[index], {
          duration: 0.4,
          height: 'auto',
          ease: 'power1.inOut'
        });
        e.currentTarget.classList.add('open');
      }
    } // resize() {
    //   if (window.innerWidth < 960) {
    //     this.titles.forEach((el, i) => {
    //       this.slides[i].style.height = el.offsetHeight + 'px'
    //     })
    //   } else {
    //     gsap.to(this.slides, { duration: 0.1, height: 'auto', ease: 'power1.inOut' })
    //   }
    // }

  }]);

  return ServiceTabs;
}();

var initServiceTabs = function initServiceTabs() {
  _newArrowCheck(this, _this4);

  var section = document.querySelector('.serviceTabs');
  if (section) new ServiceTabs(section);
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (initServiceTabs);

/***/ }),

/***/ "./src/js/modules/statistic.js":
/*!*************************************!*\
  !*** ./src/js/modules/statistic.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var gsap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! gsap */ "./node_modules/gsap/index.js");
/* harmony import */ var gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! gsap/ScrollTrigger */ "./node_modules/gsap/ScrollTrigger.js");
/* harmony import */ var countup_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! countup.js */ "./node_modules/countup.js/dist/countUp.min.js");
var _this = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }




gsap__WEBPACK_IMPORTED_MODULE_0__["default"].registerPlugin(gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__["ScrollTrigger"]);

var statistic = function statistic() {
  var _this2 = this;

  _newArrowCheck(this, _this);

  var statistic = document.querySelector('.productIntro__statistic span');
  if (!statistic) return;
  var count = new countup_js__WEBPACK_IMPORTED_MODULE_2__["CountUp"](statistic, statistic.dataset.count);
  gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__["ScrollTrigger"].create({
    trigger: '.productIntro__statistic',
    start: 'top 75%',
    end: 'top 75%',
    toggleClass: 'is-inview',
    once: true,
    // markers: true,
    onEnter: function onEnter() {
      _newArrowCheck(this, _this2);

      count.start();
    }.bind(this)
  });
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (statistic);

/***/ }),

/***/ "./src/js/modules/stickyLogo.js":
/*!**************************************!*\
  !*** ./src/js/modules/stickyLogo.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var gsap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! gsap */ "./node_modules/gsap/index.js");
/* harmony import */ var gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! gsap/ScrollTrigger */ "./node_modules/gsap/ScrollTrigger.js");
var _this = undefined;

function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }



gsap__WEBPACK_IMPORTED_MODULE_0__["gsap"].registerPlugin(gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__["ScrollTrigger"]);

var stickyLogo = function stickyLogo() {
  _newArrowCheck(this, _this);

  var logo = document.querySelector('.fixedLogo');
  if (!logo) return;
  gsap_ScrollTrigger__WEBPACK_IMPORTED_MODULE_1__["ScrollTrigger"].create({
    trigger: logo,
    pin: true,
    anticipatePin: 1,
    pinSpacing: false,
    start: 'top top',
    end: 'bottom bottom',
    endTrigger: '.bottom' // markers: true

  });
}.bind(undefined);

/* harmony default export */ __webpack_exports__["default"] = (stickyLogo);

/***/ }),

/***/ "./src/js/modules/testimonials.js":
/*!****************************************!*\
  !*** ./src/js/modules/testimonials.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return initTestimonialCarousel; });
/* harmony import */ var flickity__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! flickity */ "./node_modules/flickity/js/index.js");
/* harmony import */ var flickity__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(flickity__WEBPACK_IMPORTED_MODULE_0__);
function _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError("Cannot instantiate an arrow function"); } }


function initTestimonialCarousel() {
  var _this = this;

  var carousels = document.querySelectorAll('[data-testimonials]');
  if (!carousels) return;
  carousels.forEach(function (el) {
    _newArrowCheck(this, _this);

    var flickity = new flickity__WEBPACK_IMPORTED_MODULE_0___default.a(el, {
      contain: true,
      pageDots: false,
      prevNextButtons: true,
      imagesLoaded: true,
      draggable: true,
      cellAlign: 'left'
    });
  }.bind(this));
}

/***/ }),

/***/ "./src/scss/main.scss":
/*!****************************!*\
  !*** ./src/scss/main.scss ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

},[["./src/js/index.js","manifest","vendors"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvanMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL2xpYnJhcmllcy9Nb3JwaFNWR1BsdWdpbi5taW4uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL2xpYnJhcmllcy9TcGxpdFRleHQubWluLmpzIiwid2VicGFjazovLy8uL3NyYy9qcy9tb2R1bGVzIHN5bmMgXFwuKGpzKSQvIiwid2VicGFjazovLy8uL3NyYy9qcy9tb2R1bGVzL19zcGxpdFRleHQuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL21vZHVsZXMvYWNjb3JkaWFuLmpzIiwid2VicGFjazovLy8uL3NyYy9qcy9tb2R1bGVzL2FuaW1hdGlvbnMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL21vZHVsZXMvYmFja0J1dHRvbi5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvbW9kdWxlcy9mZWF0dXJlZFByb2R1Y3RzLmpzIiwid2VicGFjazovLy8uL3NyYy9qcy9tb2R1bGVzL2Zvcm1zRGVtby5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvbW9kdWxlcy9mb3Jtc0VtYWlsLmpzIiwid2VicGFjazovLy8uL3NyYy9qcy9tb2R1bGVzL2Zvcm1zVHJpYWwuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL21vZHVsZXMvZ29vZ2xlTWFwLmpzIiwid2VicGFjazovLy8uL3NyYy9qcy9tb2R1bGVzL2hlYWRlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvbW9kdWxlcy9oZWxsb3dvcmxkLmpzIiwid2VicGFjazovLy8uL3NyYy9qcy9tb2R1bGVzL2ltYWdlTW9kYWwuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL21vZHVsZXMvaW50cm9TbGlkZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL21vZHVsZXMvaW52aWV3LmpzIiwid2VicGFjazovLy8uL3NyYy9qcy9tb2R1bGVzL2xvZ29Db2xvdXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL21vZHVsZXMvbmV3c0xpZ2h0Ym94LmpzIiwid2VicGFjazovLy8uL3NyYy9qcy9tb2R1bGVzL3BhcmFtU3RvcmFnZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvbW9kdWxlcy9waW5JbWFnZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvbW9kdWxlcy9wcmljaW5nLmpzIiwid2VicGFjazovLy8uL3NyYy9qcy9tb2R1bGVzL3Byb2Nlc3NJbnhkckZvcm0uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL21vZHVsZXMvcHJvZHVjdFNpZGViYXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL21vZHVsZXMvcXVpY2tsaW5rcy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvbW9kdWxlcy9zY3JvbGxUby5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvbW9kdWxlcy9zZXJ2aWNlVGFicy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvbW9kdWxlcy9zdGF0aXN0aWMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL21vZHVsZXMvc3RpY2t5TG9nby5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvbW9kdWxlcy90ZXN0aW1vbmlhbHMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Njc3MvbWFpbi5zY3NzIl0sIm5hbWVzIjpbInJlcXVpcmVkTW9kdWxlcyIsInJlcXVpcmUiLCJrZXlzIiwibWFwIiwia2V5IiwidCIsImUiLCJleHBvcnRzIiwibW9kdWxlIiwiZGVmaW5lIiwibSIsIk0iLCJBIiwiYiIsIm4iLCJEIiwiTWF0aCIsIlBJIiwiRSIsInNpbiIsImsiLCJjb3MiLCJaIiwiYWJzIiwiJCIsInNxcnQiLCJoIiwiX2lzTnVtYmVyIiwicyIsIl9yb3VuZCIsInJvdW5kIiwicmV2ZXJzZVNlZ21lbnQiLCJyIiwicmV2ZXJzZSIsImxlbmd0aCIsInJldmVyc2VkIiwieiIsInJlY3QiLCJjaXJjbGUiLCJlbGxpcHNlIiwibGluZSIsImNvbnZlcnRUb1BhdGgiLCJvIiwiaSIsImEiLCJsIiwiZyIsImMiLCJwIiwidSIsImYiLCJkIiwiXyIsIlAiLCJ2IiwieSIsInciLCJ4IiwiVCIsInRhZ05hbWUiLCJ0b0xvd2VyQ2FzZSIsImdldEJCb3giLCJfY3JlYXRlUGF0aCIsImRvY3VtZW50IiwiY3JlYXRlRWxlbWVudE5TIiwic2xpY2UiLCJjYWxsIiwiYXR0cmlidXRlcyIsIm5vZGVOYW1lIiwiaW5kZXhPZiIsInNldEF0dHJpYnV0ZU5TIiwibm9kZVZhbHVlIiwiX2F0dHJUb09iaiIsInNwbGl0IiwiZ2V0QXR0cmlidXRlIiwicngiLCJyeSIsIndpZHRoIiwiaGVpZ2h0Iiwiam9pbiIsImN4IiwiY3kiLCJ4MSIsInkxIiwieDIiLCJ5MiIsIm1hdGNoIiwic2hpZnQiLCJzZXRBdHRyaWJ1dGUiLCJyYXdQYXRoVG9TdHJpbmciLCJfZ3NSYXdQYXRoIiwic3RyaW5nVG9SYXdQYXRoIiwicGFyZW50Tm9kZSIsImluc2VydEJlZm9yZSIsInJlbW92ZUNoaWxkIiwiYXJjVG9TZWdtZW50IiwiUyIsIk4iLCJSIiwiTyIsIkwiLCJZIiwiQyIsImFjb3MiLCJGIiwiaXNOYU4iLCJYIiwiSSIsImNlaWwiLCJWIiwiaiIsIkciLCJVIiwicSIsIkgiLCJCIiwicHVzaCIsInRjIiwicmVwbGFjZSIsImNvbnNvbGUiLCJsb2ciLCJ0b1VwcGVyQ2FzZSIsImNsb3NlZCIsInN1YnN0ciIsImNoYXJBdCIsInBvcCIsInRvdGFsUG9pbnRzIiwid2luZG93IiwiZ3NhcCIsInJlZ2lzdGVyUGx1Z2luIiwid2FybiIsImNlbnRlclgiLCJjZW50ZXJZIiwic2l6ZSIsImxlZnQiLCJ0b3AiLCJRIiwiVyIsIm1pbiIsInNwbGljZSIsImZsb29yIiwic29ydCIsInNoYXBlSW5kZXgiLCJwYXJzZUZsb2F0IiwidG9GaXhlZCIsImFhIiwiYmEiLCJfb2Zmc2V0UG9pbnRzIiwicGFyc2VJbnQiLCJkYSIsImlzU21vb3RoIiwic21vb3RoRGF0YSIsImVhIiwidHJpbSIsImhhIiwiX29yaWdpbiIsIl9lT3JpZ2luIiwiX3Nob3J0QW5nbGUiLCJjYSIsIl9hbmNob3JQVCIsIl9uZXh0Iiwic2EiLCJzbCIsImNsIiwiaWEiLCJwbHVnaW5zIiwibW9ycGhTVkciLCJ1dGlscyIsInRvQXJyYXkiLCJwcm90b3R5cGUiLCJfdHdlZW5Sb3RhdGlvbiIsImF0YW4yIiwiSiIsInZlcnNpb24iLCJuYW1lIiwicmVnaXN0ZXIiLCJpbml0Iiwic2hhcGUiLCJub2RlVHlwZSIsImdldENvbXB1dGVkU3R5bGUiLCJmaWxsIiwiZmlsbFJ1bGUiLCJvcmlnaW4iLCJwcm9wIiwiX3BhcnNlU2hhcGUiLCJ0ZXN0IiwiZ2V0QXR0cmlidXRlTlMiLCJwb2ludHMiLCJkZWZhdWx0TWFwIiwiX3Byb3AiLCJfcmVuZGVyIiwicmVuZGVyIiwiZGVmYXVsdFJlbmRlciIsIl9hcHBseSIsInVwZGF0ZVRhcmdldCIsImRlZmF1bHRVcGRhdGVUYXJnZXQiLCJfcm5kIiwicG93IiwicHJlY2lzaW9uIiwiX3R3ZWVuIiwiX3RhcmdldCIsInByZWNvbXBpbGUiLCJ0eXBlIiwiZGVmYXVsdFR5cGUiLCJzbW9vdGhUb2xlcmFuY2UiLCJfcmF3UGF0aCIsIl9jb250cm9sUFQiLCJsMXMiLCJsMWMiLCJsMnMiLCJsMmMiLCJhZGQiLCJfcHJvcHMiLCJlbmQiLCJlbmRQcm9wIiwiX3B0Iiwia2lsbCIsImdldFJhd1BhdGgiLCJxdWVyeVNlbGVjdG9yIiwiX2dzUGF0aCIsIl9kaXJ0eSIsInBhdGhGaWx0ZXIiLCJfcGF0aEZpbHRlciIsInBvaW50c0ZpbHRlciIsImdldFRvdGFsU2l6ZSIsImVxdWFsaXplU2VnbWVudFF1YW50aXR5IiwiY29udmVydFRvUGF0aCQxIiwiTW9ycGhTVkdQbHVnaW4iLCJPYmplY3QiLCJkZWZpbmVQcm9wZXJ0eSIsInZhbHVlIiwicXVlcnlTZWxlY3RvckFsbCIsInBvc2l0aW9uIiwiYWJzb2x1dGUiLCJmaXJzdENoaWxkIiwibmV4dFNpYmxpbmciLCJ0ZXh0Q29udGVudCIsIl9wYXJlbnQiLCJjaGlsZE5vZGVzIiwiX2lzU3BsaXQiLCJwcmV2aW91c1NpYmxpbmciLCJ0ZXh0QWxpZ24iLCJ3b3JkRGVsaW1pdGVyIiwidGFnIiwic3BhbiIsImxpbmVzQ2xhc3MiLCJLIiwiZ2V0RWxlbWVudHNCeVRhZ05hbWUiLCJvZmZzZXRUb3AiLCJfeCIsIm9mZnNldExlZnQiLCJfeSIsIl93Iiwib2Zmc2V0V2lkdGgiLCJfaCIsIm9mZnNldEhlaWdodCIsIl93b3JkRW5kIiwic3R5bGUiLCJkaXNwbGF5IiwiYXBwZW5kQ2hpbGQiLCJfaXNGaXJzdCIsImNyZWF0ZVRleHROb2RlIiwiaW5uZXJIVE1MIiwiY3JlYXRlRWxlbWVudCIsIm9mZnNldFBhcmVudCIsImNzc1RleHQiLCJjbGFzc05hbWUiLCJTdHJpbmciLCJmcm9tQ2hhckNvZGUiLCJjbGllbnRIZWlnaHQiLCJjbGllbnRXaWR0aCIsInNwZWNpYWxDaGFycyIsImdldFRleHQiLCJyZWR1Y2VXaGl0ZVNwYWNlIiwiY2hhckNvZGVBdCIsIm91dGVySFRNTCIsIkFycmF5IiwiaXNBcnJheSIsIlNwbGl0VGV4dCIsImlzU3BsaXQiLCJyZXZlcnQiLCJ2YXJzIiwiX29yaWdpbmFscyIsImNoYXJzIiwid29yZHMiLCJsaW5lcyIsImVsZW1lbnRzIiwid29yZHNDbGFzcyIsImNoYXJzQ2xhc3MiLCJmb3JFYWNoIiwiY3JlYXRlIiwiX2luaXRDb3JlIiwiaW5pdFNwbGl0VGV4dCIsImxpbmVzVG9TcGxpdCIsImxpbmVTcGxpdCIsImFkZEV2ZW50TGlzdGVuZXIiLCJBY2NvcmRpYW4iLCJlbCIsIml0ZW1zIiwiZnJvbSIsInRpdGxlcyIsIndpbmRvd1dpZHRoIiwiaW5uZXJXaWR0aCIsIm9uUmVzaXplIiwiYmluZCIsImRhdGFzZXQiLCJhY2NvcmRpYW4iLCJzZXR1cCIsIml0ZW1DbGlja0xpc3RlbmVyIiwidG9nZ2xlSXRlbSIsImZvbnRzIiwicmVhZHkiLCJ0aGVuIiwiaW5kZXgiLCJjdXJyZW50VGFyZ2V0IiwiY2xhc3NMaXN0IiwiY29udGFpbnMiLCJ0byIsImR1cmF0aW9uIiwiZWFzZSIsInJlbW92ZSIsImRvY3VtZW50RWxlbWVudCIsImRlc3Ryb3kiLCJyZW1vdmVFdmVudExpc3RlbmVyIiwiaW5pdEFjY29yZGlhbiIsImFjY29yZGlhbnMiLCJhbmltYXRpb25zIiwic2V0dXBMb2dvcyIsInNldCIsInNjYWxlIiwiaGVyb0FuaW0iLCJ0bCIsInRpbWVsaW5lIiwib25Db21wbGV0ZSIsIm9wYWNpdHkiLCJzdGFnZ2VyIiwiY2xpZW50c0FuaW0iLCJsb2dvcyIsInJhbmRvbWlzZUxvZ29zIiwicmFuZG9tIiwidGVzdGltb25pYWxzQW5pbSIsImRlbGF5IiwiYmFja0J1dHRvbiIsImJ1dHRvbiIsInByZXZlbnREZWZhdWx0IiwiaGlzdG9yeSIsInJlZmVycmVyIiwiaW5jbHVkZXMiLCJiYWNrIiwibG9jYXRpb24iLCJocmVmIiwiU2Nyb2xsVHJpZ2dlciIsIkZlYXR1cmVkUHJvZHVjdHMiLCJzbGlkZXMiLCJjYXJvdXNlbCIsInByb2R1Y3RTaGFwZSIsInByb2R1Y3RTaGFwZU1vYmlsZSIsInBhZ2luYXRpb25JdGVtcyIsInNrZXRjaGVzIiwiYWN0aXZlU2xpZGUiLCJwaW5TZWN0aW9uIiwibW9iaWxlQ2Fyb3VzZWwiLCJGbGlja2l0eSIsImNvbnRhaW4iLCJwYWdlRG90cyIsInByZXZOZXh0QnV0dG9ucyIsImRyYWdnYWJsZSIsImNlbGxBbGlnbiIsIm9uIiwiY29sb3VyIiwibGVuZ3RoTnVtYmVyIiwidHJpZ2dlciIsInBpbiIsImFudGljaXBhdGVQaW4iLCJzdGFydCIsInByb2dyZXNzIiwiY2hhbmdlU2xpZGUiLCJzbGlkZSIsImluaXRGZWF0dXJlZFByb2R1Y3RzIiwic2VjdGlvbiIsImZvcm0iLCJhcGkiLCJyZXN0cmljdGVkRG9tYWlucyIsInN1YmplY3QiLCJyZWNpcGllbnQiLCJmb290ZXIiLCJyZXN0cmljdGVkIiwicmVtb3ZlQXR0cmlidXRlIiwiZm9ybUhhc1N1Ym1pdHRlZCIsInZhbGlkYXRpb24iLCJCb3VuY2VyIiwiZGlzYWJsZVN1Ym1pdCIsImN1c3RvbVZhbGlkYXRpb25zIiwidmFsaWRhdGVEb21haW4iLCJmaWVsZCIsImRvbWFpbiIsImlucHV0dGVkRG9tYWluIiwibWVzc2FnZXMiLCJwcm9jZXNzRGVtb0Zvcm0iLCJ0YXJnZXQiLCJmb3JtRGF0YSIsIkZvcm1EYXRhIiwic3VjY2VzcyIsImVycm9yRWwiLCJhcHBlbmQiLCJXUCIsIm5vbmNlIiwiZXZlbnQiLCJkZXRhaWwiLCJlcnJvcnMiLCJheGlvcyIsIm1ldGhvZCIsInVybCIsImFqYXgiLCJkYXRhIiwicmVzcG9uc2UiLCJzdGF0dXMiLCJlcnJvciIsInBvc3REYXRhIiwiYWxsRmllbGRzIiwib3B0aW9ucyIsImJvZHkiLCJKU09OIiwic3RyaW5naWZ5IiwibW9kZSIsImhlYWRlcnMiLCJmZXRjaCIsInJlcyIsImVyciIsImluaXRQcm9jZXNzRGVtb0Zvcm0iLCJmb3JtcyIsInByb2Nlc3NGb3JtIiwiaW5pdFByb2Nlc3NGb3JtIiwicHJvY2Vzc1RyaWFsRm9ybSIsImV4aXN0c0VsIiwicmVzcG9uc2VFbCIsImFwaUtleUVsIiwiVHlwZSIsIkVycm9yIiwic3RhdHVzVGV4dCIsInRleHQiLCJBcGlLZXkiLCJwYXJzZSIsImFwaUtleSIsInRlbXBvcmFyeUZpZWxkIiwic2VsZWN0IiwiZXhlY0NvbW1hbmQiLCJzZXRUaW1lb3V0IiwiaW5pdFByb2Nlc3NUcmlhbEZvcm0iLCJpbml0R29vZ2xlTWFwcyIsImxvYWRlciIsIkxvYWRlciIsImxvYWQiLCJnb29nbGUiLCJtYXBzIiwiTWFwIiwiZ2V0RWxlbWVudEJ5SWQiLCJjZW50ZXIiLCJsYXQiLCJsbmciLCJ6b29tIiwiZGlzYWJsZURlZmF1bHRVSSIsIkhlYWRlciIsIm5hdiIsIm1lbnVCdXR0b24iLCJwcm9kdWN0c0l0ZW0iLCJpbmR1c3RyeUl0ZW0iLCJhYm91dEl0ZW0iLCJ0b2dnbGVOYXYiLCJ0b2dnbGVTdWJtZW51IiwiY2FsY1N1Ym1lbnVIZWlnaHQiLCJvbnNjcm9sbCIsIm9sZFNjcm9sbCIsInNjcm9sbFkiLCJwYWdlWU9mZnNldCIsInRvZ2dsZSIsInByb2R1Y3RNZW51IiwiaW5pdEhlYWRlciIsImhlbGxvV29ybGQiLCJJbWFnZU1vZGFsIiwibW9kYWxzIiwiY2xvc2UiLCJvcGVuTW9kYWwiLCJjbG9zZU1vZGFsIiwiaW5pdEltYWdlTW9kYWwiLCJmZWF0dXJlcyIsIkludHJvU2xpZGVyIiwiYXJyb3dOZXh0IiwiYXJyb3dQcmV2IiwiZG90cyIsIm5leHRTbGlkZSIsInByZXZTbGlkZSIsImluaXRTbGlkZXIiLCJmbGlja2l0eSIsImZhZGUiLCJhZGFwdGl2ZUhlaWdodCIsInNsaWRlQ2hhbmdlZCIsInNlbGVjdGVkSW5kZXgiLCJkaXNhYmxlZCIsIm5leHQiLCJwcmV2aW91cyIsImluaXRJbnRyb1NsaWRlciIsImNhcm91c2VscyIsImlzSW52aWV3Iiwic2VjdGlvbnMiLCJpbnZpZXciLCJvbmNlIiwidG9nZ2xlQ2xhc3MiLCJvbkVudGVyIiwicmVmcmVzaCIsImxvZ29Db2xvdXIiLCJzZWN0aW9uc0xpZ2h0IiwibG9nbyIsImJvdHRvbSIsIm9uVG9nZ2xlIiwiZGlyZWN0aW9uIiwiaXNBY3RpdmUiLCJuZXdzTGlnaHRib3giLCJMaWdodGVuc2UiLCJnZXRQYXJhbSIsIlJlZ0V4cCIsImV4ZWMiLCJzZWFyY2giLCJkZWNvZGVVUklDb21wb25lbnQiLCJnZXRFeHBpcnlSZWNvcmQiLCJleHBpcnlQZXJpb2QiLCJleHBpcnlEYXRlIiwiRGF0ZSIsImdldFRpbWUiLCJhZGRQYXJhbXMiLCJnY2xpZFBhcmFtIiwiZ2NsaWRGb3JtRmllbGRzIiwiZ2NsaWRSZWNvcmQiLCJjdXJyR2NsaWRGb3JtRmllbGQiLCJ1dG1Tb3VyY2VQYXJhbSIsInV0Y1NvdXJjZUZvcm1GaWVsZHMiLCJ1dG1Tb3VyY2VSZWNvcmQiLCJjdXJyVXRtU291cmNlRm9ybUZpZWxkIiwidXRtTWVkaXVtUGFyYW0iLCJ1dGNNZWRpdW1Gb3JtRmllbGRzIiwidXRtTWVkaXVtUmVjb3JkIiwiY3VyclV0bU1lZGl1bUZvcm1GaWVsZCIsInV0bUNhbXBhaWduUGFyYW0iLCJ1dGNDYW1wYWlnbkZvcm1GaWVsZHMiLCJ1dG1DYW1wYWlnblJlY29yZCIsImN1cnJVdG1DYW1wYWlnbkZvcm1GaWVsZCIsInV0bVRlcm1QYXJhbSIsInV0Y1Rlcm1Gb3JtRmllbGRzIiwidXRtVGVybVJlY29yZCIsImN1cnJVdG1UZXJtRm9ybUZpZWxkIiwiZ2Nsc3JjUGFyYW0iLCJpc0djbHNyY1ZhbGlkIiwibG9jYWxTdG9yYWdlIiwic2V0SXRlbSIsImdjbGlkIiwiZ2V0SXRlbSIsInV0bV9zb3VyY2UiLCJ1dG1fbWVkaXVtIiwidXRtX2NhbXBhaWduIiwidXRtX3Rlcm0iLCJpc0djbGlkVmFsaWQiLCJpc1V0bVNvdXJjZVZhbGlkIiwiaXNVdG1NZWRpdW1WYWxpZCIsImlzVXRtQ2FtcGFpZ25WYWxpZCIsImlzVXRtVGVybVZhbGlkIiwicGFyYW1TdG9yYWdlIiwicGluSW1hZ2UiLCJpbWFnZSIsInBpblNwYWNpbmciLCJlbmRUcmlnZ2VyIiwiUHJpY2luZyIsInNsaWRlciIsInByaWNlcyIsInVwZGF0ZUN1cnJlbmN5IiwiaW5pdFByaWNpbmciLCJib3VuY2VyIiwiaWQiLCJyZWplY3RlZERvbWFpbnMiLCJwcm9jZXNzSW5keHJGb3JtIiwiZW5kUG9pbnQiLCJhcGlLZXlFbGVtIiwiZmlyc3ROYW1lIiwibGFzdE5hbWUiLCJjb21wYW55TmFtZSIsImpvYlJvbGUiLCJjb250YWN0TnVtYmVyIiwiZW1haWxBZGRyZXNzIiwibWFpbExpc3RBcHByb3ZlZCIsImNoZWNrZWQiLCJzb3VyY2UiLCJtZWRpdW0iLCJjYW1wYWlnbiIsInRlcm0iLCJyZXF1ZXN0RGF0YSIsIkZpcnN0TmFtZSIsIkxhc3ROYW1lIiwiQ29tcGFueU5hbWUiLCJKb2JSb2xlIiwiQ29udGFjdE51bWJlciIsIkVtYWlsQWRkcmVzcyIsIk1haWxMaXN0QXBwcm92ZWQiLCJ1dG1DYW1wYWlnbiIsInV0bVNvdXJjZSIsInV0bU1lZGl1bSIsInV0bVRlcm0iLCJqc29uIiwidGVtcElucHV0IiwiaW5pdFByb2Nlc3NJbmR4ckZvcm0iLCJTY3JvbGxUb1BsdWdpbiIsInByb2R1Y3RTaWRlYmFyIiwic2lkZWJhciIsInNpZGViYXJCdXR0b24iLCJmaXhlZEJ1dHRvbiIsInVybFBhcmFtcyIsIlVSTFNlYXJjaFBhcmFtcyIsImhhcyIsInNjcm9sbFRvIiwiUXVpY2tsaW5rcyIsImxpc3QiLCJsaW5rcyIsInNjcm9sbFRvU2VjdGlvbiIsInBpbkxpbmtzIiwiYWN0aXZlU3RhdGVzIiwibW92ZVF1aWNrbGlua3MiLCJvbkVudGVyQmFjayIsInF1aWNrbGluayIsIm9mZnNldFkiLCJpbml0UXVpY2tsaW5rcyIsInF1aWNrbGlua3MiLCJTZXJ2aWNlVGFicyIsInNsaWRlSGVpZ2h0IiwidGFicyIsInNsaWRlSG9sZGVyIiwiY2hhbmdlVGFiIiwic2V0dXBNb2JpbGUiLCJvcGVuU2xpZGUiLCJpbml0U2VydmljZVRhYnMiLCJzdGF0aXN0aWMiLCJjb3VudCIsIkNvdW50VXAiLCJzdGlja3lMb2dvIiwiaW5pdFRlc3RpbW9uaWFsQ2Fyb3VzZWwiLCJpbWFnZXNMb2FkZWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0NBR0E7O0FBQ0E7Q0FHQTtBQUNBO0FBQ0E7O0FBQ0EsSUFBTUEsZUFBZSxHQUFHQyxnRUFBeEI7O0FBQ0FELGVBQWUsQ0FBQ0UsSUFBaEIsR0FBdUJDLEdBQXZCLENBQTJCLFVBQUFDLEdBQUcsRUFBSTtBQUFBOztBQUNoQ0osaUJBQWUsQ0FBQ0ksR0FBRCxDQUFmO0FBQ0QsQ0FGRCxrQjs7Ozs7Ozs7Ozs7OztBQ1hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQSxDQUFDLFVBQVNDLENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUMsNENBQWlCQyxPQUFqQixNQUEwQixlQUFhLE9BQU9DLE1BQTlDLEdBQXFERixDQUFDLENBQUNDLE9BQUQsQ0FBdEQsR0FBZ0UsUUFBc0NFLGlDQUFPLENBQUMsT0FBRCxDQUFELG9DQUFhSCxDQUFiO0FBQUE7QUFBQTtBQUFBLG9HQUE1QyxHQUE0REEsU0FBNUg7QUFBK0osQ0FBN0ssQ0FBOEssSUFBOUssRUFBbUwsVUFBU0QsQ0FBVCxFQUFXO0FBQUM7O0FBQWEsV0FBU0ssQ0FBVCxDQUFXTCxDQUFYLEVBQWE7QUFBQyxXQUFNLFlBQVUsT0FBT0EsQ0FBdkI7QUFBeUI7O0FBQUEsTUFBSU0sQ0FBQyxHQUFDLGtEQUFOO0FBQUEsTUFBeURDLENBQUMsR0FBQyx5Q0FBM0Q7QUFBQSxNQUFxR0MsQ0FBQyxHQUFDLCtCQUF2RztBQUFBLE1BQXVJQyxDQUFDLEdBQUMsMkJBQXpJO0FBQUEsTUFBcUtDLENBQUMsR0FBQ0MsSUFBSSxDQUFDQyxFQUFMLEdBQVEsR0FBL0s7QUFBQSxNQUFtTEMsQ0FBQyxHQUFDRixJQUFJLENBQUNHLEdBQTFMO0FBQUEsTUFBOExDLENBQUMsR0FBQ0osSUFBSSxDQUFDSyxHQUFyTTtBQUFBLE1BQXlNQyxDQUFDLEdBQUNOLElBQUksQ0FBQ08sR0FBaE47QUFBQSxNQUFvTkMsQ0FBQyxHQUFDUixJQUFJLENBQUNTLElBQTNOO0FBQUEsTUFBZ09DLENBQUMsR0FBQyxTQUFTQyxTQUFULENBQW1CdEIsQ0FBbkIsRUFBcUI7QUFBQyxXQUFNLFlBQVUsT0FBT0EsQ0FBdkI7QUFBeUIsR0FBalI7QUFBQSxNQUFrUnVCLENBQUMsR0FBQyxTQUFTQyxNQUFULENBQWdCeEIsQ0FBaEIsRUFBa0I7QUFBQyxXQUFPVyxJQUFJLENBQUNjLEtBQUwsQ0FBVyxNQUFJekIsQ0FBZixJQUFrQixHQUFsQixJQUF1QixDQUE5QjtBQUFnQyxHQUF2VTs7QUFBd1UsV0FBUzBCLGNBQVQsQ0FBd0IxQixDQUF4QixFQUEwQjtBQUFDLFFBQUlDLENBQUo7QUFBQSxRQUFNMEIsQ0FBQyxHQUFDLENBQVI7O0FBQVUsU0FBSTNCLENBQUMsQ0FBQzRCLE9BQUYsRUFBSixFQUFnQkQsQ0FBQyxHQUFDM0IsQ0FBQyxDQUFDNkIsTUFBcEIsRUFBMkJGLENBQUMsSUFBRSxDQUE5QjtBQUFnQzFCLE9BQUMsR0FBQ0QsQ0FBQyxDQUFDMkIsQ0FBRCxDQUFILEVBQU8zQixDQUFDLENBQUMyQixDQUFELENBQUQsR0FBSzNCLENBQUMsQ0FBQzJCLENBQUMsR0FBQyxDQUFILENBQWIsRUFBbUIzQixDQUFDLENBQUMyQixDQUFDLEdBQUMsQ0FBSCxDQUFELEdBQU8xQixDQUExQjtBQUFoQzs7QUFBNERELEtBQUMsQ0FBQzhCLFFBQUYsR0FBVyxDQUFDOUIsQ0FBQyxDQUFDOEIsUUFBZDtBQUF1Qjs7QUFBQSxNQUFJQyxDQUFDLEdBQUM7QUFBQ0MsUUFBSSxFQUFDLHdCQUFOO0FBQStCQyxVQUFNLEVBQUMsU0FBdEM7QUFBZ0RDLFdBQU8sRUFBQyxhQUF4RDtBQUFzRUMsUUFBSSxFQUFDO0FBQTNFLEdBQU47O0FBQWdHLFdBQVNDLGFBQVQsQ0FBdUJwQyxDQUF2QixFQUF5QkMsQ0FBekIsRUFBMkI7QUFBQyxRQUFJMEIsQ0FBSjtBQUFBLFFBQU1sQixDQUFOO0FBQUEsUUFBUTRCLENBQVI7QUFBQSxRQUFVQyxDQUFWO0FBQUEsUUFBWUMsQ0FBWjtBQUFBLFFBQWNsQixDQUFkO0FBQUEsUUFBZ0JFLENBQWhCO0FBQUEsUUFBa0JpQixDQUFsQjtBQUFBLFFBQW9CQyxDQUFwQjtBQUFBLFFBQXNCQyxDQUF0QjtBQUFBLFFBQXdCQyxDQUF4QjtBQUFBLFFBQTBCQyxDQUExQjtBQUFBLFFBQTRCQyxDQUE1QjtBQUFBLFFBQThCQyxDQUE5QjtBQUFBLFFBQWdDQyxDQUFoQztBQUFBLFFBQWtDQyxDQUFsQztBQUFBLFFBQW9DM0MsQ0FBcEM7QUFBQSxRQUFzQzRDLENBQXRDO0FBQUEsUUFBd0NDLENBQXhDO0FBQUEsUUFBMENDLENBQTFDO0FBQUEsUUFBNENDLENBQTVDO0FBQUEsUUFBOENDLENBQTlDO0FBQUEsUUFBZ0QvQyxDQUFDLEdBQUNOLENBQUMsQ0FBQ3NELE9BQUYsQ0FBVUMsV0FBVixFQUFsRDtBQUFBLFFBQTBFL0MsQ0FBQyxHQUFDLGFBQTVFOztBQUEwRixXQUFNLFdBQVNGLENBQVQsSUFBWU4sQ0FBQyxDQUFDd0QsT0FBZCxJQUF1Qm5DLENBQUMsR0FBQyxTQUFTb0MsV0FBVCxDQUFxQnpELENBQXJCLEVBQXVCQyxDQUF2QixFQUF5QjtBQUFDLFVBQUkwQixDQUFKO0FBQUEsVUFBTWxCLENBQUMsR0FBQ2lELFFBQVEsQ0FBQ0MsZUFBVCxDQUF5Qiw0QkFBekIsRUFBc0QsTUFBdEQsQ0FBUjtBQUFBLFVBQXNFdEIsQ0FBQyxHQUFDLEdBQUd1QixLQUFILENBQVNDLElBQVQsQ0FBYzdELENBQUMsQ0FBQzhELFVBQWhCLENBQXhFO0FBQUEsVUFBb0d4QixDQUFDLEdBQUNELENBQUMsQ0FBQ1IsTUFBeEc7O0FBQStHLFdBQUk1QixDQUFDLEdBQUMsTUFBSUEsQ0FBSixHQUFNLEdBQVosRUFBZ0IsQ0FBQyxDQUFELEdBQUcsRUFBRXFDLENBQXJCO0FBQXdCWCxTQUFDLEdBQUNVLENBQUMsQ0FBQ0MsQ0FBRCxDQUFELENBQUt5QixRQUFMLENBQWNSLFdBQWQsRUFBRixFQUE4QnRELENBQUMsQ0FBQytELE9BQUYsQ0FBVSxNQUFJckMsQ0FBSixHQUFNLEdBQWhCLElBQXFCLENBQXJCLElBQXdCbEIsQ0FBQyxDQUFDd0QsY0FBRixDQUFpQixJQUFqQixFQUFzQnRDLENBQXRCLEVBQXdCVSxDQUFDLENBQUNDLENBQUQsQ0FBRCxDQUFLNEIsU0FBN0IsQ0FBdEQ7QUFBeEI7O0FBQXNILGFBQU96RCxDQUFQO0FBQVMsS0FBeFEsQ0FBeVFULENBQXpRLEVBQTJRLG1EQUEzUSxDQUFGLEVBQWtVcUQsQ0FBQyxHQUFDLFNBQVNjLFVBQVQsQ0FBb0JuRSxDQUFwQixFQUFzQkMsQ0FBdEIsRUFBd0I7QUFBQyxXQUFJLElBQUkwQixDQUFDLEdBQUMxQixDQUFDLEdBQUNBLENBQUMsQ0FBQ21FLEtBQUYsQ0FBUSxHQUFSLENBQUQsR0FBYyxFQUFyQixFQUF3QjNELENBQUMsR0FBQyxFQUExQixFQUE2QjRCLENBQUMsR0FBQ1YsQ0FBQyxDQUFDRSxNQUFyQyxFQUE0QyxDQUFDLENBQUQsR0FBRyxFQUFFUSxDQUFqRDtBQUFvRDVCLFNBQUMsQ0FBQ2tCLENBQUMsQ0FBQ1UsQ0FBRCxDQUFGLENBQUQsR0FBUSxDQUFDckMsQ0FBQyxDQUFDcUUsWUFBRixDQUFlMUMsQ0FBQyxDQUFDVSxDQUFELENBQWhCLENBQUQsSUFBdUIsQ0FBL0I7QUFBcEQ7O0FBQXFGLGFBQU81QixDQUFQO0FBQVMsS0FBdkgsQ0FBd0hULENBQXhILEVBQTBIK0IsQ0FBQyxDQUFDekIsQ0FBRCxDQUEzSCxDQUFwVSxFQUFvYyxXQUFTQSxDQUFULElBQVlnQyxDQUFDLEdBQUNlLENBQUMsQ0FBQ2lCLEVBQUosRUFBTy9CLENBQUMsR0FBQ2MsQ0FBQyxDQUFDa0IsRUFBRixJQUFNakMsQ0FBZixFQUFpQjdCLENBQUMsR0FBQzRDLENBQUMsQ0FBQ0QsQ0FBckIsRUFBdUJmLENBQUMsR0FBQ2dCLENBQUMsQ0FBQ0gsQ0FBM0IsRUFBNkJSLENBQUMsR0FBQ1csQ0FBQyxDQUFDbUIsS0FBRixHQUFRLElBQUVsQyxDQUF6QyxFQUEyQ0ssQ0FBQyxHQUFDVSxDQUFDLENBQUNvQixNQUFGLEdBQVMsSUFBRWxDLENBQXhELEVBQTBEWixDQUFDLEdBQUNXLENBQUMsSUFBRUMsQ0FBSCxHQUFLLE9BQUtTLENBQUMsR0FBQyxDQUFDRixDQUFDLEdBQUMsQ0FBQ0QsQ0FBQyxHQUFDcEMsQ0FBQyxHQUFDNkIsQ0FBTCxJQUFRSSxDQUFYLElBQWNKLENBQXJCLElBQXdCLEdBQXhCLElBQTZCVyxDQUFDLEdBQUNaLENBQUMsR0FBQ0UsQ0FBakMsSUFBb0MsSUFBcEMsSUFBMENXLENBQUMsR0FBQ0QsQ0FBQyxHQUFDTixDQUE5QyxJQUFpRCxJQUFqRCxHQUFzRCxDQUFDSyxDQUFELEVBQUdHLENBQUMsR0FBQ0QsQ0FBQyxHQUFDWCxDQUFDLEdBQUMvQixDQUFULEVBQVd1QyxDQUFDLEdBQUNELENBQUMsR0FBQ1IsQ0FBQyxHQUFDOUIsQ0FBakIsRUFBbUI0QyxDQUFDLEdBQUNGLENBQUMsR0FBQ1gsQ0FBdkIsRUFBeUJPLENBQXpCLEVBQTJCTSxDQUEzQixFQUE2Qk4sQ0FBQyxHQUFDLENBQUNBLENBQUMsR0FBQ0QsQ0FBSCxJQUFNLENBQXJDLEVBQXVDTyxDQUF2QyxFQUF5Q1AsQ0FBQyxHQUFDLENBQUNDLENBQUMsR0FBQ0QsQ0FBSCxJQUFNLENBQWpELEVBQW1ETyxDQUFuRCxFQUFxRFAsQ0FBckQsRUFBdURPLENBQXZELEVBQXlEUixDQUFDLEdBQUNuQyxDQUFDLEdBQUM2QixDQUFDLElBQUUsSUFBRTlCLENBQUosQ0FBOUQsRUFBcUU0QyxDQUFyRSxFQUF1RTNDLENBQXZFLEVBQXlFMEMsQ0FBekUsRUFBMkUxQyxDQUEzRSxFQUE2RXlDLENBQTdFLEVBQStFekMsQ0FBL0UsRUFBaUZ5QyxDQUFDLEdBQUMsQ0FBQ0EsQ0FBQyxHQUFDRCxDQUFILElBQU0sQ0FBekYsRUFBMkZ4QyxDQUEzRixFQUE2RndDLENBQUMsR0FBQyxDQUFDQyxDQUFDLEdBQUNELENBQUgsSUFBTSxDQUFyRyxFQUF1R3hDLENBQXZHLEVBQXlHd0MsQ0FBekcsRUFBMkd4QyxDQUEzRyxFQUE2R0osQ0FBQyxHQUFDZ0MsQ0FBQyxHQUFDRSxDQUFDLElBQUUsSUFBRS9CLENBQUosQ0FBbEgsRUFBeUhvQyxDQUF6SCxFQUEySFAsQ0FBM0gsRUFBNkhRLENBQTdILEVBQStIUixDQUEvSCxFQUFpSVEsQ0FBQyxHQUFDLENBQUNDLENBQUMsR0FBQ0QsQ0FBSCxJQUFNLENBQXpJLEVBQTJJUixDQUEzSSxFQUE2SVMsQ0FBQyxHQUFDLENBQUNBLENBQUMsR0FBQ0QsQ0FBSCxJQUFNLENBQXJKLEVBQXVKUixDQUF2SixFQUF5SlMsQ0FBekosRUFBMkpULENBQTNKLEVBQTZKVSxDQUE3SixFQUErSlYsQ0FBL0osRUFBaUtXLENBQWpLLEVBQW1LM0MsQ0FBbkssRUFBcUsyQyxDQUFySyxFQUF1S0MsQ0FBdkssRUFBMEt5QixJQUExSyxDQUErSyxHQUEvSyxDQUF0RCxHQUEwTyxHQUEvTyxHQUFtUCxPQUFLakUsQ0FBQyxHQUFDaUMsQ0FBUCxJQUFVLEdBQVYsR0FBY0wsQ0FBZCxHQUFnQixJQUFoQixHQUFxQk0sQ0FBckIsR0FBdUIsSUFBdkIsR0FBNEIsQ0FBQ0QsQ0FBN0IsR0FBK0IsSUFBL0IsR0FBb0MsQ0FBQ0MsQ0FBckMsR0FBdUMsSUFBdkMsR0FBNENELENBQTVDLEdBQThDLEdBQXpXLElBQThXLGFBQVdwQyxDQUFYLElBQWMsY0FBWUEsQ0FBMUIsSUFBNkJrQyxDQUFDLEdBQUMsYUFBV2xDLENBQVgsR0FBYSxDQUFDZ0MsQ0FBQyxHQUFDQyxDQUFDLEdBQUNjLENBQUMsQ0FBQzFCLENBQVAsSUFBVW5CLENBQXZCLElBQTBCOEIsQ0FBQyxHQUFDZSxDQUFDLENBQUNpQixFQUFKLEVBQU8sQ0FBQy9CLENBQUMsR0FBQ2MsQ0FBQyxDQUFDa0IsRUFBTCxJQUFTL0QsQ0FBMUMsQ0FBRixFQUErQ21CLENBQUMsR0FBQyxPQUFLLENBQUNsQixDQUFDLEdBQUM0QyxDQUFDLENBQUNzQixFQUFMLElBQVNyQyxDQUFkLElBQWlCLEdBQWpCLElBQXNCRCxDQUFDLEdBQUNnQixDQUFDLENBQUN1QixFQUExQixJQUE4QixJQUE5QixHQUFtQyxDQUFDbkUsQ0FBQyxHQUFDNkIsQ0FBSCxFQUFLRCxDQUFDLEdBQUNHLENBQVAsRUFBUy9CLENBQUMsSUFBRWMsQ0FBQyxHQUFDZSxDQUFDLEdBQUM5QixDQUFOLENBQVYsRUFBbUI2QixDQUFDLEdBQUNFLENBQXJCLEVBQXVCOUIsQ0FBdkIsRUFBeUI0QixDQUFDLEdBQUNFLENBQTNCLEVBQTZCOUIsQ0FBQyxHQUFDYyxDQUEvQixFQUFpQ2MsQ0FBQyxHQUFDRSxDQUFuQyxFQUFxQzlCLENBQUMsR0FBQzZCLENBQXZDLEVBQXlDRCxDQUFDLEdBQUNHLENBQTNDLEVBQTZDL0IsQ0FBQyxHQUFDNkIsQ0FBL0MsRUFBaURELENBQWpELEVBQW1ENUIsQ0FBQyxHQUFDNkIsQ0FBckQsRUFBdURELENBQUMsR0FBQ0csQ0FBekQsRUFBMkQvQixDQUFDLEdBQUNjLENBQTdELEVBQStEYyxDQUFDLEdBQUNFLENBQWpFLEVBQW1FOUIsQ0FBbkUsRUFBcUU0QixDQUFDLEdBQUNFLENBQXZFLEVBQXlFOUIsQ0FBQyxHQUFDYyxDQUEzRSxFQUE2RWMsQ0FBQyxHQUFDRSxDQUEvRSxFQUFpRjlCLENBQUMsR0FBQzZCLENBQW5GLEVBQXFGRCxDQUFDLEdBQUNHLENBQXZGLEVBQXlGL0IsQ0FBQyxHQUFDNkIsQ0FBM0YsRUFBNkZELENBQTdGLEVBQWdHcUMsSUFBaEcsQ0FBcUcsR0FBckcsQ0FBbkMsR0FBNkksR0FBM04sSUFBZ08sV0FBU3BFLENBQVQsR0FBV3FCLENBQUMsR0FBQyxNQUFJMEIsQ0FBQyxDQUFDd0IsRUFBTixHQUFTLEdBQVQsR0FBYXhCLENBQUMsQ0FBQ3lCLEVBQWYsR0FBa0IsSUFBbEIsR0FBdUJ6QixDQUFDLENBQUMwQixFQUF6QixHQUE0QixHQUE1QixHQUFnQzFCLENBQUMsQ0FBQzJCLEVBQS9DLEdBQWtELGVBQWExRSxDQUFiLElBQWdCLGNBQVlBLENBQTVCLEtBQWdDcUIsQ0FBQyxHQUFDLE9BQUtsQixDQUFDLEdBQUMsQ0FBQ2dDLENBQUMsR0FBQyxDQUFDekMsQ0FBQyxDQUFDcUUsWUFBRixDQUFlLFFBQWYsSUFBeUIsRUFBMUIsRUFBOEJZLEtBQTlCLENBQW9DMUUsQ0FBcEMsS0FBd0MsRUFBM0MsRUFBK0MyRSxLQUEvQyxFQUFQLElBQStELEdBQS9ELElBQW9FN0MsQ0FBQyxHQUFDSSxDQUFDLENBQUN5QyxLQUFGLEVBQXRFLElBQWlGLElBQWpGLEdBQXNGekMsQ0FBQyxDQUFDaUMsSUFBRixDQUFPLEdBQVAsQ0FBeEYsRUFBb0csY0FBWXBFLENBQVosS0FBZ0JxQixDQUFDLElBQUUsTUFBSWxCLENBQUosR0FBTSxHQUFOLEdBQVU0QixDQUFWLEdBQVksR0FBL0IsQ0FBcEksQ0FBcGtDLEVBQTZ1Q2hCLENBQUMsQ0FBQzhELFlBQUYsQ0FBZSxHQUFmLEVBQW1CQyxlQUFlLENBQUMvRCxDQUFDLENBQUNnRSxVQUFGLEdBQWFDLGVBQWUsQ0FBQzNELENBQUQsQ0FBN0IsQ0FBbEMsQ0FBN3VDLEVBQWt6QzFCLENBQUMsSUFBRUQsQ0FBQyxDQUFDdUYsVUFBTCxLQUFrQnZGLENBQUMsQ0FBQ3VGLFVBQUYsQ0FBYUMsWUFBYixDQUEwQm5FLENBQTFCLEVBQTRCckIsQ0FBNUIsR0FBK0JBLENBQUMsQ0FBQ3VGLFVBQUYsQ0FBYUUsV0FBYixDQUF5QnpGLENBQXpCLENBQWpELENBQWx6QyxFQUFnNENxQixDQUF2NUMsSUFBMDVDckIsQ0FBaDZDO0FBQWs2Qzs7QUFBQSxXQUFTMEYsWUFBVCxDQUFzQjFGLENBQXRCLEVBQXdCQyxDQUF4QixFQUEwQjBCLENBQTFCLEVBQTRCbEIsQ0FBNUIsRUFBOEI0QixDQUE5QixFQUFnQ0MsQ0FBaEMsRUFBa0NDLENBQWxDLEVBQW9DbEIsQ0FBcEMsRUFBc0NFLENBQXRDLEVBQXdDO0FBQUMsUUFBR3ZCLENBQUMsS0FBR3FCLENBQUosSUFBT3BCLENBQUMsS0FBR3NCLENBQWQsRUFBZ0I7QUFBQ0ksT0FBQyxHQUFDVixDQUFDLENBQUNVLENBQUQsQ0FBSCxFQUFPbEIsQ0FBQyxHQUFDUSxDQUFDLENBQUNSLENBQUQsQ0FBVjs7QUFBYyxVQUFJK0IsQ0FBQyxHQUFDSCxDQUFDLEdBQUMsR0FBRixHQUFNM0IsQ0FBWjtBQUFBLFVBQWMrQixDQUFDLEdBQUMxQixDQUFDLENBQUN5QixDQUFELENBQWpCO0FBQUEsVUFBcUJFLENBQUMsR0FBQzdCLENBQUMsQ0FBQzJCLENBQUQsQ0FBeEI7QUFBQSxVQUE0QkcsQ0FBQyxHQUFDaEMsSUFBSSxDQUFDQyxFQUFuQztBQUFBLFVBQXNDZ0MsQ0FBQyxHQUFDLElBQUVELENBQTFDO0FBQUEsVUFBNENFLENBQUMsR0FBQyxDQUFDN0MsQ0FBQyxHQUFDcUIsQ0FBSCxJQUFNLENBQXBEO0FBQUEsVUFBc0R5QixDQUFDLEdBQUMsQ0FBQzdDLENBQUMsR0FBQ3NCLENBQUgsSUFBTSxDQUE5RDtBQUFBLFVBQWdFd0IsQ0FBQyxHQUFDTixDQUFDLEdBQUNJLENBQUYsR0FBSUgsQ0FBQyxHQUFDSSxDQUF4RTtBQUFBLFVBQTBFRSxDQUFDLEdBQUMsQ0FBQ04sQ0FBRCxHQUFHRyxDQUFILEdBQUtKLENBQUMsR0FBQ0ssQ0FBbkY7QUFBQSxVQUFxRnpDLENBQUMsR0FBQzBDLENBQUMsR0FBQ0EsQ0FBekY7QUFBQSxVQUEyRkUsQ0FBQyxHQUFDRCxDQUFDLEdBQUNBLENBQS9GO0FBQUEsVUFBaUdFLENBQUMsR0FBQzdDLENBQUMsSUFBRXNCLENBQUMsR0FBQ0EsQ0FBSixDQUFELEdBQVFzQixDQUFDLElBQUV4QyxDQUFDLEdBQUNBLENBQUosQ0FBNUc7O0FBQW1ILFVBQUV5QyxDQUFGLEtBQU12QixDQUFDLEdBQUNSLENBQUMsQ0FBQytCLENBQUQsQ0FBRCxHQUFLdkIsQ0FBUCxFQUFTbEIsQ0FBQyxHQUFDVSxDQUFDLENBQUMrQixDQUFELENBQUQsR0FBS3pDLENBQXRCO0FBQXlCLFVBQUkwQyxDQUFDLEdBQUN4QixDQUFDLEdBQUNBLENBQVI7QUFBQSxVQUFVeUIsQ0FBQyxHQUFDM0MsQ0FBQyxHQUFDQSxDQUFkO0FBQUEsVUFBZ0I0QyxDQUFDLEdBQUMsQ0FBQ0YsQ0FBQyxHQUFDQyxDQUFGLEdBQUlELENBQUMsR0FBQ0YsQ0FBTixHQUFRRyxDQUFDLEdBQUMvQyxDQUFYLEtBQWU4QyxDQUFDLEdBQUNGLENBQUYsR0FBSUcsQ0FBQyxHQUFDL0MsQ0FBckIsQ0FBbEI7QUFBMENnRCxPQUFDLEdBQUMsQ0FBRixLQUFNQSxDQUFDLEdBQUMsQ0FBUjtBQUFXLFVBQUkvQyxDQUFDLEdBQUMsQ0FBQ2dDLENBQUMsS0FBR0MsQ0FBSixHQUFNLENBQUMsQ0FBUCxHQUFTLENBQVYsSUFBYXBCLENBQUMsQ0FBQ2tDLENBQUQsQ0FBcEI7QUFBQSxVQUF3QjdDLENBQUMsR0FBQ21CLENBQUMsR0FBQ3FCLENBQUYsR0FBSXZDLENBQUosR0FBTUgsQ0FBaEM7QUFBQSxVQUFrQ3FGLENBQUMsR0FBQyxDQUFDbEYsQ0FBRCxHQUFHc0MsQ0FBSCxHQUFLcEIsQ0FBTCxHQUFPckIsQ0FBM0M7QUFBQSxVQUE2Q3NGLENBQUMsR0FBQ25ELENBQUMsR0FBQ2pDLENBQUYsR0FBSWtDLENBQUMsR0FBQ2lELENBQU4sR0FBUSxDQUFDM0YsQ0FBQyxHQUFDcUIsQ0FBSCxJQUFNLENBQTdEO0FBQUEsVUFBK0R3RSxDQUFDLEdBQUNuRCxDQUFDLEdBQUNsQyxDQUFGLEdBQUlpQyxDQUFDLEdBQUNrRCxDQUFOLEdBQVEsQ0FBQzFGLENBQUMsR0FBQ3NCLENBQUgsSUFBTSxDQUEvRTtBQUFBLFVBQWlGaEIsQ0FBQyxHQUFDLENBQUN3QyxDQUFDLEdBQUN2QyxDQUFILElBQU1tQixDQUF6RjtBQUFBLFVBQTJGSSxDQUFDLEdBQUMsQ0FBQ2lCLENBQUMsR0FBQzJDLENBQUgsSUFBTWxGLENBQW5HO0FBQUEsVUFBcUdxRixDQUFDLEdBQUMsQ0FBQyxDQUFDL0MsQ0FBRCxHQUFHdkMsQ0FBSixJQUFPbUIsQ0FBOUc7QUFBQSxVQUFnSG9FLENBQUMsR0FBQyxDQUFDLENBQUMvQyxDQUFELEdBQUcyQyxDQUFKLElBQU9sRixDQUF6SDtBQUFBLFVBQTJIdUYsQ0FBQyxHQUFDekYsQ0FBQyxHQUFDQSxDQUFGLEdBQUl3QixDQUFDLEdBQUNBLENBQW5JO0FBQUEsVUFBcUlrRSxDQUFDLEdBQUMsQ0FBQ2xFLENBQUMsR0FBQyxDQUFGLEdBQUksQ0FBQyxDQUFMLEdBQU8sQ0FBUixJQUFXcEIsSUFBSSxDQUFDdUYsSUFBTCxDQUFVM0YsQ0FBQyxHQUFDWSxDQUFDLENBQUM2RSxDQUFELENBQWIsQ0FBbEo7QUFBQSxVQUFvS0csQ0FBQyxHQUFDLENBQUM1RixDQUFDLEdBQUN3RixDQUFGLEdBQUloRSxDQUFDLEdBQUMrRCxDQUFOLEdBQVEsQ0FBUixHQUFVLENBQUMsQ0FBWCxHQUFhLENBQWQsSUFBaUJuRixJQUFJLENBQUN1RixJQUFMLENBQVUsQ0FBQzNGLENBQUMsR0FBQ3VGLENBQUYsR0FBSS9ELENBQUMsR0FBQ2dFLENBQVAsSUFBVTVFLENBQUMsQ0FBQzZFLENBQUMsSUFBRUYsQ0FBQyxHQUFDQSxDQUFGLEdBQUlDLENBQUMsR0FBQ0EsQ0FBUixDQUFGLENBQXJCLENBQXZMO0FBQTJOSyxXQUFLLENBQUNELENBQUQsQ0FBTCxLQUFXQSxDQUFDLEdBQUN4RCxDQUFiLEdBQWdCLENBQUNKLENBQUQsSUFBSSxJQUFFNEQsQ0FBTixHQUFRQSxDQUFDLElBQUV2RCxDQUFYLEdBQWFMLENBQUMsSUFBRTRELENBQUMsR0FBQyxDQUFMLEtBQVNBLENBQUMsSUFBRXZELENBQVosQ0FBN0IsRUFBNENxRCxDQUFDLElBQUVyRCxDQUEvQyxFQUFpRHVELENBQUMsSUFBRXZELENBQXBEO0FBQXNELFVBQUl5RCxDQUFKO0FBQUEsVUFBTUMsQ0FBQyxHQUFDM0YsSUFBSSxDQUFDNEYsSUFBTCxDQUFVdEYsQ0FBQyxDQUFDa0YsQ0FBRCxDQUFELElBQU12RCxDQUFDLEdBQUMsQ0FBUixDQUFWLENBQVI7QUFBQSxVQUE4QjRELENBQUMsR0FBQyxFQUFoQztBQUFBLFVBQW1DQyxDQUFDLEdBQUNOLENBQUMsR0FBQ0csQ0FBdkM7QUFBQSxVQUF5Q0ksQ0FBQyxHQUFDLElBQUUsQ0FBRixHQUFJN0YsQ0FBQyxDQUFDNEYsQ0FBQyxHQUFDLENBQUgsQ0FBTCxJQUFZLElBQUUxRixDQUFDLENBQUMwRixDQUFDLEdBQUMsQ0FBSCxDQUFmLENBQTNDO0FBQUEsVUFBaUVFLENBQUMsR0FBQ2xFLENBQUMsR0FBQ2QsQ0FBckU7QUFBQSxVQUF1RWlGLENBQUMsR0FBQ2xFLENBQUMsR0FBQ2YsQ0FBM0U7QUFBQSxVQUE2RWtGLENBQUMsR0FBQ25FLENBQUMsR0FBQyxDQUFDakMsQ0FBbEY7QUFBQSxVQUFvRnFHLENBQUMsR0FBQ3JFLENBQUMsR0FBQ2hDLENBQXhGOztBQUEwRixXQUFJNEYsQ0FBQyxHQUFDLENBQU4sRUFBUUEsQ0FBQyxHQUFDQyxDQUFWLEVBQVlELENBQUMsRUFBYjtBQUFnQnRELFNBQUMsR0FBQ2hDLENBQUMsQ0FBQ3NCLENBQUMsR0FBQzRELENBQUMsR0FBQ0ksQ0FBQyxHQUFDSSxDQUFQLENBQUgsRUFBYXpELENBQUMsR0FBQ25DLENBQUMsQ0FBQ3dCLENBQUQsQ0FBaEIsRUFBb0I5QixDQUFDLEdBQUNRLENBQUMsQ0FBQ3NCLENBQUMsSUFBRW9FLENBQUosQ0FBdkIsRUFBOEIxRSxDQUFDLEdBQUNsQixDQUFDLENBQUN3QixDQUFELENBQWpDLEVBQXFDbUUsQ0FBQyxDQUFDTyxJQUFGLENBQU9oRSxDQUFDLEdBQUMyRCxDQUFDLEdBQUMxRCxDQUFYLEVBQWFBLENBQUMsR0FBQzBELENBQUMsR0FBQzNELENBQWpCLEVBQW1CeEMsQ0FBQyxHQUFDbUcsQ0FBQyxHQUFDM0UsQ0FBdkIsRUFBeUJBLENBQUMsR0FBQzJFLENBQUMsR0FBQ25HLENBQTdCLEVBQStCQSxDQUEvQixFQUFpQ3dCLENBQWpDLENBQXJDO0FBQWhCOztBQUF5RixXQUFJc0UsQ0FBQyxHQUFDLENBQU4sRUFBUUEsQ0FBQyxHQUFDRyxDQUFDLENBQUMzRSxNQUFaLEVBQW1Cd0UsQ0FBQyxJQUFFLENBQXRCO0FBQXdCdEQsU0FBQyxHQUFDeUQsQ0FBQyxDQUFDSCxDQUFELENBQUgsRUFBT3JELENBQUMsR0FBQ3dELENBQUMsQ0FBQ0gsQ0FBQyxHQUFDLENBQUgsQ0FBVixFQUFnQkcsQ0FBQyxDQUFDSCxDQUFELENBQUQsR0FBS3RELENBQUMsR0FBQzRELENBQUYsR0FBSTNELENBQUMsR0FBQzZELENBQU4sR0FBUWpCLENBQTdCLEVBQStCWSxDQUFDLENBQUNILENBQUMsR0FBQyxDQUFILENBQUQsR0FBT3RELENBQUMsR0FBQzZELENBQUYsR0FBSTVELENBQUMsR0FBQzhELENBQU4sR0FBUWpCLENBQTlDO0FBQXhCOztBQUF3RSxhQUFPVyxDQUFDLENBQUNILENBQUMsR0FBQyxDQUFILENBQUQsR0FBT2hGLENBQVAsRUFBU21GLENBQUMsQ0FBQ0gsQ0FBQyxHQUFDLENBQUgsQ0FBRCxHQUFPOUUsQ0FBaEIsRUFBa0JpRixDQUF6QjtBQUEyQjtBQUFDOztBQUFBLFdBQVNsQixlQUFULENBQXlCdEYsQ0FBekIsRUFBMkI7QUFBQyxhQUFTZ0gsRUFBVCxDQUFZaEgsQ0FBWixFQUFjQyxDQUFkLEVBQWdCMEIsQ0FBaEIsRUFBa0JsQixDQUFsQixFQUFvQjtBQUFDZ0MsT0FBQyxHQUFDLENBQUNkLENBQUMsR0FBQzNCLENBQUgsSUFBTSxDQUFSLEVBQVUwQyxDQUFDLEdBQUMsQ0FBQ2pDLENBQUMsR0FBQ1IsQ0FBSCxJQUFNLENBQWxCLEVBQW9Cb0IsQ0FBQyxDQUFDMEYsSUFBRixDQUFPL0csQ0FBQyxHQUFDeUMsQ0FBVCxFQUFXeEMsQ0FBQyxHQUFDeUMsQ0FBYixFQUFlZixDQUFDLEdBQUNjLENBQWpCLEVBQW1CaEMsQ0FBQyxHQUFDaUMsQ0FBckIsRUFBdUJmLENBQXZCLEVBQXlCbEIsQ0FBekIsQ0FBcEI7QUFBZ0Q7O0FBQUEsUUFBSVIsQ0FBSjtBQUFBLFFBQU0wQixDQUFOO0FBQUEsUUFBUWxCLENBQVI7QUFBQSxRQUFVNEIsQ0FBVjtBQUFBLFFBQVlDLENBQVo7QUFBQSxRQUFjQyxDQUFkO0FBQUEsUUFBZ0JsQixDQUFoQjtBQUFBLFFBQWtCRSxDQUFsQjtBQUFBLFFBQW9CaUIsQ0FBcEI7QUFBQSxRQUFzQkMsQ0FBdEI7QUFBQSxRQUF3QkMsQ0FBeEI7QUFBQSxRQUEwQkMsQ0FBMUI7QUFBQSxRQUE0QkMsQ0FBNUI7QUFBQSxRQUE4QkMsQ0FBOUI7QUFBQSxRQUFnQ0MsQ0FBaEM7QUFBQSxRQUFrQ0MsQ0FBQyxHQUFDLENBQUMvQyxDQUFDLEdBQUMsRUFBSCxFQUFPaUgsT0FBUCxDQUFlekcsQ0FBZixFQUFpQixVQUFTUixDQUFULEVBQVc7QUFBQyxVQUFJQyxDQUFDLEdBQUMsQ0FBQ0QsQ0FBUDtBQUFTLGFBQU9DLENBQUMsR0FBQyxJQUFGLElBQVEsQ0FBQyxJQUFELEdBQU1BLENBQWQsR0FBZ0IsQ0FBaEIsR0FBa0JBLENBQXpCO0FBQTJCLEtBQWpFLEVBQW1FZ0YsS0FBbkUsQ0FBeUUzRSxDQUF6RSxLQUE2RSxFQUFqSDtBQUFBLFFBQW9IMEMsQ0FBQyxHQUFDLEVBQXRIO0FBQUEsUUFBeUgzQyxDQUFDLEdBQUMsQ0FBM0g7QUFBQSxRQUE2SDRDLENBQUMsR0FBQyxDQUEvSDtBQUFBLFFBQWlJQyxDQUFDLEdBQUNILENBQUMsQ0FBQ2xCLE1BQXJJO0FBQUEsUUFBNElzQixDQUFDLEdBQUMsQ0FBOUk7QUFBQSxRQUFnSkMsQ0FBQyxHQUFDLDRCQUEwQnBELENBQTVLOztBQUE4SyxRQUFHLENBQUNBLENBQUQsSUFBSSxDQUFDb0csS0FBSyxDQUFDckQsQ0FBQyxDQUFDLENBQUQsQ0FBRixDQUFWLElBQWtCcUQsS0FBSyxDQUFDckQsQ0FBQyxDQUFDLENBQUQsQ0FBRixDQUExQixFQUFpQyxPQUFPbUUsT0FBTyxDQUFDQyxHQUFSLENBQVkvRCxDQUFaLEdBQWVKLENBQXRCOztBQUF3QixTQUFJL0MsQ0FBQyxHQUFDLENBQU4sRUFBUUEsQ0FBQyxHQUFDaUQsQ0FBVixFQUFZakQsQ0FBQyxFQUFiO0FBQWdCLFVBQUcyQyxDQUFDLEdBQUNOLENBQUYsRUFBSThELEtBQUssQ0FBQ3JELENBQUMsQ0FBQzlDLENBQUQsQ0FBRixDQUFMLEdBQVlzQyxDQUFDLEdBQUMsQ0FBQ0QsQ0FBQyxHQUFDUyxDQUFDLENBQUM5QyxDQUFELENBQUQsQ0FBS21ILFdBQUwsRUFBSCxNQUF5QnJFLENBQUMsQ0FBQzlDLENBQUQsQ0FBeEMsR0FBNENBLENBQUMsRUFBakQsRUFBb0RRLENBQUMsR0FBQyxDQUFDc0MsQ0FBQyxDQUFDOUMsQ0FBQyxHQUFDLENBQUgsQ0FBeEQsRUFBOERvQyxDQUFDLEdBQUMsQ0FBQ1UsQ0FBQyxDQUFDOUMsQ0FBQyxHQUFDLENBQUgsQ0FBbEUsRUFBd0VzQyxDQUFDLEtBQUc5QixDQUFDLElBQUVKLENBQUgsRUFBS2dDLENBQUMsSUFBRVksQ0FBWCxDQUF6RSxFQUF1RmhELENBQUMsS0FBR3NCLENBQUMsR0FBQ2QsQ0FBRixFQUFJK0IsQ0FBQyxHQUFDSCxDQUFULENBQXhGLEVBQW9HLFFBQU1DLENBQTdHLEVBQStHakIsQ0FBQyxLQUFHQSxDQUFDLENBQUNRLE1BQUYsR0FBUyxDQUFULEdBQVcsRUFBRW1CLENBQUMsQ0FBQ25CLE1BQWYsR0FBc0JzQixDQUFDLElBQUU5QixDQUFDLENBQUNRLE1BQTlCLENBQUQsRUFBdUN4QixDQUFDLEdBQUNrQixDQUFDLEdBQUNkLENBQTNDLEVBQTZDd0MsQ0FBQyxHQUFDVCxDQUFDLEdBQUNILENBQWpELEVBQW1EaEIsQ0FBQyxHQUFDLENBQUNaLENBQUQsRUFBRzRCLENBQUgsQ0FBckQsRUFBMkRXLENBQUMsQ0FBQytELElBQUYsQ0FBTzFGLENBQVAsQ0FBM0QsRUFBcUVwQixDQUFDLElBQUUsQ0FBeEUsRUFBMEVxQyxDQUFDLEdBQUMsR0FBNUUsQ0FBL0csS0FBb00sSUFBRyxRQUFNQSxDQUFULEVBQVdDLENBQUMsS0FBR2xDLENBQUMsR0FBQzRDLENBQUMsR0FBQyxDQUFQLENBQUQsRUFBVyxDQUFDNUIsQ0FBQyxHQUFDQSxDQUFDLElBQUUsQ0FBQyxDQUFELEVBQUcsQ0FBSCxDQUFOLEVBQWEwRixJQUFiLENBQWtCdEcsQ0FBbEIsRUFBb0I0QixDQUFwQixFQUFzQmhDLENBQUMsR0FBQyxJQUFFMEMsQ0FBQyxDQUFDOUMsQ0FBQyxHQUFDLENBQUgsQ0FBM0IsRUFBaUNnRCxDQUFDLEdBQUMsSUFBRUYsQ0FBQyxDQUFDOUMsQ0FBQyxHQUFDLENBQUgsQ0FBdEMsRUFBNENJLENBQUMsSUFBRSxJQUFFMEMsQ0FBQyxDQUFDOUMsQ0FBQyxHQUFDLENBQUgsQ0FBbEQsRUFBd0RnRCxDQUFDLElBQUUsSUFBRUYsQ0FBQyxDQUFDOUMsQ0FBQyxHQUFDLENBQUgsQ0FBOUQsQ0FBWCxFQUFnRkEsQ0FBQyxJQUFFLENBQW5GLENBQVgsS0FBcUcsSUFBRyxRQUFNcUMsQ0FBVCxFQUFXRyxDQUFDLEdBQUNwQyxDQUFGLEVBQUlxQyxDQUFDLEdBQUNPLENBQU4sRUFBUSxRQUFNTCxDQUFOLElBQVMsUUFBTUEsQ0FBZixLQUFtQkgsQ0FBQyxJQUFFcEMsQ0FBQyxHQUFDZ0IsQ0FBQyxDQUFDQSxDQUFDLENBQUNRLE1BQUYsR0FBUyxDQUFWLENBQU4sRUFBbUJhLENBQUMsSUFBRU8sQ0FBQyxHQUFDNUIsQ0FBQyxDQUFDQSxDQUFDLENBQUNRLE1BQUYsR0FBUyxDQUFWLENBQTVDLENBQVIsRUFBa0VVLENBQUMsS0FBR2xDLENBQUMsR0FBQzRDLENBQUMsR0FBQyxDQUFQLENBQW5FLEVBQTZFNUIsQ0FBQyxDQUFDMEYsSUFBRixDQUFPdEUsQ0FBUCxFQUFTQyxDQUFULEVBQVdqQyxDQUFYLEVBQWE0QixDQUFiLEVBQWVoQyxDQUFDLElBQUUsSUFBRTBDLENBQUMsQ0FBQzlDLENBQUMsR0FBQyxDQUFILENBQXJCLEVBQTJCZ0QsQ0FBQyxJQUFFLElBQUVGLENBQUMsQ0FBQzlDLENBQUMsR0FBQyxDQUFILENBQWpDLENBQTdFLEVBQXFIQSxDQUFDLElBQUUsQ0FBeEgsQ0FBWCxLQUEwSSxJQUFHLFFBQU1xQyxDQUFULEVBQVdHLENBQUMsR0FBQ3BDLENBQUMsR0FBQyxJQUFFLENBQUYsSUFBS0ksQ0FBQyxHQUFDSixDQUFQLENBQUosRUFBY3FDLENBQUMsR0FBQ08sQ0FBQyxHQUFDLElBQUUsQ0FBRixJQUFLWixDQUFDLEdBQUNZLENBQVAsQ0FBbEIsRUFBNEJWLENBQUMsS0FBR2xDLENBQUMsR0FBQzRDLENBQUMsR0FBQyxDQUFQLENBQTdCLEVBQXVDNUMsQ0FBQyxJQUFFLElBQUUwQyxDQUFDLENBQUM5QyxDQUFDLEdBQUMsQ0FBSCxDQUE3QyxFQUFtRGdELENBQUMsSUFBRSxJQUFFRixDQUFDLENBQUM5QyxDQUFDLEdBQUMsQ0FBSCxDQUF6RCxFQUErRG9CLENBQUMsQ0FBQzBGLElBQUYsQ0FBT3RFLENBQVAsRUFBU0MsQ0FBVCxFQUFXckMsQ0FBQyxHQUFDLElBQUUsQ0FBRixJQUFLSSxDQUFDLEdBQUNKLENBQVAsQ0FBYixFQUF1QjRDLENBQUMsR0FBQyxJQUFFLENBQUYsSUFBS1osQ0FBQyxHQUFDWSxDQUFQLENBQXpCLEVBQW1DNUMsQ0FBbkMsRUFBcUM0QyxDQUFyQyxDQUEvRCxFQUF1R2hELENBQUMsSUFBRSxDQUExRyxDQUFYLEtBQTRILElBQUcsUUFBTXFDLENBQVQsRUFBV0csQ0FBQyxHQUFDcEMsQ0FBQyxHQUFDZ0IsQ0FBQyxDQUFDQSxDQUFDLENBQUNRLE1BQUYsR0FBUyxDQUFWLENBQUwsRUFBa0JhLENBQUMsR0FBQ08sQ0FBQyxHQUFDNUIsQ0FBQyxDQUFDQSxDQUFDLENBQUNRLE1BQUYsR0FBUyxDQUFWLENBQXZCLEVBQW9DUixDQUFDLENBQUMwRixJQUFGLENBQU8xRyxDQUFDLEdBQUNvQyxDQUFULEVBQVdRLENBQUMsR0FBQ1AsQ0FBYixFQUFlakMsQ0FBQyxHQUFDLElBQUUsQ0FBRixJQUFLSixDQUFDLEdBQUMsTUFBSW9DLENBQU4sR0FBUWhDLENBQWIsQ0FBakIsRUFBaUM0QixDQUFDLEdBQUMsSUFBRSxDQUFGLElBQUtZLENBQUMsR0FBQyxNQUFJUCxDQUFOLEdBQVFMLENBQWIsQ0FBbkMsRUFBbURoQyxDQUFDLEdBQUNJLENBQXJELEVBQXVEd0MsQ0FBQyxHQUFDWixDQUF6RCxDQUFwQyxFQUFnR3BDLENBQUMsSUFBRSxDQUFuRyxDQUFYLEtBQXFILElBQUcsUUFBTXFDLENBQVQsRUFBVzBFLEVBQUUsQ0FBQzNHLENBQUQsRUFBRzRDLENBQUgsRUFBSzVDLENBQUMsR0FBQ0ksQ0FBUCxFQUFTd0MsQ0FBVCxDQUFGLEVBQWNoRCxDQUFDLElBQUUsQ0FBakIsQ0FBWCxLQUFtQyxJQUFHLFFBQU1xQyxDQUFULEVBQVcwRSxFQUFFLENBQUMzRyxDQUFELEVBQUc0QyxDQUFILEVBQUs1QyxDQUFMLEVBQU80QyxDQUFDLEdBQUN4QyxDQUFDLElBQUU4QixDQUFDLEdBQUNVLENBQUMsR0FBQzVDLENBQUgsR0FBSyxDQUFSLENBQVYsQ0FBRixFQUF3QkosQ0FBQyxJQUFFLENBQTNCLENBQVgsS0FBNkMsSUFBRyxRQUFNcUMsQ0FBTixJQUFTLFFBQU1BLENBQWxCLEVBQW9CLFFBQU1BLENBQU4sS0FBVTdCLENBQUMsR0FBQ2MsQ0FBRixFQUFJYyxDQUFDLEdBQUNHLENBQU4sRUFBUW5CLENBQUMsQ0FBQ2dHLE1BQUYsR0FBUyxDQUFDLENBQTVCLEdBQStCLENBQUMsUUFBTS9FLENBQU4sSUFBUyxLQUFHckIsQ0FBQyxDQUFDWixDQUFDLEdBQUNJLENBQUgsQ0FBYixJQUFvQixLQUFHUSxDQUFDLENBQUNnQyxDQUFDLEdBQUNaLENBQUgsQ0FBekIsTUFBa0MyRSxFQUFFLENBQUMzRyxDQUFELEVBQUc0QyxDQUFILEVBQUt4QyxDQUFMLEVBQU80QixDQUFQLENBQUYsRUFBWSxRQUFNQyxDQUFOLEtBQVVyQyxDQUFDLElBQUUsQ0FBYixDQUE5QyxDQUEvQixFQUE4RkksQ0FBQyxHQUFDSSxDQUFoRyxFQUFrR3dDLENBQUMsR0FBQ1osQ0FBcEcsQ0FBcEIsS0FBK0gsSUFBRyxRQUFNQyxDQUFULEVBQVc7QUFBQyxZQUFHTyxDQUFDLEdBQUNFLENBQUMsQ0FBQzlDLENBQUMsR0FBQyxDQUFILENBQUgsRUFBUzZDLENBQUMsR0FBQ0MsQ0FBQyxDQUFDOUMsQ0FBQyxHQUFDLENBQUgsQ0FBWixFQUFrQndDLENBQUMsR0FBQ00sQ0FBQyxDQUFDOUMsQ0FBQyxHQUFDLENBQUgsQ0FBckIsRUFBMkJ5QyxDQUFDLEdBQUNLLENBQUMsQ0FBQzlDLENBQUMsR0FBQyxDQUFILENBQTlCLEVBQW9DMEIsQ0FBQyxHQUFDLENBQXRDLEVBQXdDLElBQUVrQixDQUFDLENBQUNoQixNQUFKLEtBQWFnQixDQUFDLENBQUNoQixNQUFGLEdBQVMsQ0FBVCxJQUFZYSxDQUFDLEdBQUNELENBQUYsRUFBSUEsQ0FBQyxHQUFDSyxDQUFOLEVBQVFuQixDQUFDLEVBQXJCLEtBQTBCZSxDQUFDLEdBQUNJLENBQUYsRUFBSUwsQ0FBQyxHQUFDSSxDQUFDLENBQUN5RSxNQUFGLENBQVMsQ0FBVCxDQUFOLEVBQWtCM0YsQ0FBQyxJQUFFLENBQS9DLEdBQWtEbUIsQ0FBQyxHQUFDRCxDQUFDLENBQUMwRSxNQUFGLENBQVMsQ0FBVCxDQUFwRCxFQUFnRTFFLENBQUMsR0FBQ0EsQ0FBQyxDQUFDMEUsTUFBRixDQUFTLENBQVQsQ0FBL0UsQ0FBeEMsRUFBb0k1RSxDQUFDLEdBQUMrQyxZQUFZLENBQUNyRixDQUFELEVBQUc0QyxDQUFILEVBQUssQ0FBQ0YsQ0FBQyxDQUFDOUMsQ0FBQyxHQUFDLENBQUgsQ0FBUCxFQUFhLENBQUM4QyxDQUFDLENBQUM5QyxDQUFDLEdBQUMsQ0FBSCxDQUFmLEVBQXFCLENBQUM4QyxDQUFDLENBQUM5QyxDQUFDLEdBQUMsQ0FBSCxDQUF2QixFQUE2QixDQUFDNEMsQ0FBOUIsRUFBZ0MsQ0FBQ0MsQ0FBakMsRUFBbUMsQ0FBQ1AsQ0FBQyxHQUFDbEMsQ0FBRCxHQUFHLENBQUwsSUFBUSxJQUFFb0MsQ0FBN0MsRUFBK0MsQ0FBQ0YsQ0FBQyxHQUFDVSxDQUFELEdBQUcsQ0FBTCxJQUFRLElBQUVQLENBQXpELENBQWxKLEVBQThNekMsQ0FBQyxJQUFFMEIsQ0FBak4sRUFBbU5nQixDQUF0TixFQUF3TixLQUFJaEIsQ0FBQyxHQUFDLENBQU4sRUFBUUEsQ0FBQyxHQUFDZ0IsQ0FBQyxDQUFDZCxNQUFaLEVBQW1CRixDQUFDLEVBQXBCO0FBQXVCTixXQUFDLENBQUMwRixJQUFGLENBQU9wRSxDQUFDLENBQUNoQixDQUFELENBQVI7QUFBdkI7QUFBb0N0QixTQUFDLEdBQUNnQixDQUFDLENBQUNBLENBQUMsQ0FBQ1EsTUFBRixHQUFTLENBQVYsQ0FBSCxFQUFnQm9CLENBQUMsR0FBQzVCLENBQUMsQ0FBQ0EsQ0FBQyxDQUFDUSxNQUFGLEdBQVMsQ0FBVixDQUFuQjtBQUFnQyxPQUF4UyxNQUE2U3FGLE9BQU8sQ0FBQ0MsR0FBUixDQUFZL0QsQ0FBWjtBQUFockM7O0FBQStyQyxXQUFNLENBQUNuRCxDQUFDLEdBQUNvQixDQUFDLENBQUNRLE1BQUwsSUFBYSxDQUFiLElBQWdCbUIsQ0FBQyxDQUFDd0UsR0FBRixJQUFRdkgsQ0FBQyxHQUFDLENBQTFCLElBQTZCb0IsQ0FBQyxDQUFDLENBQUQsQ0FBRCxLQUFPQSxDQUFDLENBQUNwQixDQUFDLEdBQUMsQ0FBSCxDQUFSLElBQWVvQixDQUFDLENBQUMsQ0FBRCxDQUFELEtBQU9BLENBQUMsQ0FBQ3BCLENBQUMsR0FBQyxDQUFILENBQXZCLEtBQStCb0IsQ0FBQyxDQUFDZ0csTUFBRixHQUFTLENBQUMsQ0FBekMsQ0FBN0IsRUFBeUVyRSxDQUFDLENBQUN5RSxXQUFGLEdBQWN0RSxDQUFDLEdBQUNsRCxDQUF6RixFQUEyRitDLENBQWpHO0FBQW1HOztBQUFBLFdBQVNvQyxlQUFULENBQXlCcEYsQ0FBekIsRUFBMkI7QUFBQ3FCLEtBQUMsQ0FBQ3JCLENBQUMsQ0FBQyxDQUFELENBQUYsQ0FBRCxLQUFVQSxDQUFDLEdBQUMsQ0FBQ0EsQ0FBRCxDQUFaO0FBQWlCLFFBQUlDLENBQUo7QUFBQSxRQUFNMEIsQ0FBTjtBQUFBLFFBQVFsQixDQUFSO0FBQUEsUUFBVTRCLENBQVY7QUFBQSxRQUFZQyxDQUFDLEdBQUMsRUFBZDtBQUFBLFFBQWlCQyxDQUFDLEdBQUN2QyxDQUFDLENBQUM2QixNQUFyQjs7QUFBNEIsU0FBSUYsQ0FBQyxHQUFDLENBQU4sRUFBUUEsQ0FBQyxHQUFDWSxDQUFWLEVBQVlaLENBQUMsRUFBYixFQUFnQjtBQUFDLFdBQUlVLENBQUMsR0FBQ3JDLENBQUMsQ0FBQzJCLENBQUQsQ0FBSCxFQUFPVyxDQUFDLElBQUUsTUFBSWYsQ0FBQyxDQUFDYyxDQUFDLENBQUMsQ0FBRCxDQUFGLENBQUwsR0FBWSxHQUFaLEdBQWdCZCxDQUFDLENBQUNjLENBQUMsQ0FBQyxDQUFELENBQUYsQ0FBakIsR0FBd0IsSUFBbEMsRUFBdUNwQyxDQUFDLEdBQUNvQyxDQUFDLENBQUNSLE1BQTNDLEVBQWtEcEIsQ0FBQyxHQUFDLENBQXhELEVBQTBEQSxDQUFDLEdBQUNSLENBQTVELEVBQThEUSxDQUFDLEVBQS9EO0FBQWtFNkIsU0FBQyxJQUFFZixDQUFDLENBQUNjLENBQUMsQ0FBQzVCLENBQUMsRUFBRixDQUFGLENBQUQsR0FBVSxHQUFWLEdBQWNjLENBQUMsQ0FBQ2MsQ0FBQyxDQUFDNUIsQ0FBQyxFQUFGLENBQUYsQ0FBZixHQUF3QixHQUF4QixHQUE0QmMsQ0FBQyxDQUFDYyxDQUFDLENBQUM1QixDQUFDLEVBQUYsQ0FBRixDQUE3QixHQUFzQyxHQUF0QyxHQUEwQ2MsQ0FBQyxDQUFDYyxDQUFDLENBQUM1QixDQUFDLEVBQUYsQ0FBRixDQUEzQyxHQUFvRCxHQUFwRCxHQUF3RGMsQ0FBQyxDQUFDYyxDQUFDLENBQUM1QixDQUFDLEVBQUYsQ0FBRixDQUF6RCxHQUFrRSxHQUFsRSxHQUFzRWMsQ0FBQyxDQUFDYyxDQUFDLENBQUM1QixDQUFELENBQUYsQ0FBdkUsR0FBOEUsR0FBakY7QUFBbEU7O0FBQXVKNEIsT0FBQyxDQUFDZ0YsTUFBRixLQUFXL0UsQ0FBQyxJQUFFLEdBQWQ7QUFBbUI7O0FBQUEsV0FBT0EsQ0FBUDtBQUFTOztBQUFBLFdBQVNZLENBQVQsR0FBWTtBQUFDLFdBQU92QixDQUFDLElBQUUsZUFBYSxPQUFPK0YsTUFBcEIsS0FBNkIvRixDQUFDLEdBQUMrRixNQUFNLENBQUNDLElBQXRDLEtBQTZDaEcsQ0FBQyxDQUFDaUcsY0FBL0MsSUFBK0RqRyxDQUF6RTtBQUEyRTs7QUFBQSxXQUFTb0UsQ0FBVCxDQUFXL0YsQ0FBWCxFQUFhO0FBQUMsV0FBT2tILE9BQU8sSUFBRUEsT0FBTyxDQUFDVyxJQUFSLENBQWE3SCxDQUFiLENBQWhCO0FBQWdDOztBQUFBLFdBQVM0RixDQUFULENBQVc1RixDQUFYLEVBQWE7QUFBQyxRQUFJQyxDQUFKO0FBQUEsUUFBTTBCLENBQUMsR0FBQzNCLENBQUMsQ0FBQzZCLE1BQVY7QUFBQSxRQUFpQnBCLENBQUMsR0FBQyxDQUFuQjtBQUFBLFFBQXFCNEIsQ0FBQyxHQUFDLENBQXZCOztBQUF5QixTQUFJcEMsQ0FBQyxHQUFDLENBQU4sRUFBUUEsQ0FBQyxHQUFDMEIsQ0FBVixFQUFZMUIsQ0FBQyxFQUFiO0FBQWdCUSxPQUFDLElBQUVULENBQUMsQ0FBQ0MsQ0FBQyxFQUFGLENBQUosRUFBVW9DLENBQUMsSUFBRXJDLENBQUMsQ0FBQ0MsQ0FBRCxDQUFkO0FBQWhCOztBQUFrQyxXQUFNLENBQUNRLENBQUMsSUFBRWtCLENBQUMsR0FBQyxDQUFKLENBQUYsRUFBU1UsQ0FBQyxJQUFFVixDQUFDLEdBQUMsQ0FBSixDQUFWLENBQU47QUFBd0I7O0FBQUEsV0FBU21FLENBQVQsQ0FBVzlGLENBQVgsRUFBYTtBQUFDLFFBQUlDLENBQUo7QUFBQSxRQUFNMEIsQ0FBTjtBQUFBLFFBQVFsQixDQUFSO0FBQUEsUUFBVTRCLENBQUMsR0FBQ3JDLENBQUMsQ0FBQzZCLE1BQWQ7QUFBQSxRQUFxQlMsQ0FBQyxHQUFDdEMsQ0FBQyxDQUFDLENBQUQsQ0FBeEI7QUFBQSxRQUE0QnVDLENBQUMsR0FBQ0QsQ0FBOUI7QUFBQSxRQUFnQ2pCLENBQUMsR0FBQ3JCLENBQUMsQ0FBQyxDQUFELENBQW5DO0FBQUEsUUFBdUN1QixDQUFDLEdBQUNGLENBQXpDOztBQUEyQyxTQUFJWixDQUFDLEdBQUMsQ0FBTixFQUFRQSxDQUFDLEdBQUM0QixDQUFWLEVBQVk1QixDQUFDLElBQUUsQ0FBZjtBQUFpQjZCLE9BQUMsSUFBRXJDLENBQUMsR0FBQ0QsQ0FBQyxDQUFDUyxDQUFELENBQUwsQ0FBRCxHQUFXNkIsQ0FBQyxHQUFDckMsQ0FBYixHQUFlQSxDQUFDLEdBQUNzQyxDQUFGLEtBQU1BLENBQUMsR0FBQ3RDLENBQVIsQ0FBZixFQUEwQm9CLENBQUMsSUFBRU0sQ0FBQyxHQUFDM0IsQ0FBQyxDQUFDUyxDQUFDLEdBQUMsQ0FBSCxDQUFMLENBQUQsR0FBYVksQ0FBQyxHQUFDTSxDQUFmLEdBQWlCQSxDQUFDLEdBQUNKLENBQUYsS0FBTUEsQ0FBQyxHQUFDSSxDQUFSLENBQTNDO0FBQWpCOztBQUF1RSxXQUFPM0IsQ0FBQyxDQUFDOEgsT0FBRixHQUFVLENBQUN4RixDQUFDLEdBQUNDLENBQUgsSUFBTSxDQUFoQixFQUFrQnZDLENBQUMsQ0FBQytILE9BQUYsR0FBVSxDQUFDMUcsQ0FBQyxHQUFDRSxDQUFILElBQU0sQ0FBbEMsRUFBb0N2QixDQUFDLENBQUNnSSxJQUFGLEdBQU8sQ0FBQzFGLENBQUMsR0FBQ0MsQ0FBSCxLQUFPbEIsQ0FBQyxHQUFDRSxDQUFULENBQWxEO0FBQThEOztBQUFBLFdBQVN5QixDQUFULENBQVdoRCxDQUFYLEVBQWFDLENBQWIsRUFBZTtBQUFDLFNBQUssQ0FBTCxLQUFTQSxDQUFULEtBQWFBLENBQUMsR0FBQyxDQUFmOztBQUFrQixTQUFJLElBQUkwQixDQUFKLEVBQU1sQixDQUFOLEVBQVE0QixDQUFSLEVBQVVDLENBQVYsRUFBWUMsQ0FBWixFQUFjbEIsQ0FBZCxFQUFnQkUsQ0FBaEIsRUFBa0JpQixDQUFsQixFQUFvQkMsQ0FBcEIsRUFBc0JDLENBQXRCLEVBQXdCQyxDQUF4QixFQUEwQkMsQ0FBMUIsRUFBNEJDLENBQTVCLEVBQThCQyxDQUE5QixFQUFnQ0MsQ0FBaEMsRUFBa0NDLENBQWxDLEVBQW9DM0MsQ0FBQyxHQUFDTCxDQUFDLENBQUM2QixNQUF4QyxFQUErQ29CLENBQUMsR0FBQ2pELENBQUMsQ0FBQyxDQUFELENBQUQsQ0FBSyxDQUFMLENBQWpELEVBQXlEa0QsQ0FBQyxHQUFDRCxDQUEzRCxFQUE2REUsQ0FBQyxHQUFDbkQsQ0FBQyxDQUFDLENBQUQsQ0FBRCxDQUFLLENBQUwsQ0FBL0QsRUFBdUVvRCxDQUFDLEdBQUNELENBQXpFLEVBQTJFRSxDQUFDLEdBQUMsSUFBRXBELENBQW5GLEVBQXFGLENBQUMsQ0FBRCxHQUFHLEVBQUVJLENBQTFGO0FBQTZGLFdBQUlzQixDQUFDLEdBQUMsQ0FBQ1ksQ0FBQyxHQUFDdkMsQ0FBQyxDQUFDSyxDQUFELENBQUosRUFBU3dCLE1BQVgsRUFBa0JTLENBQUMsR0FBQyxDQUF4QixFQUEwQkEsQ0FBQyxHQUFDWCxDQUE1QixFQUE4QlcsQ0FBQyxJQUFFLENBQWpDO0FBQW1DLGFBQUlHLENBQUMsR0FBQ0YsQ0FBQyxDQUFDRCxDQUFELENBQUgsRUFBT0ksQ0FBQyxHQUFDSCxDQUFDLENBQUNELENBQUMsR0FBQyxDQUFILENBQVYsRUFBZ0JLLENBQUMsR0FBQ0osQ0FBQyxDQUFDRCxDQUFDLEdBQUMsQ0FBSCxDQUFELEdBQU9HLENBQXpCLEVBQTJCSyxDQUFDLEdBQUNQLENBQUMsQ0FBQ0QsQ0FBQyxHQUFDLENBQUgsQ0FBRCxHQUFPSSxDQUFwQyxFQUFzQ0UsQ0FBQyxHQUFDTCxDQUFDLENBQUNELENBQUMsR0FBQyxDQUFILENBQUQsR0FBT0csQ0FBL0MsRUFBaURNLENBQUMsR0FBQ1IsQ0FBQyxDQUFDRCxDQUFDLEdBQUMsQ0FBSCxDQUFELEdBQU9JLENBQTFELEVBQTRERyxDQUFDLEdBQUNOLENBQUMsQ0FBQ0QsQ0FBQyxHQUFDLENBQUgsQ0FBRCxHQUFPRyxDQUFyRSxFQUF1RU8sQ0FBQyxHQUFDVCxDQUFDLENBQUNELENBQUMsR0FBQyxDQUFILENBQUQsR0FBT0ksQ0FBaEYsRUFBa0ZyQixDQUFDLEdBQUNwQixDQUF4RixFQUEwRixDQUFDLENBQUQsR0FBRyxFQUFFb0IsQ0FBL0Y7QUFBa0c0QixXQUFDLElBQUV4QyxDQUFDLEdBQUMsQ0FBQyxDQUFDYyxDQUFDLEdBQUM4QixDQUFDLEdBQUNoQyxDQUFMLElBQVFFLENBQVIsR0FBVXNCLENBQVYsR0FBWSxLQUFHTCxDQUFDLEdBQUMsSUFBRWpCLENBQVAsS0FBV0EsQ0FBQyxHQUFDcUIsQ0FBRixHQUFJSixDQUFDLEdBQUNHLENBQWpCLENBQWIsSUFBa0NwQixDQUFsQyxHQUFvQ2tCLENBQXhDLENBQUQsR0FBNENRLENBQUMsR0FBQ3hDLENBQTlDLEdBQWdEQSxDQUFDLEdBQUN5QyxDQUFGLEtBQU1BLENBQUMsR0FBQ3pDLENBQVIsQ0FBaEQsRUFBMkQwQyxDQUFDLElBQUVkLENBQUMsR0FBQyxDQUFDZCxDQUFDLEdBQUNBLENBQUYsR0FBSXlCLENBQUosR0FBTSxJQUFFUixDQUFGLElBQUtqQixDQUFDLEdBQUN3QixDQUFGLEdBQUlQLENBQUMsR0FBQ00sQ0FBWCxDQUFQLElBQXNCdkIsQ0FBdEIsR0FBd0JtQixDQUE1QixDQUFELEdBQWdDUyxDQUFDLEdBQUNkLENBQWxDLEdBQW9DQSxDQUFDLEdBQUNlLENBQUYsS0FBTUEsQ0FBQyxHQUFDZixDQUFSLENBQS9GO0FBQWxHO0FBQW5DO0FBQTdGOztBQUE0VSxXQUFPckMsQ0FBQyxDQUFDOEgsT0FBRixHQUFVLENBQUM3RSxDQUFDLEdBQUNDLENBQUgsSUFBTSxDQUFoQixFQUFrQmxELENBQUMsQ0FBQytILE9BQUYsR0FBVSxDQUFDNUUsQ0FBQyxHQUFDQyxDQUFILElBQU0sQ0FBbEMsRUFBb0NwRCxDQUFDLENBQUNpSSxJQUFGLEdBQU8vRSxDQUEzQyxFQUE2Q2xELENBQUMsQ0FBQ3dFLEtBQUYsR0FBUXZCLENBQUMsR0FBQ0MsQ0FBdkQsRUFBeURsRCxDQUFDLENBQUNrSSxHQUFGLEdBQU05RSxDQUEvRCxFQUFpRXBELENBQUMsQ0FBQ3lFLE1BQUYsR0FBU3RCLENBQUMsR0FBQ0MsQ0FBNUUsRUFBOEVwRCxDQUFDLENBQUNnSSxJQUFGLEdBQU8sQ0FBQy9FLENBQUMsR0FBQ0MsQ0FBSCxLQUFPQyxDQUFDLEdBQUNDLENBQVQsQ0FBNUY7QUFBd0c7O0FBQUEsV0FBUytFLENBQVQsQ0FBV25JLENBQVgsRUFBYUMsQ0FBYixFQUFlO0FBQUMsV0FBT0EsQ0FBQyxDQUFDNEIsTUFBRixHQUFTN0IsQ0FBQyxDQUFDNkIsTUFBbEI7QUFBeUI7O0FBQUEsV0FBU2dFLENBQVQsQ0FBVzdGLENBQVgsRUFBYUMsQ0FBYixFQUFlO0FBQUMsUUFBSTBCLENBQUMsR0FBQzNCLENBQUMsQ0FBQ2dJLElBQUYsSUFBUWxDLENBQUMsQ0FBQzlGLENBQUQsQ0FBZjtBQUFBLFFBQW1CUyxDQUFDLEdBQUNSLENBQUMsQ0FBQytILElBQUYsSUFBUWxDLENBQUMsQ0FBQzdGLENBQUQsQ0FBOUI7QUFBa0MsV0FBT1UsSUFBSSxDQUFDTyxHQUFMLENBQVNULENBQUMsR0FBQ2tCLENBQVgsSUFBYyxDQUFDQSxDQUFDLEdBQUNsQixDQUFILElBQU0sRUFBcEIsR0FBdUJSLENBQUMsQ0FBQzZILE9BQUYsR0FBVTlILENBQUMsQ0FBQzhILE9BQVosSUFBcUI3SCxDQUFDLENBQUM4SCxPQUFGLEdBQVUvSCxDQUFDLENBQUMrSCxPQUF4RCxHQUFnRXRILENBQUMsR0FBQ2tCLENBQXpFO0FBQTJFOztBQUFBLFdBQVNnRSxDQUFULENBQVczRixDQUFYLEVBQWFDLENBQWIsRUFBZTtBQUFDLFFBQUkwQixDQUFKO0FBQUEsUUFBTWxCLENBQU47QUFBQSxRQUFRNEIsQ0FBQyxHQUFDckMsQ0FBQyxDQUFDNEQsS0FBRixDQUFRLENBQVIsQ0FBVjtBQUFBLFFBQXFCdEIsQ0FBQyxHQUFDdEMsQ0FBQyxDQUFDNkIsTUFBekI7QUFBQSxRQUFnQ1UsQ0FBQyxHQUFDRCxDQUFDLEdBQUMsQ0FBcEM7O0FBQXNDLFNBQUlyQyxDQUFDLElBQUUsQ0FBSCxFQUFLMEIsQ0FBQyxHQUFDLENBQVgsRUFBYUEsQ0FBQyxHQUFDVyxDQUFmLEVBQWlCWCxDQUFDLEVBQWxCO0FBQXFCbEIsT0FBQyxHQUFDLENBQUNrQixDQUFDLEdBQUMxQixDQUFILElBQU1zQyxDQUFSLEVBQVV2QyxDQUFDLENBQUMyQixDQUFDLEVBQUYsQ0FBRCxHQUFPVSxDQUFDLENBQUM1QixDQUFELENBQWxCLEVBQXNCVCxDQUFDLENBQUMyQixDQUFELENBQUQsR0FBS1UsQ0FBQyxDQUFDLElBQUU1QixDQUFILENBQTVCO0FBQXJCO0FBQXVEOztBQUFBLFdBQVM0QyxDQUFULENBQVdyRCxDQUFYLEVBQWFDLENBQWIsRUFBZTBCLENBQWYsRUFBaUJsQixDQUFqQixFQUFtQjRCLENBQW5CLEVBQXFCO0FBQUMsUUFBSUMsQ0FBSjtBQUFBLFFBQU1DLENBQU47QUFBQSxRQUFRbEIsQ0FBUjtBQUFBLFFBQVVFLENBQVY7QUFBQSxRQUFZaUIsQ0FBQyxHQUFDeEMsQ0FBQyxDQUFDNkIsTUFBaEI7QUFBQSxRQUF1QlksQ0FBQyxHQUFDLENBQXpCO0FBQUEsUUFBMkJDLENBQUMsR0FBQ0YsQ0FBQyxHQUFDLENBQS9COztBQUFpQyxTQUFJYixDQUFDLElBQUUsQ0FBSCxFQUFLWSxDQUFDLEdBQUMsQ0FBWCxFQUFhQSxDQUFDLEdBQUNDLENBQWYsRUFBaUJELENBQUMsSUFBRSxDQUFwQjtBQUFzQmhCLE9BQUMsR0FBQ3ZCLENBQUMsQ0FBQ3NDLENBQUMsR0FBQyxDQUFDQyxDQUFDLEdBQUNaLENBQUgsSUFBTWUsQ0FBVCxDQUFELElBQWN6QyxDQUFDLENBQUNzQyxDQUFELENBQUQsR0FBSzlCLENBQW5CLENBQUYsRUFBd0JZLENBQUMsR0FBQ3JCLENBQUMsQ0FBQyxJQUFFc0MsQ0FBSCxDQUFELElBQVFyQyxDQUFDLENBQUNzQyxDQUFDLEdBQUMsQ0FBSCxDQUFELEdBQU9GLENBQWYsQ0FBMUIsRUFBNENJLENBQUMsSUFBRVEsQ0FBQyxDQUFDNUIsQ0FBQyxHQUFDQSxDQUFGLEdBQUlFLENBQUMsR0FBQ0EsQ0FBUCxDQUFoRDtBQUF0Qjs7QUFBZ0YsV0FBT2tCLENBQVA7QUFBUzs7QUFBQSxXQUFTa0UsQ0FBVCxDQUFXM0csQ0FBWCxFQUFhQyxDQUFiLEVBQWUwQixDQUFmLEVBQWlCO0FBQUMsUUFBSWxCLENBQUo7QUFBQSxRQUFNNEIsQ0FBTjtBQUFBLFFBQVFDLENBQVI7QUFBQSxRQUFVQyxDQUFDLEdBQUN2QyxDQUFDLENBQUM2QixNQUFkO0FBQUEsUUFBcUJSLENBQUMsR0FBQ3VFLENBQUMsQ0FBQzVGLENBQUQsQ0FBeEI7QUFBQSxRQUE0QnVCLENBQUMsR0FBQ3FFLENBQUMsQ0FBQzNGLENBQUQsQ0FBL0I7QUFBQSxRQUFtQ3VDLENBQUMsR0FBQ2pCLENBQUMsQ0FBQyxDQUFELENBQUQsR0FBS0YsQ0FBQyxDQUFDLENBQUQsQ0FBM0M7QUFBQSxRQUErQ29CLENBQUMsR0FBQ2xCLENBQUMsQ0FBQyxDQUFELENBQUQsR0FBS0YsQ0FBQyxDQUFDLENBQUQsQ0FBdkQ7QUFBQSxRQUEyRHFCLENBQUMsR0FBQ1csQ0FBQyxDQUFDckQsQ0FBRCxFQUFHQyxDQUFILEVBQUssQ0FBTCxFQUFPdUMsQ0FBUCxFQUFTQyxDQUFULENBQTlEO0FBQUEsUUFBMEVFLENBQUMsR0FBQyxDQUE1RTs7QUFBOEUsU0FBSUwsQ0FBQyxHQUFDLENBQU4sRUFBUUEsQ0FBQyxHQUFDQyxDQUFWLEVBQVlELENBQUMsSUFBRSxDQUFmO0FBQWlCLE9BQUNELENBQUMsR0FBQ2dCLENBQUMsQ0FBQ3JELENBQUQsRUFBR0MsQ0FBSCxFQUFLcUMsQ0FBQyxHQUFDLENBQVAsRUFBU0UsQ0FBVCxFQUFXQyxDQUFYLENBQUosSUFBbUJDLENBQW5CLEtBQXVCQSxDQUFDLEdBQUNMLENBQUYsRUFBSU0sQ0FBQyxHQUFDTCxDQUE3QjtBQUFqQjs7QUFBaUQsUUFBR1gsQ0FBSCxFQUFLLEtBQUlELGNBQWMsQ0FBQ2pCLENBQUMsR0FBQ1QsQ0FBQyxDQUFDNEQsS0FBRixDQUFRLENBQVIsQ0FBSCxDQUFkLEVBQTZCdEIsQ0FBQyxHQUFDLENBQW5DLEVBQXFDQSxDQUFDLEdBQUNDLENBQXZDLEVBQXlDRCxDQUFDLElBQUUsQ0FBNUM7QUFBOEMsT0FBQ0QsQ0FBQyxHQUFDZ0IsQ0FBQyxDQUFDNUMsQ0FBRCxFQUFHUixDQUFILEVBQUtxQyxDQUFDLEdBQUMsQ0FBUCxFQUFTRSxDQUFULEVBQVdDLENBQVgsQ0FBSixJQUFtQkMsQ0FBbkIsS0FBdUJBLENBQUMsR0FBQ0wsQ0FBRixFQUFJTSxDQUFDLEdBQUMsQ0FBQ0wsQ0FBOUI7QUFBOUM7QUFBK0UsV0FBT0ssQ0FBQyxHQUFDLENBQVQ7QUFBVzs7QUFBQSxXQUFTNkQsQ0FBVCxDQUFXeEcsQ0FBWCxFQUFhQyxDQUFiLEVBQWUwQixDQUFmLEVBQWlCO0FBQUMsU0FBSSxJQUFJbEIsQ0FBSixFQUFNNEIsQ0FBTixFQUFRQyxDQUFSLEVBQVVDLENBQVYsRUFBWWxCLENBQVosRUFBY0UsQ0FBZCxFQUFnQmlCLENBQUMsR0FBQ3hDLENBQUMsQ0FBQzZCLE1BQXBCLEVBQTJCWSxDQUFDLEdBQUMsSUFBN0IsRUFBa0NDLENBQUMsR0FBQyxDQUFwQyxFQUFzQ0MsQ0FBQyxHQUFDLENBQTVDLEVBQThDLENBQUMsQ0FBRCxHQUFHLEVBQUVILENBQW5EO0FBQXNELFdBQUlqQixDQUFDLEdBQUMsQ0FBQ2QsQ0FBQyxHQUFDVCxDQUFDLENBQUN3QyxDQUFELENBQUosRUFBU1gsTUFBWCxFQUFrQlIsQ0FBQyxHQUFDLENBQXhCLEVBQTBCQSxDQUFDLEdBQUNFLENBQTVCLEVBQThCRixDQUFDLElBQUUsQ0FBakM7QUFBbUNnQixTQUFDLEdBQUM1QixDQUFDLENBQUNZLENBQUQsQ0FBRCxHQUFLcEIsQ0FBUCxFQUFTcUMsQ0FBQyxHQUFDN0IsQ0FBQyxDQUFDWSxDQUFDLEdBQUMsQ0FBSCxDQUFELEdBQU9NLENBQWxCLEVBQW9CLENBQUNZLENBQUMsR0FBQ1UsQ0FBQyxDQUFDWixDQUFDLEdBQUNBLENBQUYsR0FBSUMsQ0FBQyxHQUFDQSxDQUFQLENBQUosSUFBZUcsQ0FBZixLQUFtQkEsQ0FBQyxHQUFDRixDQUFGLEVBQUlHLENBQUMsR0FBQ2pDLENBQUMsQ0FBQ1ksQ0FBRCxDQUFQLEVBQVdzQixDQUFDLEdBQUNsQyxDQUFDLENBQUNZLENBQUMsR0FBQyxDQUFILENBQWpDLENBQXBCO0FBQW5DO0FBQXREOztBQUFxSixXQUFNLENBQUNxQixDQUFELEVBQUdDLENBQUgsQ0FBTjtBQUFZOztBQUFBLFdBQVN5RixDQUFULENBQVdwSSxDQUFYLEVBQWFDLENBQWIsRUFBZTBCLENBQWYsRUFBaUJsQixDQUFqQixFQUFtQjRCLENBQW5CLEVBQXFCQyxDQUFyQixFQUF1QjtBQUFDLFFBQUlDLENBQUo7QUFBQSxRQUFNbEIsQ0FBTjtBQUFBLFFBQVFFLENBQVI7QUFBQSxRQUFVaUIsQ0FBVjtBQUFBLFFBQVlDLENBQUMsR0FBQ3hDLENBQUMsQ0FBQzRCLE1BQWhCO0FBQUEsUUFBdUJhLENBQUMsR0FBQyxDQUF6QjtBQUFBLFFBQTJCQyxDQUFDLEdBQUNoQyxJQUFJLENBQUMwSCxHQUFMLENBQVNySSxDQUFDLENBQUNnSSxJQUFGLElBQVFsQyxDQUFDLENBQUM5RixDQUFELENBQWxCLEVBQXNCQyxDQUFDLENBQUMwQixDQUFELENBQUQsQ0FBS3FHLElBQUwsSUFBV2xDLENBQUMsQ0FBQzdGLENBQUMsQ0FBQzBCLENBQUQsQ0FBRixDQUFsQyxJQUEwQ2xCLENBQXZFO0FBQUEsUUFBeUVtQyxDQUFDLEdBQUMsSUFBM0U7QUFBQSxRQUFnRkMsQ0FBQyxHQUFDN0MsQ0FBQyxDQUFDOEgsT0FBRixHQUFVekYsQ0FBNUY7QUFBQSxRQUE4RlMsQ0FBQyxHQUFDOUMsQ0FBQyxDQUFDK0gsT0FBRixHQUFVekYsQ0FBMUc7O0FBQTRHLFNBQUlDLENBQUMsR0FBQ1osQ0FBTixFQUFRWSxDQUFDLEdBQUNFLENBQUYsSUFBSyxFQUFFLENBQUN4QyxDQUFDLENBQUNzQyxDQUFELENBQUQsQ0FBS3lGLElBQUwsSUFBV2xDLENBQUMsQ0FBQzdGLENBQUMsQ0FBQ3NDLENBQUQsQ0FBRixDQUFiLElBQXFCSSxDQUF2QixDQUFiLEVBQXVDSixDQUFDLEVBQXhDO0FBQTJDbEIsT0FBQyxHQUFDcEIsQ0FBQyxDQUFDc0MsQ0FBRCxDQUFELENBQUt1RixPQUFMLEdBQWFqRixDQUFmLEVBQWlCdEIsQ0FBQyxHQUFDdEIsQ0FBQyxDQUFDc0MsQ0FBRCxDQUFELENBQUt3RixPQUFMLEdBQWFqRixDQUFoQyxFQUFrQyxDQUFDTixDQUFDLEdBQUNTLENBQUMsQ0FBQzVCLENBQUMsR0FBQ0EsQ0FBRixHQUFJRSxDQUFDLEdBQUNBLENBQVAsQ0FBSixJQUFlcUIsQ0FBZixLQUFtQkYsQ0FBQyxHQUFDSCxDQUFGLEVBQUlLLENBQUMsR0FBQ0osQ0FBekIsQ0FBbEM7QUFBM0M7O0FBQXlHLFdBQU9BLENBQUMsR0FBQ3ZDLENBQUMsQ0FBQ3lDLENBQUQsQ0FBSCxFQUFPekMsQ0FBQyxDQUFDcUksTUFBRixDQUFTNUYsQ0FBVCxFQUFXLENBQVgsQ0FBUCxFQUFxQkYsQ0FBNUI7QUFBOEI7O0FBQUEsV0FBUzZELENBQVQsQ0FBV3JHLENBQVgsRUFBYUMsQ0FBYixFQUFlO0FBQUMsUUFBSTBCLENBQUo7QUFBQSxRQUFNbEIsQ0FBTjtBQUFBLFFBQVE0QixDQUFSO0FBQUEsUUFBVUMsQ0FBVjtBQUFBLFFBQVlDLENBQVo7QUFBQSxRQUFjbEIsQ0FBZDtBQUFBLFFBQWdCRSxDQUFoQjtBQUFBLFFBQWtCaUIsQ0FBbEI7QUFBQSxRQUFvQkMsQ0FBcEI7QUFBQSxRQUFzQkMsQ0FBdEI7QUFBQSxRQUF3QkMsQ0FBeEI7QUFBQSxRQUEwQkMsQ0FBMUI7QUFBQSxRQUE0QkMsQ0FBNUI7QUFBQSxRQUE4QkMsQ0FBOUI7QUFBQSxRQUFnQ0MsQ0FBQyxHQUFDLENBQWxDO0FBQUEsUUFBb0NDLENBQUMsR0FBQ2hELENBQUMsQ0FBQzZCLE1BQXhDO0FBQUEsUUFBK0N4QixDQUFDLEdBQUNKLENBQUMsSUFBRSxDQUFDK0MsQ0FBQyxHQUFDLENBQUgsSUFBTSxDQUFSLENBQWxEOztBQUE2RCxTQUFJSCxDQUFDLEdBQUMsQ0FBTixFQUFRQSxDQUFDLEdBQUNHLENBQVYsRUFBWUgsQ0FBQyxJQUFFLENBQWY7QUFBaUIsV0FBSUUsQ0FBQyxJQUFFMUMsQ0FBUCxFQUFTLFVBQVEwQyxDQUFqQjtBQUFvQnBCLFNBQUMsR0FBQzNCLENBQUMsQ0FBQzZDLENBQUMsR0FBQyxDQUFILENBQUgsRUFBU3BDLENBQUMsR0FBQ1QsQ0FBQyxDQUFDNkMsQ0FBQyxHQUFDLENBQUgsQ0FBWixFQUFrQlIsQ0FBQyxHQUFDckMsQ0FBQyxDQUFDNkMsQ0FBRCxDQUFyQixFQUF5QlAsQ0FBQyxHQUFDdEMsQ0FBQyxDQUFDNkMsQ0FBQyxHQUFDLENBQUgsQ0FBNUIsRUFBa0NOLENBQUMsR0FBQ3ZDLENBQUMsQ0FBQzZDLENBQUMsR0FBQyxDQUFILENBQXJDLEVBQTJDeEIsQ0FBQyxHQUFDckIsQ0FBQyxDQUFDNkMsQ0FBQyxHQUFDLENBQUgsQ0FBOUMsRUFBb0R0QixDQUFDLEdBQUN2QixDQUFDLENBQUM2QyxDQUFDLEdBQUMsQ0FBSCxDQUF2RCxFQUE2REwsQ0FBQyxHQUFDeEMsQ0FBQyxDQUFDNkMsQ0FBQyxHQUFDLENBQUgsQ0FBaEUsRUFBc0VKLENBQUMsR0FBQ2QsQ0FBQyxHQUFDLENBQUNVLENBQUMsR0FBQ1YsQ0FBSCxLQUFPbUIsQ0FBQyxHQUFDLEtBQUcsQ0FBQ25DLElBQUksQ0FBQzRILEtBQUwsQ0FBV3hGLENBQVgsS0FBZSxDQUFoQixJQUFtQixDQUF0QixDQUFULENBQTFFLEVBQTZHTixDQUFDLElBQUUsQ0FBQyxDQUFDRSxDQUFDLEdBQUNOLENBQUMsR0FBQyxDQUFDRSxDQUFDLEdBQUNGLENBQUgsSUFBTVMsQ0FBWCxJQUFjTCxDQUFmLElBQWtCSyxDQUFsSSxFQUFvSUgsQ0FBQyxJQUFFLENBQUNKLENBQUMsR0FBQyxDQUFDaEIsQ0FBQyxHQUFDZ0IsQ0FBSCxJQUFNTyxDQUFSLEdBQVVILENBQVgsSUFBY0csQ0FBckosRUFBdUpKLENBQUMsR0FBQ2pDLENBQUMsR0FBQyxDQUFDNkIsQ0FBQyxHQUFDN0IsQ0FBSCxJQUFNcUMsQ0FBakssRUFBbUtKLENBQUMsSUFBRSxDQUFDLENBQUNFLENBQUMsR0FBQ04sQ0FBQyxHQUFDLENBQUNqQixDQUFDLEdBQUNpQixDQUFILElBQU1RLENBQVgsSUFBY0osQ0FBZixJQUFrQkksQ0FBeEwsRUFBMExGLENBQUMsSUFBRSxDQUFDdkIsQ0FBQyxHQUFDLENBQUNtQixDQUFDLEdBQUNuQixDQUFILElBQU15QixDQUFSLEdBQVVGLENBQVgsSUFBY0UsQ0FBM00sRUFBNk05QyxDQUFDLENBQUNzSSxNQUFGLENBQVN6RixDQUFULEVBQVcsQ0FBWCxFQUFhbEIsQ0FBQyxHQUFDLENBQUNVLENBQUMsR0FBQ1YsQ0FBSCxJQUFNbUIsQ0FBckIsRUFBdUJyQyxDQUFDLEdBQUMsQ0FBQzZCLENBQUMsR0FBQzdCLENBQUgsSUFBTXFDLENBQS9CLEVBQWlDTCxDQUFqQyxFQUFtQ0MsQ0FBbkMsRUFBcUNELENBQUMsR0FBQyxDQUFDRSxDQUFDLEdBQUNGLENBQUgsSUFBTUssQ0FBN0MsRUFBK0NKLENBQUMsR0FBQyxDQUFDRSxDQUFDLEdBQUNGLENBQUgsSUFBTUksQ0FBdkQsRUFBeURILENBQXpELEVBQTJEQyxDQUEzRCxFQUE2REwsQ0FBQyxHQUFDLENBQUNoQixDQUFDLEdBQUNnQixDQUFILElBQU1PLENBQXJFLEVBQXVFekIsQ0FBQyxHQUFDLENBQUNtQixDQUFDLEdBQUNuQixDQUFILElBQU15QixDQUEvRSxDQUE3TSxFQUErUkQsQ0FBQyxJQUFFLENBQWxTLEVBQW9TRyxDQUFDLElBQUUsQ0FBdlMsRUFBeVNELENBQUMsRUFBMVM7QUFBcEI7QUFBakI7O0FBQWtWLFdBQU8vQyxDQUFQO0FBQVM7O0FBQUEsV0FBU2dHLENBQVQsQ0FBV2hHLENBQVgsRUFBYUMsQ0FBYixFQUFlMEIsQ0FBZixFQUFpQmxCLENBQWpCLEVBQW1CNEIsQ0FBbkIsRUFBcUI7QUFBQyxRQUFJQyxDQUFKO0FBQUEsUUFBTUMsQ0FBTjtBQUFBLFFBQVFsQixDQUFSO0FBQUEsUUFBVUUsQ0FBVjtBQUFBLFFBQVlpQixDQUFaO0FBQUEsUUFBY0MsQ0FBZDtBQUFBLFFBQWdCQyxDQUFoQjtBQUFBLFFBQWtCQyxDQUFDLEdBQUMxQyxDQUFDLENBQUM0QixNQUFGLEdBQVM3QixDQUFDLENBQUM2QixNQUEvQjtBQUFBLFFBQXNDZSxDQUFDLEdBQUMsSUFBRUQsQ0FBRixHQUFJMUMsQ0FBSixHQUFNRCxDQUE5QztBQUFBLFFBQWdENkMsQ0FBQyxHQUFDLElBQUVGLENBQUYsR0FBSTNDLENBQUosR0FBTUMsQ0FBeEQ7QUFBQSxRQUEwRDZDLENBQUMsR0FBQyxDQUE1RDtBQUFBLFFBQThEQyxDQUFDLEdBQUMsaUJBQWV0QyxDQUFmLEdBQWlCMEgsQ0FBakIsR0FBbUJ0QyxDQUFuRjtBQUFBLFFBQXFGeEYsQ0FBQyxHQUFDLGVBQWFJLENBQWIsR0FBZSxDQUFmLEdBQWlCLFlBQVUsT0FBT0EsQ0FBakIsR0FBbUJBLENBQW5CLEdBQXFCLEVBQTdIO0FBQUEsUUFBZ0l3QyxDQUFDLEdBQUNKLENBQUMsQ0FBQ2hCLE1BQXBJO0FBQUEsUUFBMklxQixDQUFDLEdBQUMsb0JBQWlCdkIsQ0FBakIsS0FBb0JBLENBQUMsQ0FBQ29GLElBQXRCLEdBQTJCcEYsQ0FBQyxDQUFDaUMsS0FBRixDQUFRLENBQVIsQ0FBM0IsR0FBc0MsQ0FBQ2pDLENBQUQsQ0FBbkw7QUFBQSxRQUF1THdCLENBQUMsR0FBQyxjQUFZRCxDQUFDLENBQUMsQ0FBRCxDQUFiLElBQWtCQSxDQUFDLENBQUMsQ0FBRCxDQUFELEdBQUssQ0FBaE47QUFBQSxRQUFrTkUsQ0FBQyxHQUFDLFVBQVF6QixDQUE1Tjs7QUFBOE4sUUFBR2tCLENBQUMsQ0FBQyxDQUFELENBQUosRUFBUTtBQUFDLFVBQUcsSUFBRUQsQ0FBQyxDQUFDZixNQUFKLEtBQWE3QixDQUFDLENBQUN3SSxJQUFGLENBQU96RixDQUFQLEdBQVU5QyxDQUFDLENBQUN1SSxJQUFGLENBQU96RixDQUFQLENBQVYsRUFBb0JILENBQUMsQ0FBQ29GLElBQUYsSUFBUWhGLENBQUMsQ0FBQ0osQ0FBRCxDQUE3QixFQUFpQ0MsQ0FBQyxDQUFDbUYsSUFBRixJQUFRaEYsQ0FBQyxDQUFDSCxDQUFELENBQTFDLEVBQThDSixDQUFDLEdBQUNHLENBQUMsQ0FBQ2tGLE9BQUYsR0FBVWpGLENBQUMsQ0FBQ2lGLE9BQTVELEVBQW9FcEYsQ0FBQyxHQUFDRSxDQUFDLENBQUNtRixPQUFGLEdBQVVsRixDQUFDLENBQUNrRixPQUFsRixFQUEwRmhGLENBQUMsS0FBRzhDLENBQTNHLENBQUgsRUFBaUgsS0FBSTVDLENBQUMsR0FBQyxDQUFOLEVBQVFBLENBQUMsR0FBQ0osQ0FBQyxDQUFDaEIsTUFBWixFQUFtQm9CLENBQUMsRUFBcEI7QUFBdUJMLFNBQUMsQ0FBQzBGLE1BQUYsQ0FBU3JGLENBQVQsRUFBVyxDQUFYLEVBQWFtRixDQUFDLENBQUN2RixDQUFDLENBQUNJLENBQUQsQ0FBRixFQUFNTCxDQUFOLEVBQVFLLENBQVIsRUFBVTVDLENBQVYsRUFBWW9DLENBQVosRUFBY0MsQ0FBZCxDQUFkO0FBQXZCO0FBQXVELFVBQUdDLENBQUgsRUFBSyxLQUFJQSxDQUFDLEdBQUMsQ0FBRixLQUFNQSxDQUFDLEdBQUMsQ0FBQ0EsQ0FBVCxHQUFZQyxDQUFDLENBQUMsQ0FBRCxDQUFELENBQUtmLE1BQUwsR0FBWWdCLENBQUMsQ0FBQyxDQUFELENBQUQsQ0FBS2hCLE1BQWpCLElBQXlCd0UsQ0FBQyxDQUFDeEQsQ0FBQyxDQUFDLENBQUQsQ0FBRixFQUFNLENBQUNELENBQUMsQ0FBQyxDQUFELENBQUQsQ0FBS2YsTUFBTCxHQUFZZ0IsQ0FBQyxDQUFDLENBQUQsQ0FBRCxDQUFLaEIsTUFBbEIsSUFBMEIsQ0FBMUIsR0FBNEIsQ0FBbEMsQ0FBdEMsRUFBMkVvQixDQUFDLEdBQUNKLENBQUMsQ0FBQ2hCLE1BQW5GLEVBQTBGaUIsQ0FBQyxHQUFDSCxDQUE1RjtBQUErRkMsU0FBQyxDQUFDSyxDQUFELENBQUQsQ0FBSytFLElBQUwsSUFBV2xDLENBQUMsQ0FBQ2xELENBQUMsQ0FBQ0ssQ0FBRCxDQUFGLENBQVosRUFBbUIxQixDQUFDLEdBQUMsQ0FBQ0YsQ0FBQyxHQUFDbUYsQ0FBQyxDQUFDM0QsQ0FBRCxFQUFHRCxDQUFDLENBQUNLLENBQUQsQ0FBRCxDQUFLNkUsT0FBUixFQUFnQmxGLENBQUMsQ0FBQ0ssQ0FBRCxDQUFELENBQUs4RSxPQUFyQixDQUFKLEVBQW1DLENBQW5DLENBQXJCLEVBQTJEdkYsQ0FBQyxHQUFDbkIsQ0FBQyxDQUFDLENBQUQsQ0FBOUQsRUFBa0V3QixDQUFDLENBQUNJLENBQUMsRUFBRixDQUFELEdBQU8sQ0FBQzFCLENBQUQsRUFBR2lCLENBQUgsRUFBS2pCLENBQUwsRUFBT2lCLENBQVAsRUFBU2pCLENBQVQsRUFBV2lCLENBQVgsRUFBYWpCLENBQWIsRUFBZWlCLENBQWYsQ0FBekUsRUFBMkZLLENBQUMsQ0FBQzRFLFdBQUYsSUFBZSxDQUExRyxFQUE0RzNFLENBQUMsRUFBN0c7QUFBL0Y7O0FBQStNLFdBQUlHLENBQUMsR0FBQyxDQUFOLEVBQVFBLENBQUMsR0FBQ2pELENBQUMsQ0FBQzZCLE1BQVosRUFBbUJvQixDQUFDLEVBQXBCO0FBQXVCWCxTQUFDLEdBQUNyQyxDQUFDLENBQUNnRCxDQUFELENBQUgsRUFBT1YsQ0FBQyxHQUFDdkMsQ0FBQyxDQUFDaUQsQ0FBRCxDQUFWLEVBQWMsQ0FBQ04sQ0FBQyxHQUFDTCxDQUFDLENBQUNULE1BQUYsR0FBU1UsQ0FBQyxDQUFDVixNQUFkLElBQXNCLENBQXRCLEdBQXdCd0UsQ0FBQyxDQUFDL0QsQ0FBRCxFQUFHLENBQUNLLENBQUQsR0FBRyxDQUFILEdBQUssQ0FBUixDQUF6QixHQUFvQyxJQUFFQSxDQUFGLElBQUswRCxDQUFDLENBQUM5RCxDQUFELEVBQUdJLENBQUMsR0FBQyxDQUFGLEdBQUksQ0FBUCxDQUF4RCxFQUFrRVEsQ0FBQyxJQUFFLENBQUMsQ0FBRCxLQUFLZCxDQUFSLElBQVcsQ0FBQ0UsQ0FBQyxDQUFDVCxRQUFkLElBQXdCSixjQUFjLENBQUNhLENBQUQsQ0FBeEcsRUFBNEcsQ0FBQ1osQ0FBQyxHQUFDdUIsQ0FBQyxDQUFDRCxDQUFELENBQUQsSUFBTSxNQUFJQyxDQUFDLENBQUNELENBQUQsQ0FBWCxHQUFlQyxDQUFDLENBQUNELENBQUQsQ0FBaEIsR0FBb0IsTUFBdkIsTUFBaUNWLENBQUMsQ0FBQzhFLE1BQUYsSUFBVTFHLElBQUksQ0FBQ08sR0FBTCxDQUFTcUIsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFLQSxDQUFDLENBQUNBLENBQUMsQ0FBQ1YsTUFBRixHQUFTLENBQVYsQ0FBZixJQUE2QixFQUE3QixJQUFpQ2xCLElBQUksQ0FBQ08sR0FBTCxDQUFTcUIsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFLQSxDQUFDLENBQUNBLENBQUMsQ0FBQ1YsTUFBRixHQUFTLENBQVYsQ0FBZixJQUE2QixFQUF4RSxHQUEyRSxXQUFTRixDQUFULElBQVksVUFBUUEsQ0FBcEIsSUFBdUJ1QixDQUFDLENBQUNELENBQUQsQ0FBRCxHQUFLdEIsQ0FBQyxHQUFDZ0YsQ0FBQyxDQUFDcEUsQ0FBRCxFQUFHRCxDQUFILEVBQUssQ0FBQ1csQ0FBRCxJQUFJLENBQUMsQ0FBRCxLQUFLWixDQUFkLENBQVIsRUFBeUJWLENBQUMsR0FBQyxDQUFGLEtBQU13QixDQUFDLEdBQUMsQ0FBQyxDQUFILEVBQUt6QixjQUFjLENBQUNhLENBQUQsQ0FBbkIsRUFBdUJaLENBQUMsR0FBQyxDQUFDQSxDQUFoQyxDQUF6QixFQUE0RGdFLENBQUMsQ0FBQ3BELENBQUQsRUFBRyxJQUFFWixDQUFMLENBQXBGLElBQTZGLGNBQVlBLENBQVosS0FBZ0JzQixDQUFDLElBQUV0QixDQUFDLEdBQUMsQ0FBTCxJQUFRRCxjQUFjLENBQUNhLENBQUQsQ0FBdEIsRUFBMEJvRCxDQUFDLENBQUNwRCxDQUFELEVBQUcsS0FBR1osQ0FBQyxHQUFDLENBQUYsR0FBSSxDQUFDQSxDQUFMLEdBQU9BLENBQVYsQ0FBSCxDQUEzQyxDQUF4SyxHQUFxTyxDQUFDd0IsQ0FBRCxLQUFLLFdBQVN4QixDQUFULElBQVloQixJQUFJLENBQUNPLEdBQUwsQ0FBU29CLENBQUMsQ0FBQyxDQUFELENBQUQsR0FBS0MsQ0FBQyxDQUFDLENBQUQsQ0FBZixJQUFvQjVCLElBQUksQ0FBQ08sR0FBTCxDQUFTb0IsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFLQyxDQUFDLENBQUMsQ0FBRCxDQUFmLENBQXBCLEdBQXdDNUIsSUFBSSxDQUFDTyxHQUFMLENBQVNvQixDQUFDLENBQUNBLENBQUMsQ0FBQ1QsTUFBRixHQUFTLENBQVYsQ0FBRCxHQUFjVSxDQUFDLENBQUNBLENBQUMsQ0FBQ1YsTUFBRixHQUFTLENBQVYsQ0FBeEIsQ0FBeEMsR0FBOEVsQixJQUFJLENBQUNPLEdBQUwsQ0FBU29CLENBQUMsQ0FBQ0EsQ0FBQyxDQUFDVCxNQUFGLEdBQVMsQ0FBVixDQUFELEdBQWNVLENBQUMsQ0FBQ0EsQ0FBQyxDQUFDVixNQUFGLEdBQVMsQ0FBVixDQUF4QixDQUE5RSxHQUFvSGxCLElBQUksQ0FBQ08sR0FBTCxDQUFTb0IsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFLQyxDQUFDLENBQUNBLENBQUMsQ0FBQ1YsTUFBRixHQUFTLENBQVYsQ0FBZixJQUE2QmxCLElBQUksQ0FBQ08sR0FBTCxDQUFTb0IsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFLQyxDQUFDLENBQUNBLENBQUMsQ0FBQ1YsTUFBRixHQUFTLENBQVYsQ0FBZixDQUE3QixHQUEwRGxCLElBQUksQ0FBQ08sR0FBTCxDQUFTb0IsQ0FBQyxDQUFDQSxDQUFDLENBQUNULE1BQUYsR0FBUyxDQUFWLENBQUQsR0FBY1UsQ0FBQyxDQUFDLENBQUQsQ0FBeEIsQ0FBMUQsR0FBdUY1QixJQUFJLENBQUNPLEdBQUwsQ0FBU29CLENBQUMsQ0FBQ0EsQ0FBQyxDQUFDVCxNQUFGLEdBQVMsQ0FBVixDQUFELEdBQWNVLENBQUMsQ0FBQyxDQUFELENBQXhCLENBQXZOLElBQXFQWixDQUFDLEdBQUMsQ0FBNVAsS0FBZ1FELGNBQWMsQ0FBQ2EsQ0FBRCxDQUFkLEVBQWtCVyxDQUFDLENBQUNELENBQUQsQ0FBRCxHQUFLLENBQUMsQ0FBeEIsRUFBMEJFLENBQUMsR0FBQyxDQUFDLENBQTdSLElBQWdTLFdBQVN4QixDQUFULEdBQVd1QixDQUFDLENBQUNELENBQUQsQ0FBRCxHQUFLLENBQWhCLEdBQWtCLGNBQVl0QixDQUFaLEtBQWdCdUIsQ0FBQyxDQUFDRCxDQUFELENBQUQsR0FBSyxDQUFDLENBQXRCLENBQXZoQixFQUFnakJWLENBQUMsQ0FBQzhFLE1BQUYsS0FBVy9FLENBQUMsQ0FBQytFLE1BQWIsS0FBc0I5RSxDQUFDLENBQUM4RSxNQUFGLEdBQVMvRSxDQUFDLENBQUMrRSxNQUFGLEdBQVMsQ0FBQyxDQUF6QyxDQUFqbEIsQ0FBNUc7QUFBdkI7O0FBQWl3QixhQUFPakUsQ0FBQyxJQUFFMkMsQ0FBQyxDQUFDLGlCQUFlN0MsQ0FBQyxDQUFDd0IsSUFBRixDQUFPLEdBQVAsQ0FBZixHQUEyQixHQUE1QixDQUFKLEVBQXFDMUUsQ0FBQyxDQUFDeUksVUFBRixHQUFhdkYsQ0FBekQ7QUFBMkQ7QUFBQzs7QUFBQSxXQUFTSCxDQUFULENBQVcvQyxDQUFYLEVBQWFDLENBQWIsRUFBZTtBQUFDLFFBQUkwQixDQUFKO0FBQUEsUUFBTWxCLENBQU47QUFBQSxRQUFRNEIsQ0FBUjtBQUFBLFFBQVVDLENBQVY7QUFBQSxRQUFZQyxDQUFaO0FBQUEsUUFBY2xCLENBQWQ7QUFBQSxRQUFnQkUsQ0FBaEI7QUFBQSxRQUFrQmlCLENBQUMsR0FBQyxDQUFwQjtBQUFBLFFBQXNCQyxDQUFDLEdBQUNpRyxVQUFVLENBQUMxSSxDQUFDLENBQUMsQ0FBRCxDQUFGLENBQWxDO0FBQUEsUUFBeUMwQyxDQUFDLEdBQUNnRyxVQUFVLENBQUMxSSxDQUFDLENBQUMsQ0FBRCxDQUFGLENBQXJEO0FBQUEsUUFBNEQyQyxDQUFDLEdBQUNGLENBQUMsR0FBQyxHQUFGLEdBQU1DLENBQU4sR0FBUSxHQUF0RTs7QUFBMEUsU0FBSWYsQ0FBQyxHQUFDLEtBQUcxQixDQUFILElBQU0sTUFBSW9DLENBQUMsR0FBQ3JDLENBQUMsQ0FBQzZCLE1BQVIsSUFBZ0IsQ0FBdEIsQ0FBRixFQUEyQnBCLENBQUMsR0FBQyxDQUFqQyxFQUFtQ0EsQ0FBQyxHQUFDNEIsQ0FBQyxHQUFDLENBQXZDLEVBQXlDNUIsQ0FBQyxJQUFFLENBQTVDLEVBQThDO0FBQUMsVUFBRytCLENBQUMsSUFBRWIsQ0FBSCxFQUFLTixDQUFDLEdBQUNxSCxVQUFVLENBQUMxSSxDQUFDLENBQUNTLENBQUMsR0FBQyxDQUFILENBQUYsQ0FBakIsRUFBMEJjLENBQUMsR0FBQ21ILFVBQVUsQ0FBQzFJLENBQUMsQ0FBQ1MsQ0FBQyxHQUFDLENBQUgsQ0FBRixDQUF0QyxFQUErQyxVQUFRK0IsQ0FBMUQsRUFBNEQsS0FBSUQsQ0FBQyxHQUFDLEtBQUc1QixJQUFJLENBQUM0SCxLQUFMLENBQVcvRixDQUFYLElBQWMsQ0FBakIsQ0FBRixFQUFzQkYsQ0FBQyxHQUFDLENBQTVCLEVBQThCLFVBQVFFLENBQXRDO0FBQXlDRyxTQUFDLElBQUUsQ0FBQ0YsQ0FBQyxHQUFDLENBQUNwQixDQUFDLEdBQUNvQixDQUFILElBQU1GLENBQU4sR0FBUUQsQ0FBWCxFQUFjcUcsT0FBZCxDQUFzQixDQUF0QixJQUF5QixHQUF6QixHQUE2QixDQUFDakcsQ0FBQyxHQUFDLENBQUNuQixDQUFDLEdBQUNtQixDQUFILElBQU1ILENBQU4sR0FBUUQsQ0FBWCxFQUFjcUcsT0FBZCxDQUFzQixDQUF0QixDQUE3QixHQUFzRCxHQUF6RCxFQUE2RG5HLENBQUMsRUFBOUQsRUFBaUVGLENBQUMsRUFBbEU7QUFBekM7QUFBOEdLLE9BQUMsSUFBRXRCLENBQUMsR0FBQyxHQUFGLEdBQU1FLENBQU4sR0FBUSxHQUFYLEVBQWVrQixDQUFDLEdBQUNwQixDQUFqQixFQUFtQnFCLENBQUMsR0FBQ25CLENBQXJCO0FBQXVCOztBQUFBLFdBQU9vQixDQUFQO0FBQVM7O0FBQUEsV0FBU2lHLEVBQVQsQ0FBWTVJLENBQVosRUFBYztBQUFDLFFBQUlDLENBQUMsR0FBQ0QsQ0FBQyxDQUFDLENBQUQsQ0FBRCxDQUFLaUYsS0FBTCxDQUFXeUIsQ0FBWCxLQUFlLEVBQXJCO0FBQUEsUUFBd0IvRSxDQUFDLEdBQUMzQixDQUFDLENBQUMsQ0FBRCxDQUFELENBQUtpRixLQUFMLENBQVd5QixDQUFYLEtBQWUsRUFBekM7QUFBQSxRQUE0Q2pHLENBQUMsR0FBQ2tCLENBQUMsQ0FBQ0UsTUFBRixHQUFTNUIsQ0FBQyxDQUFDNEIsTUFBekQ7QUFBZ0UsUUFBRXBCLENBQUYsR0FBSVQsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFLK0MsQ0FBQyxDQUFDOUMsQ0FBRCxFQUFHUSxDQUFILENBQVYsR0FBZ0JULENBQUMsQ0FBQyxDQUFELENBQUQsR0FBSytDLENBQUMsQ0FBQ3BCLENBQUQsRUFBRyxDQUFDbEIsQ0FBSixDQUF0QjtBQUE2Qjs7QUFBQSxXQUFTb0ksRUFBVCxDQUFZNUksQ0FBWixFQUFjO0FBQUMsV0FBT21HLEtBQUssQ0FBQ25HLENBQUQsQ0FBTCxHQUFTMkksRUFBVCxHQUFZLFVBQVM1SSxDQUFULEVBQVc7QUFBQzRJLFFBQUUsQ0FBQzVJLENBQUQsQ0FBRixFQUFNQSxDQUFDLENBQUMsQ0FBRCxDQUFELEdBQUssU0FBUzhJLGFBQVQsQ0FBdUI5SSxDQUF2QixFQUF5QkMsQ0FBekIsRUFBMkI7QUFBQyxZQUFHLENBQUNBLENBQUosRUFBTSxPQUFPRCxDQUFQO0FBQVMsWUFBSTJCLENBQUo7QUFBQSxZQUFNbEIsQ0FBTjtBQUFBLFlBQVE0QixDQUFSO0FBQUEsWUFBVUMsQ0FBQyxHQUFDdEMsQ0FBQyxDQUFDaUYsS0FBRixDQUFReUIsQ0FBUixLQUFZLEVBQXhCO0FBQUEsWUFBMkJuRSxDQUFDLEdBQUNELENBQUMsQ0FBQ1QsTUFBL0I7QUFBQSxZQUFzQ1IsQ0FBQyxHQUFDLEVBQXhDOztBQUEyQyxhQUFJTSxDQUFDLEdBQUMsY0FBWTFCLENBQVosSUFBZVEsQ0FBQyxHQUFDOEIsQ0FBQyxHQUFDLENBQUosRUFBTSxDQUFDLENBQXRCLEtBQTBCOUIsQ0FBQyxHQUFDLENBQUMsS0FBR3NJLFFBQVEsQ0FBQzlJLENBQUQsRUFBRyxFQUFILENBQVIsSUFBZ0IsQ0FBbkIsSUFBc0IsQ0FBdEIsR0FBd0IsTUFBSXNDLENBQTdCLElBQWdDQSxDQUFsQyxFQUFvQyxDQUE5RCxDQUFGLEVBQW1FRixDQUFDLEdBQUMsQ0FBekUsRUFBMkVBLENBQUMsR0FBQ0UsQ0FBN0UsRUFBK0VGLENBQUMsSUFBRSxDQUFsRjtBQUFvRmhCLFdBQUMsSUFBRWlCLENBQUMsQ0FBQzdCLENBQUMsR0FBQyxDQUFILENBQUQsR0FBTyxHQUFQLEdBQVc2QixDQUFDLENBQUM3QixDQUFELENBQVosR0FBZ0IsR0FBbkIsRUFBdUJBLENBQUMsR0FBQyxDQUFDQSxDQUFDLEdBQUNrQixDQUFILElBQU1ZLENBQS9CO0FBQXBGOztBQUFxSCxlQUFPbEIsQ0FBUDtBQUFTLE9BQXBOLENBQXFOckIsQ0FBQyxDQUFDLENBQUQsQ0FBdE4sRUFBME4rSSxRQUFRLENBQUM5SSxDQUFELEVBQUcsRUFBSCxDQUFsTyxDQUFYO0FBQXFQLEtBQXBSO0FBQXFSOztBQUFBLFdBQVMrSSxFQUFULENBQVloSixDQUFaLEVBQWNDLENBQWQsRUFBZ0I7QUFBQyxTQUFJLElBQUkwQixDQUFKLEVBQU1sQixDQUFOLEVBQVE0QixDQUFSLEVBQVVDLENBQVYsRUFBWUMsQ0FBWixFQUFjbEIsQ0FBZCxFQUFnQkUsQ0FBaEIsRUFBa0JpQixDQUFsQixFQUFvQkMsQ0FBcEIsRUFBc0JDLENBQXRCLEVBQXdCQyxDQUF4QixFQUEwQkMsQ0FBMUIsRUFBNEJDLENBQUMsR0FBQzdDLENBQUMsQ0FBQzZCLE1BQWhDLEVBQXVDaUIsQ0FBQyxHQUFDLE1BQUk3QyxDQUFDLElBQUUsQ0FBUCxDQUE3QyxFQUF1RCxDQUFDLENBQUQsR0FBRyxFQUFFNEMsQ0FBNUQsR0FBK0Q7QUFBQyxXQUFJRixDQUFDLEdBQUMsQ0FBQ2xDLENBQUMsR0FBQ1QsQ0FBQyxDQUFDNkMsQ0FBRCxDQUFKLEVBQVNvRyxRQUFULEdBQWtCeEksQ0FBQyxDQUFDd0ksUUFBRixJQUFZLENBQUMsQ0FBRCxFQUFHLENBQUgsRUFBSyxDQUFMLEVBQU8sQ0FBUCxDQUFoQyxFQUEwQ3JHLENBQUMsR0FBQ25DLENBQUMsQ0FBQ3lJLFVBQUYsR0FBYXpJLENBQUMsQ0FBQ3lJLFVBQUYsSUFBYyxDQUFDLENBQUQsRUFBRyxDQUFILEVBQUssQ0FBTCxFQUFPLENBQVAsQ0FBdkUsRUFBaUZ2RyxDQUFDLENBQUNkLE1BQUYsR0FBUyxDQUExRixFQUE0RlcsQ0FBQyxHQUFDL0IsQ0FBQyxDQUFDb0IsTUFBRixHQUFTLENBQXZHLEVBQXlHTixDQUFDLEdBQUMsQ0FBL0csRUFBaUhBLENBQUMsR0FBQ2lCLENBQW5ILEVBQXFIakIsQ0FBQyxJQUFFLENBQXhIO0FBQTBIYyxTQUFDLEdBQUM1QixDQUFDLENBQUNjLENBQUQsQ0FBRCxHQUFLZCxDQUFDLENBQUNjLENBQUMsR0FBQyxDQUFILENBQVIsRUFBY2UsQ0FBQyxHQUFDN0IsQ0FBQyxDQUFDYyxDQUFDLEdBQUMsQ0FBSCxDQUFELEdBQU9kLENBQUMsQ0FBQ2MsQ0FBQyxHQUFDLENBQUgsQ0FBeEIsRUFBOEJnQixDQUFDLEdBQUM5QixDQUFDLENBQUNjLENBQUMsR0FBQyxDQUFILENBQUQsR0FBT2QsQ0FBQyxDQUFDYyxDQUFELENBQXhDLEVBQTRDRixDQUFDLEdBQUNaLENBQUMsQ0FBQ2MsQ0FBQyxHQUFDLENBQUgsQ0FBRCxHQUFPZCxDQUFDLENBQUNjLENBQUMsR0FBQyxDQUFILENBQXRELEVBQTREa0IsQ0FBQyxHQUFDVSxDQUFDLENBQUNiLENBQUQsRUFBR0QsQ0FBSCxDQUEvRCxFQUFxRUssQ0FBQyxHQUFDUyxDQUFDLENBQUM5QixDQUFELEVBQUdrQixDQUFILENBQXhFLEVBQThFLENBQUNaLENBQUMsR0FBQ2hCLElBQUksQ0FBQ08sR0FBTCxDQUFTdUIsQ0FBQyxHQUFDQyxDQUFYLElBQWNJLENBQWpCLE1BQXNCRixDQUFDLENBQUNyQixDQUFDLEdBQUMsQ0FBSCxDQUFELEdBQU9rQixDQUFQLEVBQVNHLENBQUMsQ0FBQ3JCLENBQUMsR0FBQyxDQUFILENBQUQsR0FBT21CLENBQWhCLEVBQWtCRSxDQUFDLENBQUNyQixDQUFDLEdBQUMsQ0FBSCxDQUFELEdBQU8wQixDQUFDLENBQUNaLENBQUMsR0FBQ0EsQ0FBRixHQUFJQyxDQUFDLEdBQUNBLENBQVAsQ0FBMUIsRUFBb0NNLENBQUMsQ0FBQ3JCLENBQUMsR0FBQyxDQUFILENBQUQsR0FBTzBCLENBQUMsQ0FBQ1YsQ0FBQyxHQUFDQSxDQUFGLEdBQUlsQixDQUFDLEdBQUNBLENBQVAsQ0FBbEUsQ0FBOUUsRUFBMkpzQixDQUFDLENBQUNvRSxJQUFGLENBQU9wRixDQUFQLEVBQVNBLENBQVQsRUFBVyxDQUFYLEVBQWEsQ0FBYixFQUFlQSxDQUFmLEVBQWlCQSxDQUFqQixDQUEzSjtBQUExSDs7QUFBeVNsQixPQUFDLENBQUMrQixDQUFELENBQUQsS0FBTy9CLENBQUMsQ0FBQyxDQUFELENBQVIsSUFBYUEsQ0FBQyxDQUFDLElBQUUrQixDQUFILENBQUQsS0FBUy9CLENBQUMsQ0FBQyxDQUFELENBQXZCLEtBQTZCNEIsQ0FBQyxHQUFDNUIsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFLQSxDQUFDLENBQUMrQixDQUFDLEdBQUMsQ0FBSCxDQUFSLEVBQWNGLENBQUMsR0FBQzdCLENBQUMsQ0FBQyxDQUFELENBQUQsR0FBS0EsQ0FBQyxDQUFDK0IsQ0FBQyxHQUFDLENBQUgsQ0FBdEIsRUFBNEJELENBQUMsR0FBQzlCLENBQUMsQ0FBQyxDQUFELENBQUQsR0FBS0EsQ0FBQyxDQUFDLENBQUQsQ0FBcEMsRUFBd0NZLENBQUMsR0FBQ1osQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFLQSxDQUFDLENBQUMsQ0FBRCxDQUFoRCxFQUFvRGdDLENBQUMsR0FBQ1UsQ0FBQyxDQUFDYixDQUFELEVBQUdELENBQUgsQ0FBdkQsRUFBNkRLLENBQUMsR0FBQ1MsQ0FBQyxDQUFDOUIsQ0FBRCxFQUFHa0IsQ0FBSCxDQUFoRSxFQUFzRTVCLElBQUksQ0FBQ08sR0FBTCxDQUFTdUIsQ0FBQyxHQUFDQyxDQUFYLElBQWNJLENBQWQsS0FBa0JGLENBQUMsQ0FBQ0osQ0FBQyxHQUFDLENBQUgsQ0FBRCxHQUFPQyxDQUFQLEVBQVNHLENBQUMsQ0FBQyxDQUFELENBQUQsR0FBS0YsQ0FBZCxFQUFnQkUsQ0FBQyxDQUFDSixDQUFDLEdBQUMsQ0FBSCxDQUFELEdBQU9TLENBQUMsQ0FBQ1osQ0FBQyxHQUFDQSxDQUFGLEdBQUlDLENBQUMsR0FBQ0EsQ0FBUCxDQUF4QixFQUFrQ00sQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFLSyxDQUFDLENBQUNWLENBQUMsR0FBQ0EsQ0FBRixHQUFJbEIsQ0FBQyxHQUFDQSxDQUFQLENBQXhDLEVBQWtEc0IsQ0FBQyxDQUFDSCxDQUFDLEdBQUMsQ0FBSCxDQUFELEdBQU9HLENBQUMsQ0FBQ0gsQ0FBQyxHQUFDLENBQUgsQ0FBRCxHQUFPLENBQUMsQ0FBbkYsQ0FBbkc7QUFBMEw7O0FBQUEsV0FBT3hDLENBQVA7QUFBUzs7QUFBQSxXQUFTbUosRUFBVCxDQUFZbkosQ0FBWixFQUFjO0FBQUMsUUFBSUMsQ0FBQyxHQUFDRCxDQUFDLENBQUNvSixJQUFGLEdBQVNoRixLQUFULENBQWUsR0FBZixDQUFOO0FBQTBCLFdBQU07QUFBQ2hCLE9BQUMsRUFBQyxDQUFDLENBQUNwRCxDQUFDLENBQUNnRSxPQUFGLENBQVUsTUFBVixDQUFELEdBQW1CLENBQW5CLEdBQXFCLENBQUNoRSxDQUFDLENBQUNnRSxPQUFGLENBQVUsT0FBVixDQUFELEdBQW9CLEdBQXBCLEdBQXdCb0MsS0FBSyxDQUFDc0MsVUFBVSxDQUFDekksQ0FBQyxDQUFDLENBQUQsQ0FBRixDQUFYLENBQUwsR0FBd0IsRUFBeEIsR0FBMkJ5SSxVQUFVLENBQUN6SSxDQUFDLENBQUMsQ0FBRCxDQUFGLENBQW5GLElBQTJGLEdBQTlGO0FBQWtHaUQsT0FBQyxFQUFDLENBQUMsQ0FBQ2xELENBQUMsQ0FBQ2dFLE9BQUYsQ0FBVSxLQUFWLENBQUQsR0FBa0IsQ0FBbEIsR0FBb0IsQ0FBQ2hFLENBQUMsQ0FBQ2dFLE9BQUYsQ0FBVSxRQUFWLENBQUQsR0FBcUIsR0FBckIsR0FBeUJvQyxLQUFLLENBQUNzQyxVQUFVLENBQUN6SSxDQUFDLENBQUMsQ0FBRCxDQUFGLENBQVgsQ0FBTCxHQUF3QixFQUF4QixHQUEyQnlJLFVBQVUsQ0FBQ3pJLENBQUMsQ0FBQyxDQUFELENBQUYsQ0FBbkYsSUFBMkY7QUFBL0wsS0FBTjtBQUEwTTs7QUFBQSxXQUFTb0osRUFBVCxDQUFZckosQ0FBWixFQUFjQyxDQUFkLEVBQWdCMEIsQ0FBaEIsRUFBa0JsQixDQUFsQixFQUFvQjtBQUFDLFFBQUk0QixDQUFKO0FBQUEsUUFBTUMsQ0FBTjtBQUFBLFFBQVFDLENBQUMsR0FBQyxLQUFLK0csT0FBZjtBQUFBLFFBQXVCakksQ0FBQyxHQUFDLEtBQUtrSSxRQUE5QjtBQUFBLFFBQXVDaEksQ0FBQyxHQUFDdkIsQ0FBQyxDQUFDMkIsQ0FBRCxDQUFELEdBQUtZLENBQUMsQ0FBQ2EsQ0FBaEQ7QUFBQSxRQUFrRFosQ0FBQyxHQUFDeEMsQ0FBQyxDQUFDMkIsQ0FBQyxHQUFDLENBQUgsQ0FBRCxHQUFPWSxDQUFDLENBQUNXLENBQTdEO0FBQUEsUUFBK0RULENBQUMsR0FBQ1EsQ0FBQyxDQUFDMUIsQ0FBQyxHQUFDQSxDQUFGLEdBQUlpQixDQUFDLEdBQUNBLENBQVAsQ0FBbEU7QUFBQSxRQUE0RUUsQ0FBQyxHQUFDUyxDQUFDLENBQUNYLENBQUQsRUFBR2pCLENBQUgsQ0FBL0U7QUFBcUYsV0FBT0EsQ0FBQyxHQUFDdEIsQ0FBQyxDQUFDMEIsQ0FBRCxDQUFELEdBQUtOLENBQUMsQ0FBQytCLENBQVQsRUFBV1osQ0FBQyxHQUFDdkMsQ0FBQyxDQUFDMEIsQ0FBQyxHQUFDLENBQUgsQ0FBRCxHQUFPTixDQUFDLENBQUM2QixDQUF0QixFQUF3QlosQ0FBQyxHQUFDLFNBQVNrSCxXQUFULENBQXFCeEosQ0FBckIsRUFBdUI7QUFBQyxhQUFPQSxDQUFDLEtBQUdBLENBQUMsR0FBQzJDLENBQU4sR0FBUTNDLENBQUMsSUFBRUEsQ0FBQyxHQUFDLENBQUYsR0FBSTRDLENBQUosR0FBTSxDQUFDQSxDQUFULENBQVQsR0FBcUI1QyxDQUE1QjtBQUE4QixLQUF0RCxDQUF1RHFDLENBQUMsR0FBQ2MsQ0FBQyxDQUFDWCxDQUFELEVBQUdqQixDQUFILENBQUQsR0FBT21CLENBQWhFLENBQTFCLEVBQTZGLENBQUNqQyxDQUFELElBQUkwRixDQUFKLElBQU94RixJQUFJLENBQUNPLEdBQUwsQ0FBU29CLENBQUMsR0FBQzZELENBQUMsQ0FBQ3NELEVBQWIsSUFBaUI1RyxDQUF4QixLQUE0QnBDLENBQUMsR0FBQzBGLENBQTlCLENBQTdGLEVBQThILEtBQUt1RCxTQUFMLEdBQWV2RCxDQUFDLEdBQUM7QUFBQ3dELFdBQUssRUFBQyxLQUFLRCxTQUFaO0FBQXNCMUosT0FBQyxFQUFDQSxDQUF4QjtBQUEwQjRKLFFBQUUsRUFBQ2xILENBQTdCO0FBQStCK0csUUFBRSxFQUFDaEosQ0FBQyxJQUFFNkIsQ0FBQyxHQUFDN0IsQ0FBQyxDQUFDZ0osRUFBSixHQUFPLENBQVYsSUFBYTlJLElBQUksQ0FBQ08sR0FBTCxDQUFTb0IsQ0FBVCxJQUFZUSxDQUF6QixHQUEyQlQsQ0FBM0IsR0FBNkJDLENBQS9EO0FBQWlFdUgsUUFBRSxFQUFDcEgsQ0FBcEU7QUFBc0VxSCxRQUFFLEVBQUM3RyxDQUFDLENBQUMxQixDQUFDLEdBQUNBLENBQUYsR0FBSWlCLENBQUMsR0FBQ0EsQ0FBUCxDQUFELEdBQVdDLENBQXBGO0FBQXNGSCxPQUFDLEVBQUNYO0FBQXhGLEtBQXRKO0FBQWlQOztBQUFBLFdBQVNvSSxFQUFULENBQVkvSixDQUFaLEVBQWM7QUFBQzJCLEtBQUMsR0FBQ3VCLENBQUMsRUFBSCxFQUFNYixDQUFDLEdBQUNBLENBQUMsSUFBRVYsQ0FBQyxJQUFFQSxDQUFDLENBQUNxSSxPQUFGLENBQVVDLFFBQXhCLEVBQWlDdEksQ0FBQyxJQUFFVSxDQUFILElBQU00RCxDQUFDLEdBQUN0RSxDQUFDLENBQUN1SSxLQUFGLENBQVFDLE9BQVYsRUFBa0I5SCxDQUFDLENBQUMrSCxTQUFGLENBQVlDLGNBQVosR0FBMkJoQixFQUE3QyxFQUFnRC9DLENBQUMsR0FBQyxDQUF4RCxJQUEyRHRHLENBQUMsSUFBRStGLENBQUMsQ0FBQyw0Q0FBRCxDQUFoRztBQUErSTs7QUFBQSxNQUFJcEUsQ0FBSjtBQUFBLE1BQU1zRSxDQUFOO0FBQUEsTUFBUUUsQ0FBUjtBQUFBLE1BQVVHLENBQVY7QUFBQSxNQUFZakUsQ0FBWjtBQUFBLE1BQWNjLENBQUMsR0FBQ3hDLElBQUksQ0FBQzJKLEtBQXJCO0FBQUEsTUFBMkJsSCxDQUFDLEdBQUN6QyxJQUFJLENBQUNLLEdBQWxDO0FBQUEsTUFBc0N5RixDQUFDLEdBQUM5RixJQUFJLENBQUNHLEdBQTdDO0FBQUEsTUFBaURtQyxDQUFDLEdBQUN0QyxJQUFJLENBQUNTLElBQXhEO0FBQUEsTUFBNkR1QixDQUFDLEdBQUNoQyxJQUFJLENBQUNDLEVBQXBFO0FBQUEsTUFBdUVnQyxDQUFDLEdBQUMsSUFBRUQsQ0FBM0U7QUFBQSxNQUE2RUUsQ0FBQyxHQUFDLEtBQUdGLENBQWxGO0FBQUEsTUFBb0ZHLENBQUMsR0FBQyxLQUFHSCxDQUF6RjtBQUFBLE1BQTJGK0QsQ0FBQyxHQUFDLHVDQUE3RjtBQUFBLE1BQXFJRSxDQUFDLEdBQUMsNEJBQXZJO0FBQUEsTUFBb0tDLENBQUMsR0FBQyxnQkFBdEs7QUFBQSxNQUF1TEMsQ0FBQyxHQUFDLDBFQUF6TDtBQUFBLE1BQW9ReUQsQ0FBQyxHQUFDO0FBQUNDLFdBQU8sRUFBQyxPQUFUO0FBQWlCQyxRQUFJLEVBQUMsVUFBdEI7QUFBaUNDLFlBQVEsRUFBQyxTQUFTQSxRQUFULENBQWtCMUssQ0FBbEIsRUFBb0JDLENBQXBCLEVBQXNCO0FBQUMwQixPQUFDLEdBQUMzQixDQUFGLEVBQUlxQyxDQUFDLEdBQUNwQyxDQUFOLEVBQVE4SixFQUFFLEVBQVY7QUFBYSxLQUE5RTtBQUErRVksUUFBSSxFQUFDLFNBQVNBLElBQVQsQ0FBYzNLLENBQWQsRUFBZ0JDLENBQWhCLEVBQWtCMEIsQ0FBbEIsRUFBb0JsQixDQUFwQixFQUFzQjRCLENBQXRCLEVBQXdCO0FBQUMsVUFBR2lFLENBQUMsSUFBRXlELEVBQUUsQ0FBQyxDQUFELENBQUwsRUFBUyxDQUFDOUosQ0FBYixFQUFlLE9BQU84RixDQUFDLENBQUMsZUFBRCxDQUFELEVBQW1CLENBQUMsQ0FBM0I7QUFBNkIsT0FBQyxZQUFVLE9BQU85RixDQUFqQixJQUFvQkEsQ0FBQyxDQUFDdUQsT0FBdEIsSUFBK0J2RCxDQUFDLENBQUMsQ0FBRCxDQUFqQyxNQUF3Q0EsQ0FBQyxHQUFDO0FBQUMySyxhQUFLLEVBQUMzSztBQUFQLE9BQTFDOztBQUFxRCxVQUFJcUMsQ0FBSjtBQUFBLFVBQU1DLENBQU47QUFBQSxVQUFRbEIsQ0FBUjtBQUFBLFVBQVVFLENBQVY7QUFBQSxVQUFZaUIsQ0FBWjtBQUFBLFVBQWNDLENBQWQ7QUFBQSxVQUFnQkMsQ0FBaEI7QUFBQSxVQUFrQkMsQ0FBbEI7QUFBQSxVQUFvQkMsQ0FBcEI7QUFBQSxVQUFzQkMsQ0FBdEI7QUFBQSxVQUF3QkMsQ0FBeEI7QUFBQSxVQUEwQkMsQ0FBMUI7QUFBQSxVQUE0QjFDLENBQTVCO0FBQUEsVUFBOEI0QyxDQUE5QjtBQUFBLFVBQWdDQyxDQUFoQztBQUFBLFVBQWtDQyxDQUFsQztBQUFBLFVBQW9DQyxDQUFwQztBQUFBLFVBQXNDQyxDQUF0QztBQUFBLFVBQXdDL0MsQ0FBeEM7QUFBQSxVQUEwQ0UsQ0FBMUM7QUFBQSxVQUE0Q21GLENBQTVDO0FBQUEsVUFBOENDLENBQTlDO0FBQUEsVUFBZ0RDLENBQUMsR0FBQzdGLENBQUMsQ0FBQzZLLFFBQUYsR0FBV25ELE1BQU0sQ0FBQ29ELGdCQUFQLENBQXdCOUssQ0FBeEIsQ0FBWCxHQUFzQyxFQUF4RjtBQUFBLFVBQTJGTyxDQUFDLEdBQUNzRixDQUFDLENBQUNrRixJQUFGLEdBQU8sRUFBcEc7QUFBQSxVQUF1R2hKLENBQUMsR0FBQyxFQUFFLFdBQVN4QixDQUFULElBQVksUUFBTSxDQUFDQSxDQUFDLENBQUMwRSxLQUFGLENBQVF5QixDQUFSLEtBQVksRUFBYixFQUFpQixDQUFqQixDQUFsQixJQUF1QyxjQUFZYixDQUFDLENBQUNtRixRQUF2RCxDQUF6RztBQUFBLFVBQTBLbEYsQ0FBQyxHQUFDLENBQUM3RixDQUFDLENBQUNnTCxNQUFGLElBQVUsT0FBWCxFQUFvQjdHLEtBQXBCLENBQTBCLEdBQTFCLENBQTVLOztBQUEyTSxVQUFHNUIsQ0FBQyxHQUFDLGdCQUFjRixDQUFDLEdBQUMsQ0FBQ3RDLENBQUMsQ0FBQytELFFBQUYsR0FBVyxFQUFaLEVBQWdCcUQsV0FBaEIsRUFBaEIsS0FBZ0QsY0FBWTlFLENBQTlELEVBQWdFLFdBQVNBLENBQVQsSUFBWSxDQUFDRSxDQUFiLElBQWdCLENBQUN2QyxDQUFDLENBQUNpTCxJQUF0RixFQUEyRixPQUFPbkYsQ0FBQyxDQUFDLHFCQUFtQnpELENBQW5CLEdBQXFCLGFBQXJCLEdBQW1Dd0UsQ0FBcEMsQ0FBRCxFQUF3QyxDQUFDLENBQWhEO0FBQWtELFVBQUd2RSxDQUFDLEdBQUMsV0FBU0QsQ0FBVCxHQUFXLEdBQVgsR0FBZSxRQUFqQixFQUEwQixDQUFDckMsQ0FBQyxDQUFDaUwsSUFBSCxJQUFTLGNBQVksT0FBT2xMLENBQUMsQ0FBQ21GLFlBQTNELEVBQXdFLE9BQU0sQ0FBQyxDQUFQO0FBQVMsVUFBRzVELENBQUMsR0FBQyxTQUFTNEosV0FBVCxDQUFxQm5MLENBQXJCLEVBQXVCQyxDQUF2QixFQUF5QjBCLENBQXpCLEVBQTJCO0FBQUMsWUFBSWxCLENBQUosRUFBTTRCLENBQU47QUFBUSxlQUFNLENBQUMsRUFBRSxZQUFVLE9BQU9yQyxDQUFuQixLQUF1QjRHLENBQUMsQ0FBQ3dFLElBQUYsQ0FBT3BMLENBQVAsQ0FBdkIsSUFBa0MsQ0FBQ0EsQ0FBQyxDQUFDaUYsS0FBRixDQUFReUIsQ0FBUixLQUFZLEVBQWIsRUFBaUI3RSxNQUFqQixHQUF3QixDQUEzRCxNQUFnRSxDQUFDcEIsQ0FBQyxHQUFDd0YsQ0FBQyxDQUFDakcsQ0FBRCxDQUFELENBQUssQ0FBTCxDQUFILEtBQWFxQyxDQUFDLEdBQUMsQ0FBQzVCLENBQUMsQ0FBQ3NELFFBQUYsR0FBVyxFQUFaLEVBQWdCcUQsV0FBaEIsRUFBRixFQUFnQ25ILENBQUMsSUFBRSxXQUFTb0MsQ0FBWixLQUFnQjVCLENBQUMsR0FBQzJCLGFBQWEsQ0FBQzNCLENBQUQsRUFBRyxDQUFDLENBQUosQ0FBZixFQUFzQjRCLENBQUMsR0FBQyxNQUF4QyxDQUFoQyxFQUFnRnJDLENBQUMsR0FBQ1MsQ0FBQyxDQUFDNEQsWUFBRixDQUFlLFdBQVNoQyxDQUFULEdBQVcsR0FBWCxHQUFlLFFBQTlCLEtBQXlDLEVBQTNILEVBQThINUIsQ0FBQyxLQUFHa0IsQ0FBSixLQUFRM0IsQ0FBQyxHQUFDUyxDQUFDLENBQUM0SyxjQUFGLENBQWlCLElBQWpCLEVBQXNCLGVBQXRCLEtBQXdDckwsQ0FBbEQsQ0FBM0ksS0FBa00rRixDQUFDLENBQUMsZ0NBQThCL0YsQ0FBL0IsQ0FBRCxFQUFtQ0EsQ0FBQyxHQUFDLENBQUMsQ0FBeE8sQ0FBaEUsR0FBNFNBLENBQWxUO0FBQW9ULE9BQXhWLENBQXlWQyxDQUFDLENBQUMySyxLQUFGLElBQVMzSyxDQUFDLENBQUM2QyxDQUFYLElBQWM3QyxDQUFDLENBQUNxTCxNQUFoQixJQUF3QixFQUFqWCxFQUFvWCxPQUFLL0ksQ0FBelgsRUFBMlh2QyxDQUEzWCxDQUFGLEVBQWdZd0MsQ0FBQyxJQUFFcUUsQ0FBQyxDQUFDdUUsSUFBRixDQUFPN0osQ0FBUCxDQUF0WSxFQUFnWixPQUFPd0UsQ0FBQyxDQUFDLFFBQU16RCxDQUFOLEdBQVEsNkJBQVIsR0FBc0N3RSxDQUF2QyxDQUFELEVBQTJDLENBQUMsQ0FBbkQ7O0FBQXFELFVBQUdyRSxDQUFDLEdBQUN4QyxDQUFDLENBQUN3SSxVQUFGLElBQWMsTUFBSXhJLENBQUMsQ0FBQ3dJLFVBQXBCLEdBQStCeEksQ0FBQyxDQUFDd0ksVUFBakMsR0FBNEMsTUFBOUMsRUFBcUQvRixDQUFDLEdBQUN6QyxDQUFDLENBQUNILEdBQUYsSUFBT3lLLENBQUMsQ0FBQ2dCLFVBQWhFLEVBQTJFLEtBQUtDLEtBQUwsR0FBV3ZMLENBQUMsQ0FBQ2lMLElBQXhGLEVBQTZGLEtBQUtPLE9BQUwsR0FBYXhMLENBQUMsQ0FBQ3lMLE1BQUYsSUFBVW5CLENBQUMsQ0FBQ29CLGFBQXRILEVBQW9JLEtBQUtDLE1BQUwsR0FBWSxrQkFBaUIzTCxDQUFqQixHQUFtQkEsQ0FBQyxDQUFDNEwsWUFBckIsR0FBa0N0QixDQUFDLENBQUN1QixtQkFBcEwsRUFBd00sS0FBS0MsSUFBTCxHQUFVcEwsSUFBSSxDQUFDcUwsR0FBTCxDQUFTLEVBQVQsRUFBWTVGLEtBQUssQ0FBQ25HLENBQUMsQ0FBQ2dNLFNBQUgsQ0FBTCxHQUFtQixDQUFuQixHQUFxQixDQUFDaE0sQ0FBQyxDQUFDZ00sU0FBcEMsQ0FBbE4sRUFBaVEsS0FBS0MsTUFBTCxHQUFZdkssQ0FBN1EsRUFBK1FKLENBQWxSLEVBQW9SO0FBQUMsWUFBRyxLQUFLNEssT0FBTCxHQUFhbk0sQ0FBYixFQUFlb0QsQ0FBQyxHQUFDLG9CQUFpQm5ELENBQUMsQ0FBQ21NLFVBQW5CLENBQWpCLEVBQStDdkosQ0FBQyxHQUFDLEtBQUsySSxLQUFMLEdBQVd4TCxDQUFDLENBQUMsS0FBS3dMLEtBQU4sQ0FBWixHQUF5QnhMLENBQUMsQ0FBQ3FFLFlBQUYsQ0FBZTlCLENBQWYsQ0FBMUUsRUFBNEYsS0FBS2lKLEtBQUwsSUFBWXhMLENBQUMsQ0FBQ3FMLGNBQUYsQ0FBaUIsSUFBakIsRUFBc0IsZUFBdEIsQ0FBWixJQUFvRHJMLENBQUMsQ0FBQ2lFLGNBQUYsQ0FBaUIsSUFBakIsRUFBc0IsZUFBdEIsRUFBc0NwQixDQUF0QyxDQUFoSixFQUF5TCxPQUFLTixDQUFMLElBQVEsS0FBS2lKLEtBQXpNLEVBQStNO0FBQUMsY0FBRzNJLENBQUMsR0FBQ3lDLGVBQWUsQ0FBQ2xDLENBQUMsR0FBQ25ELENBQUMsQ0FBQ21NLFVBQUYsQ0FBYSxDQUFiLENBQUQsR0FBaUJ2SixDQUFuQixDQUFqQixFQUF1Q0MsQ0FBQyxHQUFDd0MsZUFBZSxDQUFDbEMsQ0FBQyxHQUFDbkQsQ0FBQyxDQUFDbU0sVUFBRixDQUFhLENBQWIsQ0FBRCxHQUFpQjdLLENBQW5CLENBQXhELEVBQThFLENBQUM2QixDQUFELElBQUksQ0FBQzRDLENBQUMsQ0FBQ25ELENBQUQsRUFBR0MsQ0FBSCxFQUFLTCxDQUFMLEVBQU9DLENBQVAsRUFBU1gsQ0FBVCxDQUF2RixFQUFtRyxPQUFNLENBQUMsQ0FBUDs7QUFBUyxlQUFJLFVBQVE5QixDQUFDLENBQUNtTSxVQUFWLElBQXNCLENBQUMsQ0FBRCxLQUFLbk0sQ0FBQyxDQUFDbU0sVUFBN0IsSUFBeUNyRyxDQUFDLENBQUMsa0JBQWdCWCxlQUFlLENBQUN2QyxDQUFELENBQS9CLEdBQW1DLEtBQW5DLEdBQXlDdUMsZUFBZSxDQUFDdEMsQ0FBRCxDQUF4RCxHQUE0RCxJQUE3RCxDQUExQyxFQUE2RyxDQUFDNkMsQ0FBQyxHQUFDLGNBQVkxRixDQUFDLENBQUNvTSxJQUFGLElBQVE5QixDQUFDLENBQUMrQixXQUF0QixDQUFILE1BQXlDekosQ0FBQyxHQUFDbUcsRUFBRSxDQUFDbkcsQ0FBRCxFQUFHNUMsQ0FBQyxDQUFDc00sZUFBTCxDQUFKLEVBQTBCekosQ0FBQyxHQUFDa0csRUFBRSxDQUFDbEcsQ0FBRCxFQUFHN0MsQ0FBQyxDQUFDc00sZUFBTCxDQUE5QixFQUFvRDFKLENBQUMsQ0FBQ21GLElBQUYsSUFBUWhGLENBQUMsQ0FBQ0gsQ0FBRCxDQUE3RCxFQUFpRUMsQ0FBQyxDQUFDa0YsSUFBRixJQUFRaEYsQ0FBQyxDQUFDRixDQUFELENBQTFFLEVBQThFdEMsQ0FBQyxHQUFDMkksRUFBRSxDQUFDckQsQ0FBQyxDQUFDLENBQUQsQ0FBRixDQUFsRixFQUF5RixLQUFLd0QsT0FBTCxHQUFhekcsQ0FBQyxDQUFDb0ksTUFBRixHQUFTO0FBQUM3SCxhQUFDLEVBQUNQLENBQUMsQ0FBQ29GLElBQUYsR0FBT3pILENBQUMsQ0FBQzRDLENBQUYsR0FBSVAsQ0FBQyxDQUFDMkIsS0FBaEI7QUFBc0J0QixhQUFDLEVBQUNMLENBQUMsQ0FBQ3FGLEdBQUYsR0FBTTFILENBQUMsQ0FBQzBDLENBQUYsR0FBSUwsQ0FBQyxDQUFDNEI7QUFBcEMsV0FBL0csRUFBMkpxQixDQUFDLENBQUMsQ0FBRCxDQUFELEtBQU90RixDQUFDLEdBQUMySSxFQUFFLENBQUNyRCxDQUFDLENBQUMsQ0FBRCxDQUFGLENBQVgsQ0FBM0osRUFBOEssS0FBS3lELFFBQUwsR0FBYztBQUFDbkcsYUFBQyxFQUFDTixDQUFDLENBQUNtRixJQUFGLEdBQU96SCxDQUFDLENBQUM0QyxDQUFGLEdBQUlOLENBQUMsQ0FBQzBCLEtBQWhCO0FBQXNCdEIsYUFBQyxFQUFDSixDQUFDLENBQUNvRixHQUFGLEdBQU0xSCxDQUFDLENBQUMwQyxDQUFGLEdBQUlKLENBQUMsQ0FBQzJCO0FBQXBDLFdBQXJPLENBQTdHLEVBQStYLEtBQUsrSCxRQUFMLEdBQWN4TSxDQUFDLENBQUNxRixVQUFGLEdBQWF4QyxDQUExWixFQUE0WnhDLENBQUMsR0FBQ3dDLENBQUMsQ0FBQ2hCLE1BQXBhLEVBQTJhLENBQUMsQ0FBRCxHQUFHLEVBQUV4QixDQUFoYjtBQUFtYixpQkFBSTZDLENBQUMsR0FBQ0wsQ0FBQyxDQUFDeEMsQ0FBRCxDQUFILEVBQU84QyxDQUFDLEdBQUNMLENBQUMsQ0FBQ3pDLENBQUQsQ0FBVixFQUFjc0MsQ0FBQyxHQUFDTyxDQUFDLENBQUMrRixRQUFGLElBQVksRUFBNUIsRUFBK0JyRyxDQUFDLEdBQUNPLENBQUMsQ0FBQzhGLFFBQUYsSUFBWSxFQUE3QyxFQUFnRGhHLENBQUMsR0FBQ0MsQ0FBQyxDQUFDckIsTUFBcEQsRUFBMkRrQixDQUFDLEdBQUNvRCxDQUFDLEdBQUMsQ0FBbkUsRUFBcUVwRCxDQUFDLEdBQUNFLENBQXZFLEVBQXlFRixDQUFDLElBQUUsQ0FBNUU7QUFBOEVJLGVBQUMsQ0FBQ0osQ0FBRCxDQUFELEtBQU9HLENBQUMsQ0FBQ0gsQ0FBRCxDQUFSLElBQWFJLENBQUMsQ0FBQ0osQ0FBQyxHQUFDLENBQUgsQ0FBRCxLQUFTRyxDQUFDLENBQUNILENBQUMsR0FBQyxDQUFILENBQXZCLEtBQStCNEMsQ0FBQyxHQUFDaEQsQ0FBQyxDQUFDSSxDQUFELENBQUQsSUFBTUgsQ0FBQyxDQUFDRyxDQUFELENBQVAsSUFBWU0sQ0FBQyxHQUFDSCxDQUFDLENBQUNnRyxVQUFKLEVBQWU1SSxDQUFDLEdBQUM2QyxDQUFDLENBQUMrRixVQUFuQixFQUE4QnRELENBQUMsR0FBQzdDLENBQUMsSUFBRUEsQ0FBQyxLQUFHRSxDQUFDLEdBQUMsQ0FBTixHQUFRLElBQUVBLENBQVYsR0FBWSxDQUFkLENBQWpDLEVBQWtELEtBQUt3SixVQUFMLEdBQWdCO0FBQUM5QyxxQkFBSyxFQUFDLEtBQUs4QyxVQUFaO0FBQXVCbkssaUJBQUMsRUFBQ1MsQ0FBekI7QUFBMkIwRCxpQkFBQyxFQUFDcEcsQ0FBN0I7QUFBK0JxTSxtQkFBRyxFQUFDckosQ0FBQyxDQUFDTixDQUFDLEdBQUMsQ0FBSCxDQUFwQztBQUEwQzRKLG1CQUFHLEVBQUNyTSxDQUFDLENBQUN5QyxDQUFDLEdBQUMsQ0FBSCxDQUFELEdBQU9NLENBQUMsQ0FBQ04sQ0FBQyxHQUFDLENBQUgsQ0FBdEQ7QUFBNEQ2SixtQkFBRyxFQUFDdkosQ0FBQyxDQUFDdUMsQ0FBRCxDQUFqRTtBQUFxRWlILG1CQUFHLEVBQUN2TSxDQUFDLENBQUNzRixDQUFELENBQUQsR0FBS3ZDLENBQUMsQ0FBQ3VDLENBQUQ7QUFBL0UsZUFBbEUsRUFBc0p2RSxDQUFDLEdBQUMsS0FBS2dKLGNBQUwsQ0FBb0JuSCxDQUFwQixFQUFzQkMsQ0FBdEIsRUFBd0JKLENBQUMsR0FBQyxDQUExQixDQUF4SixFQUFxTCxLQUFLc0gsY0FBTCxDQUFvQm5ILENBQXBCLEVBQXNCQyxDQUF0QixFQUF3QkosQ0FBeEIsRUFBMEIxQixDQUExQixDQUFyTCxFQUFrTixLQUFLZ0osY0FBTCxDQUFvQm5ILENBQXBCLEVBQXNCQyxDQUF0QixFQUF3QnlDLENBQUMsR0FBQyxDQUExQixFQUE0QnZFLENBQTVCLENBQWxOLEVBQWlQMEIsQ0FBQyxJQUFFLENBQWhRLElBQW1RLEtBQUtzSCxjQUFMLENBQW9CbkgsQ0FBcEIsRUFBc0JDLENBQXRCLEVBQXdCSixDQUF4QixDQUFwUSxJQUFnUzFCLENBQUMsR0FBQyxLQUFLeUwsR0FBTCxDQUFTNUosQ0FBVCxFQUFXSCxDQUFYLEVBQWFHLENBQUMsQ0FBQ0gsQ0FBRCxDQUFkLEVBQWtCSSxDQUFDLENBQUNKLENBQUQsQ0FBbkIsQ0FBRixFQUEwQjFCLENBQUMsR0FBQyxLQUFLeUwsR0FBTCxDQUFTNUosQ0FBVCxFQUFXSCxDQUFDLEdBQUMsQ0FBYixFQUFlRyxDQUFDLENBQUNILENBQUMsR0FBQyxDQUFILENBQWhCLEVBQXNCSSxDQUFDLENBQUNKLENBQUMsR0FBQyxDQUFILENBQXZCLEtBQStCMUIsQ0FBM1YsQ0FBaEM7QUFBOUU7QUFBbmI7QUFBZzRCLFNBQTVyQyxNQUFpc0NBLENBQUMsR0FBQyxLQUFLeUwsR0FBTCxDQUFTOU0sQ0FBVCxFQUFXLGNBQVgsRUFBMEJBLENBQUMsQ0FBQ3FFLFlBQUYsQ0FBZTlCLENBQWYsSUFBa0IsRUFBNUMsRUFBK0NoQixDQUFDLEdBQUMsRUFBakQsRUFBb0RkLENBQXBELEVBQXNENEIsQ0FBdEQsRUFBd0QsQ0FBeEQsRUFBMER3RyxFQUFFLENBQUNwRyxDQUFELENBQTVELEVBQWdFRixDQUFoRSxDQUFGOztBQUFxRW9ELFNBQUMsS0FBRyxLQUFLbUgsR0FBTCxDQUFTLEtBQUt4RCxPQUFkLEVBQXNCLEdBQXRCLEVBQTBCLEtBQUtBLE9BQUwsQ0FBYWxHLENBQXZDLEVBQXlDLEtBQUttRyxRQUFMLENBQWNuRyxDQUF2RCxHQUEwRC9CLENBQUMsR0FBQyxLQUFLeUwsR0FBTCxDQUFTLEtBQUt4RCxPQUFkLEVBQXNCLEdBQXRCLEVBQTBCLEtBQUtBLE9BQUwsQ0FBYXBHLENBQXZDLEVBQXlDLEtBQUtxRyxRQUFMLENBQWNyRyxDQUF2RCxDQUEvRCxDQUFELEVBQTJIN0IsQ0FBQyxLQUFHLEtBQUswTCxNQUFMLENBQVloRyxJQUFaLENBQWlCLFVBQWpCLEdBQTZCMUYsQ0FBQyxDQUFDMkwsR0FBRixHQUFNekwsQ0FBbkMsRUFBcUNGLENBQUMsQ0FBQzRMLE9BQUYsR0FBVTFLLENBQWxELENBQTVIO0FBQWlMOztBQUFBLGFBQU8sQ0FBUDtBQUFTLEtBQWp4RjtBQUFreEZtSixVQUFNLEVBQUMsU0FBU0EsTUFBVCxDQUFnQjFMLENBQWhCLEVBQWtCQyxDQUFsQixFQUFvQjtBQUFDLFdBQUksSUFBSTBCLENBQUosRUFBTWxCLENBQU4sRUFBUTRCLENBQVIsRUFBVUMsQ0FBVixFQUFZQyxDQUFaLEVBQWNsQixDQUFkLEVBQWdCRSxDQUFoQixFQUFrQmlCLENBQWxCLEVBQW9CQyxDQUFwQixFQUFzQkMsQ0FBdEIsRUFBd0JDLENBQXhCLEVBQTBCQyxDQUExQixFQUE0QkMsQ0FBQyxHQUFDNUMsQ0FBQyxDQUFDdU0sUUFBaEMsRUFBeUMxSixDQUFDLEdBQUM3QyxDQUFDLENBQUN3TSxVQUE3QyxFQUF3RDFKLENBQUMsR0FBQzlDLENBQUMsQ0FBQ3lKLFNBQTVELEVBQXNFMUcsQ0FBQyxHQUFDL0MsQ0FBQyxDQUFDOEwsSUFBMUUsRUFBK0UxTCxDQUFDLEdBQUNKLENBQUMsQ0FBQ2tNLE9BQW5GLEVBQTJGbEosQ0FBQyxHQUFDaEQsQ0FBQyxDQUFDaU4sR0FBbkcsRUFBdUdqSyxDQUF2RztBQUEwR0EsU0FBQyxDQUFDdEIsQ0FBRixDQUFJM0IsQ0FBSixFQUFNaUQsQ0FBQyxDQUFDSCxDQUFSLEdBQVdHLENBQUMsR0FBQ0EsQ0FBQyxDQUFDMEcsS0FBZjtBQUExRzs7QUFBK0gsVUFBRyxNQUFJM0osQ0FBSixJQUFPQyxDQUFDLENBQUMyTCxNQUFaLEVBQW1CLEtBQUkzSSxDQUFDLEdBQUNoRCxDQUFDLENBQUNpTixHQUFSLEVBQVlqSyxDQUFaO0FBQWVBLFNBQUMsQ0FBQytKLEdBQUYsS0FBUS9NLENBQUMsQ0FBQ3VMLEtBQUYsR0FBUW5MLENBQUMsQ0FBQ0osQ0FBQyxDQUFDdUwsS0FBSCxDQUFELEdBQVd2SSxDQUFDLENBQUMrSixHQUFyQixHQUF5QjNNLENBQUMsQ0FBQzhFLFlBQUYsQ0FBZWxDLENBQUMsQ0FBQ2dLLE9BQWpCLEVBQXlCaEssQ0FBQyxDQUFDK0osR0FBM0IsQ0FBakMsR0FBa0UvSixDQUFDLEdBQUNBLENBQUMsQ0FBQzBHLEtBQXRFO0FBQWYsT0FBbkIsTUFBbUgsSUFBRzlHLENBQUgsRUFBSztBQUFDLGVBQUtFLENBQUw7QUFBUVIsV0FBQyxHQUFDUSxDQUFDLENBQUM2RyxFQUFGLEdBQUs1SixDQUFDLEdBQUMrQyxDQUFDLENBQUMwRyxFQUFYLEVBQWNuSCxDQUFDLEdBQUNTLENBQUMsQ0FBQzhHLEVBQUYsR0FBSzdKLENBQUMsR0FBQytDLENBQUMsQ0FBQytHLEVBQXpCLEVBQTRCL0csQ0FBQyxDQUFDL0MsQ0FBRixDQUFJK0MsQ0FBQyxDQUFDVCxDQUFOLElBQVNyQyxDQUFDLENBQUNxSixPQUFGLENBQVVsRyxDQUFWLEdBQVlBLENBQUMsQ0FBQ2IsQ0FBRCxDQUFELEdBQUtELENBQXRELEVBQXdEUyxDQUFDLENBQUMvQyxDQUFGLENBQUkrQyxDQUFDLENBQUNULENBQUYsR0FBSSxDQUFSLElBQVdyQyxDQUFDLENBQUNxSixPQUFGLENBQVVwRyxDQUFWLEdBQVl1RCxDQUFDLENBQUNsRSxDQUFELENBQUQsR0FBS0QsQ0FBcEYsRUFBc0ZTLENBQUMsR0FBQ0EsQ0FBQyxDQUFDNEcsS0FBMUY7QUFBUjs7QUFBd0csYUFBSWxKLENBQUMsR0FBQ1QsQ0FBQyxHQUFDLEVBQUYsR0FBSyxJQUFFQSxDQUFGLEdBQUlBLENBQVQsR0FBVyxDQUFDLElBQUUsSUFBRUEsQ0FBTCxJQUFRQSxDQUFSLEdBQVUsQ0FBM0IsRUFBNkI4QyxDQUE3QjtBQUFnQ0YsV0FBQyxHQUFDLENBQUN2QixDQUFDLEdBQUN5QixDQUFDLENBQUNSLENBQUwsS0FBU2pCLENBQUMsS0FBRyxDQUFDZ0IsQ0FBQyxHQUFDUSxDQUFDLENBQUNDLENBQUMsQ0FBQzJELENBQUgsQ0FBSixFQUFXNUUsTUFBWCxHQUFrQixDQUF0QixHQUF3QixJQUFFUSxDQUFDLENBQUNSLE1BQTVCLEdBQW1DLENBQTVDLENBQUYsRUFBaURVLENBQUMsR0FBQ1ksQ0FBQyxDQUFDZCxDQUFDLENBQUNPLENBQUQsQ0FBRCxHQUFLUCxDQUFDLENBQUNoQixDQUFDLEdBQUMsQ0FBSCxDQUFQLEVBQWFnQixDQUFDLENBQUNPLENBQUMsR0FBQyxDQUFILENBQUQsR0FBT1AsQ0FBQyxDQUFDaEIsQ0FBRCxDQUFyQixDQUFwRCxFQUE4RXFCLENBQUMsR0FBQytELENBQUMsQ0FBQ2xFLENBQUQsQ0FBakYsRUFBcUZJLENBQUMsR0FBQ1MsQ0FBQyxDQUFDYixDQUFELENBQXhGLEVBQTRGQyxDQUFDLEdBQUNILENBQUMsQ0FBQ2hCLENBQUMsR0FBQyxDQUFILENBQS9GLEVBQXFHb0IsQ0FBQyxHQUFDSixDQUFDLENBQUNoQixDQUFDLEdBQUMsQ0FBSCxDQUF4RyxFQUE4R2lCLENBQUMsR0FBQ1EsQ0FBQyxDQUFDNEosR0FBRixHQUFNak0sQ0FBQyxHQUFDcUMsQ0FBQyxDQUFDNkosR0FBMUgsRUFBOEh0SyxDQUFDLENBQUNoQixDQUFELENBQUQsR0FBS21CLENBQUMsR0FBQ0csQ0FBQyxHQUFDTCxDQUF2SSxFQUF5SUQsQ0FBQyxDQUFDaEIsQ0FBQyxHQUFDLENBQUgsQ0FBRCxHQUFPb0IsQ0FBQyxHQUFDQyxDQUFDLEdBQUNKLENBQXBKLEVBQXNKQSxDQUFDLEdBQUNRLENBQUMsQ0FBQzhKLEdBQUYsR0FBTW5NLENBQUMsR0FBQ3FDLENBQUMsQ0FBQytKLEdBQWxLLEVBQXNLeEssQ0FBQyxDQUFDTyxDQUFDLEdBQUMsQ0FBSCxDQUFELEdBQU9KLENBQUMsR0FBQ0csQ0FBQyxHQUFDTCxDQUFqTCxFQUFtTEQsQ0FBQyxDQUFDTyxDQUFELENBQUQsR0FBS0gsQ0FBQyxHQUFDQyxDQUFDLEdBQUNKLENBQTVMLEVBQThMUSxDQUFDLEdBQUNBLENBQUMsQ0FBQzZHLEtBQWxNO0FBQWhDOztBQUF3TyxZQUFHdEosQ0FBQyxDQUFDZ0YsVUFBRixHQUFheEMsQ0FBYixFQUFlNUMsQ0FBQyxDQUFDMkwsTUFBcEIsRUFBMkI7QUFBQyxlQUFJakssQ0FBQyxHQUFDLEVBQUYsRUFBS0osQ0FBQyxHQUFDLENBQVgsRUFBYUEsQ0FBQyxHQUFDc0IsQ0FBQyxDQUFDaEIsTUFBakIsRUFBd0JOLENBQUMsRUFBekI7QUFBNEIsaUJBQUllLENBQUMsR0FBQyxDQUFDRCxDQUFDLEdBQUNRLENBQUMsQ0FBQ3RCLENBQUQsQ0FBSixFQUFTTSxNQUFYLEVBQWtCRixDQUFDLElBQUUsTUFBSSxDQUFDVSxDQUFDLENBQUMsQ0FBRCxDQUFELEdBQUtXLENBQUwsR0FBTyxDQUFSLElBQVdBLENBQWYsR0FBaUIsR0FBakIsR0FBcUIsQ0FBQ1gsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFLVyxDQUFMLEdBQU8sQ0FBUixJQUFXQSxDQUFoQyxHQUFrQyxJQUF2RCxFQUE0RDNCLENBQUMsR0FBQyxDQUFsRSxFQUFvRUEsQ0FBQyxHQUFDaUIsQ0FBdEUsRUFBd0VqQixDQUFDLEVBQXpFO0FBQTRFTSxlQUFDLElBQUUsQ0FBQ1UsQ0FBQyxDQUFDaEIsQ0FBRCxDQUFELEdBQUsyQixDQUFMLEdBQU8sQ0FBUixJQUFXQSxDQUFYLEdBQWEsR0FBaEI7QUFBNUU7QUFBNUI7O0FBQTRIL0MsV0FBQyxDQUFDdUwsS0FBRixHQUFRbkwsQ0FBQyxDQUFDSixDQUFDLENBQUN1TCxLQUFILENBQUQsR0FBVzdKLENBQW5CLEdBQXFCdEIsQ0FBQyxDQUFDOEUsWUFBRixDQUFlLEdBQWYsRUFBbUJ4RCxDQUFuQixDQUFyQjtBQUEyQztBQUFDO0FBQUExQixPQUFDLENBQUN3TCxPQUFGLElBQVc1SSxDQUFYLElBQWM1QyxDQUFDLENBQUN3TCxPQUFGLENBQVU1SCxJQUFWLENBQWU1RCxDQUFDLENBQUNpTSxNQUFqQixFQUF3QnJKLENBQXhCLEVBQTBCeEMsQ0FBMUIsQ0FBZDtBQUEyQyxLQUFybUg7QUFBc21IOE0sUUFBSSxFQUFDLFNBQVNBLElBQVQsR0FBZTtBQUFDLFdBQUtELEdBQUwsR0FBUyxLQUFLVixRQUFMLEdBQWMsQ0FBdkI7QUFBeUIsS0FBcHBIO0FBQXFwSFksY0FBVSxFQUFDLFNBQVNBLFVBQVQsQ0FBb0JwTixDQUFwQixFQUFzQjtBQUFDLFVBQUlDLENBQUo7QUFBQSxVQUFNMEIsQ0FBQyxHQUFDLENBQUMzQixDQUFDLEdBQUNLLENBQUMsQ0FBQ0wsQ0FBRCxDQUFELElBQU1TLENBQUMsQ0FBQzJLLElBQUYsQ0FBT3BMLENBQVAsQ0FBTixJQUFpQjBELFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUJyTixDQUF2QixDQUFqQixJQUE0Q0EsQ0FBL0MsRUFBa0RxRSxZQUFsRCxHQUErRHJFLENBQS9ELEdBQWlFLENBQXpFO0FBQTJFLGFBQU8yQixDQUFDLEtBQUczQixDQUFDLEdBQUNBLENBQUMsQ0FBQ3FFLFlBQUYsQ0FBZSxHQUFmLENBQUwsQ0FBRCxJQUE0QjFDLENBQUMsQ0FBQzJMLE9BQUYsS0FBWTNMLENBQUMsQ0FBQzJMLE9BQUYsR0FBVSxFQUF0QixHQUEwQixDQUFDck4sQ0FBQyxHQUFDMEIsQ0FBQyxDQUFDMkwsT0FBRixDQUFVdE4sQ0FBVixDQUFILEtBQWtCLENBQUNDLENBQUMsQ0FBQ3NOLE1BQXJCLEdBQTRCdE4sQ0FBNUIsR0FBOEIwQixDQUFDLENBQUMyTCxPQUFGLENBQVV0TixDQUFWLElBQWFzRixlQUFlLENBQUN0RixDQUFELENBQWhILElBQXFIQSxDQUFDLEdBQUNLLENBQUMsQ0FBQ0wsQ0FBRCxDQUFELEdBQUtzRixlQUFlLENBQUN0RixDQUFELENBQXBCLEdBQXdCcUIsQ0FBQyxDQUFDckIsQ0FBQyxDQUFDLENBQUQsQ0FBRixDQUFELEdBQVEsQ0FBQ0EsQ0FBRCxDQUFSLEdBQVlBLENBQXJDLEdBQXVDa0gsT0FBTyxDQUFDVyxJQUFSLENBQWEsdURBQWIsQ0FBcEs7QUFBME8sS0FBNStIO0FBQTYrSHZDLG1CQUFlLEVBQUNBLGVBQTcvSDtBQUE2Z0lGLG1CQUFlLEVBQUNBLGVBQTdoSTtBQUE2aUlvSSxjQUFVLEVBQUMsU0FBU0MsV0FBVCxDQUFxQnpOLENBQXJCLEVBQXVCQyxDQUF2QixFQUF5QjBCLENBQXpCLEVBQTJCbEIsQ0FBM0IsRUFBNkI0QixDQUE3QixFQUErQjtBQUFDLFVBQUlDLENBQUMsR0FBQ2dELGVBQWUsQ0FBQ3RGLENBQUMsQ0FBQyxDQUFELENBQUYsQ0FBckI7QUFBQSxVQUE0QnVDLENBQUMsR0FBQytDLGVBQWUsQ0FBQ3RGLENBQUMsQ0FBQyxDQUFELENBQUYsQ0FBN0M7QUFBb0RnRyxPQUFDLENBQUMxRCxDQUFELEVBQUdDLENBQUgsRUFBS3RDLENBQUMsSUFBRSxNQUFJQSxDQUFQLEdBQVNBLENBQVQsR0FBVyxNQUFoQixFQUF1QjBCLENBQXZCLEVBQXlCVSxDQUF6QixDQUFELEtBQStCckMsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFLb0YsZUFBZSxDQUFDOUMsQ0FBRCxDQUFwQixFQUF3QnRDLENBQUMsQ0FBQyxDQUFELENBQUQsR0FBS29GLGVBQWUsQ0FBQzdDLENBQUQsQ0FBNUMsRUFBZ0QsVUFBUTlCLENBQVIsSUFBVyxDQUFDLENBQUQsS0FBS0EsQ0FBaEIsSUFBbUJzRixDQUFDLENBQUMsa0JBQWdCL0YsQ0FBQyxDQUFDLENBQUQsQ0FBakIsR0FBcUIsS0FBckIsR0FBMkJBLENBQUMsQ0FBQyxDQUFELENBQTVCLEdBQWdDLElBQWpDLENBQW5HO0FBQTJJLEtBQXZ4STtBQUF3eEkwTixnQkFBWSxFQUFDOUUsRUFBcnlJO0FBQXd5SStFLGdCQUFZLEVBQUMzSyxDQUFyekk7QUFBdXpJNEssMkJBQXVCLEVBQUM1SCxDQUEvMEk7QUFBaTFJNUQsaUJBQWEsRUFBQyxTQUFTeUwsZUFBVCxDQUF5QjdOLENBQXpCLEVBQTJCQyxDQUEzQixFQUE2QjtBQUFDLGFBQU9nRyxDQUFDLENBQUNqRyxDQUFELENBQUQsQ0FBS0YsR0FBTCxDQUFTLFVBQVNFLENBQVQsRUFBVztBQUFDLGVBQU9vQyxhQUFhLENBQUNwQyxDQUFELEVBQUcsQ0FBQyxDQUFELEtBQUtDLENBQVIsQ0FBcEI7QUFBK0IsT0FBcEQsQ0FBUDtBQUE2RCxLQUExN0k7QUFBMjdJcU0sZUFBVyxFQUFDLFFBQXY4STtBQUFnOUlSLHVCQUFtQixFQUFDLENBQUMsQ0FBcitJO0FBQXUrSVAsY0FBVSxFQUFDO0FBQWwvSSxHQUF0UTtBQUFnd0pySSxHQUFDLE1BQUl2QixDQUFDLENBQUNpRyxjQUFGLENBQWlCMkMsQ0FBakIsQ0FBTCxFQUF5QnZLLENBQUMsQ0FBQzhOLGNBQUYsR0FBaUJ2RCxDQUExQyxFQUE0Q3ZLLENBQUMsV0FBRCxHQUFVdUssQ0FBdEQ7O0FBQXdELE1BQUksT0FBTzdDLE1BQVAsS0FBaUIsV0FBakIsSUFBOEJBLE1BQU0sS0FBRzFILENBQTNDLEVBQTZDO0FBQUMrTixVQUFNLENBQUNDLGNBQVAsQ0FBc0JoTyxDQUF0QixFQUF3QixZQUF4QixFQUFxQztBQUFDaU8sV0FBSyxFQUFDLENBQUM7QUFBUixLQUFyQztBQUFpRCxHQUEvRixNQUFxRztBQUFDLFdBQU9qTyxDQUFDLFdBQVI7QUFBaUI7QUFBQyxDQUF6cmYsQ0FBRCxDOzs7Ozs7Ozs7Ozs7O0FDVEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBLENBQUMsVUFBU1UsQ0FBVCxFQUFXa0MsQ0FBWCxFQUFhO0FBQUMsNENBQWlCMUMsT0FBakIsTUFBMEIsZUFBYSxPQUFPQyxNQUE5QyxHQUFxRHlDLENBQUMsQ0FBQzFDLE9BQUQsQ0FBdEQsR0FBZ0UsUUFBc0NFLGlDQUFPLENBQUMsT0FBRCxDQUFELG9DQUFhd0MsQ0FBYjtBQUFBO0FBQUE7QUFBQSxvR0FBNUMsR0FBNERBLFNBQTVIO0FBQStKLENBQTdLLENBQThLLElBQTlLLEVBQW1MLFVBQVNsQyxDQUFULEVBQVc7QUFBQzs7QUFBYSxNQUFJRixDQUFDLEdBQUMsMmdPQUFOOztBQUFraE8sV0FBU08sQ0FBVCxDQUFXTCxDQUFYLEVBQWE7QUFBQyxXQUFPVCxDQUFDLENBQUM2SyxnQkFBRixDQUFtQnBLLENBQW5CLENBQVA7QUFBNkI7O0FBQUEsV0FBU0QsQ0FBVCxDQUFXQyxDQUFYLEVBQWFrQyxDQUFiLEVBQWU7QUFBQyxRQUFJM0MsQ0FBSjtBQUFNLFdBQU9xQyxDQUFDLENBQUM1QixDQUFELENBQUQsR0FBS0EsQ0FBTCxHQUFPLGFBQVdULENBQUMsV0FBUVMsQ0FBUixDQUFaLEtBQXdCLENBQUNrQyxDQUF6QixJQUE0QmxDLENBQTVCLEdBQThCRyxDQUFDLENBQUNnRCxJQUFGLENBQU9zRSxDQUFDLENBQUMrRixnQkFBRixDQUFtQnhOLENBQW5CLENBQVAsRUFBNkIsQ0FBN0IsQ0FBOUIsR0FBOERBLENBQUMsSUFBRSxZQUFVVCxDQUFiLElBQWdCLFlBQVdTLENBQTNCLEdBQTZCRyxDQUFDLENBQUNnRCxJQUFGLENBQU9uRCxDQUFQLEVBQVMsQ0FBVCxDQUE3QixHQUF5Q0EsQ0FBQyxHQUFDLENBQUNBLENBQUQsQ0FBRCxHQUFLLEVBQTNIO0FBQThIOztBQUFBLFdBQVMyQixDQUFULENBQVczQixDQUFYLEVBQWE7QUFBQyxXQUFNLGVBQWFBLENBQUMsQ0FBQ3lOLFFBQWYsSUFBeUIsQ0FBQyxDQUFELEtBQUt6TixDQUFDLENBQUMwTixRQUF0QztBQUErQzs7QUFBQSxXQUFTekwsQ0FBVCxDQUFXakMsQ0FBWCxFQUFha0MsQ0FBYixFQUFlO0FBQUMsU0FBSSxJQUFJM0MsQ0FBSixFQUFNa0csQ0FBQyxHQUFDdkQsQ0FBQyxDQUFDZixNQUFkLEVBQXFCLENBQUMsQ0FBRCxHQUFHLEVBQUVzRSxDQUExQjtBQUE2QixVQUFHbEcsQ0FBQyxHQUFDMkMsQ0FBQyxDQUFDdUQsQ0FBRCxDQUFILEVBQU96RixDQUFDLENBQUM0RyxNQUFGLENBQVMsQ0FBVCxFQUFXckgsQ0FBQyxDQUFDNEIsTUFBYixNQUF1QjVCLENBQWpDLEVBQW1DLE9BQU9BLENBQUMsQ0FBQzRCLE1BQVQ7QUFBaEU7QUFBZ0Y7O0FBQUEsV0FBU0YsQ0FBVCxDQUFXakIsQ0FBWCxFQUFha0MsQ0FBYixFQUFlO0FBQUMsU0FBSyxDQUFMLEtBQVNsQyxDQUFULEtBQWFBLENBQUMsR0FBQyxFQUFmO0FBQW1CLFFBQUlULENBQUMsR0FBQyxDQUFDUyxDQUFDLENBQUNzRCxPQUFGLENBQVUsSUFBVixDQUFQO0FBQUEsUUFBdUJtQyxDQUFDLEdBQUMsQ0FBekI7QUFBMkIsV0FBT2xHLENBQUMsS0FBR1MsQ0FBQyxHQUFDQSxDQUFDLENBQUMwRCxLQUFGLENBQVEsSUFBUixFQUFjTSxJQUFkLENBQW1CLEVBQW5CLENBQUwsQ0FBRCxFQUE4QixZQUFVO0FBQUMsYUFBTSxNQUFJOUIsQ0FBSixHQUFNLGtEQUFOLElBQTBEbEMsQ0FBQyxHQUFDLGFBQVdBLENBQVgsSUFBY1QsQ0FBQyxHQUFDa0csQ0FBQyxFQUFGLEdBQUssRUFBcEIsSUFBd0IsSUFBekIsR0FBOEIsR0FBekYsQ0FBTjtBQUFvRyxLQUFwSjtBQUFxSjs7QUFBQSxXQUFTNUUsQ0FBVCxDQUFXYixDQUFYLEVBQWFrQyxDQUFiLEVBQWUzQyxDQUFmLEVBQWlCO0FBQUMsUUFBSWtHLENBQUMsR0FBQ3pGLENBQUMsQ0FBQ21LLFFBQVI7QUFBaUIsUUFBRyxNQUFJMUUsQ0FBSixJQUFPLE1BQUlBLENBQVgsSUFBYyxPQUFLQSxDQUF0QixFQUF3QixLQUFJekYsQ0FBQyxHQUFDQSxDQUFDLENBQUMyTixVQUFSLEVBQW1CM04sQ0FBbkIsRUFBcUJBLENBQUMsR0FBQ0EsQ0FBQyxDQUFDNE4sV0FBekI7QUFBcUMvTSxPQUFDLENBQUNiLENBQUQsRUFBR2tDLENBQUgsRUFBSzNDLENBQUwsQ0FBRDtBQUFyQyxLQUF4QixNQUEyRSxNQUFJa0csQ0FBSixJQUFPLE1BQUlBLENBQVgsS0FBZXpGLENBQUMsQ0FBQ3dELFNBQUYsR0FBWXhELENBQUMsQ0FBQ3dELFNBQUYsQ0FBWUUsS0FBWixDQUFrQnhCLENBQWxCLEVBQXFCOEIsSUFBckIsQ0FBMEJ6RSxDQUExQixDQUEzQjtBQUF5RDs7QUFBQSxXQUFTRCxDQUFULENBQVdVLENBQVgsRUFBYWtDLENBQWIsRUFBZTtBQUFDLFNBQUksSUFBSTNDLENBQUMsR0FBQzJDLENBQUMsQ0FBQ2YsTUFBWixFQUFtQixDQUFDLENBQUQsR0FBRyxFQUFFNUIsQ0FBeEI7QUFBMkJTLE9BQUMsQ0FBQ3FHLElBQUYsQ0FBT25FLENBQUMsQ0FBQzNDLENBQUQsQ0FBUjtBQUEzQjtBQUF3Qzs7QUFBQSxXQUFTMkMsQ0FBVCxDQUFXbEMsQ0FBWCxFQUFha0MsQ0FBYixFQUFlM0MsQ0FBZixFQUFpQjtBQUFDLFNBQUksSUFBSWtHLENBQVIsRUFBVXpGLENBQUMsSUFBRUEsQ0FBQyxLQUFHa0MsQ0FBakIsR0FBb0I7QUFBQyxVQUFHdUQsQ0FBQyxHQUFDekYsQ0FBQyxDQUFDaUosS0FBRixJQUFTakosQ0FBQyxDQUFDNE4sV0FBaEIsRUFBNEIsT0FBT25JLENBQUMsQ0FBQ29JLFdBQUYsQ0FBY2hILE1BQWQsQ0FBcUIsQ0FBckIsTUFBMEJ0SCxDQUFqQztBQUFtQ1MsT0FBQyxHQUFDQSxDQUFDLENBQUM2RSxVQUFGLElBQWM3RSxDQUFDLENBQUM4TixPQUFsQjtBQUEwQjtBQUFDOztBQUFBLFdBQVN2TCxDQUFULENBQVd2QyxDQUFYLEVBQWE7QUFBQyxRQUFJa0MsQ0FBSjtBQUFBLFFBQU0zQyxDQUFOO0FBQUEsUUFBUWtHLENBQUMsR0FBQzFGLENBQUMsQ0FBQ0MsQ0FBQyxDQUFDK04sVUFBSCxDQUFYO0FBQUEsUUFBMEJ6TyxDQUFDLEdBQUNtRyxDQUFDLENBQUN0RSxNQUE5Qjs7QUFBcUMsU0FBSWUsQ0FBQyxHQUFDLENBQU4sRUFBUUEsQ0FBQyxHQUFDNUMsQ0FBVixFQUFZNEMsQ0FBQyxFQUFiO0FBQWdCLE9BQUMzQyxDQUFDLEdBQUNrRyxDQUFDLENBQUN2RCxDQUFELENBQUosRUFBUzhMLFFBQVQsR0FBa0J6TCxDQUFDLENBQUNoRCxDQUFELENBQW5CLElBQXdCMkMsQ0FBQyxJQUFFLE1BQUkzQyxDQUFDLENBQUMwTyxlQUFGLENBQWtCOUQsUUFBekIsR0FBa0M1SyxDQUFDLENBQUMwTyxlQUFGLENBQWtCekssU0FBbEIsSUFBNkIsTUFBSWpFLENBQUMsQ0FBQzRLLFFBQU4sR0FBZTVLLENBQUMsQ0FBQ2lFLFNBQWpCLEdBQTJCakUsQ0FBQyxDQUFDb08sVUFBRixDQUFhbkssU0FBdkcsR0FBaUgsTUFBSWpFLENBQUMsQ0FBQzRLLFFBQU4sSUFBZ0JuSyxDQUFDLENBQUM4RSxZQUFGLENBQWV2RixDQUFDLENBQUNvTyxVQUFqQixFQUE0QnBPLENBQTVCLENBQWpJLEVBQWdLUyxDQUFDLENBQUMrRSxXQUFGLENBQWN4RixDQUFkLENBQXhMO0FBQWhCO0FBQTBOOztBQUFBLFdBQVNrRCxDQUFULENBQVd6QyxDQUFYLEVBQWFrQyxDQUFiLEVBQWU7QUFBQyxXQUFPOEYsVUFBVSxDQUFDOUYsQ0FBQyxDQUFDbEMsQ0FBRCxDQUFGLENBQVYsSUFBa0IsQ0FBekI7QUFBMkI7O0FBQUEsV0FBUzBDLENBQVQsQ0FBVzFDLENBQVgsRUFBYVQsQ0FBYixFQUFla0csQ0FBZixFQUFpQkYsQ0FBakIsRUFBbUIzRCxDQUFuQixFQUFxQjdCLENBQXJCLEVBQXVCSSxDQUF2QixFQUF5QjtBQUFDLFFBQUljLENBQUo7QUFBQSxRQUFNYSxDQUFOO0FBQUEsUUFBUUQsQ0FBUjtBQUFBLFFBQVVJLENBQVY7QUFBQSxRQUFZRyxDQUFaO0FBQUEsUUFBY3pCLENBQWQ7QUFBQSxRQUFnQnlGLENBQWhCO0FBQUEsUUFBa0JqRSxDQUFsQjtBQUFBLFFBQW9CdEMsQ0FBcEI7QUFBQSxRQUFzQm1DLENBQXRCO0FBQUEsUUFBd0JELENBQXhCO0FBQUEsUUFBMEJXLENBQTFCO0FBQUEsUUFBNEJGLENBQUMsR0FBQ25DLENBQUMsQ0FBQ0wsQ0FBRCxDQUEvQjtBQUFBLFFBQW1DRixDQUFDLEdBQUMyQyxDQUFDLENBQUMsYUFBRCxFQUFlRCxDQUFmLENBQXRDO0FBQUEsUUFBd0RILENBQUMsR0FBQyxDQUFDLEdBQTNEO0FBQUEsUUFBK0Q0QyxDQUFDLEdBQUN4QyxDQUFDLENBQUMsbUJBQUQsRUFBcUJELENBQXJCLENBQUQsR0FBeUJDLENBQUMsQ0FBQyxnQkFBRCxFQUFrQkQsQ0FBbEIsQ0FBM0Y7QUFBQSxRQUFnSEcsQ0FBQyxHQUFDRixDQUFDLENBQUMsaUJBQUQsRUFBbUJELENBQW5CLENBQUQsR0FBdUJDLENBQUMsQ0FBQyxrQkFBRCxFQUFvQkQsQ0FBcEIsQ0FBMUk7QUFBQSxRQUFpSzBDLENBQUMsR0FBQ3pDLENBQUMsQ0FBQyxZQUFELEVBQWNELENBQWQsQ0FBRCxHQUFrQkMsQ0FBQyxDQUFDLGVBQUQsRUFBaUJELENBQWpCLENBQXRMO0FBQUEsUUFBME03QyxDQUFDLEdBQUM4QyxDQUFDLENBQUMsYUFBRCxFQUFlRCxDQUFmLENBQUQsR0FBbUJDLENBQUMsQ0FBQyxjQUFELEVBQWdCRCxDQUFoQixDQUFoTztBQUFBLFFBQW1QNkMsQ0FBQyxHQUFDLEtBQUc1QyxDQUFDLENBQUMsVUFBRCxFQUFZRCxDQUFaLENBQXpQO0FBQUEsUUFBd1FrRixDQUFDLEdBQUNsRixDQUFDLENBQUMwTCxTQUE1UTtBQUFBLFFBQXNSL0gsQ0FBQyxHQUFDLEVBQXhSO0FBQUEsUUFBMlJmLENBQUMsR0FBQyxFQUE3UjtBQUFBLFFBQWdTVSxDQUFDLEdBQUMsRUFBbFM7QUFBQSxRQUFxU0MsQ0FBQyxHQUFDeEcsQ0FBQyxDQUFDNE8sYUFBRixJQUFpQixHQUF4VDtBQUFBLFFBQTRUdk8sQ0FBQyxHQUFDTCxDQUFDLENBQUM2TyxHQUFGLEdBQU03TyxDQUFDLENBQUM2TyxHQUFSLEdBQVk3TyxDQUFDLENBQUM4TyxJQUFGLEdBQU8sTUFBUCxHQUFjLEtBQXhWO0FBQUEsUUFBOFZsSixDQUFDLEdBQUM1RixDQUFDLENBQUNvTSxJQUFGLElBQVFwTSxDQUFDLENBQUNtRSxLQUFWLElBQWlCLG1CQUFqWDtBQUFBLFFBQXFZckMsQ0FBQyxHQUFDTyxDQUFDLElBQUUsQ0FBQ3VELENBQUMsQ0FBQzdCLE9BQUYsQ0FBVSxPQUFWLENBQUosR0FBdUIsRUFBdkIsR0FBMEIsSUFBamE7QUFBQSxRQUFzYWhCLENBQUMsR0FBQyxDQUFDNkMsQ0FBQyxDQUFDN0IsT0FBRixDQUFVLE9BQVYsQ0FBemE7QUFBQSxRQUE0YjRDLENBQUMsR0FBQyxDQUFDZixDQUFDLENBQUM3QixPQUFGLENBQVUsT0FBVixDQUEvYjtBQUFBLFFBQWtkMEMsQ0FBQyxHQUFDckUsQ0FBQyxDQUFDcEMsQ0FBRCxDQUFyZDtBQUFBLFFBQXlkcUcsQ0FBQyxHQUFDckcsQ0FBQyxDQUFDK08sVUFBN2Q7QUFBQSxRQUF3ZXpFLENBQUMsR0FBQyxDQUFDLENBQUNqRSxDQUFDLElBQUUsRUFBSixFQUFRdEMsT0FBUixDQUFnQixJQUFoQixDQUEzZTtBQUFBLFFBQWlnQmlMLENBQUMsR0FBQyxFQUFuZ0I7O0FBQXNnQixTQUFJMUUsQ0FBQyxLQUFHakUsQ0FBQyxHQUFDQSxDQUFDLENBQUNsQyxLQUFGLENBQVEsSUFBUixFQUFjTSxJQUFkLENBQW1CLEVBQW5CLENBQUwsQ0FBRCxFQUE4Qm5DLENBQUMsR0FBQyxDQUFDQyxDQUFDLEdBQUM5QixDQUFDLENBQUN3TyxvQkFBRixDQUF1QixHQUF2QixDQUFILEVBQWdDck4sTUFBaEUsRUFBdUVpQixDQUFDLEdBQUMsRUFBekUsRUFBNEVuQixDQUFDLEdBQUMsQ0FBbEYsRUFBb0ZBLENBQUMsR0FBQ1ksQ0FBdEYsRUFBd0ZaLENBQUMsRUFBekY7QUFBNEZtQixPQUFDLENBQUNuQixDQUFELENBQUQsR0FBS2EsQ0FBQyxDQUFDYixDQUFELENBQU47QUFBNUY7O0FBQXNHLFFBQUdJLENBQUMsSUFBRTJFLENBQU4sRUFBUSxLQUFJL0UsQ0FBQyxHQUFDLENBQU4sRUFBUUEsQ0FBQyxHQUFDWSxDQUFWLEVBQVlaLENBQUMsRUFBYjtBQUFnQixPQUFDLENBQUNOLENBQUMsR0FBQyxDQUFDc0IsQ0FBQyxHQUFDRyxDQUFDLENBQUNuQixDQUFELENBQUosRUFBUzRELFVBQVQsS0FBc0I3RSxDQUF6QixLQUE2QmdHLENBQTdCLElBQWdDRSxDQUFDLElBQUUsQ0FBQzVELENBQXJDLE1BQTBDSSxDQUFDLEdBQUNULENBQUMsQ0FBQ3dNLFNBQUosRUFBY3BOLENBQUMsSUFBRVYsQ0FBSCxJQUFNVixJQUFJLENBQUNPLEdBQUwsQ0FBU2tDLENBQUMsR0FBQ0wsQ0FBWCxJQUFjZ0QsQ0FBcEIsS0FBd0IsU0FBT3BELENBQUMsQ0FBQ29CLFFBQVQsSUFBbUIsTUFBSXBDLENBQS9DLE1BQW9EbUYsQ0FBQyxHQUFDLEVBQUYsRUFBSy9FLENBQUMsQ0FBQ2dGLElBQUYsQ0FBT0QsQ0FBUCxDQUFMLEVBQWUvRCxDQUFDLEdBQUNLLENBQXJFLENBQWQsRUFBc0ZzRCxDQUFDLEtBQUcvRCxDQUFDLENBQUN5TSxFQUFGLEdBQUt6TSxDQUFDLENBQUMwTSxVQUFQLEVBQWtCMU0sQ0FBQyxDQUFDMk0sRUFBRixHQUFLbE0sQ0FBdkIsRUFBeUJULENBQUMsQ0FBQzRNLEVBQUYsR0FBSzVNLENBQUMsQ0FBQzZNLFdBQWhDLEVBQTRDN00sQ0FBQyxDQUFDOE0sRUFBRixHQUFLOU0sQ0FBQyxDQUFDK00sWUFBdEQsQ0FBdkYsRUFBMkozTixDQUFDLEtBQUcsQ0FBQ1ksQ0FBQyxDQUFDK0wsUUFBRixJQUFZck4sQ0FBWixJQUFlLENBQUN1RixDQUFELElBQUl2RixDQUFuQixJQUFzQjJCLENBQUMsSUFBRTNCLENBQXpCLElBQTRCLENBQUMyQixDQUFELElBQUlMLENBQUMsQ0FBQzRDLFVBQUYsQ0FBYUEsVUFBYixLQUEwQjdFLENBQTlCLElBQWlDLENBQUNpQyxDQUFDLENBQUM0QyxVQUFGLENBQWFtSixRQUE1RSxNQUF3RjVILENBQUMsQ0FBQ0MsSUFBRixDQUFPcEUsQ0FBUCxHQUFVQSxDQUFDLENBQUN5TSxFQUFGLElBQU01TyxDQUFoQixFQUFrQm9DLENBQUMsQ0FBQ0QsQ0FBRCxFQUFHakMsQ0FBSCxFQUFLK0YsQ0FBTCxDQUFELEtBQVc5RCxDQUFDLENBQUNnTixRQUFGLEdBQVcsQ0FBQyxDQUF2QixDQUExRyxHQUFxSSxTQUFPaE4sQ0FBQyxDQUFDb0IsUUFBVCxLQUFvQnBCLENBQUMsQ0FBQzJMLFdBQUYsSUFBZSxTQUFPM0wsQ0FBQyxDQUFDMkwsV0FBRixDQUFjdkssUUFBcEMsSUFBOEMsTUFBSXBDLENBQXRFLEtBQTBFSSxDQUFDLENBQUNnRixJQUFGLENBQU8sRUFBUCxDQUFsTixDQUF0TTtBQUFoQjs7QUFBcWIsU0FBSXBGLENBQUMsR0FBQyxDQUFOLEVBQVFBLENBQUMsR0FBQ1ksQ0FBVixFQUFZWixDQUFDLEVBQWI7QUFBZ0JOLE9BQUMsR0FBQyxDQUFDc0IsQ0FBQyxHQUFDRyxDQUFDLENBQUNuQixDQUFELENBQUosRUFBUzRELFVBQVQsS0FBc0I3RSxDQUF4QixFQUEwQixTQUFPaUMsQ0FBQyxDQUFDb0IsUUFBVCxJQUFtQjJDLENBQUMsS0FBR25HLENBQUMsR0FBQ29DLENBQUMsQ0FBQ2lOLEtBQUosRUFBVTVNLENBQUMsSUFBRTNCLENBQUgsS0FBT3NCLENBQUMsQ0FBQ3lNLEVBQUYsSUFBTXpNLENBQUMsQ0FBQzRDLFVBQUYsQ0FBYTZKLEVBQW5CLEVBQXNCek0sQ0FBQyxDQUFDMk0sRUFBRixJQUFNM00sQ0FBQyxDQUFDNEMsVUFBRixDQUFhK0osRUFBaEQsQ0FBVixFQUE4RC9PLENBQUMsQ0FBQzBILElBQUYsR0FBT3RGLENBQUMsQ0FBQ3lNLEVBQUYsR0FBSyxJQUExRSxFQUErRTdPLENBQUMsQ0FBQzJILEdBQUYsR0FBTXZGLENBQUMsQ0FBQzJNLEVBQUYsR0FBSyxJQUExRixFQUErRi9PLENBQUMsQ0FBQzROLFFBQUYsR0FBVyxVQUExRyxFQUFxSDVOLENBQUMsQ0FBQ3NQLE9BQUYsR0FBVSxPQUEvSCxFQUF1SXRQLENBQUMsQ0FBQ2lFLEtBQUYsR0FBUTdCLENBQUMsQ0FBQzRNLEVBQUYsR0FBSyxDQUFMLEdBQU8sSUFBdEosRUFBMkpoUCxDQUFDLENBQUNrRSxNQUFGLEdBQVM5QixDQUFDLENBQUM4TSxFQUFGLEdBQUssSUFBNUssQ0FBRCxFQUFtTCxDQUFDek0sQ0FBRCxJQUFJNEQsQ0FBSixHQUFNakUsQ0FBQyxDQUFDK0wsUUFBRixJQUFZL0wsQ0FBQyxDQUFDZ0gsS0FBRixHQUFRaEgsQ0FBQyxDQUFDMkwsV0FBVixFQUFzQjNMLENBQUMsQ0FBQzRDLFVBQUYsQ0FBYXVLLFdBQWIsQ0FBeUJuTixDQUF6QixDQUFsQyxJQUErREEsQ0FBQyxDQUFDNEMsVUFBRixDQUFhbUosUUFBYixJQUF1Qi9MLENBQUMsQ0FBQzZMLE9BQUYsR0FBVTdMLENBQUMsQ0FBQzRDLFVBQVosRUFBdUIsQ0FBQzVDLENBQUMsQ0FBQ2dNLGVBQUgsSUFBb0JoTSxDQUFDLENBQUMwTCxVQUF0QixLQUFtQzFMLENBQUMsQ0FBQzBMLFVBQUYsQ0FBYTBCLFFBQWIsR0FBc0IsQ0FBQyxDQUExRCxDQUF2QixFQUFvRnBOLENBQUMsQ0FBQzJMLFdBQUYsSUFBZSxRQUFNM0wsQ0FBQyxDQUFDMkwsV0FBRixDQUFjQyxXQUFuQyxJQUFnRCxDQUFDNUwsQ0FBQyxDQUFDMkwsV0FBRixDQUFjQSxXQUEvRCxJQUE0RVcsQ0FBQyxDQUFDbEksSUFBRixDQUFPcEUsQ0FBQyxDQUFDMkwsV0FBVCxDQUFoSyxFQUFzTDNMLENBQUMsQ0FBQ2dILEtBQUYsR0FBUWhILENBQUMsQ0FBQzJMLFdBQUYsSUFBZTNMLENBQUMsQ0FBQzJMLFdBQUYsQ0FBY3lCLFFBQTdCLEdBQXNDLElBQXRDLEdBQTJDcE4sQ0FBQyxDQUFDMkwsV0FBM08sRUFBdVAzTCxDQUFDLENBQUM0QyxVQUFGLENBQWFFLFdBQWIsQ0FBeUI5QyxDQUF6QixDQUF2UCxFQUFtUkcsQ0FBQyxDQUFDd0YsTUFBRixDQUFTM0csQ0FBQyxFQUFWLEVBQWEsQ0FBYixDQUFuUixFQUFtU1ksQ0FBQyxFQUEzVCxJQUErVGxCLENBQUMsS0FBRytCLENBQUMsR0FBQyxDQUFDVCxDQUFDLENBQUMyTCxXQUFILElBQWdCMUwsQ0FBQyxDQUFDRCxDQUFDLENBQUM0QyxVQUFILEVBQWM3RSxDQUFkLEVBQWdCK0YsQ0FBaEIsQ0FBbkIsRUFBc0M5RCxDQUFDLENBQUM0QyxVQUFGLENBQWFpSixPQUFiLElBQXNCN0wsQ0FBQyxDQUFDNEMsVUFBRixDQUFhaUosT0FBYixDQUFxQnNCLFdBQXJCLENBQWlDbk4sQ0FBakMsQ0FBNUQsRUFBZ0dTLENBQUMsSUFBRVQsQ0FBQyxDQUFDNEMsVUFBRixDQUFhdUssV0FBYixDQUF5QjNILENBQUMsQ0FBQzZILGNBQUYsQ0FBaUIsR0FBakIsQ0FBekIsQ0FBbkcsRUFBbUosV0FBUzFQLENBQVQsS0FBYXFDLENBQUMsQ0FBQ2lOLEtBQUYsQ0FBUUMsT0FBUixHQUFnQixRQUE3QixDQUFuSixFQUEwTGhKLENBQUMsQ0FBQ0UsSUFBRixDQUFPcEUsQ0FBUCxDQUE3TCxDQUFyWSxHQUE2a0JBLENBQUMsQ0FBQzRDLFVBQUYsQ0FBYW1KLFFBQWIsSUFBdUIsQ0FBQy9MLENBQUMsQ0FBQytMLFFBQTFCLElBQW9DLE9BQUsvTCxDQUFDLENBQUNzTixTQUEzQyxHQUFxRG5LLENBQUMsQ0FBQ2lCLElBQUYsQ0FBT3BFLENBQVAsQ0FBckQsR0FBK0RpRSxDQUFDLElBQUUsQ0FBQ2pFLENBQUMsQ0FBQytMLFFBQU4sS0FBaUIsV0FBU3BPLENBQVQsS0FBYXFDLENBQUMsQ0FBQ2lOLEtBQUYsQ0FBUUMsT0FBUixHQUFnQixRQUE3QixHQUF1Q2hKLENBQUMsQ0FBQ0UsSUFBRixDQUFPcEUsQ0FBUCxDQUF4RCxDQUFsMUIsSUFBczVCWixDQUFDLElBQUUyRSxDQUFILElBQU0vRCxDQUFDLENBQUM0QyxVQUFGLElBQWM1QyxDQUFDLENBQUM0QyxVQUFGLENBQWFFLFdBQWIsQ0FBeUI5QyxDQUF6QixDQUFkLEVBQTBDRyxDQUFDLENBQUN3RixNQUFGLENBQVMzRyxDQUFDLEVBQVYsRUFBYSxDQUFiLENBQTFDLEVBQTBEWSxDQUFDLEVBQWpFLElBQXFFUyxDQUFDLElBQUV0QyxDQUFDLENBQUNvUCxXQUFGLENBQWNuTixDQUFkLENBQXgvQjtBQUFoQjs7QUFBeWhDLFNBQUloQixDQUFDLEdBQUNzTixDQUFDLENBQUNwTixNQUFSLEVBQWUsQ0FBQyxDQUFELEdBQUcsRUFBRUYsQ0FBcEI7QUFBdUJzTixPQUFDLENBQUN0TixDQUFELENBQUQsQ0FBSzRELFVBQUwsQ0FBZ0JFLFdBQWhCLENBQTRCd0osQ0FBQyxDQUFDdE4sQ0FBRCxDQUE3QjtBQUF2Qjs7QUFBeUQsUUFBR0ksQ0FBSCxFQUFLO0FBQUMsV0FBSTJFLENBQUMsS0FBR2hFLENBQUMsR0FBQ3lGLENBQUMsQ0FBQytILGFBQUYsQ0FBZ0I1UCxDQUFoQixDQUFGLEVBQXFCSSxDQUFDLENBQUNvUCxXQUFGLENBQWNwTixDQUFkLENBQXJCLEVBQXNDRCxDQUFDLEdBQUNDLENBQUMsQ0FBQzhNLFdBQUYsR0FBYyxJQUF0RCxFQUEyRHBNLENBQUMsR0FBQ1YsQ0FBQyxDQUFDeU4sWUFBRixLQUFpQnpQLENBQWpCLEdBQW1CLENBQW5CLEdBQXFCQSxDQUFDLENBQUMyTyxVQUFwRixFQUErRjNPLENBQUMsQ0FBQytFLFdBQUYsQ0FBYy9DLENBQWQsQ0FBbEcsQ0FBRCxFQUFxSG5DLENBQUMsR0FBQ0csQ0FBQyxDQUFDa1AsS0FBRixDQUFRUSxPQUEvSCxFQUF1STFQLENBQUMsQ0FBQ2tQLEtBQUYsQ0FBUVEsT0FBUixHQUFnQixlQUEzSixFQUEySzFQLENBQUMsQ0FBQzJOLFVBQTdLO0FBQXlMM04sU0FBQyxDQUFDK0UsV0FBRixDQUFjL0UsQ0FBQyxDQUFDMk4sVUFBaEI7QUFBekw7O0FBQXFOLFdBQUl4TCxDQUFDLEdBQUMsUUFBTTRELENBQU4sS0FBVSxDQUFDQyxDQUFELElBQUksQ0FBQzFELENBQUQsSUFBSSxDQUFDNEQsQ0FBbkIsQ0FBRixFQUF3QmpGLENBQUMsR0FBQyxDQUE5QixFQUFnQ0EsQ0FBQyxHQUFDSSxDQUFDLENBQUNGLE1BQXBDLEVBQTJDRixDQUFDLEVBQTVDLEVBQStDO0FBQUMsYUFBSW1GLENBQUMsR0FBQy9FLENBQUMsQ0FBQ0osQ0FBRCxDQUFILEVBQU8sQ0FBQ2UsQ0FBQyxHQUFDeUYsQ0FBQyxDQUFDK0gsYUFBRixDQUFnQjVQLENBQWhCLENBQUgsRUFBdUJzUCxLQUF2QixDQUE2QlEsT0FBN0IsR0FBcUMsOEJBQTRCaEksQ0FBNUIsR0FBOEIsWUFBOUIsSUFBNEMxQixDQUFDLEdBQUMsV0FBRCxHQUFhLFdBQTFELENBQTVDLEVBQW1ISixDQUFDLEtBQUc1RCxDQUFDLENBQUMyTixTQUFGLEdBQVkvSixDQUFDLElBQUVpRSxDQUFDLEdBQUM1SSxDQUFDLEdBQUMsQ0FBSCxHQUFLLEVBQVIsQ0FBaEIsQ0FBcEgsRUFBaUo2RSxDQUFDLENBQUNPLElBQUYsQ0FBT3JFLENBQVAsQ0FBakosRUFBMkpILENBQUMsR0FBQ3VFLENBQUMsQ0FBQ2pGLE1BQS9KLEVBQXNLVyxDQUFDLEdBQUMsQ0FBNUssRUFBOEtBLENBQUMsR0FBQ0QsQ0FBaEwsRUFBa0xDLENBQUMsRUFBbkw7QUFBc0wsbUJBQU9zRSxDQUFDLENBQUN0RSxDQUFELENBQUQsQ0FBS3VCLFFBQVosS0FBdUJwQixDQUFDLEdBQUNtRSxDQUFDLENBQUN0RSxDQUFELENBQUgsRUFBT0UsQ0FBQyxDQUFDb04sV0FBRixDQUFjbk4sQ0FBZCxDQUFQLEVBQXdCRSxDQUFDLElBQUVGLENBQUMsQ0FBQ2dOLFFBQUwsSUFBZWpOLENBQUMsQ0FBQ29OLFdBQUYsQ0FBYzNILENBQUMsQ0FBQzZILGNBQUYsQ0FBaUIsR0FBakIsQ0FBZCxDQUF2QyxFQUE0RXRKLENBQUMsS0FBRyxNQUFJbEUsQ0FBSixLQUFRRSxDQUFDLENBQUNrTixLQUFGLENBQVExSCxHQUFSLEdBQVl2RixDQUFDLENBQUMyTSxFQUFGLEdBQUssSUFBakIsRUFBc0I1TSxDQUFDLENBQUNrTixLQUFGLENBQVEzSCxJQUFSLEdBQWF6SCxDQUFDLEdBQUM0QyxDQUFGLEdBQUksSUFBL0MsR0FBcURULENBQUMsQ0FBQ2lOLEtBQUYsQ0FBUTFILEdBQVIsR0FBWSxLQUFqRSxFQUF1RTlFLENBQUMsS0FBR1QsQ0FBQyxDQUFDaU4sS0FBRixDQUFRM0gsSUFBUixHQUFhdEYsQ0FBQyxDQUFDeU0sRUFBRixHQUFLaE0sQ0FBTCxHQUFPLElBQXZCLENBQTNFLENBQXBHO0FBQXRMOztBQUFvWSxjQUFJYixDQUFKLEdBQU1HLENBQUMsQ0FBQ3VOLFNBQUYsR0FBWSxRQUFsQixHQUEyQmpOLENBQUMsSUFBRTRELENBQUgsS0FBTzNELENBQUMsQ0FBQ1AsQ0FBRCxDQUFELEVBQUtuQixDQUFDLENBQUNtQixDQUFELEVBQUc0TixNQUFNLENBQUNDLFlBQVAsQ0FBb0IsR0FBcEIsQ0FBSCxFQUE0QixHQUE1QixDQUFiLENBQTNCLEVBQTBFN0osQ0FBQyxLQUFHaEUsQ0FBQyxDQUFDa04sS0FBRixDQUFRcEwsS0FBUixHQUFjL0IsQ0FBZCxFQUFnQkMsQ0FBQyxDQUFDa04sS0FBRixDQUFRbkwsTUFBUixHQUFlOUIsQ0FBQyxDQUFDOE0sRUFBRixHQUFLLElBQXZDLENBQTNFLEVBQXdIL08sQ0FBQyxDQUFDb1AsV0FBRixDQUFjcE4sQ0FBZCxDQUF4SDtBQUF5STs7QUFBQWhDLE9BQUMsQ0FBQ2tQLEtBQUYsQ0FBUVEsT0FBUixHQUFnQjdQLENBQWhCO0FBQWtCOztBQUFBbUcsS0FBQyxLQUFHN0YsQ0FBQyxHQUFDSCxDQUFDLENBQUM4UCxZQUFKLEtBQW1COVAsQ0FBQyxDQUFDa1AsS0FBRixDQUFRbkwsTUFBUixHQUFlNUQsQ0FBQyxHQUFDK0UsQ0FBRixHQUFJLElBQW5CLEVBQXdCbEYsQ0FBQyxDQUFDOFAsWUFBRixHQUFlM1AsQ0FBZixLQUFtQkgsQ0FBQyxDQUFDa1AsS0FBRixDQUFRbkwsTUFBUixHQUFlNUQsQ0FBQyxHQUFDOEUsQ0FBRixHQUFJLElBQXRDLENBQTNDLEdBQXdGbEYsQ0FBQyxHQUFDQyxDQUFDLENBQUMrUCxXQUFKLEtBQWtCL1AsQ0FBQyxDQUFDa1AsS0FBRixDQUFRcEwsS0FBUixHQUFjL0QsQ0FBQyxHQUFDSixDQUFGLEdBQUksSUFBbEIsRUFBdUJLLENBQUMsQ0FBQytQLFdBQUYsR0FBY2hRLENBQWQsS0FBa0JDLENBQUMsQ0FBQ2tQLEtBQUYsQ0FBUXBMLEtBQVIsR0FBYy9ELENBQUMsR0FBQzRDLENBQUYsR0FBSSxJQUFwQyxDQUF6QyxDQUEzRixDQUFELEVBQWlMckQsQ0FBQyxDQUFDbUcsQ0FBRCxFQUFHVSxDQUFILENBQWxMLEVBQXdMN0QsQ0FBQyxJQUFFaEQsQ0FBQyxDQUFDaUcsQ0FBRCxFQUFHSCxDQUFILENBQTVMLEVBQWtNOUYsQ0FBQyxDQUFDc0MsQ0FBRCxFQUFHa0UsQ0FBSCxDQUFuTTtBQUF5TTs7QUFBQSxXQUFTdEQsQ0FBVCxDQUFXeEMsQ0FBWCxFQUFha0MsQ0FBYixFQUFlM0MsQ0FBZixFQUFpQmtHLENBQWpCLEVBQW1CO0FBQUMsUUFBSW5HLENBQUo7QUFBQSxRQUFNaUcsQ0FBTjtBQUFBLFFBQVEzRCxDQUFSO0FBQUEsUUFBVTdCLENBQVY7QUFBQSxRQUFZSSxDQUFaO0FBQUEsUUFBY2MsQ0FBZDtBQUFBLFFBQWdCYSxDQUFoQjtBQUFBLFFBQWtCRCxDQUFsQjtBQUFBLFFBQW9CTyxDQUFDLEdBQUNGLENBQUMsQ0FBQ2tNLEdBQUYsR0FBTWxNLENBQUMsQ0FBQ2tNLEdBQVIsR0FBWWxNLENBQUMsQ0FBQ21NLElBQUYsR0FBTyxNQUFQLEdBQWMsS0FBaEQ7QUFBQSxRQUFzRDFOLENBQUMsR0FBQyxDQUFDLENBQUN1QixDQUFDLENBQUN5SixJQUFGLElBQVF6SixDQUFDLENBQUN3QixLQUFWLElBQWlCLG1CQUFsQixFQUF1Q0osT0FBdkMsQ0FBK0MsT0FBL0MsQ0FBekQ7QUFBQSxRQUFpSDhDLENBQUMsR0FBQ3pFLENBQUMsQ0FBQ08sQ0FBRCxDQUFwSDtBQUFBLFFBQXdIQyxDQUFDLEdBQUNELENBQUMsQ0FBQ2lNLGFBQUYsSUFBaUIsR0FBM0k7QUFBQSxRQUErSXRPLENBQUMsR0FBQyxRQUFNc0MsQ0FBTixHQUFRLEVBQVIsR0FBV2lFLENBQUMsR0FBQyxTQUFELEdBQVcsR0FBeEs7QUFBQSxRQUE0S3BFLENBQUMsR0FBQyxPQUFLSSxDQUFMLEdBQU8sR0FBckw7QUFBQSxRQUF5TEwsQ0FBQyxHQUFDLENBQTNMO0FBQUEsUUFBNkxXLENBQUMsR0FBQ1IsQ0FBQyxDQUFDOE4sWUFBRixHQUFlLGNBQVksT0FBTzlOLENBQUMsQ0FBQzhOLFlBQXJCLEdBQWtDOU4sQ0FBQyxDQUFDOE4sWUFBcEMsR0FBaUQvTixDQUFoRSxHQUFrRSxJQUFqUTtBQUFBLFFBQXNRTyxDQUFDLEdBQUNpRixDQUFDLENBQUMrSCxhQUFGLENBQWdCLEtBQWhCLENBQXhRO0FBQUEsUUFBK1JqTixDQUFDLEdBQUN2QyxDQUFDLENBQUM2RSxVQUFuUzs7QUFBOFMsU0FBSXRDLENBQUMsQ0FBQ3VDLFlBQUYsQ0FBZXRDLENBQWYsRUFBaUJ4QyxDQUFqQixHQUFvQndDLENBQUMsQ0FBQ3FMLFdBQUYsR0FBYzdOLENBQUMsQ0FBQ3dELFNBQXBDLEVBQThDakIsQ0FBQyxDQUFDd0MsV0FBRixDQUFjL0UsQ0FBZCxDQUE5QyxFQUErRDhCLENBQUMsR0FBQyxDQUFDLENBQUQsS0FBSyxDQUFDeEMsQ0FBQyxHQUFDLFNBQVMyUSxPQUFULENBQWlCalEsQ0FBakIsRUFBbUI7QUFBQyxVQUFJa0MsQ0FBQyxHQUFDbEMsQ0FBQyxDQUFDbUssUUFBUjtBQUFBLFVBQWlCNUssQ0FBQyxHQUFDLEVBQW5COztBQUFzQixVQUFHLE1BQUkyQyxDQUFKLElBQU8sTUFBSUEsQ0FBWCxJQUFjLE9BQUtBLENBQXRCLEVBQXdCO0FBQUMsWUFBRyxZQUFVLE9BQU9sQyxDQUFDLENBQUM2TixXQUF0QixFQUFrQyxPQUFPN04sQ0FBQyxDQUFDNk4sV0FBVDs7QUFBcUIsYUFBSTdOLENBQUMsR0FBQ0EsQ0FBQyxDQUFDMk4sVUFBUixFQUFtQjNOLENBQW5CLEVBQXFCQSxDQUFDLEdBQUNBLENBQUMsQ0FBQzROLFdBQXpCO0FBQXFDck8sV0FBQyxJQUFFMFEsT0FBTyxDQUFDalEsQ0FBRCxDQUFWO0FBQXJDO0FBQW1ELE9BQW5JLE1BQXdJLElBQUcsTUFBSWtDLENBQUosSUFBTyxNQUFJQSxDQUFkLEVBQWdCLE9BQU9sQyxDQUFDLENBQUN3RCxTQUFUOztBQUFtQixhQUFPakUsQ0FBUDtBQUFTLEtBQTlOLENBQStOUyxDQUFDLEdBQUN3QyxDQUFqTyxDQUFILEVBQXdPYyxPQUF4TyxDQUFnUCxHQUFoUCxDQUF0RSxFQUEyVCxDQUFDLENBQUQsS0FBS3BCLENBQUMsQ0FBQ2dPLGdCQUFQLEtBQTBCNVEsQ0FBQyxHQUFDQSxDQUFDLENBQUNpSCxPQUFGLENBQVV0QixDQUFWLEVBQVksR0FBWixFQUFpQnNCLE9BQWpCLENBQXlCbEUsQ0FBekIsRUFBMkIsRUFBM0IsQ0FBNUIsQ0FBM1QsRUFBdVhQLENBQUMsS0FBR3hDLENBQUMsR0FBQ0EsQ0FBQyxDQUFDb0UsS0FBRixDQUFRLEdBQVIsRUFBYU0sSUFBYixDQUFrQixRQUFsQixDQUFMLENBQXhYLEVBQTBaN0QsQ0FBQyxHQUFDYixDQUFDLENBQUM2QixNQUE5WixFQUFxYW9FLENBQUMsR0FBQyxDQUFDLFFBQU1qRyxDQUFDLENBQUN1SCxNQUFGLENBQVMsQ0FBVCxDQUFOLEdBQWtCaEgsQ0FBbEIsR0FBb0IsRUFBckIsSUFBeUJOLENBQUMsRUFBamMsRUFBb2NxQyxDQUFDLEdBQUMsQ0FBMWMsRUFBNGNBLENBQUMsR0FBQ3pCLENBQTljLEVBQWdkeUIsQ0FBQyxFQUFqZDtBQUFvZCxVQUFHWCxDQUFDLEdBQUMzQixDQUFDLENBQUN1SCxNQUFGLENBQVNqRixDQUFULENBQUYsRUFBY2MsQ0FBQyxLQUFHYixDQUFDLEdBQUNhLENBQUMsQ0FBQ3BELENBQUMsQ0FBQ3NILE1BQUYsQ0FBU2hGLENBQVQsQ0FBRCxFQUFhTSxDQUFDLENBQUM4TixZQUFmLENBQU4sQ0FBbEIsRUFBc0QvTyxDQUFDLEdBQUMzQixDQUFDLENBQUNzSCxNQUFGLENBQVNoRixDQUFULEVBQVdDLENBQUMsSUFBRSxDQUFkLENBQUYsRUFBbUIwRCxDQUFDLElBQUU1RSxDQUFDLElBQUUsUUFBTU0sQ0FBVCxHQUFXd0UsQ0FBQyxLQUFHeEUsQ0FBSixHQUFNLElBQU4sR0FBV21CLENBQVgsR0FBYSxHQUF4QixHQUE0Qm5CLENBQWxELEVBQW9EVyxDQUFDLElBQUVDLENBQUMsR0FBQyxDQUF6RCxDQUF0RCxLQUFzSCxJQUFHWixDQUFDLEtBQUdrQixDQUFKLElBQU83QyxDQUFDLENBQUN1SCxNQUFGLENBQVNqRixDQUFDLEdBQUMsQ0FBWCxNQUFnQk8sQ0FBdkIsSUFBMEJQLENBQTdCLEVBQStCO0FBQUMsYUFBSTJELENBQUMsSUFBRXhELENBQUMsR0FBQ0MsQ0FBRCxHQUFHLEVBQVAsRUFBVUQsQ0FBQyxHQUFDLENBQWhCLEVBQWtCekMsQ0FBQyxDQUFDdUgsTUFBRixDQUFTakYsQ0FBQyxHQUFDLENBQVgsTUFBZ0JPLENBQWxDO0FBQXFDb0QsV0FBQyxJQUFFMUYsQ0FBSCxFQUFLK0IsQ0FBQyxFQUFOO0FBQXJDOztBQUE4Q0EsU0FBQyxLQUFHekIsQ0FBQyxHQUFDLENBQU4sR0FBUW9GLENBQUMsSUFBRTFGLENBQVgsR0FBYSxRQUFNUCxDQUFDLENBQUN1SCxNQUFGLENBQVNqRixDQUFDLEdBQUMsQ0FBWCxDQUFOLEtBQXNCMkQsQ0FBQyxJQUFFMUYsQ0FBQyxHQUFDTixDQUFDLEVBQU4sRUFBU3dDLENBQUMsR0FBQyxDQUFqQyxDQUFiO0FBQWlELE9BQS9ILE1BQW1JLFFBQU1kLENBQU4sSUFBUyxhQUFXM0IsQ0FBQyxDQUFDc0gsTUFBRixDQUFTaEYsQ0FBVCxFQUFXLENBQVgsQ0FBcEIsSUFBbUMyRCxDQUFDLElBQUU1RSxDQUFDLEdBQUM4RSxDQUFDLEtBQUcsVUFBSixHQUFlckQsQ0FBZixHQUFpQixHQUFsQixHQUFzQixRQUExQixFQUFtQ1IsQ0FBQyxJQUFFLENBQXpFLElBQTRFLFNBQU9YLENBQUMsQ0FBQ2tQLFVBQUYsQ0FBYSxDQUFiLENBQVAsSUFBd0JsUCxDQUFDLENBQUNrUCxVQUFGLENBQWEsQ0FBYixLQUFpQixLQUF6QyxJQUFnRCxTQUFPN1EsQ0FBQyxDQUFDNlEsVUFBRixDQUFhdk8sQ0FBQyxHQUFDLENBQWYsQ0FBUCxJQUEwQnRDLENBQUMsQ0FBQzZRLFVBQUYsQ0FBYXZPLENBQUMsR0FBQyxDQUFmLEtBQW1CLEtBQTdGLElBQW9HN0IsQ0FBQyxHQUFDLENBQUMsQ0FBQ1QsQ0FBQyxDQUFDc0gsTUFBRixDQUFTaEYsQ0FBVCxFQUFXLEVBQVgsRUFBZThCLEtBQWYsQ0FBcUI1RCxDQUFyQixLQUF5QixFQUExQixFQUE4QixDQUE5QixLQUFrQyxFQUFuQyxFQUF1Q3FCLE1BQXZDLElBQStDLENBQWpELEVBQW1Eb0UsQ0FBQyxJQUFFNUUsQ0FBQyxJQUFFLFFBQU1NLENBQVQsR0FBV3dFLENBQUMsS0FBR25HLENBQUMsQ0FBQ3NILE1BQUYsQ0FBU2hGLENBQVQsRUFBVzdCLENBQVgsQ0FBSixHQUFrQixJQUFsQixHQUF1QnFDLENBQXZCLEdBQXlCLEdBQXBDLEdBQXdDOUMsQ0FBQyxDQUFDc0gsTUFBRixDQUFTaEYsQ0FBVCxFQUFXN0IsQ0FBWCxDQUE5RixFQUE0RzZCLENBQUMsSUFBRTdCLENBQUMsR0FBQyxDQUFyTixJQUF3TndGLENBQUMsSUFBRTVFLENBQUMsSUFBRSxRQUFNTSxDQUFULEdBQVd3RSxDQUFDLEtBQUd4RSxDQUFKLEdBQU0sSUFBTixHQUFXbUIsQ0FBWCxHQUFhLEdBQXhCLEdBQTRCbkIsQ0FBblU7QUFBN3NCOztBQUFraENqQixLQUFDLENBQUNvUSxTQUFGLEdBQVk3SyxDQUFDLElBQUV4RCxDQUFDLEdBQUNDLENBQUQsR0FBRyxFQUFOLENBQWIsRUFBdUJGLENBQUMsSUFBRWpCLENBQUMsQ0FBQzBCLENBQUQsRUFBRyxRQUFILEVBQVksR0FBWixDQUEzQjtBQUE0Qzs7QUFBQSxXQUFTbEIsQ0FBVCxDQUFXckIsQ0FBWCxFQUFha0MsQ0FBYixFQUFlM0MsQ0FBZixFQUFpQmtHLENBQWpCLEVBQW1CO0FBQUMsUUFBSW5HLENBQUo7QUFBQSxRQUFNaUcsQ0FBTjtBQUFBLFFBQVEzRCxDQUFDLEdBQUM3QixDQUFDLENBQUNDLENBQUMsQ0FBQytOLFVBQUgsQ0FBWDtBQUFBLFFBQTBCNU4sQ0FBQyxHQUFDeUIsQ0FBQyxDQUFDVCxNQUE5QjtBQUFBLFFBQXFDTixDQUFDLEdBQUNjLENBQUMsQ0FBQ08sQ0FBRCxDQUF4Qzs7QUFBNEMsUUFBRyxNQUFJbEMsQ0FBQyxDQUFDbUssUUFBTixJQUFnQixJQUFFaEssQ0FBckIsRUFBdUI7QUFBQyxXQUFJK0IsQ0FBQyxDQUFDd0wsUUFBRixHQUFXLENBQUMsQ0FBWixFQUFjcE8sQ0FBQyxHQUFDLENBQXBCLEVBQXNCQSxDQUFDLEdBQUNhLENBQXhCLEVBQTBCYixDQUFDLEVBQTNCO0FBQThCLGNBQUksQ0FBQ2lHLENBQUMsR0FBQzNELENBQUMsQ0FBQ3RDLENBQUQsQ0FBSixFQUFTNkssUUFBYixJQUF1QixDQUFDLE1BQU1PLElBQU4sQ0FBV25GLENBQUMsQ0FBQy9CLFNBQWIsQ0FBeEIsS0FBa0QzQyxDQUFDLElBQUUsTUFBSTBFLENBQUMsQ0FBQzRFLFFBQVQsSUFBbUIsYUFBVzlKLENBQUMsQ0FBQ2tGLENBQUQsQ0FBRCxDQUFLNEosT0FBbkMsS0FBNkM1SixDQUFDLENBQUMySixLQUFGLENBQVFDLE9BQVIsR0FBZ0IsY0FBaEIsRUFBK0I1SixDQUFDLENBQUMySixLQUFGLENBQVF6QixRQUFSLEdBQWlCLFVBQTdGLEdBQXlHbEksQ0FBQyxDQUFDeUksUUFBRixHQUFXLENBQUMsQ0FBckgsRUFBdUgzTSxDQUFDLENBQUNrRSxDQUFELEVBQUdyRCxDQUFILEVBQUszQyxDQUFMLEVBQU9rRyxDQUFQLENBQTFLO0FBQTlCOztBQUFtTixhQUFPdkQsQ0FBQyxDQUFDd0wsUUFBRixHQUFXN00sQ0FBWCxFQUFhLE1BQUtiLENBQUMsQ0FBQ2dPLFFBQUYsR0FBVyxDQUFDLENBQWpCLENBQXBCO0FBQXdDOztBQUFBeEwsS0FBQyxDQUFDeEMsQ0FBRCxFQUFHa0MsQ0FBSCxFQUFLM0MsQ0FBTCxFQUFPa0csQ0FBUCxDQUFEO0FBQVc7O0FBQUEsTUFBSWdDLENBQUo7QUFBQSxNQUFNbEksQ0FBTjtBQUFBLE1BQVFrRyxDQUFSO0FBQUEsTUFBVUYsQ0FBVjtBQUFBLE1BQVlsRCxDQUFDLEdBQUMsaUJBQWQ7QUFBQSxNQUFnQzRDLENBQUMsR0FBQyxZQUFsQztBQUFBLE1BQStDckQsQ0FBQyxHQUFDeU8sS0FBSyxDQUFDQyxPQUF2RDtBQUFBLE1BQStEblEsQ0FBQyxHQUFDLEdBQUcrQyxLQUFwRTtBQUFBLE1BQTBFcEIsQ0FBQyxJQUFFLENBQUN5RCxDQUFDLEdBQUNnTCxTQUFTLENBQUM3RyxTQUFiLEVBQXdCaEcsS0FBeEIsR0FBOEIsU0FBU0EsS0FBVCxDQUFlMUQsQ0FBZixFQUFpQjtBQUFDLFNBQUt3USxPQUFMLElBQWMsS0FBS0MsTUFBTCxFQUFkLEVBQTRCLEtBQUtDLElBQUwsR0FBVTFRLENBQUMsR0FBQ0EsQ0FBQyxJQUFFLEtBQUswUSxJQUFoRCxFQUFxRCxLQUFLQyxVQUFMLENBQWdCeFAsTUFBaEIsR0FBdUIsS0FBS3lQLEtBQUwsQ0FBV3pQLE1BQVgsR0FBa0IsS0FBSzBQLEtBQUwsQ0FBVzFQLE1BQVgsR0FBa0IsS0FBSzJQLEtBQUwsQ0FBVzNQLE1BQVgsR0FBa0IsQ0FBbEk7O0FBQW9JLFNBQUksSUFBSWUsQ0FBSixFQUFNM0MsQ0FBTixFQUFRa0csQ0FBUixFQUFVbkcsQ0FBQyxHQUFDLEtBQUt5UixRQUFMLENBQWM1UCxNQUExQixFQUFpQ29FLENBQUMsR0FBQ3ZGLENBQUMsQ0FBQ29PLEdBQUYsR0FBTXBPLENBQUMsQ0FBQ29PLEdBQVIsR0FBWXBPLENBQUMsQ0FBQ3FPLElBQUYsR0FBTyxNQUFQLEdBQWMsS0FBN0QsRUFBbUV6TSxDQUFDLEdBQUNYLENBQUMsQ0FBQ2pCLENBQUMsQ0FBQ2dSLFVBQUgsRUFBY3pMLENBQWQsQ0FBdEUsRUFBdUZ4RixDQUFDLEdBQUNrQixDQUFDLENBQUNqQixDQUFDLENBQUNpUixVQUFILEVBQWMxTCxDQUFkLENBQTlGLEVBQStHLENBQUMsQ0FBRCxHQUFHLEVBQUVqRyxDQUFwSDtBQUF1SG1HLE9BQUMsR0FBQyxLQUFLc0wsUUFBTCxDQUFjelIsQ0FBZCxDQUFGLEVBQW1CLEtBQUtxUixVQUFMLENBQWdCclIsQ0FBaEIsSUFBbUJtRyxDQUFDLENBQUM4SixTQUF4QyxFQUFrRHJOLENBQUMsR0FBQ3VELENBQUMsQ0FBQ3FLLFlBQXRELEVBQW1FdlEsQ0FBQyxHQUFDa0csQ0FBQyxDQUFDc0ssV0FBdkUsRUFBbUYxTyxDQUFDLENBQUNvRSxDQUFELEVBQUd6RixDQUFILEVBQUs0QixDQUFMLEVBQU83QixDQUFQLENBQXBGLEVBQThGMkMsQ0FBQyxDQUFDK0MsQ0FBRCxFQUFHekYsQ0FBSCxFQUFLLEtBQUs0USxLQUFWLEVBQWdCLEtBQUtDLEtBQXJCLEVBQTJCLEtBQUtDLEtBQWhDLEVBQXNDdlIsQ0FBdEMsRUFBd0MyQyxDQUF4QyxDQUEvRjtBQUF2SDs7QUFBaVEsV0FBTyxLQUFLME8sS0FBTCxDQUFXMVAsT0FBWCxJQUFxQixLQUFLMlAsS0FBTCxDQUFXM1AsT0FBWCxFQUFyQixFQUEwQyxLQUFLNFAsS0FBTCxDQUFXNVAsT0FBWCxFQUExQyxFQUErRCxLQUFLc1AsT0FBTCxHQUFhLENBQUMsQ0FBN0UsRUFBK0UsSUFBdEY7QUFBMkYsR0FBaGhCLEVBQWloQmpMLENBQUMsQ0FBQ2tMLE1BQUYsR0FBUyxTQUFTQSxNQUFULEdBQWlCO0FBQUMsUUFBSWxSLENBQUMsR0FBQyxLQUFLb1IsVUFBWDtBQUFzQixRQUFHLENBQUNwUixDQUFKLEVBQU0sTUFBSyx1Q0FBTDtBQUE2QyxXQUFPLEtBQUt3UixRQUFMLENBQWNHLE9BQWQsQ0FBc0IsVUFBU2xSLENBQVQsRUFBV2tDLENBQVgsRUFBYTtBQUFDLGFBQU9sQyxDQUFDLENBQUN1UCxTQUFGLEdBQVloUSxDQUFDLENBQUMyQyxDQUFELENBQXBCO0FBQXdCLEtBQTVELEdBQThELEtBQUswTyxLQUFMLEdBQVcsRUFBekUsRUFBNEUsS0FBS0MsS0FBTCxHQUFXLEVBQXZGLEVBQTBGLEtBQUtDLEtBQUwsR0FBVyxFQUFyRyxFQUF3RyxLQUFLTixPQUFMLEdBQWEsQ0FBQyxDQUF0SCxFQUF3SCxJQUEvSDtBQUFvSSxHQUF6dkIsRUFBMHZCRCxTQUFTLENBQUNZLE1BQVYsR0FBaUIsU0FBU0EsTUFBVCxDQUFnQm5SLENBQWhCLEVBQWtCa0MsQ0FBbEIsRUFBb0I7QUFBQyxXQUFPLElBQUlxTyxTQUFKLENBQWN2USxDQUFkLEVBQWdCa0MsQ0FBaEIsQ0FBUDtBQUEwQixHQUExekIsRUFBMnpCcU8sU0FBN3pCLENBQTNFOztBQUFtNUIsV0FBU0EsU0FBVCxDQUFtQnZRLENBQW5CLEVBQXFCa0MsQ0FBckIsRUFBdUI7QUFBQ3VELEtBQUMsSUFBRSxTQUFTMkwsU0FBVCxHQUFvQjtBQUFDM0osT0FBQyxHQUFDekUsUUFBRixFQUFXekQsQ0FBQyxHQUFDeUgsTUFBYixFQUFvQnZCLENBQUMsR0FBQyxDQUF0QjtBQUF3QixLQUE3QyxFQUFILEVBQW1ELEtBQUtzTCxRQUFMLEdBQWNoUixDQUFDLENBQUNDLENBQUQsQ0FBbEUsRUFBc0UsS0FBSzRRLEtBQUwsR0FBVyxFQUFqRixFQUFvRixLQUFLQyxLQUFMLEdBQVcsRUFBL0YsRUFBa0csS0FBS0MsS0FBTCxHQUFXLEVBQTdHLEVBQWdILEtBQUtILFVBQUwsR0FBZ0IsRUFBaEksRUFBbUksS0FBS0QsSUFBTCxHQUFVeE8sQ0FBQyxJQUFFLEVBQWhKLEVBQW1KLEtBQUt3QixLQUFMLENBQVd4QixDQUFYLENBQW5KO0FBQWlLOztBQUFBSixHQUFDLENBQUNnSSxPQUFGLEdBQVUsT0FBVixFQUFrQjlKLENBQUMsQ0FBQ3VRLFNBQUYsR0FBWXpPLENBQTlCLEVBQWdDOUIsQ0FBQyxXQUFELEdBQVU4QixDQUExQzs7QUFBNEMsTUFBSSxPQUFPa0YsTUFBUCxLQUFpQixXQUFqQixJQUE4QkEsTUFBTSxLQUFHaEgsQ0FBM0MsRUFBNkM7QUFBQ3FOLFVBQU0sQ0FBQ0MsY0FBUCxDQUFzQnROLENBQXRCLEVBQXdCLFlBQXhCLEVBQXFDO0FBQUN1TixXQUFLLEVBQUMsQ0FBQztBQUFSLEtBQXJDO0FBQWlELEdBQS9GLE1BQXFHO0FBQUMsV0FBT3ZOLENBQUMsV0FBUjtBQUFpQjtBQUFDLENBQTMvYyxDQUFELEM7Ozs7Ozs7Ozs7O0FDVEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaERBO0FBQ0E7QUFFQWlILDRDQUFJLENBQUNDLGNBQUwsQ0FBb0JxSixrRUFBcEI7QUFFZSxTQUFTYyxhQUFULEdBQXlCO0FBQUE7O0FBRXRDLE1BQU1DLFlBQVksR0FBR3RPLFFBQVEsQ0FBQ3dLLGdCQUFULENBQTBCLG9CQUExQixDQUFyQjtBQUVBLE1BQU0rRCxTQUFTLEdBQUcsSUFBSWhCLGtFQUFKLENBQWNlLFlBQWQsRUFBNEI7QUFBRTNGLFFBQUksRUFBRSxPQUFSO0FBQWlCMkMsY0FBVSxFQUFFO0FBQTdCLEdBQTVCLENBQWxCO0FBRUF0SCxRQUFNLENBQUN3SyxnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxZQUFNO0FBQUE7O0FBQ3RDRCxhQUFTLENBQUM3TixLQUFWO0FBQ0QsR0FGRDtBQUlELEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2ZEOztJQUVNK04sUztBQUNKLHFCQUFZQyxFQUFaLEVBQWdCO0FBQUE7O0FBRWQsU0FBS0EsRUFBTCxHQUFVQSxFQUFWO0FBQ0EsU0FBS0MsS0FBTCxHQUFhdEIsS0FBSyxDQUFDdUIsSUFBTixDQUFXRixFQUFFLENBQUNsRSxnQkFBSCxDQUFvQix1QkFBcEIsQ0FBWCxDQUFiO0FBQ0EsU0FBS3FFLE1BQUwsR0FBY3hCLEtBQUssQ0FBQ3VCLElBQU4sQ0FBV0YsRUFBRSxDQUFDbEUsZ0JBQUgsQ0FBb0Isd0JBQXBCLENBQVgsQ0FBZDtBQUNBLFNBQUtzRSxXQUFMLEdBQW1COUssTUFBTSxDQUFDK0ssVUFBMUI7QUFFQS9LLFVBQU0sQ0FBQ3dLLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDLEtBQUtRLFFBQUwsQ0FBY0MsSUFBZCxDQUFtQixJQUFuQixDQUFsQzs7QUFFQSxRQUFJUCxFQUFFLENBQUNRLE9BQUgsQ0FBV0MsU0FBWCxLQUF5QixRQUE3QixFQUF1QztBQUNyQyxVQUFJbkwsTUFBTSxDQUFDK0ssVUFBUCxJQUFxQixHQUF6QixFQUE4QixLQUFLSyxLQUFMO0FBQy9CLEtBRkQsTUFFTztBQUNMLFdBQUtBLEtBQUw7QUFDRDtBQUVGOzs7O1dBQ0QsaUJBQVE7QUFBQTs7QUFFTixXQUFLQyxpQkFBTCxHQUF5QixLQUFLQyxVQUFMLENBQWdCTCxJQUFoQixDQUFxQixJQUFyQixDQUF6QjtBQUVBalAsY0FBUSxDQUFDdVAsS0FBVCxDQUFlQyxLQUFmLENBQXFCQyxJQUFyQixDQUEwQixZQUFNO0FBQUE7O0FBQUE7O0FBQzlCLGFBQUtkLEtBQUwsQ0FBV1QsT0FBWCxDQUFtQixVQUFDUSxFQUFELEVBQUs5UCxDQUFMLEVBQVc7QUFBQTs7QUFDNUI4UCxZQUFFLENBQUN4QyxLQUFILENBQVNuTCxNQUFULEdBQWtCLEtBQUs4TixNQUFMLENBQVlqUSxDQUFaLEVBQWVvTixZQUFmLEdBQThCLElBQWhEO0FBQ0EwQyxZQUFFLENBQUNGLGdCQUFILENBQW9CLE9BQXBCLEVBQTZCLEtBQUthLGlCQUFsQztBQUNELFNBSEQ7QUFJRCxPQUxEO0FBT0Q7OztXQUNELG9CQUFXOVMsQ0FBWCxFQUFjO0FBRVosVUFBTW1ULEtBQUssR0FBRyxLQUFLZixLQUFMLENBQVdyTyxPQUFYLENBQW1CL0QsQ0FBQyxDQUFDb1QsYUFBckIsQ0FBZDs7QUFFQSxVQUFJcFQsQ0FBQyxDQUFDb1QsYUFBRixDQUFnQkMsU0FBaEIsQ0FBMEJDLFFBQTFCLENBQW1DLFNBQW5DLENBQUosRUFBbUQ7QUFFakQ1TCxvREFBSSxDQUFDNkwsRUFBTCxDQUFRLEtBQUtuQixLQUFMLENBQVdlLEtBQVgsQ0FBUixFQUEyQjtBQUFFSyxrQkFBUSxFQUFFLEdBQVo7QUFBaUJoUCxnQkFBTSxFQUFFLEtBQUs4TixNQUFMLENBQVlhLEtBQVosRUFBbUIxRCxZQUFuQixHQUFrQyxJQUEzRDtBQUFpRWdFLGNBQUksRUFBRTtBQUF2RSxTQUEzQjtBQUNBelQsU0FBQyxDQUFDb1QsYUFBRixDQUFnQkMsU0FBaEIsQ0FBMEJLLE1BQTFCLENBQWlDLFNBQWpDO0FBRUQsT0FMRCxNQUtPO0FBRUxoTSxvREFBSSxDQUFDNkwsRUFBTCxDQUFRLEtBQUtuQixLQUFMLENBQVdlLEtBQVgsQ0FBUixFQUEyQjtBQUFFSyxrQkFBUSxFQUFFLEdBQVo7QUFBaUJoUCxnQkFBTSxFQUFFLE1BQXpCO0FBQWlDaVAsY0FBSSxFQUFFO0FBQXZDLFNBQTNCO0FBQ0F6VCxTQUFDLENBQUNvVCxhQUFGLENBQWdCQyxTQUFoQixDQUEwQnhHLEdBQTFCLENBQThCLFNBQTlCO0FBRUQ7QUFDRjs7O1dBQ0Qsa0JBQVM3TSxDQUFULEVBQVk7QUFBQTs7QUFFVixVQUFJeUQsUUFBUSxDQUFDa1EsZUFBVCxDQUF5Qm5ELFdBQXpCLEtBQXlDLEtBQUsrQixXQUFsRCxFQUErRDs7QUFFL0QsVUFBSSxLQUFLSixFQUFMLENBQVFRLE9BQVIsQ0FBZ0JDLFNBQWhCLEtBQThCLFFBQTlCLElBQTBDbkwsTUFBTSxDQUFDK0ssVUFBUCxJQUFxQixHQUFuRSxFQUF3RTtBQUV0RSxhQUFLSyxLQUFMO0FBRUQsT0FKRCxNQUlPLElBQUksS0FBS1YsRUFBTCxDQUFRUSxPQUFSLENBQWdCQyxTQUFoQixLQUE4QixRQUFsQyxFQUE0QztBQUVqRCxhQUFLZ0IsT0FBTDtBQUVELE9BSk0sTUFJQTtBQUVMLGFBQUt4QixLQUFMLENBQVdULE9BQVgsQ0FBbUIsVUFBQ1EsRUFBRCxFQUFLOVAsQ0FBTCxFQUFXO0FBQUE7O0FBQzVCcUYsc0RBQUksQ0FBQzZMLEVBQUwsQ0FBUXBCLEVBQVIsRUFBWTtBQUFFcUIsb0JBQVEsRUFBRSxHQUFaO0FBQWlCaFAsa0JBQU0sRUFBRSxLQUFLOE4sTUFBTCxDQUFZalEsQ0FBWixFQUFlb04sWUFBZixHQUE4QixJQUF2RDtBQUE2RGdFLGdCQUFJLEVBQUU7QUFBbkUsV0FBWjtBQUNBdEIsWUFBRSxDQUFDa0IsU0FBSCxDQUFhSyxNQUFiLENBQW9CLE1BQXBCO0FBQ0QsU0FIRDtBQUtEO0FBQ0Y7OztXQUNELG1CQUFVO0FBQUE7O0FBRVIsV0FBS3RCLEtBQUwsQ0FBV1QsT0FBWCxDQUFtQixVQUFBUSxFQUFFLEVBQUk7QUFBQTs7QUFDdkJBLFVBQUUsQ0FBQ3hDLEtBQUgsQ0FBU25MLE1BQVQsR0FBa0IsTUFBbEI7QUFDQTJOLFVBQUUsQ0FBQzBCLG1CQUFILENBQXVCLE9BQXZCLEVBQWdDLEtBQUtmLGlCQUFyQztBQUNELE9BSEQ7QUFLRDs7Ozs7O0FBR0gsSUFBTWdCLGFBQWEsR0FBRyx5QkFBTTtBQUFBOztBQUFBOztBQUUxQixNQUFNQyxVQUFVLEdBQUd0USxRQUFRLENBQUN3SyxnQkFBVCxDQUEwQixrQkFBMUIsQ0FBbkI7QUFFQSxNQUFJLENBQUM4RixVQUFMLEVBQWlCO0FBRWpCQSxZQUFVLENBQUNwQyxPQUFYLENBQW1CLFVBQUFRLEVBQUUsRUFBSTtBQUFBOztBQUN2QixRQUFJRCxTQUFKLENBQWNDLEVBQWQ7QUFDRCxHQUZEO0FBSUQsQ0FWa0IsZ0JBQW5COztBQVllMkIsNEVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzFGQTtBQUVlLFNBQVNFLFVBQVQsR0FBc0IsQ0FBRzs7QUFFeEMsSUFBTUMsVUFBVSxHQUFHLHNCQUFNO0FBQUE7O0FBRXZCdk0sOENBQUksQ0FBQ3dNLEdBQUwsQ0FBUyx3QkFBVCxFQUFtQztBQUFFQyxTQUFLLEVBQUU7QUFBVCxHQUFuQztBQUVELENBSmUsZ0JBQWhCOztBQU1BRixVQUFVO0FBRUgsU0FBU0csUUFBVCxDQUFrQmpDLEVBQWxCLEVBQXNCO0FBQUE7O0FBRTNCLE1BQUksQ0FBQ0EsRUFBTCxFQUFTO0FBRVQsTUFBTWtDLEVBQUUsR0FBRzNNLDRDQUFJLENBQUM0TSxRQUFMLENBQWM7QUFBRUMsY0FBVSxFQUFFLHNCQUFNO0FBQUE7O0FBQzNDcEMsUUFBRSxDQUFDa0IsU0FBSCxDQUFheEcsR0FBYixDQUFpQixVQUFqQjtBQUNELEtBRm9DO0FBQVosR0FBZCxDQUFYOztBQUlBLE1BQUlwRixNQUFNLENBQUMrSyxVQUFQLEdBQW9CLEdBQXhCLEVBQTZCO0FBQzNCNkIsTUFBRSxDQUFDZCxFQUFILENBQU0sQ0FBQyx5QkFBRCxFQUE0QixrQkFBNUIsQ0FBTixFQUF1RDtBQUFFQyxjQUFRLEVBQUUsQ0FBWjtBQUFlZ0IsYUFBTyxFQUFFLENBQXhCO0FBQTJCdlIsT0FBQyxFQUFFLElBQTlCO0FBQW9Dd1IsYUFBTyxFQUFFLElBQTdDO0FBQW1EaEIsVUFBSSxFQUFFO0FBQXpELEtBQXZEO0FBQ0FZLE1BQUUsQ0FBQ2QsRUFBSCxDQUFNLENBQUMsa0JBQUQsQ0FBTixFQUE0QjtBQUFFQyxjQUFRLEVBQUUsR0FBWjtBQUFpQnZRLE9BQUMsRUFBRSxJQUFwQjtBQUEwQnVSLGFBQU8sRUFBRSxDQUFuQztBQUFzQ0MsYUFBTyxFQUFFLElBQS9DO0FBQXFEaEIsVUFBSSxFQUFFO0FBQTNELEtBQTVCLEVBQXVHLE9BQXZHO0FBQ0FZLE1BQUUsQ0FBQ2QsRUFBSCxDQUFNLFdBQU4sRUFBbUI7QUFBRUMsY0FBUSxFQUFFLEdBQVo7QUFBaUJoUCxZQUFNLEVBQUUsTUFBekI7QUFBaUNpUCxVQUFJLEVBQUU7QUFBdkMsS0FBbkIsRUFBMEUsT0FBMUU7QUFDRDtBQUVGO0FBRU0sU0FBU2lCLFdBQVQsQ0FBcUJ2QyxFQUFyQixFQUF5QjtBQUFBOztBQUU5QixNQUFJLENBQUNBLEVBQUwsRUFBUztBQUVULE1BQU1aLEtBQUssR0FBR1ksRUFBRSxDQUFDbEUsZ0JBQUgsQ0FBb0IsT0FBcEIsQ0FBZDtBQUNBLE1BQU0wRyxLQUFLLEdBQUc3RCxLQUFLLENBQUN1QixJQUFOLENBQVdGLEVBQUUsQ0FBQ2xFLGdCQUFILENBQW9CLEtBQXBCLENBQVgsQ0FBZDtBQUNBLE1BQU0yRyxjQUFjLEdBQUdELEtBQUssQ0FBQ3BNLElBQU4sQ0FBVztBQUFBOztBQUFBLFdBQU03SCxJQUFJLENBQUNtVSxNQUFMLEtBQWdCLEdBQXRCO0FBQUEsR0FBWCxZQUF2QjtBQUVBLE1BQU1SLEVBQUUsR0FBRzNNLDRDQUFJLENBQUM0TSxRQUFMLENBQWM7QUFBRUMsY0FBVSxFQUFFLHNCQUFNO0FBQUE7O0FBQzNDcEMsUUFBRSxDQUFDa0IsU0FBSCxDQUFheEcsR0FBYixDQUFpQixVQUFqQjtBQUNELEtBRm9DO0FBQVosR0FBZCxDQUFYO0FBSUF3SCxJQUFFLENBQUNkLEVBQUgsQ0FBTWhDLEtBQU4sRUFBYTtBQUFFaUMsWUFBUSxFQUFFLEdBQVo7QUFBaUJ2USxLQUFDLEVBQUUsSUFBcEI7QUFBMEJ1UixXQUFPLEVBQUUsQ0FBbkM7QUFBc0NDLFdBQU8sRUFBRSxJQUEvQztBQUFxRGhCLFFBQUksRUFBRTtBQUEzRCxHQUFiO0FBQ0FZLElBQUUsQ0FBQ2QsRUFBSCxDQUFNcUIsY0FBTixFQUFzQjtBQUFFcEIsWUFBUSxFQUFFLENBQVo7QUFBZVcsU0FBSyxFQUFFLENBQXRCO0FBQXlCTSxXQUFPLEVBQUUsR0FBbEM7QUFBdUNoQixRQUFJLEVBQUU7QUFBN0MsR0FBdEIsRUFBc0YsT0FBdEY7QUFFRDtBQUVNLFNBQVNxQixnQkFBVCxDQUEwQjNDLEVBQTFCLEVBQThCO0FBRW5DLE1BQUksQ0FBQ0EsRUFBTCxFQUFTO0FBRVQsTUFBTUMsS0FBSyxHQUFHRCxFQUFFLENBQUNsRSxnQkFBSCxDQUFvQixxQkFBcEIsQ0FBZDtBQUVBdkcsOENBQUksQ0FBQzZMLEVBQUwsQ0FBUW5CLEtBQVIsRUFBZTtBQUFFb0IsWUFBUSxFQUFFLEdBQVo7QUFBaUJ1QixTQUFLLEVBQUUsR0FBeEI7QUFBNkI5UixLQUFDLEVBQUUsSUFBaEM7QUFBc0N1UixXQUFPLEVBQUUsQ0FBL0M7QUFBa0RMLFNBQUssRUFBRSxDQUF6RDtBQUE0RE0sV0FBTyxFQUFFLElBQXJFO0FBQTJFaEIsUUFBSSxFQUFFO0FBQWpGLEdBQWY7QUFFRCxDOzs7Ozs7Ozs7Ozs7Ozs7OztBQ3JERCxJQUFNdUIsVUFBVSxHQUFHLHNCQUFNO0FBQUE7O0FBQUE7O0FBRXZCLE1BQU1DLE1BQU0sR0FBR3hSLFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsa0JBQXZCLENBQWY7QUFFQSxNQUFJLENBQUM2SCxNQUFMLEVBQWE7QUFFYkEsUUFBTSxDQUFDaEQsZ0JBQVAsQ0FBd0IsT0FBeEIsRUFBaUMsVUFBQ2pTLENBQUQsRUFBTztBQUFBOztBQUN0Q0EsS0FBQyxDQUFDa1YsY0FBRjs7QUFFQSxRQUFJek4sTUFBTSxDQUFDME4sT0FBUCxDQUFldlQsTUFBZixHQUF3QixDQUF4QixJQUE2QjZCLFFBQVEsQ0FBQzJSLFFBQVQsQ0FBa0JDLFFBQWxCLENBQTJCLFVBQTNCLENBQWpDLEVBQXlFO0FBQ3ZFRixhQUFPLENBQUNHLElBQVI7QUFDRCxLQUZELE1BRU87QUFDTDdOLFlBQU0sQ0FBQzhOLFFBQVAsQ0FBZ0JDLElBQWhCLEdBQXVCLE9BQXZCO0FBQ0Q7QUFFRixHQVREO0FBV0QsQ0FqQmUsZ0JBQWhCOztBQW1CZVIseUVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ25CQTtBQUNBO0FBQ0E7QUFDQTtBQUVBdE4sNENBQUksQ0FBQ0MsY0FBTCxDQUFvQjhOLGdFQUFwQixFQUFtQzVILCtFQUFuQzs7SUFFTTZILGdCO0FBQ0osNEJBQVl2RCxFQUFaLEVBQWdCO0FBQUE7O0FBRWQsU0FBS0EsRUFBTCxHQUFVQSxFQUFWO0FBQ0EsU0FBS3dELE1BQUwsR0FBY3hELEVBQUUsQ0FBQ2xFLGdCQUFILENBQW9CLDBCQUFwQixDQUFkO0FBQ0EsU0FBSzJILFFBQUwsR0FBZ0J6RCxFQUFFLENBQUMvRSxhQUFILENBQWlCLDZCQUFqQixDQUFoQjtBQUNBLFNBQUt5SSxZQUFMLEdBQW9CMUQsRUFBRSxDQUFDL0UsYUFBSCxDQUFpQixpQkFBakIsQ0FBcEI7QUFDQSxTQUFLMEksa0JBQUwsR0FBMEIzRCxFQUFFLENBQUMvRSxhQUFILENBQWlCLHdCQUFqQixDQUExQjtBQUNBLFNBQUsySSxlQUFMLEdBQXVCNUQsRUFBRSxDQUFDbEUsZ0JBQUgsQ0FBb0IsbUNBQXBCLENBQXZCO0FBQ0EsU0FBSytILFFBQUwsR0FBZ0I3RCxFQUFFLENBQUNsRSxnQkFBSCxDQUFvQiwyQkFBcEIsQ0FBaEIsQ0FSYyxDQVVkOztBQUNBLFNBQUs4SCxlQUFMLENBQXFCLENBQXJCLEVBQXdCMUMsU0FBeEIsQ0FBa0N4RyxHQUFsQyxDQUFzQyxRQUF0QztBQUNBLFNBQUttSixRQUFMLENBQWMsQ0FBZCxFQUFpQjNDLFNBQWpCLENBQTJCeEcsR0FBM0IsQ0FBK0IsUUFBL0I7QUFDQSxTQUFLOEksTUFBTCxDQUFZLENBQVosRUFBZXRDLFNBQWYsQ0FBeUJ4RyxHQUF6QixDQUE2QixRQUE3QjtBQUNBLFNBQUtvSixXQUFMLEdBQW1CLEtBQUtOLE1BQUwsQ0FBWSxDQUFaLEVBQWVoRCxPQUFmLENBQXVCaEksS0FBMUM7O0FBRUEsUUFBSWxELE1BQU0sQ0FBQytLLFVBQVAsSUFBcUIsR0FBekIsRUFBOEI7QUFDNUIsV0FBSzBELFVBQUw7QUFDRCxLQUZELE1BRU87QUFDTCxXQUFLQyxjQUFMO0FBQ0Q7QUFFRjs7OztXQUNELDBCQUFpQjtBQUFBOztBQUVmLFVBQU1QLFFBQVEsR0FBRyxJQUFJUSwrQ0FBSixDQUFhLEtBQUtSLFFBQWxCLEVBQTRCO0FBQzNDUyxlQUFPLEVBQUUsS0FEa0M7QUFFM0NDLGdCQUFRLEVBQUUsSUFGaUM7QUFHM0NDLHVCQUFlLEVBQUUsSUFIMEI7QUFJM0NDLGlCQUFTLEVBQUUsSUFKZ0M7QUFLM0NDLGlCQUFTLEVBQUU7QUFMZ0MsT0FBNUIsQ0FBakI7QUFRQWIsY0FBUSxDQUFDYyxFQUFULENBQVksUUFBWixFQUFzQixVQUFDdkQsS0FBRCxFQUFXO0FBQUE7O0FBQy9Cekwsb0RBQUksQ0FBQzZMLEVBQUwsQ0FBUSxVQUFSLEVBQW9CO0FBQUV2SixrQkFBUSxFQUFFLFlBQVltSixLQUFaLEdBQW9CLEVBQWhDO0FBQW9DSyxrQkFBUSxFQUFFLEdBQTlDO0FBQW1EQyxjQUFJLEVBQUU7QUFBekQsU0FBcEI7QUFDQSxhQUFLcUMsa0JBQUwsQ0FBd0JuRyxLQUF4QixDQUE4QjdFLElBQTlCLEdBQXFDLEtBQUs2SyxNQUFMLENBQVl4QyxLQUFaLEVBQW1CUixPQUFuQixDQUEyQmdFLE1BQWhFO0FBQ0QsT0FIRDtBQUtEOzs7V0FDRCxzQkFBYTtBQUFBO0FBQUE7O0FBRVgsV0FBS0MsWUFBTCxHQUFvQixHQUFwQjtBQUVBbkIsc0VBQWEsQ0FBQzdELE1BQWQ7QUFDRWlGLGVBQU8sRUFBRSxLQUFLMUUsRUFEaEI7QUFFRTJFLFdBQUcsRUFBRSxJQUZQO0FBR0VDLHFCQUFhLEVBQUUsQ0FIakI7QUFJRUMsYUFBSyxFQUFFLGVBSlQ7QUFLRWpLLFdBQUcsRUFBRTtBQUxQLHVEQU1RLEtBQUs2SixZQUFMLEdBQW9CLEtBQUtqQixNQUFMLENBQVkvVCxNQUFqQyxHQUEyQyxJQU5sRCxzREFRWSx3QkFBa0I7QUFBQTs7QUFBQSxZQUFmcVYsUUFBZSxRQUFmQSxRQUFlOztBQUFBOztBQUMxQixhQUFLdEIsTUFBTCxDQUFZaEUsT0FBWixDQUFvQixVQUFDUSxFQUFELEVBQUs5UCxDQUFMLEVBQVc7QUFBQTs7QUFDN0IsY0FBSTRVLFFBQVEsSUFBSyxJQUFLLEtBQUt0QixNQUFMLENBQVkvVCxNQUFqQixHQUEyQlMsQ0FBeEMsSUFBOEM0VSxRQUFRLEdBQUksSUFBSyxLQUFLdEIsTUFBTCxDQUFZL1QsTUFBakIsSUFBNEJTLENBQUMsR0FBRyxDQUFoQyxDQUE5RCxFQUFtRztBQUNqRyxpQkFBSzZVLFdBQUwsQ0FBaUIvRSxFQUFqQjtBQUNEO0FBQ0YsU0FKRDtBQUtELE9BZEg7QUFpQkQ7OztXQUNELHFCQUFZQSxFQUFaLEVBQWdCO0FBQUE7O0FBRWQsVUFBSUEsRUFBRSxDQUFDUSxPQUFILENBQVdoSSxLQUFYLEtBQXFCLEtBQUtzTCxXQUE5QixFQUEyQztBQUUzQyxXQUFLQSxXQUFMLEdBQW1COUQsRUFBRSxDQUFDUSxPQUFILENBQVdoSSxLQUE5Qjs7QUFFQSxVQUFJbEQsTUFBTSxDQUFDK0ssVUFBUCxJQUFxQixHQUF6QixFQUE4QjtBQUM1QjlLLG9EQUFJLENBQUM2TCxFQUFMLENBQVEsU0FBUixFQUFtQjtBQUFFdkosa0JBQVEsRUFBRSxXQUFXbUksRUFBRSxDQUFDUSxPQUFILENBQVdoSSxLQUF0QixHQUE4QixFQUExQztBQUE4QzZJLGtCQUFRLEVBQUUsR0FBeEQ7QUFBNkRDLGNBQUksRUFBRTtBQUFuRSxTQUFuQjtBQUNBLGFBQUtvQyxZQUFMLENBQWtCbEcsS0FBbEIsQ0FBd0I3RSxJQUF4QixHQUErQnFILEVBQUUsQ0FBQ1EsT0FBSCxDQUFXZ0UsTUFBMUM7QUFDRCxPQUhELE1BR08sQ0FDTDtBQUNBO0FBQ0Q7O0FBRUQsV0FBS1osZUFBTCxDQUFxQnBFLE9BQXJCLENBQTZCLFVBQUFRLEVBQUUsRUFBSTtBQUFBOztBQUFFQSxVQUFFLENBQUNrQixTQUFILENBQWFLLE1BQWIsQ0FBb0IsUUFBcEI7QUFBK0IsT0FBcEU7QUFDQSxXQUFLc0MsUUFBTCxDQUFjckUsT0FBZCxDQUFzQixVQUFBUSxFQUFFLEVBQUk7QUFBQTs7QUFBRUEsVUFBRSxDQUFDa0IsU0FBSCxDQUFhSyxNQUFiLENBQW9CLFFBQXBCO0FBQStCLE9BQTdEO0FBQ0EsV0FBS2lDLE1BQUwsQ0FBWWhFLE9BQVosQ0FBb0IsVUFBQVEsRUFBRSxFQUFJO0FBQUE7O0FBQUVBLFVBQUUsQ0FBQ2tCLFNBQUgsQ0FBYUssTUFBYixDQUFvQixRQUFwQjtBQUErQixPQUEzRDtBQUVBLFdBQUtpQyxNQUFMLENBQVloRSxPQUFaLENBQW9CLFVBQUN3RixLQUFELEVBQVE5VSxDQUFSLEVBQWM7QUFBQTs7QUFDaEMsWUFBSThQLEVBQUUsS0FBSyxLQUFLd0QsTUFBTCxDQUFZdFQsQ0FBWixDQUFYLEVBQTJCO0FBQ3pCLGVBQUswVCxlQUFMLENBQXFCMVQsQ0FBckIsRUFBd0JnUixTQUF4QixDQUFrQ3hHLEdBQWxDLENBQXNDLFFBQXRDO0FBQ0EsZUFBS21KLFFBQUwsQ0FBYzNULENBQWQsRUFBaUJnUixTQUFqQixDQUEyQnhHLEdBQTNCLENBQStCLFFBQS9CO0FBQ0FzRixZQUFFLENBQUNrQixTQUFILENBQWF4RyxHQUFiLENBQWlCLFFBQWpCO0FBQ0Q7QUFDRixPQU5EO0FBUUQ7Ozs7OztBQUdILElBQU11SyxvQkFBb0IsR0FBRyxnQ0FBTTtBQUFBOztBQUVqQyxNQUFNQyxPQUFPLEdBQUc1VCxRQUFRLENBQUMySixhQUFULENBQXVCLG1CQUF2QixDQUFoQjtBQUVBLE1BQUlpSyxPQUFKLEVBQWEsSUFBSTNCLGdCQUFKLENBQXFCMkIsT0FBckI7QUFFZCxDQU55QixnQkFBMUI7O0FBUWVELG1GQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzFHQTtDQUdBOztBQUVBLElBQU1FLElBQUksR0FBRzdULFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsYUFBdkIsQ0FBYjtBQUVBLElBQUltSyxHQUFKLEVBQVNDLGlCQUFULEVBQTRCQyxPQUE1QixFQUFxQ0MsU0FBckMsRUFBZ0RDLE1BQWhEOztBQUVBLElBQUlMLElBQUosRUFBVTtBQUVSQyxLQUFHLEdBQUdELElBQUksQ0FBQzNFLE9BQUwsQ0FBYTRFLEdBQW5CO0FBQ0FJLFFBQU0sR0FBR0wsSUFBSSxDQUFDM0UsT0FBTCxDQUFhZ0YsTUFBdEI7QUFDQUYsU0FBTyxHQUFHSCxJQUFJLENBQUMzRSxPQUFMLENBQWE4RSxPQUF2QjtBQUNBQyxXQUFTLEdBQUdKLElBQUksQ0FBQzNFLE9BQUwsQ0FBYStFLFNBQXpCO0FBQ0FGLG1CQUFpQixHQUFHRixJQUFJLENBQUMzRSxPQUFMLENBQWFpRixVQUFiLENBQXdCelQsS0FBeEIsQ0FBOEIsSUFBOUIsQ0FBcEI7QUFFQW1ULE1BQUksQ0FBQ08sZUFBTCxDQUFxQixVQUFyQjtBQUNBUCxNQUFJLENBQUNPLGVBQUwsQ0FBcUIsYUFBckI7QUFDQVAsTUFBSSxDQUFDTyxlQUFMLENBQXFCLGNBQXJCO0FBQ0FQLE1BQUksQ0FBQ08sZUFBTCxDQUFxQixnQkFBckI7QUFDQVAsTUFBSSxDQUFDTyxlQUFMLENBQXFCLGlCQUFyQjtBQUVEOztBQUVELElBQUlDLGdCQUFnQixHQUFHLEtBQXZCO0FBRUEsSUFBTUMsVUFBVSxHQUFHLElBQUlDLG9EQUFKLENBQVkscUJBQVosRUFBbUM7QUFDcERDLGVBQWEsRUFBRSxJQURxQztBQUVwREMsbUJBQWlCLEVBQUU7QUFDakJDLGtCQUFjLEVBQUUsd0JBQUNDLEtBQUQsRUFBVztBQUFBOztBQUN6QixVQUFJQSxLQUFLLENBQUNoTSxJQUFOLElBQWMsT0FBbEIsRUFBMkI7QUFDekIsWUFBSWlNLE1BQU0sR0FBRyxFQUFiO0FBQ0EsWUFBTUMsY0FBYyxHQUFHRixLQUFLLENBQUNwSyxLQUFOLENBQVk3SixLQUFaLENBQWtCLEdBQWxCLENBQXZCO0FBQ0EsWUFBSW1VLGNBQWMsQ0FBQyxDQUFELENBQWxCLEVBQXVCRCxNQUFNLEdBQUdDLGNBQWMsQ0FBQyxDQUFELENBQWQsQ0FBa0JuVSxLQUFsQixDQUF3QixHQUF4QixFQUE2QixDQUE3QixDQUFUO0FBQ3ZCLFlBQUlxVCxpQkFBaUIsQ0FBQ3pULE9BQWxCLENBQTBCc1UsTUFBMUIsS0FBcUMsQ0FBekMsRUFDRSxPQUFPLElBQVAsQ0FERixLQUdFLE9BQU8sS0FBUDtBQUNIO0FBQ0YsS0FWYTtBQURHLEdBRmlDO0FBZXBERSxVQUFRLEVBQUU7QUFDVkosa0JBQWMsRUFBRTtBQUROO0FBZjBDLENBQW5DLENBQW5COztBQW9CQSxJQUFNSyxlQUFlLEdBQUcseUJBQUN4WSxDQUFELEVBQU87QUFBQTs7QUFBQTs7QUFDN0JBLEdBQUMsQ0FBQ2tWLGNBQUY7QUFFQSxNQUFNb0MsSUFBSSxHQUFHdFgsQ0FBQyxDQUFDeVksTUFBZjtBQUFBLE1BQ01DLFFBQVEsR0FBRyxJQUFJQyxRQUFKLENBQWFyQixJQUFiLENBRGpCO0FBQUEsTUFFTXNCLE9BQU8sR0FBR25WLFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsdUJBQXZCLENBRmhCO0FBQUEsTUFHTXlMLE9BQU8sR0FBR3BWLFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIscUJBQXZCLENBSGhCO0FBS0FzTCxVQUFRLENBQUNJLE1BQVQsQ0FBZ0IsT0FBaEIsRUFBeUJDLEVBQUUsQ0FBQ0MsS0FBNUI7QUFDQU4sVUFBUSxDQUFDSSxNQUFULENBQWdCLFFBQWhCLEVBQTBCLGNBQTFCO0FBQ0FKLFVBQVEsQ0FBQ0ksTUFBVCxDQUFnQixRQUFoQixFQUEwQm5CLE1BQTFCO0FBQ0FlLFVBQVEsQ0FBQ0ksTUFBVCxDQUFnQixTQUFoQixFQUEyQnJCLE9BQTNCO0FBQ0FpQixVQUFRLENBQUNJLE1BQVQsQ0FBZ0IsV0FBaEIsRUFBNkJwQixTQUE3QjtBQUVBalUsVUFBUSxDQUFDd08sZ0JBQVQsQ0FBMEIsb0JBQTFCLEVBQWdELFVBQUNnSCxLQUFELEVBQVc7QUFBQTs7QUFDekRoUyxXQUFPLENBQUNDLEdBQVIsQ0FBWStSLEtBQUssQ0FBQ0MsTUFBTixDQUFhQyxNQUF6QjtBQUNELEdBRkQsYUFFRyxLQUZIO0FBSUExVixVQUFRLENBQUN3TyxnQkFBVCxDQUEwQixrQkFBMUIsRUFBOEMsVUFBQ2pTLENBQUQsRUFBTztBQUFBOztBQUFBOztBQUVuRCxRQUFJOFgsZ0JBQUosRUFBc0IsT0FBTyxLQUFQO0FBRXRCQSxvQkFBZ0IsR0FBRyxJQUFuQixDQUptRCxDQU1uRDs7QUFDQXNCLGdEQUFLLENBQUM7QUFDSkMsWUFBTSxFQUFFLE1BREo7QUFFSkMsU0FBRyxFQUFFUCxFQUFFLENBQUNRLElBRko7QUFHSkMsVUFBSSxFQUFFZDtBQUhGLEtBQUQsQ0FBTCxDQUtDeEYsSUFMRCxDQUtNLFVBQVV1RyxRQUFWLEVBQW9CO0FBQ3hCeFMsYUFBTyxDQUFDQyxHQUFSLENBQVksV0FBV3VTLFFBQVEsQ0FBQ0QsSUFBaEM7QUFDQXZTLGFBQU8sQ0FBQ0MsR0FBUixDQUFZLGFBQWF1UyxRQUFRLENBQUNDLE1BQWxDO0FBQ0QsS0FSRCxFQVNBLFVBQUNDLEtBQUQsRUFBVztBQUFBOztBQUNUMVMsYUFBTyxDQUFDQyxHQUFSLENBQVl5UyxLQUFaO0FBQ0QsS0FYRCxhQVBtRCxDQW9CbkQ7O0FBQ0EsUUFBTUMsUUFBUSxHQUFHLEVBQWpCO0FBQ0EsUUFBTUMsU0FBUyxHQUFHdkMsSUFBSSxDQUFDckosZ0JBQUwsQ0FBc0IsT0FBdEIsQ0FBbEIsQ0F0Qm1ELENBd0JuRDs7QUFDQTRMLGFBQVMsQ0FBQ2xJLE9BQVYsQ0FBa0IsVUFBQXlHLEtBQUssRUFBSTtBQUFBOztBQUN6QixVQUFJQSxLQUFLLENBQUM1TixJQUFOLElBQWMsZUFBZCxJQUFpQzROLEtBQUssQ0FBQzVOLElBQU4sSUFBYyxrQkFBbkQsRUFBdUU7QUFDckVvUCxnQkFBUSxDQUFDeEIsS0FBSyxDQUFDNU4sSUFBUCxDQUFSLEdBQXVCNE4sS0FBSyxDQUFDcEssS0FBN0I7QUFDRDs7QUFDRCxVQUFJb0ssS0FBSyxDQUFDNU4sSUFBTixJQUFjLGtCQUFkLElBQW9DNE4sS0FBSyxDQUFDcEssS0FBTixJQUFlLElBQXZELEVBQTZEO0FBQzNENEwsZ0JBQVEsQ0FBQyxrQkFBRCxDQUFSLEdBQStCLElBQS9CO0FBQ0QsT0FGRCxNQUVPLElBQUl4QixLQUFLLENBQUM1TixJQUFOLElBQWMsa0JBQWQsSUFBb0M0TixLQUFLLENBQUNwSyxLQUFOLElBQWUsS0FBdkQsRUFBOEQ7QUFDbkU0TCxnQkFBUSxDQUFDLGtCQUFELENBQVIsR0FBK0IsS0FBL0I7QUFDRDtBQUNGLEtBVEQ7QUFXQSxRQUFNRSxPQUFPLEdBQUc7QUFDZFQsWUFBTSxFQUFFLE1BRE07QUFFZFUsVUFBSSxFQUFFQyxJQUFJLENBQUNDLFNBQUwsQ0FBZUwsUUFBZixDQUZRO0FBR2RNLFVBQUksRUFBRSxNQUhRO0FBSWRDLGFBQU8sRUFBRTtBQUNQLDRDQUFvQyxNQUQ3QjtBQUVQLHdCQUFnQixrQkFGVDtBQUdQLHdDQUFnQztBQUh6QjtBQUpLLEtBQWhCO0FBV0FDLFNBQUssQ0FBQzdDLEdBQUQsRUFBTXVDLE9BQU4sQ0FBTCxDQUNHNUcsSUFESCxDQUNRLFVBQUFtSCxHQUFHLEVBQUk7QUFBQTs7QUFDWC9DLFVBQUksQ0FBQzNILEtBQUwsQ0FBV0MsT0FBWCxHQUFxQixNQUFyQjtBQUNBZ0osYUFBTyxDQUFDakosS0FBUixDQUFjQyxPQUFkLEdBQXdCLE9BQXhCO0FBQ0QsS0FKSCxzQkFLUyxVQUFBMEssR0FBRyxFQUFJO0FBQUE7O0FBQ1pyVCxhQUFPLENBQUNDLEdBQVIsK0JBQW1Db1QsR0FBbkM7QUFDQWhELFVBQUksQ0FBQzNILEtBQUwsQ0FBV0MsT0FBWCxHQUFxQixNQUFyQjtBQUNBaUosYUFBTyxDQUFDbEosS0FBUixDQUFjQyxPQUFkLEdBQXdCLE9BQXhCO0FBQ0QsS0FUSDtBQVdFMEgsUUFBSSxDQUFDakUsU0FBTCxDQUFleEcsR0FBZixDQUFtQixXQUFuQjtBQUVILEdBNURELGFBNERHLEtBNURIO0FBOERELENBaEZvQixnQkFBckI7O0FBa0ZBLElBQU0wTixtQkFBbUIsR0FBRywrQkFBTTtBQUFBOztBQUVoQyxNQUFNQyxLQUFLLEdBQUcvVyxRQUFRLENBQUN3SyxnQkFBVCxDQUEwQixhQUExQixDQUFkOztBQUVBLE9BQUssSUFBSTVMLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdtWSxLQUFLLENBQUM1WSxNQUExQixFQUFrQ1MsQ0FBQyxFQUFuQyxFQUF1QztBQUNyQ21ZLFNBQUssQ0FBQ25ZLENBQUQsQ0FBTCxDQUFTNFAsZ0JBQVQsQ0FBMEIsUUFBMUIsRUFBb0N1RyxlQUFwQyxFQUFxRCxLQUFyRDtBQUNEO0FBRUYsQ0FSd0IsZ0JBQXpCOztBQVVlK0Isa0ZBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM0lBO0NBR0E7O0FBRUEsSUFBTWpELElBQUksR0FBRzdULFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsZ0JBQXZCLENBQWI7QUFFQSxJQUFJb0ssaUJBQUosRUFBdUJDLE9BQXZCLEVBQWdDQyxTQUFoQyxFQUEyQ0MsTUFBM0M7O0FBRUEsSUFBSUwsSUFBSixFQUFVO0FBRVJLLFFBQU0sR0FBR0wsSUFBSSxDQUFDM0UsT0FBTCxDQUFhZ0YsTUFBdEI7QUFDQUYsU0FBTyxHQUFHSCxJQUFJLENBQUMzRSxPQUFMLENBQWE4RSxPQUF2QjtBQUNBQyxXQUFTLEdBQUdKLElBQUksQ0FBQzNFLE9BQUwsQ0FBYStFLFNBQXpCO0FBQ0FGLG1CQUFpQixHQUFHRixJQUFJLENBQUMzRSxPQUFMLENBQWFpRixVQUFiLENBQXdCelQsS0FBeEIsQ0FBOEIsSUFBOUIsQ0FBcEI7QUFFQW1ULE1BQUksQ0FBQ08sZUFBTCxDQUFxQixhQUFyQjtBQUNBUCxNQUFJLENBQUNPLGVBQUwsQ0FBcUIsY0FBckI7QUFDQVAsTUFBSSxDQUFDTyxlQUFMLENBQXFCLGdCQUFyQjtBQUNBUCxNQUFJLENBQUNPLGVBQUwsQ0FBcUIsaUJBQXJCO0FBRUQ7O0FBRUQsSUFBSUMsZ0JBQWdCLEdBQUcsS0FBdkI7QUFFQSxJQUFNQyxVQUFVLEdBQUcsSUFBSUMsb0RBQUosQ0FBWSxpQkFBWixFQUErQjtBQUNoREMsZUFBYSxFQUFFLElBRGlDO0FBRWhEQyxtQkFBaUIsRUFBRTtBQUNqQkMsa0JBQWMsRUFBRSx3QkFBQ0MsS0FBRCxFQUFXO0FBQUE7O0FBQ3pCLFVBQUlBLEtBQUssQ0FBQ2hNLElBQU4sSUFBYyxPQUFsQixFQUEyQjtBQUN6QixZQUFJaU0sTUFBTSxHQUFHLEVBQWI7QUFDQSxZQUFNQyxjQUFjLEdBQUdGLEtBQUssQ0FBQ3BLLEtBQU4sQ0FBWTdKLEtBQVosQ0FBa0IsR0FBbEIsQ0FBdkI7QUFDQSxZQUFJbVUsY0FBYyxDQUFDLENBQUQsQ0FBbEIsRUFBdUJELE1BQU0sR0FBR0MsY0FBYyxDQUFDLENBQUQsQ0FBZCxDQUFrQm5VLEtBQWxCLENBQXdCLEdBQXhCLEVBQTZCLENBQTdCLENBQVQ7QUFDdkIsWUFBSXFULGlCQUFpQixDQUFDelQsT0FBbEIsQ0FBMEJzVSxNQUExQixLQUFxQyxDQUF6QyxFQUNFLE9BQU8sSUFBUCxDQURGLEtBR0UsT0FBTyxLQUFQO0FBQ0g7QUFDRixLQVZhO0FBREcsR0FGNkI7QUFlaERFLFVBQVEsRUFBRTtBQUNWSixrQkFBYyxFQUFFO0FBRE47QUFmc0MsQ0FBL0IsQ0FBbkI7O0FBb0JBLElBQU1zQyxXQUFXLEdBQUcscUJBQUN6YSxDQUFELEVBQU87QUFBQTs7QUFDekJBLEdBQUMsQ0FBQ2tWLGNBQUY7QUFDQSxNQUFNb0MsSUFBSSxHQUFHdFgsQ0FBQyxDQUFDeVksTUFBZjtBQUVBLE1BQU1DLFFBQVEsR0FBRyxJQUFJQyxRQUFKLENBQWFyQixJQUFiLENBQWpCO0FBRUFvQixVQUFRLENBQUNJLE1BQVQsQ0FBZ0IsUUFBaEIsRUFBMEIsY0FBMUI7QUFDQUosVUFBUSxDQUFDSSxNQUFULENBQWdCLE9BQWhCLEVBQXlCQyxFQUFFLENBQUNDLEtBQTVCO0FBQ0FOLFVBQVEsQ0FBQ0ksTUFBVCxDQUFnQixTQUFoQixFQUEyQnhCLElBQUksQ0FBQ2xULFlBQUwsQ0FBa0IsY0FBbEIsQ0FBM0I7QUFDQXNVLFVBQVEsQ0FBQ0ksTUFBVCxDQUFnQixRQUFoQixFQUEwQm5CLE1BQTFCO0FBQ0FlLFVBQVEsQ0FBQ0ksTUFBVCxDQUFnQixTQUFoQixFQUEyQnJCLE9BQTNCO0FBQ0FpQixVQUFRLENBQUNJLE1BQVQsQ0FBZ0IsV0FBaEIsRUFBNkJwQixTQUE3QixFQVh5QixDQWF6QjtBQUNBO0FBQ0E7O0FBRUFqVSxVQUFRLENBQUN3TyxnQkFBVCxDQUEwQixvQkFBMUIsRUFBZ0QsVUFBVWdILEtBQVYsRUFBaUI7QUFDL0RoUyxXQUFPLENBQUNDLEdBQVIsQ0FBWStSLEtBQUssQ0FBQ0MsTUFBTixDQUFhQyxNQUF6QjtBQUNELEdBRkQsRUFFRyxLQUZIO0FBS0E7O0FBQ0ExVixVQUFRLENBQUN3TyxnQkFBVCxDQUEwQixrQkFBMUIsRUFBOEMsVUFBVWpTLENBQVYsRUFBYTtBQUFBOztBQUV6RCxRQUFJOFgsZ0JBQUosRUFBc0IsT0FBTyxLQUFQO0FBRXRCQSxvQkFBZ0IsR0FBRyxJQUFuQjtBQUVBUixRQUFJLENBQUNqRSxTQUFMLENBQWV4RyxHQUFmLENBQW1CLFdBQW5CO0FBRUF1TSxnREFBSyxDQUFDO0FBQ0pDLFlBQU0sRUFBRSxNQURKO0FBRUpDLFNBQUcsRUFBRVAsRUFBRSxDQUFDUSxJQUZKO0FBR0pDLFVBQUksRUFBRWQ7QUFIRixLQUFELENBQUwsQ0FLQ3hGLElBTEQsQ0FLTSxVQUFVdUcsUUFBVixFQUFvQjtBQUN4QnhTLGFBQU8sQ0FBQ0MsR0FBUixDQUFZLFdBQVd1UyxRQUFRLENBQUNELElBQWhDO0FBQ0F2UyxhQUFPLENBQUNDLEdBQVIsQ0FBWSxhQUFhdVMsUUFBUSxDQUFDQyxNQUFsQztBQUNBcEMsVUFBSSxDQUFDdEgsU0FBTCxHQUFpQiw4QkFBOEJzSCxJQUFJLENBQUNsVCxZQUFMLENBQWtCLGNBQWxCLENBQTlCLEdBQWtFLFFBQW5GO0FBQ0QsS0FURCxFQVVBLFVBQUN1VixLQUFELEVBQVc7QUFBQTs7QUFDVDFTLGFBQU8sQ0FBQ0MsR0FBUixDQUFZeVMsS0FBWjtBQUNELEtBWkQ7QUFjRCxHQXRCRCxFQXNCRyxLQXRCSDtBQXdCRCxDQS9DZ0IsZ0JBQWpCOztBQWlEQSxJQUFNZSxlQUFlLEdBQUcsMkJBQU07QUFBQTs7QUFFNUIsTUFBTUYsS0FBSyxHQUFHL1csUUFBUSxDQUFDd0ssZ0JBQVQsQ0FBMEIsZ0JBQTFCLENBQWQ7O0FBRUEsT0FBSyxJQUFJNUwsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR21ZLEtBQUssQ0FBQzVZLE1BQTFCLEVBQWtDUyxDQUFDLEVBQW5DLEVBQXVDO0FBQ3JDbVksU0FBSyxDQUFDblksQ0FBRCxDQUFMLENBQVM0UCxnQkFBVCxDQUEwQixRQUExQixFQUFvQ3dJLFdBQXBDLEVBQWlELEtBQWpEO0FBQ0QsR0FOMkIsQ0FNMUI7O0FBRUgsQ0FSb0IsZ0JBQXJCOztBQVVlQyw4RUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4R0E7Q0FHQTs7QUFFQSxJQUFNcEQsSUFBSSxHQUFHN1QsUUFBUSxDQUFDMkosYUFBVCxDQUF1QixxQkFBdkIsQ0FBYjtBQUVBLElBQUltSyxHQUFKLEVBQVNuTCxJQUFULEVBQWVvTCxpQkFBZjs7QUFFQSxJQUFJRixJQUFKLEVBQVU7QUFFUkMsS0FBRyxHQUFHRCxJQUFJLENBQUMzRSxPQUFMLENBQWE0RSxHQUFuQjtBQUNBbkwsTUFBSSxHQUFHa0wsSUFBSSxDQUFDM0UsT0FBTCxDQUFhdkcsSUFBcEI7QUFDQW9MLG1CQUFpQixHQUFHRixJQUFJLENBQUMzRSxPQUFMLENBQWFpRixVQUFiLENBQXdCelQsS0FBeEIsQ0FBOEIsSUFBOUIsQ0FBcEI7QUFFQW1ULE1BQUksQ0FBQ08sZUFBTCxDQUFxQixVQUFyQjtBQUNBUCxNQUFJLENBQUNPLGVBQUwsQ0FBcUIsV0FBckI7QUFDQVAsTUFBSSxDQUFDTyxlQUFMLENBQXFCLGlCQUFyQjtBQUVEOztBQUVELElBQUlDLGdCQUFnQixHQUFHLEtBQXZCO0FBRUEsSUFBTUMsVUFBVSxHQUFHLElBQUlDLG9EQUFKLENBQVksc0JBQVosRUFBb0M7QUFDckRDLGVBQWEsRUFBRSxJQURzQztBQUVyREMsbUJBQWlCLEVBQUU7QUFDakJDLGtCQUFjLEVBQUUsd0JBQUNDLEtBQUQsRUFBVztBQUFBOztBQUN6QixVQUFJQSxLQUFLLENBQUNoTSxJQUFOLElBQWMsT0FBbEIsRUFBMkI7QUFDekIsWUFBSWlNLE1BQU0sR0FBRyxFQUFiO0FBQ0EsWUFBTUMsY0FBYyxHQUFHRixLQUFLLENBQUNwSyxLQUFOLENBQVk3SixLQUFaLENBQWtCLEdBQWxCLENBQXZCO0FBQ0EsWUFBSW1VLGNBQWMsQ0FBQyxDQUFELENBQWxCLEVBQXVCRCxNQUFNLEdBQUdDLGNBQWMsQ0FBQyxDQUFELENBQWQsQ0FBa0JuVSxLQUFsQixDQUF3QixHQUF4QixFQUE2QixDQUE3QixDQUFUO0FBQ3ZCLFlBQUlxVCxpQkFBaUIsQ0FBQ3pULE9BQWxCLENBQTBCc1UsTUFBMUIsS0FBcUMsQ0FBekMsRUFDRSxPQUFPLElBQVAsQ0FERixLQUdFLE9BQU8sS0FBUDtBQUNIO0FBQ0YsS0FWYTtBQURHLEdBRmtDO0FBZXJERSxVQUFRLEVBQUU7QUFDVkosa0JBQWMsRUFBRTtBQUROO0FBZjJDLENBQXBDLENBQW5COztBQW9CQSxJQUFNd0MsZ0JBQWdCLEdBQUcsMEJBQUMzYSxDQUFELEVBQU87QUFBQTs7QUFBQTs7QUFFOUJBLEdBQUMsQ0FBQ2tWLGNBQUY7QUFFQSxNQUFNb0MsSUFBSSxHQUFHdFgsQ0FBQyxDQUFDeVksTUFBZjtBQUFBLE1BQ01HLE9BQU8sR0FBR25WLFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsdUJBQXZCLENBRGhCO0FBQUEsTUFFTXlMLE9BQU8sR0FBR3BWLFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIscUJBQXZCLENBRmhCO0FBQUEsTUFHTXdOLFFBQVEsR0FBR25YLFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsc0JBQXZCLENBSGpCO0FBQUEsTUFJTXlOLFVBQVUsR0FBR3BYLFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsd0JBQXZCLENBSm5CO0FBQUEsTUFLTTBOLFFBQVEsR0FBR3JYLFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsY0FBdkIsQ0FMakI7QUFPQTNKLFVBQVEsQ0FBQ3dPLGdCQUFULENBQTBCLG9CQUExQixFQUFnRCxVQUFDZ0gsS0FBRDtBQUFBOztBQUFBLFdBQVdoUyxPQUFPLENBQUNDLEdBQVIsQ0FBWStSLEtBQUssQ0FBQ0MsTUFBTixDQUFhQyxNQUF6QixDQUFYO0FBQUEsR0FBaEQsYUFBNkYsS0FBN0Y7QUFDQTFWLFVBQVEsQ0FBQ3dPLGdCQUFULENBQTBCLGtCQUExQixFQUE4QyxVQUFValMsQ0FBVixFQUFhO0FBQUE7O0FBRXpELFFBQUk4WCxnQkFBSixFQUFzQixPQUFPLEtBQVA7QUFFdEJBLG9CQUFnQixHQUFHLElBQW5CO0FBRUEsUUFBTThCLFFBQVEsR0FBRztBQUFFbUIsVUFBSSxFQUFFM087QUFBUixLQUFqQjtBQUNBLFFBQU15TixTQUFTLEdBQUd2QyxJQUFJLENBQUNySixnQkFBTCxDQUFzQixPQUF0QixDQUFsQixDQVB5RCxDQVN6RDs7QUFDQTRMLGFBQVMsQ0FBQ2xJLE9BQVYsQ0FBa0IsVUFBQXlHLEtBQUssRUFBSTtBQUFBOztBQUN6QixVQUFJQSxLQUFLLENBQUM1TixJQUFOLElBQWMsZUFBZCxJQUFpQzROLEtBQUssQ0FBQzVOLElBQU4sSUFBYyxrQkFBbkQsRUFBdUU7QUFDckVvUCxnQkFBUSxDQUFDeEIsS0FBSyxDQUFDNU4sSUFBUCxDQUFSLEdBQXVCNE4sS0FBSyxDQUFDcEssS0FBN0I7QUFDRDs7QUFDRCxVQUFJb0ssS0FBSyxDQUFDNU4sSUFBTixJQUFjLGtCQUFkLElBQW9DNE4sS0FBSyxDQUFDcEssS0FBTixJQUFlLElBQXZELEVBQTZEO0FBQzNENEwsZ0JBQVEsQ0FBQyxrQkFBRCxDQUFSLEdBQStCLElBQS9CO0FBQ0QsT0FGRCxNQUVPLElBQUl4QixLQUFLLENBQUM1TixJQUFOLElBQWMsa0JBQWQsSUFBb0M0TixLQUFLLENBQUNwSyxLQUFOLElBQWUsS0FBdkQsRUFBOEQ7QUFDbkU0TCxnQkFBUSxDQUFDLGtCQUFELENBQVIsR0FBK0IsS0FBL0I7QUFDRDtBQUNGLEtBVEQ7QUFXQTNTLFdBQU8sQ0FBQ0MsR0FBUixDQUFZMFMsUUFBWjtBQUVBLFFBQU1FLE9BQU8sR0FBRztBQUNkVCxZQUFNLEVBQUUsTUFETTtBQUVkVSxVQUFJLEVBQUVDLElBQUksQ0FBQ0MsU0FBTCxDQUFlTCxRQUFmLENBRlE7QUFHZE0sVUFBSSxFQUFFLE1BSFE7QUFJZEMsYUFBTyxFQUFFO0FBQ1AsNENBQW9DLE1BRDdCO0FBRVAsd0JBQWdCLGtCQUZUO0FBR1Asa0JBQVUsa0JBSEg7QUFJUCx3Q0FBZ0M7QUFKekI7QUFKSyxLQUFoQjtBQVdBQyxTQUFLLENBQUM3QyxHQUFELEVBQU11QyxPQUFOLENBQUwsQ0FDRzVHLElBREgsQ0FDUSxVQUFBbUgsR0FBRyxFQUFJO0FBQUE7O0FBQ1hwVCxhQUFPLENBQUNDLEdBQVIsQ0FBWSxXQUFXbVQsR0FBRyxDQUFDWCxNQUEzQjtBQUNBLFVBQUlXLEdBQUcsQ0FBQ1gsTUFBSixJQUFjLEdBQWxCLEVBQXVCLE1BQU1zQixLQUFLLENBQUN2QixRQUFRLENBQUN3QixVQUFWLENBQVgsQ0FBdkIsS0FDSyxPQUFPWixHQUFHLENBQUNhLElBQUosRUFBUDtBQUNOLEtBTEgsYUFNR2hJLElBTkgsQ0FNUSxVQUFBbUgsR0FBRyxFQUFJO0FBQUE7O0FBRVhwVCxhQUFPLENBQUNDLEdBQVIsQ0FBWW1ULEdBQVo7QUFDQXBULGFBQU8sQ0FBQ0MsR0FBUixDQUFZLGFBQWFtVCxHQUFHLENBQUNjLE1BQTdCO0FBQ0FsVSxhQUFPLENBQUNDLEdBQVIsQ0FBWSxZQUFZbVQsR0FBRyxDQUFDVyxLQUE1Qjs7QUFFQSxVQUFJWCxHQUFHLElBQUksdUJBQVgsRUFBb0M7QUFDbEMvQyxZQUFJLENBQUMzSCxLQUFMLENBQVdDLE9BQVgsR0FBcUIsTUFBckI7QUFDQWdMLGdCQUFRLENBQUNqTCxLQUFULENBQWVDLE9BQWYsR0FBeUIsT0FBekI7QUFDRCxPQUhELE1BR087QUFFTCxZQUFNNkosU0FBUSxHQUFHTyxJQUFJLENBQUNvQixLQUFMLENBQVdmLEdBQVgsQ0FBakI7O0FBRUFwVCxlQUFPLENBQUNDLEdBQVIsQ0FBWXVTLFNBQVo7O0FBRUEsWUFBSUEsU0FBUSxDQUFDdUIsS0FBVCxJQUFrQixJQUF0QixFQUE0QjtBQUMxQnZYLGtCQUFRLENBQUMySixhQUFULENBQXVCLDJCQUF2QixFQUFvRDRDLFNBQXBELEdBQWdFeUosU0FBUSxDQUFDdUIsS0FBekU7QUFDQTFELGNBQUksQ0FBQzNILEtBQUwsQ0FBV0MsT0FBWCxHQUFxQixNQUFyQjtBQUNBaUwsb0JBQVUsQ0FBQ2xMLEtBQVgsQ0FBaUJDLE9BQWpCLEdBQTJCLE9BQTNCO0FBQ0QsU0FKRCxNQUlPO0FBQ0xrTCxrQkFBUSxDQUFDOUssU0FBVCxHQUFxQnlKLFNBQVEsQ0FBQzBCLE1BQTlCO0FBQ0E3RCxjQUFJLENBQUMzSCxLQUFMLENBQVdDLE9BQVgsR0FBcUIsTUFBckI7QUFDQWdKLGlCQUFPLENBQUNqSixLQUFSLENBQWNDLE9BQWQsR0FBd0IsT0FBeEI7QUFDRDtBQUNGO0FBRUYsS0FoQ0gsc0JBaUNTLFVBQUEwSyxHQUFHLEVBQUk7QUFBQTs7QUFDWnJULGFBQU8sQ0FBQ0MsR0FBUiwrQkFBbUNvVCxHQUFuQztBQUNBaEQsVUFBSSxDQUFDM0gsS0FBTCxDQUFXQyxPQUFYLEdBQXFCLE1BQXJCO0FBQ0FpSixhQUFPLENBQUNsSixLQUFSLENBQWNDLE9BQWQsR0FBd0IsT0FBeEI7QUFDRCxLQXJDSDtBQXVDQTBILFFBQUksQ0FBQ2pFLFNBQUwsQ0FBZXhHLEdBQWYsQ0FBbUIsV0FBbkI7QUFFRCxHQTNFRCxFQTJFRyxLQTNFSCxFQVo4QixDQXlGOUI7O0FBQ0FpTyxVQUFRLENBQUM3SSxnQkFBVCxDQUEwQixPQUExQixFQUFtQyxZQUFNO0FBQUE7O0FBQUE7O0FBRXZDLFFBQU1vSixNQUFNLEdBQUdQLFFBQVEsQ0FBQzlLLFNBQXhCO0FBQUEsUUFDTXNMLGNBQWMsR0FBRzdYLFFBQVEsQ0FBQ3dNLGFBQVQsQ0FBdUIsT0FBdkIsQ0FEdkI7QUFHQXFMLGtCQUFjLENBQUN0TixLQUFmLEdBQXVCcU4sTUFBdkI7QUFDQTVYLFlBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsTUFBdkIsRUFBK0J5QyxXQUEvQixDQUEyQ3lMLGNBQTNDO0FBQ0FBLGtCQUFjLENBQUNDLE1BQWY7QUFDQTlYLFlBQVEsQ0FBQytYLFdBQVQsQ0FBcUIsTUFBckI7QUFDQUYsa0JBQWMsQ0FBQzVILE1BQWY7QUFDQWpRLFlBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsVUFBdkIsRUFBbUM0QyxTQUFuQyxHQUErQyxTQUEvQztBQUNBeUwsY0FBVSxDQUFDO0FBQUE7O0FBQUEsYUFBTWhZLFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsVUFBdkIsRUFBbUM0QyxTQUFuQyxHQUErQyxlQUFyRDtBQUFBLEtBQUQsYUFBdUUsSUFBdkUsQ0FBVjtBQUNELEdBWkQ7QUFjRCxDQXhHcUIsZ0JBQXRCOztBQTBHQSxJQUFNMEwsb0JBQW9CLEdBQUcsZ0NBQU07QUFBQTs7QUFBQTs7QUFFakMsTUFBTWxCLEtBQUssR0FBRy9XLFFBQVEsQ0FBQ3dLLGdCQUFULENBQTBCLHFCQUExQixDQUFkO0FBRUF1TSxPQUFLLENBQUM3SSxPQUFOLENBQWMsVUFBQVEsRUFBRTtBQUFBOztBQUFBLFdBQUlBLEVBQUUsQ0FBQ0YsZ0JBQUgsQ0FBb0IsUUFBcEIsRUFBOEIwSSxnQkFBOUIsRUFBZ0QsS0FBaEQsQ0FBSjtBQUFBLEdBQWhCO0FBRUQsQ0FOeUIsZ0JBQTFCOztBQVFlZSxtRkFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3SkE7O0FBRUEsSUFBTUMsY0FBYyxHQUFHLDBCQUFNO0FBQUE7O0FBQUE7O0FBRTNCLE1BQUksQ0FBQ2xZLFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsTUFBdkIsQ0FBTCxFQUFxQztBQUVyQyxNQUFNd08sTUFBTSxHQUFHLElBQUlDLGdFQUFKLENBQVc7QUFDeEJSLFVBQU0sRUFBRSx5Q0FEZ0I7QUFFeEI5USxXQUFPLEVBQUU7QUFGZSxHQUFYLENBQWY7QUFLQXFSLFFBQU0sQ0FBQ0UsSUFBUCxHQUFjNUksSUFBZCxDQUFtQixZQUFNO0FBQUE7O0FBQ3ZCclQsT0FBRyxHQUFHLElBQUlrYyxNQUFNLENBQUNDLElBQVAsQ0FBWUMsR0FBaEIsQ0FBb0J4WSxRQUFRLENBQUN5WSxjQUFULENBQXdCLEtBQXhCLENBQXBCLEVBQW9EO0FBQ3hEQyxZQUFNLEVBQUU7QUFBRUMsV0FBRyxFQUFFLFNBQVA7QUFBa0JDLFdBQUcsRUFBRSxDQUFDO0FBQXhCLE9BRGdEO0FBRXhEQyxVQUFJLEVBQUUsRUFGa0Q7QUFHeERDLHNCQUFnQixFQUFFO0FBSHNDLEtBQXBELENBQU47QUFLRCxHQU5EO0FBU0QsQ0FsQm1CLGdCQUFwQjs7QUFvQmVaLDZFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDdEJNYSxNO0FBQ0osa0JBQVlySyxFQUFaLEVBQWdCO0FBQUE7O0FBQUE7O0FBRWQsU0FBS0EsRUFBTCxHQUFVQSxFQUFWO0FBQ0EsU0FBS3NLLEdBQUwsR0FBV3RLLEVBQUUsQ0FBQy9FLGFBQUgsQ0FBaUIsY0FBakIsQ0FBWDtBQUNBLFNBQUtzUCxVQUFMLEdBQWtCdkssRUFBRSxDQUFDL0UsYUFBSCxDQUFpQixxQkFBakIsQ0FBbEI7QUFDQSxTQUFLdVAsWUFBTCxHQUFvQnhLLEVBQUUsQ0FBQy9FLGFBQUgsQ0FBaUIsaUJBQWpCLENBQXBCO0FBQ0EsU0FBS3dQLFlBQUwsR0FBb0J6SyxFQUFFLENBQUMvRSxhQUFILENBQWlCLG1CQUFqQixDQUFwQjtBQUNBLFNBQUt5UCxTQUFMLEdBQWlCMUssRUFBRSxDQUFDL0UsYUFBSCxDQUFpQixpQkFBakIsQ0FBakI7QUFFQSxTQUFLdVAsWUFBTCxDQUFrQnRKLFNBQWxCLENBQTRCeEcsR0FBNUIsQ0FBZ0MsZ0JBQWhDO0FBQ0EsU0FBSytQLFlBQUwsQ0FBa0J2SixTQUFsQixDQUE0QnhHLEdBQTVCLENBQWdDLGdCQUFoQztBQUNBLFNBQUtnUSxTQUFMLENBQWV4SixTQUFmLENBQXlCeEcsR0FBekIsQ0FBNkIsZ0JBQTdCO0FBRUEsU0FBSzZQLFVBQUwsQ0FBZ0J6SyxnQkFBaEIsQ0FBaUMsT0FBakMsRUFBMEMsS0FBSzZLLFNBQUwsQ0FBZXBLLElBQWYsQ0FBb0IsSUFBcEIsQ0FBMUM7QUFFQSxTQUFLaUssWUFBTCxDQUFrQjFLLGdCQUFsQixDQUFtQyxPQUFuQyxFQUE0QyxLQUFLOEssYUFBTCxDQUFtQnJLLElBQW5CLENBQXdCLElBQXhCLENBQTVDO0FBQ0EsU0FBS2tLLFlBQUwsQ0FBa0IzSyxnQkFBbEIsQ0FBbUMsT0FBbkMsRUFBNEMsS0FBSzhLLGFBQUwsQ0FBbUJySyxJQUFuQixDQUF3QixJQUF4QixDQUE1QztBQUNBLFNBQUttSyxTQUFMLENBQWU1SyxnQkFBZixDQUFnQyxPQUFoQyxFQUF5QyxLQUFLOEssYUFBTCxDQUFtQnJLLElBQW5CLENBQXdCLElBQXhCLENBQXpDO0FBRUEsUUFBSWpMLE1BQU0sQ0FBQytLLFVBQVAsR0FBb0IsR0FBeEIsRUFBNkJpSixVQUFVLENBQUM7QUFBQTs7QUFBQSxhQUFNLEtBQUtrQixZQUFMLENBQWtCclgsVUFBbEIsQ0FBNkIrTixTQUE3QixDQUF1Q3hHLEdBQXZDLENBQTJDLFFBQTNDLENBQU47QUFBQSxLQUFELGFBQTZELEdBQTdELENBQVY7QUFFN0JwRixVQUFNLENBQUN3SyxnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxLQUFLK0ssaUJBQUwsQ0FBdUJ0SyxJQUF2QixDQUE0QixJQUE1QixDQUFsQztBQUVBLFNBQUtzSyxpQkFBTDs7QUFFQXZWLFVBQU0sQ0FBQ3dWLFFBQVAsR0FBa0IsVUFBU2pkLENBQVQsRUFBWTtBQUM1QixVQUFJLEtBQUtrZCxTQUFMLEdBQWlCLEtBQUtDLE9BQXRCLElBQWlDMVYsTUFBTSxDQUFDMlYsV0FBUCxLQUF1QixDQUE1RCxFQUErRDtBQUM3RGpMLFVBQUUsQ0FBQ2tCLFNBQUgsQ0FBYUssTUFBYixDQUFvQixRQUFwQjtBQUNELE9BRkQsTUFFTztBQUNMdkIsVUFBRSxDQUFDa0IsU0FBSCxDQUFheEcsR0FBYixDQUFpQixRQUFqQjtBQUNEOztBQUNELFVBQUksS0FBS3NRLE9BQUwsSUFBZ0IsR0FBcEIsRUFBeUI7QUFDdkJoTCxVQUFFLENBQUNrQixTQUFILENBQWF4RyxHQUFiLENBQWlCLE9BQWpCO0FBQ0QsT0FGRCxNQUVPO0FBQ0xzRixVQUFFLENBQUNrQixTQUFILENBQWFLLE1BQWIsQ0FBb0IsT0FBcEI7QUFDRDs7QUFDRCxXQUFLd0osU0FBTCxHQUFpQixLQUFLQyxPQUF0QjtBQUNELEtBWkQ7QUFjRDs7OztXQUNELG1CQUFVbmQsQ0FBVixFQUFhO0FBQ1hBLE9BQUMsQ0FBQ2tWLGNBQUY7QUFDQSxXQUFLdUgsR0FBTCxDQUFTcEosU0FBVCxDQUFtQmdLLE1BQW5CLENBQTBCLFNBQTFCO0FBQ0EsV0FBS1gsVUFBTCxDQUFnQnJKLFNBQWhCLENBQTBCZ0ssTUFBMUIsQ0FBaUMsU0FBakM7QUFDQTVaLGNBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsTUFBdkIsRUFBK0JpRyxTQUEvQixDQUF5Q2dLLE1BQXpDLENBQWdELFdBQWhEO0FBQ0Q7OztXQUNELHVCQUFjcmQsQ0FBZCxFQUFpQjtBQUNmQSxPQUFDLENBQUNrVixjQUFGO0FBQ0FsVixPQUFDLENBQUNvVCxhQUFGLENBQWdCOU4sVUFBaEIsQ0FBMkIrTixTQUEzQixDQUFxQ2dLLE1BQXJDLENBQTRDLFFBQTVDO0FBQ0Q7OztXQUNELDJCQUFrQnJkLENBQWxCLEVBQXFCO0FBQ25CLFVBQU1zZCxXQUFXLEdBQUc3WixRQUFRLENBQUMySixhQUFULENBQXVCLGVBQXZCLENBQXBCO0FBQ0FrUSxpQkFBVyxDQUFDM04sS0FBWixDQUFrQm5MLE1BQWxCLEdBQTJCLE1BQTNCLENBRm1CLENBR25CO0FBQ0Q7Ozs7OztBQUdILElBQU0rWSxVQUFVLEdBQUcsc0JBQU07QUFBQTs7QUFFdkIsTUFBSWYsTUFBSixDQUFXL1ksUUFBUSxDQUFDMkosYUFBVCxDQUF1QixTQUF2QixDQUFYO0FBRUQsQ0FKZSxnQkFBaEI7O0FBTWVtUSx5RUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7OztBQ2hFQSxJQUFNQyxVQUFVLEdBQUcsc0JBQU07QUFBQTs7QUFFdkJ2VyxTQUFPLENBQUNDLEdBQVIsQ0FBWSxZQUFZLDBCQUFaLEdBQXlDLDJCQUFyRCxFQUFrRix1RUFBbEYsRUFBMkosZ0NBQTNKLEVBQTZMLGlCQUE3TDtBQUVELENBSmUsZ0JBQWhCOztBQU1lc1cseUVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNOTUMsVTtBQUNKLHNCQUFZdEwsRUFBWixFQUFnQjtBQUFBOztBQUFBOztBQUVkLFNBQUtDLEtBQUwsR0FBYXRCLEtBQUssQ0FBQ3VCLElBQU4sQ0FBV0YsRUFBRSxDQUFDbEUsZ0JBQUgsQ0FBb0IsZ0JBQXBCLENBQVgsQ0FBYjtBQUNBLFNBQUt5UCxNQUFMLEdBQWNqYSxRQUFRLENBQUN3SyxnQkFBVCxDQUEwQixxQkFBMUIsQ0FBZDtBQUNBLFNBQUswUCxLQUFMLEdBQWFsYSxRQUFRLENBQUN3SyxnQkFBVCxDQUEwQixxQkFBMUIsQ0FBYjs7QUFFQSxRQUFJeEcsTUFBTSxDQUFDK0ssVUFBUCxHQUFvQixHQUF4QixFQUE2QjtBQUUzQixXQUFLSixLQUFMLENBQVdULE9BQVgsQ0FBbUIsVUFBQVEsRUFBRSxFQUFJO0FBQUE7O0FBQ3ZCQSxVQUFFLENBQUNGLGdCQUFILENBQW9CLE9BQXBCLEVBQTZCLEtBQUsyTCxTQUFMLENBQWVsTCxJQUFmLENBQW9CLElBQXBCLENBQTdCO0FBQ0QsT0FGRDtBQUlBLFdBQUtpTCxLQUFMLENBQVdoTSxPQUFYLENBQW1CLFVBQUFRLEVBQUUsRUFBSTtBQUFBOztBQUN2QkEsVUFBRSxDQUFDRixnQkFBSCxDQUFvQixPQUFwQixFQUE2QixLQUFLNEwsVUFBTCxDQUFnQm5MLElBQWhCLENBQXFCLElBQXJCLENBQTdCO0FBQ0QsT0FGRDtBQUlEO0FBRUY7Ozs7V0FDRCxtQkFBVTFTLENBQVYsRUFBYTtBQUNYQSxPQUFDLENBQUNrVixjQUFGO0FBRUEsVUFBTS9CLEtBQUssR0FBRyxLQUFLZixLQUFMLENBQVdyTyxPQUFYLENBQW1CL0QsQ0FBQyxDQUFDb1QsYUFBckIsQ0FBZDtBQUVBLFdBQUtzSyxNQUFMLENBQVl2SyxLQUFaLEVBQW1CRSxTQUFuQixDQUE2QnhHLEdBQTdCLENBQWlDLFNBQWpDO0FBQ0FwSixjQUFRLENBQUMySixhQUFULENBQXVCLE1BQXZCLEVBQStCaUcsU0FBL0IsQ0FBeUN4RyxHQUF6QyxDQUE2QyxXQUE3QztBQUVEOzs7V0FDRCxvQkFBVzdNLENBQVgsRUFBYztBQUFBOztBQUNaQSxPQUFDLENBQUNrVixjQUFGO0FBRUEsV0FBS3dJLE1BQUwsQ0FBWS9MLE9BQVosQ0FBb0IsVUFBQVEsRUFBRSxFQUFJO0FBQUE7O0FBQ3hCQSxVQUFFLENBQUNrQixTQUFILENBQWFLLE1BQWIsQ0FBb0IsU0FBcEI7QUFDRCxPQUZEO0FBSUFqUSxjQUFRLENBQUMySixhQUFULENBQXVCLE1BQXZCLEVBQStCaUcsU0FBL0IsQ0FBeUNLLE1BQXpDLENBQWdELFdBQWhEO0FBRUQ7Ozs7OztBQUdILElBQU1vSyxjQUFjLEdBQUcsMEJBQU07QUFBQTs7QUFFM0IsTUFBTUMsUUFBUSxHQUFHdGEsUUFBUSxDQUFDMkosYUFBVCxDQUF1QixpQkFBdkIsQ0FBakI7QUFFQSxNQUFJLENBQUMyUSxRQUFMLEVBQWU7QUFFZixNQUFJTixVQUFKLENBQWVNLFFBQWY7QUFFRCxDQVJtQixnQkFBcEI7O0FBVWVELDZFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbkRBLElBQUkxSCxRQUFRLEdBQUd6VyxtQkFBTyxDQUFDLG9FQUFELENBQXRCOztJQUVNcWUsVztBQUNKLHVCQUFZN0wsRUFBWixFQUFnQjtBQUFBOztBQUVkLFNBQUtBLEVBQUwsR0FBVUEsRUFBVjtBQUNBLFNBQUs4TCxTQUFMLEdBQWlCOUwsRUFBRSxDQUFDN00sVUFBSCxDQUFjOEgsYUFBZCxDQUE0QixPQUE1QixDQUFqQjtBQUNBLFNBQUs4USxTQUFMLEdBQWlCL0wsRUFBRSxDQUFDN00sVUFBSCxDQUFjOEgsYUFBZCxDQUE0QixXQUE1QixDQUFqQjtBQUNBLFNBQUsrUSxJQUFMLEdBQVloTSxFQUFFLENBQUM3TSxVQUFILENBQWMySSxnQkFBZCxDQUErQixtQkFBL0IsQ0FBWjtBQUVBLFNBQUtnUSxTQUFMLENBQWVoTSxnQkFBZixDQUFnQyxPQUFoQyxFQUF5QyxLQUFLbU0sU0FBTCxDQUFlMUwsSUFBZixDQUFvQixJQUFwQixDQUF6QztBQUNBLFNBQUt3TCxTQUFMLENBQWVqTSxnQkFBZixDQUFnQyxPQUFoQyxFQUF5QyxLQUFLb00sU0FBTCxDQUFlM0wsSUFBZixDQUFvQixJQUFwQixDQUF6QztBQUVBLFNBQUt5TCxJQUFMLENBQVUsQ0FBVixFQUFhOUssU0FBYixDQUF1QnhHLEdBQXZCLENBQTJCLFFBQTNCO0FBRUEsU0FBS3lSLFVBQUw7QUFFRDs7OztXQUNELHNCQUFhO0FBRVgsV0FBS0MsUUFBTCxHQUFnQixJQUFJbkksUUFBSixDQUFhLEtBQUtqRSxFQUFsQixFQUFzQjtBQUNwQ2tFLGVBQU8sRUFBRSxJQUQyQjtBQUVwQ0MsZ0JBQVEsRUFBRSxLQUYwQjtBQUdwQ0MsdUJBQWUsRUFBRSxLQUhtQjtBQUlwQ0MsaUJBQVMsRUFBRSxJQUp5QjtBQUtwQ2dJLFlBQUksRUFBRSxJQUw4QjtBQU1wQ0Msc0JBQWMsRUFBRTtBQU5vQixPQUF0QixDQUFoQjtBQVNBLFdBQUtGLFFBQUwsQ0FBYzdILEVBQWQsQ0FBa0IsUUFBbEIsRUFBNEIsS0FBS2dJLFlBQUwsQ0FBa0JoTSxJQUFsQixDQUF1QixJQUF2QixDQUE1QjtBQUVEOzs7V0FDRCxzQkFBYTFTLENBQWIsRUFBZ0I7QUFBQTs7QUFFZCxVQUFJLEtBQUt1ZSxRQUFMLENBQWNJLGFBQWQsS0FBZ0MsQ0FBcEMsRUFBdUM7QUFDckMsYUFBS1QsU0FBTCxDQUFlVSxRQUFmLEdBQTBCLElBQTFCO0FBQ0EsYUFBS1gsU0FBTCxDQUFlVyxRQUFmLEdBQTBCLEtBQTFCO0FBQ0QsT0FIRCxNQUdPLElBQUksS0FBS0wsUUFBTCxDQUFjSSxhQUFkLEtBQWdDLEtBQUtKLFFBQUwsQ0FBYzVJLE1BQWQsQ0FBcUIvVCxNQUFyQixHQUE4QixDQUFsRSxFQUFxRTtBQUMxRSxhQUFLcWMsU0FBTCxDQUFlVyxRQUFmLEdBQTBCLElBQTFCO0FBQ0EsYUFBS1YsU0FBTCxDQUFlVSxRQUFmLEdBQTBCLEtBQTFCO0FBQ0QsT0FITSxNQUdBO0FBQ0wsYUFBS1YsU0FBTCxDQUFlVSxRQUFmLEdBQTBCLEtBQTFCO0FBQ0EsYUFBS1gsU0FBTCxDQUFlVyxRQUFmLEdBQTBCLEtBQTFCO0FBQ0Q7O0FBRUQsV0FBS1QsSUFBTCxDQUFVeE0sT0FBVixDQUFrQixVQUFBUSxFQUFFLEVBQUk7QUFBQTs7QUFBRUEsVUFBRSxDQUFDa0IsU0FBSCxDQUFhSyxNQUFiLENBQW9CLFFBQXBCO0FBQStCLE9BQXpEO0FBQ0EsV0FBS3lLLElBQUwsQ0FBVSxLQUFLSSxRQUFMLENBQWNJLGFBQXhCLEVBQXVDdEwsU0FBdkMsQ0FBaUR4RyxHQUFqRCxDQUFxRCxRQUFyRDtBQUVEOzs7V0FDRCxtQkFBVTdNLENBQVYsRUFBYTtBQUVYQSxPQUFDLENBQUNrVixjQUFGO0FBQ0EsV0FBS3FKLFFBQUwsQ0FBY00sSUFBZDtBQUVEOzs7V0FDRCxtQkFBVTdlLENBQVYsRUFBYTtBQUVYQSxPQUFDLENBQUNrVixjQUFGO0FBQ0EsV0FBS3FKLFFBQUwsQ0FBY08sUUFBZDtBQUVEOzs7Ozs7QUFHSCxJQUFNQyxlQUFlLEdBQUcsMkJBQU07QUFBQTs7QUFFNUIsTUFBTUMsU0FBUyxHQUFHdmIsUUFBUSxDQUFDMkosYUFBVCxDQUF1QixzQkFBdkIsQ0FBbEI7QUFFQSxNQUFJNFIsU0FBSixFQUFlLElBQUloQixXQUFKLENBQWdCZ0IsU0FBaEI7QUFFaEIsQ0FOb0IsZ0JBQXJCOztBQVFlRCw4RUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3ZFQTtBQUNBO0FBQ0E7QUFFQXJYLDRDQUFJLENBQUNDLGNBQUwsQ0FBb0I4TixnRUFBcEI7O0FBRUEsSUFBTXdKLFFBQVEsR0FBRyxvQkFBTTtBQUFBOztBQUFBOztBQUVyQixNQUFNQyxRQUFRLEdBQUd6YixRQUFRLENBQUN3SyxnQkFBVCxDQUEwQixlQUExQixDQUFqQjtBQUVBLE1BQUksQ0FBQ2lSLFFBQUwsRUFBZTtBQUVmQSxVQUFRLENBQUN2TixPQUFULENBQWlCLFVBQUEwRixPQUFPLEVBQUk7QUFBQTs7QUFBQTs7QUFFMUI1QixvRUFBYSxDQUFDN0QsTUFBZCxDQUFxQjtBQUNuQmlGLGFBQU8sRUFBRVEsT0FEVTtBQUVuQkwsV0FBSyxFQUFHSyxPQUFPLENBQUMxRSxPQUFSLENBQWdCd00sTUFBaEIsS0FBMkIsT0FBNUIsR0FBdUMsU0FBdkMsR0FBbUQsU0FGdkM7QUFHbkJwUyxTQUFHLEVBQUUsZUFIYztBQUluQnFTLFVBQUksRUFBRSxJQUphO0FBS25CQyxpQkFBVyxFQUFFLFdBTE07QUFNbkI7QUFDQUMsYUFBTyxFQUFFLG1CQUFNO0FBQUE7O0FBQ2IsZ0JBQVFqSSxPQUFPLENBQUMxRSxPQUFSLENBQWdCd00sTUFBeEI7QUFDRSxlQUFLLE1BQUw7QUFDRW5MLG1FQUFBLENBQW9CcUQsT0FBcEI7QUFDRjs7QUFDQSxlQUFLLFFBQUw7QUFDRXJELHNFQUFBLENBQXVCcUQsT0FBdkI7QUFDRjs7QUFDQSxlQUFLLGNBQUw7QUFDRXJELDJFQUFBLENBQTRCcUQsT0FBNUI7QUFDRjtBQVRGO0FBV0QsT0FaTTtBQVBZLEtBQXJCO0FBc0JELEdBeEJEO0FBMEJBNVQsVUFBUSxDQUFDdVAsS0FBVCxDQUFlQyxLQUFmLENBQXFCQyxJQUFyQixDQUEwQixZQUFNO0FBQUE7O0FBQzlCdUMsb0VBQWEsQ0FBQzhKLE9BQWQ7QUFDRCxHQUZEO0FBSUQsQ0FwQ2EsZ0JBQWQ7O0FBc0NlTix1RUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDNUNBO0FBQ0E7QUFDQXZYLDRDQUFJLENBQUNDLGNBQUwsQ0FBb0I4TixnRUFBcEI7O0FBRUEsSUFBTStKLFVBQVUsR0FBRyxzQkFBTTtBQUFBOztBQUFBOztBQUV2QixNQUFNQyxhQUFhLEdBQUdoYyxRQUFRLENBQUN3SyxnQkFBVCxDQUEwQixjQUExQixDQUF0QjtBQUNBLE1BQU15UixJQUFJLEdBQUdqYyxRQUFRLENBQUMySixhQUFULENBQXVCLFlBQXZCLENBQWI7O0FBRUEsTUFBSXFTLGFBQUosRUFBbUI7QUFFakJBLGlCQUFhLENBQUM5TixPQUFkLENBQXNCLFVBQUFRLEVBQUUsRUFBSTtBQUFBOztBQUFBOztBQUUxQnNELHNFQUFhLENBQUM3RCxNQUFkLENBQXFCO0FBQ25CaUYsZUFBTyxFQUFFMUUsRUFEVTtBQUVuQjZFLGFBQUssRUFBRSxnQkFGWTtBQUduQjJJLGNBQU0sRUFBRSxlQUhXO0FBSW5CQyxnQkFBUSxFQUFFLHdCQUFxQztBQUFBLGNBQW5DM0ksUUFBbUMsUUFBbkNBLFFBQW1DO0FBQUEsY0FBekI0SSxTQUF5QixRQUF6QkEsU0FBeUI7QUFBQSxjQUFkQyxRQUFjLFFBQWRBLFFBQWM7O0FBQUE7O0FBQzdDLGNBQUlBLFFBQUosRUFBY0osSUFBSSxDQUFDck0sU0FBTCxDQUFleEcsR0FBZixDQUFtQixhQUFuQixFQUFkLEtBQ0s2UyxJQUFJLENBQUNyTSxTQUFMLENBQWVLLE1BQWYsQ0FBc0IsYUFBdEI7QUFDTixTQUhPO0FBSlcsT0FBckI7QUFVRCxLQVpEO0FBY0Q7QUFHRixDQXhCZSxnQkFBaEI7O0FBMEJlOEwseUVBQWYsRTs7Ozs7Ozs7Ozs7O0FDOUJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFZSxTQUFTTyxZQUFULEdBQXdCO0FBRXJDdFksUUFBTSxDQUFDd0ssZ0JBQVAsQ0FBd0IsTUFBeEIsRUFBZ0MsWUFBWTtBQUMxQytOLDJEQUFTLENBQUMsd0JBQUQsQ0FBVDtBQUNELEdBRkQsRUFFRyxLQUZIO0FBSUQsQzs7Ozs7Ozs7Ozs7O0FDUkQ7QUFBQTtBQUFBLFNBQVNDLFFBQVQsQ0FBa0J2ZCxDQUFsQixFQUFxQjtBQUNuQixNQUFJc0MsS0FBSyxHQUFHa2IsTUFBTSxDQUFDLFNBQVN4ZCxDQUFULEdBQWEsVUFBZCxDQUFOLENBQWdDeWQsSUFBaEMsQ0FBcUMxWSxNQUFNLENBQUM4TixRQUFQLENBQWdCNkssTUFBckQsQ0FBWjtBQUNBLFNBQU9wYixLQUFLLElBQUlxYixrQkFBa0IsQ0FBQ3JiLEtBQUssQ0FBQyxDQUFELENBQUwsQ0FBU2dDLE9BQVQsQ0FBaUIsS0FBakIsRUFBd0IsR0FBeEIsQ0FBRCxDQUFsQztBQUNEOztBQUVELFNBQVNzWixlQUFULENBQXlCdFMsS0FBekIsRUFBZ0M7QUFDOUIsTUFBSXVTLFlBQVksR0FBRyxLQUFLLEVBQUwsR0FBVSxFQUFWLEdBQWUsRUFBZixHQUFvQixJQUF2QyxDQUQ4QixDQUNlOztBQUM3QyxNQUFJQyxVQUFVLEdBQUcsSUFBSUMsSUFBSixHQUFXQyxPQUFYLEtBQXVCSCxZQUF4QztBQUNBLFNBQU87QUFDTHZTLFNBQUssRUFBRUEsS0FERjtBQUVMd1MsY0FBVSxFQUFFQTtBQUZQLEdBQVA7QUFJRDs7QUFFRCxTQUFTRyxTQUFULEdBQXFCO0FBQ25CLE1BQUlDLFVBQVUsR0FBR1gsUUFBUSxDQUFDLE9BQUQsQ0FBekI7QUFDQSxNQUFJWSxlQUFlLEdBQUcsQ0FBQyxhQUFELENBQXRCO0FBQ0EsTUFBSUMsV0FBVyxHQUFHLElBQWxCO0FBQ0EsTUFBSUMsa0JBQUo7QUFFQSxNQUFJQyxjQUFjLEdBQUdmLFFBQVEsQ0FBQyxZQUFELENBQTdCO0FBQ0EsTUFBSWdCLG1CQUFtQixHQUFHLENBQUMsWUFBRCxDQUExQjtBQUNBLE1BQUlDLGVBQWUsR0FBRyxJQUF0QjtBQUNBLE1BQUlDLHNCQUFKO0FBRUEsTUFBSUMsY0FBYyxHQUFHbkIsUUFBUSxDQUFDLFlBQUQsQ0FBN0I7QUFDQSxNQUFJb0IsbUJBQW1CLEdBQUcsQ0FBQyxZQUFELENBQTFCO0FBQ0EsTUFBSUMsZUFBZSxHQUFHLElBQXRCO0FBQ0EsTUFBSUMsc0JBQUo7QUFFQSxNQUFJQyxnQkFBZ0IsR0FBR3ZCLFFBQVEsQ0FBQyxjQUFELENBQS9CO0FBQ0EsTUFBSXdCLHFCQUFxQixHQUFHLENBQUMsY0FBRCxDQUE1QjtBQUNBLE1BQUlDLGlCQUFpQixHQUFHLElBQXhCO0FBQ0EsTUFBSUMsd0JBQUo7QUFFQSxNQUFJQyxZQUFZLEdBQUczQixRQUFRLENBQUMsVUFBRCxDQUEzQjtBQUNBLE1BQUk0QixpQkFBaUIsR0FBRyxDQUFDLFVBQUQsQ0FBeEI7QUFDQSxNQUFJQyxhQUFhLEdBQUcsSUFBcEI7QUFDQSxNQUFJQyxvQkFBSjtBQUVBLE1BQUlDLFdBQVcsR0FBRy9CLFFBQVEsQ0FBQyxRQUFELENBQTFCO0FBQ0EsTUFBSWdDLGFBQWEsR0FBRyxDQUFDRCxXQUFELElBQWdCQSxXQUFXLENBQUNqZSxPQUFaLENBQW9CLElBQXBCLE1BQThCLENBQUMsQ0FBbkU7QUFFQThjLGlCQUFlLENBQUNsUCxPQUFoQixDQUF3QixVQUFVeUcsS0FBVixFQUFpQjtBQUN2QyxRQUFJM1UsUUFBUSxDQUFDeVksY0FBVCxDQUF3QjlELEtBQXhCLENBQUosRUFBb0M7QUFDbEMySSx3QkFBa0IsR0FBR3RkLFFBQVEsQ0FBQ3lZLGNBQVQsQ0FBd0I5RCxLQUF4QixDQUFyQjtBQUNEO0FBQ0YsR0FKRDtBQUtBNkkscUJBQW1CLENBQUN0UCxPQUFwQixDQUE0QixVQUFVeUcsS0FBVixFQUFpQjtBQUMzQyxRQUFJM1UsUUFBUSxDQUFDeVksY0FBVCxDQUF3QjlELEtBQXhCLENBQUosRUFBb0M7QUFDbEMrSSw0QkFBc0IsR0FBRzFkLFFBQVEsQ0FBQ3lZLGNBQVQsQ0FBd0I5RCxLQUF4QixDQUF6QjtBQUNEO0FBQ0YsR0FKRDtBQUtBaUoscUJBQW1CLENBQUMxUCxPQUFwQixDQUE0QixVQUFVeUcsS0FBVixFQUFpQjtBQUMzQyxRQUFJM1UsUUFBUSxDQUFDeVksY0FBVCxDQUF3QjlELEtBQXhCLENBQUosRUFBb0M7QUFDbENtSiw0QkFBc0IsR0FBRzlkLFFBQVEsQ0FBQ3lZLGNBQVQsQ0FBd0I5RCxLQUF4QixDQUF6QjtBQUNEO0FBQ0YsR0FKRDtBQUtBcUosdUJBQXFCLENBQUM5UCxPQUF0QixDQUE4QixVQUFVeUcsS0FBVixFQUFpQjtBQUM3QyxRQUFJM1UsUUFBUSxDQUFDeVksY0FBVCxDQUF3QjlELEtBQXhCLENBQUosRUFBb0M7QUFDbEN1Siw4QkFBd0IsR0FBR2xlLFFBQVEsQ0FBQ3lZLGNBQVQsQ0FBd0I5RCxLQUF4QixDQUEzQjtBQUNEO0FBQ0YsR0FKRDtBQUtBeUosbUJBQWlCLENBQUNsUSxPQUFsQixDQUEwQixVQUFVeUcsS0FBVixFQUFpQjtBQUN6QyxRQUFJM1UsUUFBUSxDQUFDeVksY0FBVCxDQUF3QjlELEtBQXhCLENBQUosRUFBb0M7QUFDbEMySiwwQkFBb0IsR0FBR3RlLFFBQVEsQ0FBQ3lZLGNBQVQsQ0FBd0I5RCxLQUF4QixDQUF2QjtBQUNEO0FBQ0YsR0FKRDs7QUFNQSxNQUFJd0ksVUFBVSxJQUFJcUIsYUFBbEIsRUFBaUM7QUFDL0JuQixlQUFXLEdBQUdSLGVBQWUsQ0FBQ00sVUFBRCxDQUE3QjtBQUNBc0IsZ0JBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixFQUE4Qm5JLElBQUksQ0FBQ0MsU0FBTCxDQUFlNkcsV0FBZixDQUE5QjtBQUNEOztBQUNELE1BQUlFLGNBQUosRUFBb0I7QUFDbEJFLG1CQUFlLEdBQUdaLGVBQWUsQ0FBQ1UsY0FBRCxDQUFqQztBQUNBa0IsZ0JBQVksQ0FBQ0MsT0FBYixDQUFxQixZQUFyQixFQUFtQ25JLElBQUksQ0FBQ0MsU0FBTCxDQUFlaUgsZUFBZixDQUFuQztBQUNEOztBQUNELE1BQUlFLGNBQUosRUFBb0I7QUFDbEJFLG1CQUFlLEdBQUdoQixlQUFlLENBQUNjLGNBQUQsQ0FBakM7QUFDQWMsZ0JBQVksQ0FBQ0MsT0FBYixDQUFxQixZQUFyQixFQUFtQ25JLElBQUksQ0FBQ0MsU0FBTCxDQUFlcUgsZUFBZixDQUFuQztBQUNEOztBQUNELE1BQUlFLGdCQUFKLEVBQXNCO0FBQ3BCRSxxQkFBaUIsR0FBR3BCLGVBQWUsQ0FBQ2tCLGdCQUFELENBQW5DO0FBQ0FVLGdCQUFZLENBQUNDLE9BQWIsQ0FBcUIsY0FBckIsRUFBcUNuSSxJQUFJLENBQUNDLFNBQUwsQ0FBZXlILGlCQUFmLENBQXJDO0FBQ0Q7O0FBQ0QsTUFBSUUsWUFBSixFQUFrQjtBQUNoQkUsaUJBQWEsR0FBR3hCLGVBQWUsQ0FBQ3NCLFlBQUQsQ0FBL0I7QUFDQU0sZ0JBQVksQ0FBQ0MsT0FBYixDQUFxQixVQUFyQixFQUFpQ25JLElBQUksQ0FBQ0MsU0FBTCxDQUFlNkgsYUFBZixDQUFqQztBQUNEOztBQUVELE1BQUlNLEtBQUssR0FBR3RCLFdBQVcsSUFBSTlHLElBQUksQ0FBQ29CLEtBQUwsQ0FBVzhHLFlBQVksQ0FBQ0csT0FBYixDQUFxQixPQUFyQixDQUFYLENBQTNCO0FBQ0EsTUFBSUMsVUFBVSxHQUFHcEIsZUFBZSxJQUFJbEgsSUFBSSxDQUFDb0IsS0FBTCxDQUFXOEcsWUFBWSxDQUFDRyxPQUFiLENBQXFCLFlBQXJCLENBQVgsQ0FBcEM7QUFDQSxNQUFJRSxVQUFVLEdBQUdqQixlQUFlLElBQUl0SCxJQUFJLENBQUNvQixLQUFMLENBQVc4RyxZQUFZLENBQUNHLE9BQWIsQ0FBcUIsWUFBckIsQ0FBWCxDQUFwQztBQUNBLE1BQUlHLFlBQVksR0FBR2QsaUJBQWlCLElBQUkxSCxJQUFJLENBQUNvQixLQUFMLENBQVc4RyxZQUFZLENBQUNHLE9BQWIsQ0FBcUIsY0FBckIsQ0FBWCxDQUF4QztBQUNBLE1BQUlJLFFBQVEsR0FBR1gsYUFBYSxJQUFJOUgsSUFBSSxDQUFDb0IsS0FBTCxDQUFXOEcsWUFBWSxDQUFDRyxPQUFiLENBQXFCLFVBQXJCLENBQVgsQ0FBaEM7QUFDQSxNQUFJSyxZQUFZLEdBQUdOLEtBQUssSUFBSSxJQUFJM0IsSUFBSixHQUFXQyxPQUFYLEtBQXVCMEIsS0FBSyxDQUFDNUIsVUFBekQ7QUFDQSxNQUFJbUMsZ0JBQWdCLEdBQUdMLFVBQVUsSUFBSSxJQUFJN0IsSUFBSixHQUFXQyxPQUFYLEtBQXVCNEIsVUFBVSxDQUFDOUIsVUFBdkU7QUFDQSxNQUFJb0MsZ0JBQWdCLEdBQUdMLFVBQVUsSUFBSSxJQUFJOUIsSUFBSixHQUFXQyxPQUFYLEtBQXVCNkIsVUFBVSxDQUFDL0IsVUFBdkU7QUFDQSxNQUFJcUMsa0JBQWtCLEdBQUdMLFlBQVksSUFBSSxJQUFJL0IsSUFBSixHQUFXQyxPQUFYLEtBQXVCOEIsWUFBWSxDQUFDaEMsVUFBN0U7QUFDQSxNQUFJc0MsY0FBYyxHQUFHTCxRQUFRLElBQUksSUFBSWhDLElBQUosR0FBV0MsT0FBWCxLQUF1QitCLFFBQVEsQ0FBQ2pDLFVBQWpFOztBQUVBLE1BQUlPLGtCQUFrQixJQUFJMkIsWUFBMUIsRUFBd0M7QUFDdEMzQixzQkFBa0IsQ0FBQy9TLEtBQW5CLEdBQTJCb1UsS0FBSyxDQUFDcFUsS0FBakM7QUFDRDs7QUFDRCxNQUFJbVQsc0JBQXNCLElBQUl3QixnQkFBOUIsRUFBZ0Q7QUFDOUN4QiwwQkFBc0IsQ0FBQ25ULEtBQXZCLEdBQStCc1UsVUFBVSxDQUFDdFUsS0FBMUM7QUFDRDs7QUFDRCxNQUFJdVQsc0JBQXNCLElBQUlxQixnQkFBOUIsRUFBZ0Q7QUFDOUNyQiwwQkFBc0IsQ0FBQ3ZULEtBQXZCLEdBQStCdVUsVUFBVSxDQUFDdlUsS0FBMUM7QUFDRDs7QUFDRCxNQUFJMlQsd0JBQXdCLElBQUlrQixrQkFBaEMsRUFBb0Q7QUFDbERsQiw0QkFBd0IsQ0FBQzNULEtBQXpCLEdBQWlDd1UsWUFBWSxDQUFDeFUsS0FBOUM7QUFDRDs7QUFDRCxNQUFJK1Qsb0JBQW9CLElBQUllLGNBQTVCLEVBQTRDO0FBQzFDZix3QkFBb0IsQ0FBQy9ULEtBQXJCLEdBQTZCeVUsUUFBUSxDQUFDelUsS0FBdEM7QUFDRDtBQUNGOztBQUVEdkcsTUFBTSxDQUFDd0ssZ0JBQVAsQ0FBd0IsTUFBeEIsRUFBZ0MwTyxTQUFoQztBQUVlLFNBQVNvQyxZQUFULEdBQXdCLENBQUcsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hIMUM7QUFDQTtBQUVBcmIseUNBQUksQ0FBQ0MsY0FBTCxDQUFvQjhOLGdFQUFwQjs7QUFFQSxJQUFNdU4sUUFBUSxHQUFHLG9CQUFNO0FBQUE7O0FBRXJCLE1BQU1DLEtBQUssR0FBR3hmLFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIseUJBQXZCLENBQWQ7QUFFQSxNQUFJLENBQUM2VixLQUFMLEVBQVk7O0FBRVosTUFBSXhiLE1BQU0sQ0FBQytLLFVBQVAsR0FBb0IsR0FBeEIsRUFBNkI7QUFFM0JpRCxvRUFBYSxDQUFDN0QsTUFBZCxDQUFxQjtBQUNuQmlGLGFBQU8sRUFBRW9NLEtBRFU7QUFFbkJuTSxTQUFHLEVBQUUsSUFGYztBQUduQkMsbUJBQWEsRUFBRSxDQUhJO0FBSW5CbU0sZ0JBQVUsRUFBRSxLQUpPO0FBS25CbE0sV0FBSyxFQUFFLFVBTFk7QUFNbkJqSyxTQUFHLEVBQUUsc0JBTmM7QUFPbkJvVyxnQkFBVSxFQUFFLGNBUE8sQ0FRbkI7O0FBUm1CLEtBQXJCO0FBV0Q7QUFFRixDQXJCYSxnQkFBZDs7QUF1QmVILHVFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM1QkE7O0lBRU1JLE87QUFDSixtQkFBWWpSLEVBQVosRUFBZ0I7QUFBQTs7QUFFZCxTQUFLa1IsTUFBTCxHQUFjbFIsRUFBRSxDQUFDL0UsYUFBSCxDQUFpQixrQkFBakIsQ0FBZDtBQUNBLFNBQUtrVyxNQUFMLEdBQWNuUixFQUFFLENBQUNsRSxnQkFBSCxDQUFvQixrQkFBcEIsQ0FBZDtBQUNBLFNBQUtrUSxJQUFMLEdBQVloTSxFQUFFLENBQUNsRSxnQkFBSCxDQUFvQixlQUFwQixDQUFaO0FBQ0EsU0FBS3NOLE1BQUwsR0FBY3BKLEVBQUUsQ0FBQy9FLGFBQUgsQ0FBaUIseUJBQWpCLENBQWQ7QUFDQSxTQUFLbU8sTUFBTCxDQUFZdEosZ0JBQVosQ0FBNkIsUUFBN0IsRUFBdUMsS0FBS3NSLGNBQUwsQ0FBb0I3USxJQUFwQixDQUF5QixJQUF6QixDQUF2QztBQUNBLFNBQUt1TCxTQUFMLEdBQWlCOUwsRUFBRSxDQUFDN00sVUFBSCxDQUFjOEgsYUFBZCxDQUE0QixPQUE1QixDQUFqQjtBQUNBLFNBQUs4USxTQUFMLEdBQWlCL0wsRUFBRSxDQUFDN00sVUFBSCxDQUFjOEgsYUFBZCxDQUE0QixXQUE1QixDQUFqQjtBQUVBLFFBQUkrRSxFQUFFLENBQUNrQixTQUFILENBQWFDLFFBQWIsQ0FBc0IsaUJBQXRCLENBQUosRUFBOEMsS0FBS2dMLFVBQUw7QUFFL0M7Ozs7V0FDRCxzQkFBYTtBQUVYLFdBQUtDLFFBQUwsR0FBZ0IsSUFBSW5JLCtDQUFKLENBQWEsS0FBS2lOLE1BQWxCLEVBQTBCO0FBQ3hDaE4sZUFBTyxFQUFFLEtBRCtCO0FBRXhDQyxnQkFBUSxFQUFFLEtBRjhCO0FBR3hDQyx1QkFBZSxFQUFFLEtBSHVCO0FBSXhDQyxpQkFBUyxFQUFFLElBSjZCO0FBS3hDQyxpQkFBUyxFQUFFO0FBTDZCLE9BQTFCLENBQWhCO0FBUUEsV0FBSzBILElBQUwsQ0FBVSxDQUFWLEVBQWE5SyxTQUFiLENBQXVCeEcsR0FBdkIsQ0FBMkIsUUFBM0I7QUFDQSxXQUFLb1IsU0FBTCxDQUFlaE0sZ0JBQWYsQ0FBZ0MsT0FBaEMsRUFBeUMsS0FBS21NLFNBQUwsQ0FBZTFMLElBQWYsQ0FBb0IsSUFBcEIsQ0FBekM7QUFDQSxXQUFLd0wsU0FBTCxDQUFlak0sZ0JBQWYsQ0FBZ0MsT0FBaEMsRUFBeUMsS0FBS29NLFNBQUwsQ0FBZTNMLElBQWYsQ0FBb0IsSUFBcEIsQ0FBekM7QUFFQSxXQUFLNkwsUUFBTCxDQUFjN0gsRUFBZCxDQUFrQixRQUFsQixFQUE0QixLQUFLZ0ksWUFBTCxDQUFrQmhNLElBQWxCLENBQXVCLElBQXZCLENBQTVCO0FBRUQ7OztXQUNELHNCQUFhMVMsQ0FBYixFQUFnQjtBQUFBOztBQUVkLFVBQUksS0FBS3VlLFFBQUwsQ0FBY0ksYUFBZCxLQUFnQyxDQUFwQyxFQUF1QztBQUNyQyxhQUFLVCxTQUFMLENBQWVVLFFBQWYsR0FBMEIsSUFBMUI7QUFDRCxPQUZELE1BRU8sSUFBSSxLQUFLTCxRQUFMLENBQWNJLGFBQWQsS0FBZ0MsS0FBS0osUUFBTCxDQUFjNUksTUFBZCxDQUFxQi9ULE1BQXJCLEdBQThCLENBQWxFLEVBQXFFO0FBQzFFLGFBQUtxYyxTQUFMLENBQWVXLFFBQWYsR0FBMEIsSUFBMUI7QUFDRCxPQUZNLE1BRUE7QUFDTCxhQUFLVixTQUFMLENBQWVVLFFBQWYsR0FBMEIsS0FBMUI7QUFDQSxhQUFLWCxTQUFMLENBQWVXLFFBQWYsR0FBMEIsS0FBMUI7QUFDRDs7QUFFRCxXQUFLVCxJQUFMLENBQVV4TSxPQUFWLENBQWtCLFVBQUFRLEVBQUUsRUFBSTtBQUFBOztBQUFFQSxVQUFFLENBQUNrQixTQUFILENBQWFLLE1BQWIsQ0FBb0IsUUFBcEI7QUFBK0IsT0FBekQ7QUFDQSxXQUFLeUssSUFBTCxDQUFVLEtBQUtJLFFBQUwsQ0FBY0ksYUFBeEIsRUFBdUN0TCxTQUF2QyxDQUFpRHhHLEdBQWpELENBQXFELFFBQXJEO0FBRUQ7OztXQUNELHdCQUFlN00sQ0FBZixFQUFrQjtBQUFBOztBQUVoQixXQUFLc2pCLE1BQUwsQ0FBWTNSLE9BQVosQ0FBb0IsVUFBQVEsRUFBRSxFQUFJO0FBQUE7O0FBQ3hCQSxVQUFFLENBQUNqTixZQUFILENBQWdCLGVBQWhCLEVBQWlDbEYsQ0FBQyxDQUFDeVksTUFBRixDQUFTekssS0FBMUM7QUFDRCxPQUZEO0FBSUQ7OztXQUNELG1CQUFVaE8sQ0FBVixFQUFhO0FBRVhBLE9BQUMsQ0FBQ2tWLGNBQUY7QUFDQSxXQUFLcUosUUFBTCxDQUFjTSxJQUFkO0FBRUQ7OztXQUNELG1CQUFVN2UsQ0FBVixFQUFhO0FBRVhBLE9BQUMsQ0FBQ2tWLGNBQUY7QUFDQSxXQUFLcUosUUFBTCxDQUFjTyxRQUFkO0FBRUQ7Ozs7OztBQUdILElBQU0wRSxXQUFXLEdBQUcsdUJBQU07QUFBQTs7QUFFeEIsTUFBTW5NLE9BQU8sR0FBRzVULFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsVUFBdkIsQ0FBaEI7QUFFQSxNQUFJaUssT0FBSixFQUFhLElBQUkrTCxPQUFKLENBQVkvTCxPQUFaO0FBRWQsQ0FOZ0IsZ0JBQWpCOztBQVFlbU0sMEVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0VBO0FBQ0E7QUFFQSxJQUFJMUwsZ0JBQWdCLEdBQUcsS0FBdkI7QUFFQSxJQUFJMkwsT0FBTyxHQUFHLElBQUl6TCxvREFBSixDQUFZLHVCQUFaLEVBQXFDO0FBQ2pEQyxlQUFhLEVBQUUsSUFEa0M7QUFFakRDLG1CQUFpQixFQUFFO0FBQ2pCQyxrQkFBYyxFQUFFLHdCQUFDQyxLQUFELEVBQVc7QUFBQTs7QUFDekIsVUFBSUEsS0FBSyxDQUFDc0wsRUFBTixLQUFhLGVBQWpCLEVBQWtDO0FBRWhDO0FBQ0EsWUFBTUMsZUFBZSxHQUFHLENBQUMsT0FBRCxFQUFVLE9BQVYsRUFBbUIsS0FBbkIsRUFBMEIsU0FBMUIsRUFBcUMsU0FBckMsRUFBZ0QsS0FBaEQsRUFBdUQsTUFBdkQsRUFBK0QsWUFBL0QsRUFBNkUsUUFBN0UsQ0FBeEI7QUFDQSxZQUFNckwsY0FBYyxHQUFHRixLQUFLLENBQUNwSyxLQUFOLENBQVk3SixLQUFaLENBQWtCLEdBQWxCLENBQXZCO0FBQ0EsWUFBSWtVLE1BQU0sR0FBRyxFQUFiOztBQUVBLFlBQUlDLGNBQWMsQ0FBQyxDQUFELENBQWxCLEVBQXVCO0FBQ3JCRCxnQkFBTSxHQUFHQyxjQUFjLENBQUMsQ0FBRCxDQUFkLENBQWtCblUsS0FBbEIsQ0FBd0IsR0FBeEIsRUFBNkIsQ0FBN0IsQ0FBVDtBQUNEOztBQUVELFlBQUl3ZixlQUFlLENBQUM1ZixPQUFoQixDQUF3QnNVLE1BQXhCLEtBQW1DLENBQXZDLEVBQTBDO0FBQ3hDLGlCQUFPLElBQVA7QUFDRCxTQUZELE1BRU87QUFDTCxpQkFBTyxLQUFQO0FBQ0Q7QUFFRjtBQUNGLEtBbkJhO0FBREcsR0FGOEI7QUF3QmpERSxVQUFRLEVBQUU7QUFDVkosa0JBQWMsRUFBRTtBQUROO0FBeEJ1QyxDQUFyQyxDQUFkOztBQTZCQSxJQUFNeUwsZ0JBQWdCLEdBQUcsMEJBQUM1akIsQ0FBRCxFQUFPO0FBQUE7O0FBQUE7O0FBQzlCQSxHQUFDLENBQUNrVixjQUFGO0FBQ0EsTUFBTW9DLElBQUksR0FBR3RYLENBQUMsQ0FBQ3lZLE1BQWY7QUFDQSxNQUFNb0wsUUFBUSxHQUFHLGdKQUFqQjtBQUNBLE1BQU1qTCxPQUFPLEdBQUduVixRQUFRLENBQUMySixhQUFULENBQXVCLHVCQUF2QixDQUFoQjtBQUNBLE1BQU15TCxPQUFPLEdBQUdwVixRQUFRLENBQUMySixhQUFULENBQXVCLHFCQUF2QixDQUFoQjtBQUNBLE1BQU13TixRQUFRLEdBQUduWCxRQUFRLENBQUMySixhQUFULENBQXVCLHNCQUF2QixDQUFqQjtBQUNBLE1BQU15TixVQUFVLEdBQUdwWCxRQUFRLENBQUMySixhQUFULENBQXVCLHdCQUF2QixDQUFuQjtBQUNBLE1BQU0wVyxVQUFVLEdBQUdyZ0IsUUFBUSxDQUFDMkosYUFBVCxDQUF1QixjQUF2QixDQUFuQjtBQUVBM0osVUFBUSxDQUFDd08sZ0JBQVQsQ0FBMEIsb0JBQTFCLEVBQWdELFVBQVVnSCxLQUFWLEVBQWlCO0FBQy9EaFMsV0FBTyxDQUFDQyxHQUFSLENBQVkrUixLQUFLLENBQUNDLE1BQU4sQ0FBYUMsTUFBekI7QUFDRCxHQUZELEVBRUcsS0FGSDtBQUlBOztBQUNBMVYsVUFBUSxDQUFDd08sZ0JBQVQsQ0FBMEIsa0JBQTFCLEVBQThDLFVBQVVqUyxDQUFWLEVBQWE7QUFBQTs7QUFFekQsUUFBSThYLGdCQUFKLEVBQXNCLE9BQU8sS0FBUDtBQUV0QkEsb0JBQWdCLEdBQUcsSUFBbkI7QUFFQSxRQUFJaU0sU0FBUyxHQUFHdGdCLFFBQVEsQ0FBQ3lZLGNBQVQsQ0FBd0IsWUFBeEIsRUFBc0NsTyxLQUF0RDtBQUNBLFFBQUlnVyxRQUFRLEdBQUd2Z0IsUUFBUSxDQUFDeVksY0FBVCxDQUF3QixXQUF4QixFQUFxQ2xPLEtBQXBEO0FBQ0EsUUFBSWlXLFdBQVcsR0FBR3hnQixRQUFRLENBQUN5WSxjQUFULENBQXdCLFNBQXhCLEVBQW1DbE8sS0FBckQ7QUFDQSxRQUFJa1csT0FBTyxHQUFHemdCLFFBQVEsQ0FBQ3lZLGNBQVQsQ0FBd0IsTUFBeEIsRUFBZ0NsTyxLQUE5QztBQUNBLFFBQUltVyxhQUFhLEdBQUcxZ0IsUUFBUSxDQUFDeVksY0FBVCxDQUF3QixjQUF4QixFQUF3Q2xPLEtBQTVEO0FBQ0EsUUFBSW9XLFlBQVksR0FBRzNnQixRQUFRLENBQUN5WSxjQUFULENBQXdCLGVBQXhCLEVBQXlDbE8sS0FBNUQ7QUFDQSxRQUFJcVcsZ0JBQWdCLEdBQUc1Z0IsUUFBUSxDQUFDeVksY0FBVCxDQUF3QixZQUF4QixFQUFzQ29JLE9BQTdEO0FBQ0EsUUFBSWxDLEtBQUssR0FBRzNlLFFBQVEsQ0FBQ3lZLGNBQVQsQ0FBd0IsYUFBeEIsRUFBdUNsTyxLQUFuRDtBQUNBLFFBQUl1VyxNQUFNLEdBQUc5Z0IsUUFBUSxDQUFDeVksY0FBVCxDQUF3QixZQUF4QixFQUFzQ2xPLEtBQW5EO0FBQ0EsUUFBSXdXLE1BQU0sR0FBRy9nQixRQUFRLENBQUN5WSxjQUFULENBQXdCLFlBQXhCLEVBQXNDbE8sS0FBbkQ7QUFDQSxRQUFJeVcsUUFBUSxHQUFHaGhCLFFBQVEsQ0FBQ3lZLGNBQVQsQ0FBd0IsY0FBeEIsRUFBd0NsTyxLQUF2RDtBQUNBLFFBQUkwVyxJQUFJLEdBQUdqaEIsUUFBUSxDQUFDeVksY0FBVCxDQUF3QixVQUF4QixFQUFvQ2xPLEtBQS9DO0FBRUEsUUFBSTJXLFdBQVcsR0FBRztBQUFFNUosVUFBSSxFQUFFLFVBQVI7QUFBb0I2SixlQUFTLEVBQUViLFNBQS9CO0FBQTBDYyxjQUFRLEVBQUViLFFBQXBEO0FBQThEYyxpQkFBVyxFQUFFYixXQUEzRTtBQUF3RmMsYUFBTyxFQUFFYixPQUFqRztBQUEwR2MsbUJBQWEsRUFBRWIsYUFBekg7QUFBd0ljLGtCQUFZLEVBQUViLFlBQXRKO0FBQW9LYyxzQkFBZ0IsRUFBRWIsZ0JBQXRMO0FBQXdNYyxpQkFBVyxFQUFFVixRQUFyTjtBQUErTlcsZUFBUyxFQUFFYixNQUExTztBQUFrUGMsZUFBUyxFQUFFYixNQUE3UDtBQUFxUWMsYUFBTyxFQUFFWixJQUE5UTtBQUFvUnRDLFdBQUssRUFBRUE7QUFBM1IsS0FBbEI7QUFFQSxRQUFNdEksT0FBTyxHQUFHO0FBQ2RULFlBQU0sRUFBRSxNQURNO0FBRWRVLFVBQUksRUFBRUMsSUFBSSxDQUFDQyxTQUFMLENBQWUwSyxXQUFmLENBRlE7QUFHZHpLLFVBQUksRUFBRSxNQUhRO0FBSWRDLGFBQU8sRUFBRTtBQUNQLDRDQUFvQyxNQUQ3QjtBQUVQLHdCQUFnQixrQkFGVDtBQUdQLHdDQUFnQztBQUh6QjtBQUpLLEtBQWhCO0FBV0FDLFNBQUssQ0FBQ3lKLFFBQUQsRUFBVy9KLE9BQVgsQ0FBTCxDQUNHNUcsSUFESCxDQUNRLFVBQUFtSCxHQUFHO0FBQUE7O0FBQUEsYUFBSUEsR0FBRyxDQUFDa0wsSUFBSixFQUFKO0FBQUEsS0FEWCxhQUVHclMsSUFGSCxDQUVRLFVBQUFtSCxHQUFHLEVBQUk7QUFBQTs7QUFDWHBULGFBQU8sQ0FBQ0MsR0FBUixDQUFZbVQsR0FBWjtBQUNBcFQsYUFBTyxDQUFDQyxHQUFSLENBQVksYUFBYW1ULEdBQUcsQ0FBQ2MsTUFBN0I7QUFDQWxVLGFBQU8sQ0FBQ0MsR0FBUixDQUFZLFlBQVltVCxHQUFHLENBQUNXLEtBQTVCOztBQUNBLFVBQUlYLEdBQUcsQ0FBQ1csS0FBSixJQUFhLElBQWpCLEVBQXVCO0FBQ3JCdlgsZ0JBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsMkJBQXZCLEVBQW9ENEMsU0FBcEQsR0FBZ0VxSyxHQUFHLENBQUNXLEtBQXBFO0FBQ0ExRCxZQUFJLENBQUMzSCxLQUFMLENBQVdDLE9BQVgsR0FBcUIsTUFBckI7QUFDQWlMLGtCQUFVLENBQUNsTCxLQUFYLENBQWlCQyxPQUFqQixHQUEyQixPQUEzQjtBQUNELE9BSkQsTUFJTztBQUNMa1Usa0JBQVUsQ0FBQzlULFNBQVgsR0FBdUJxSyxHQUFHLENBQUNjLE1BQTNCO0FBQ0E3RCxZQUFJLENBQUMzSCxLQUFMLENBQVdDLE9BQVgsR0FBcUIsTUFBckI7QUFDQWdKLGVBQU8sQ0FBQ2pKLEtBQVIsQ0FBY0MsT0FBZCxHQUF3QixPQUF4QjtBQUNEO0FBQ0YsS0FmSCxzQkFnQlMsVUFBQTBLLEdBQUcsRUFBSTtBQUFBOztBQUNaclQsYUFBTyxDQUFDQyxHQUFSLENBQVlvVCxHQUFaO0FBQ0FyVCxhQUFPLENBQUNDLEdBQVIsK0JBQW1Db1QsR0FBbkM7QUFDQWhELFVBQUksQ0FBQzNILEtBQUwsQ0FBV0MsT0FBWCxHQUFxQixNQUFyQjtBQUNBaUosYUFBTyxDQUFDbEosS0FBUixDQUFjQyxPQUFkLEdBQXdCLE9BQXhCO0FBQ0QsS0FyQkg7QUF1QkEwSCxRQUFJLENBQUNqRSxTQUFMLENBQWV4RyxHQUFmLENBQW1CLFdBQW5CO0FBRUQsR0F6REQsRUF5REcsS0F6REgsRUFmOEIsQ0EwRTlCOztBQUNBaVgsWUFBVSxDQUFDN1IsZ0JBQVgsQ0FBNEIsT0FBNUIsRUFBcUMsWUFBTTtBQUFBOztBQUFBOztBQUV6QyxRQUFNb0osTUFBTSxHQUFHeUksVUFBVSxDQUFDOVQsU0FBMUI7QUFDQSxRQUFNd1YsU0FBUyxHQUFHL2hCLFFBQVEsQ0FBQ3dNLGFBQVQsQ0FBdUIsT0FBdkIsQ0FBbEI7QUFDQXVWLGFBQVMsQ0FBQ3hYLEtBQVYsR0FBa0JxTixNQUFsQjtBQUNBNVgsWUFBUSxDQUFDMkosYUFBVCxDQUF1QixNQUF2QixFQUErQnlDLFdBQS9CLENBQTJDMlYsU0FBM0M7QUFDQUEsYUFBUyxDQUFDakssTUFBVjtBQUNBOVgsWUFBUSxDQUFDK1gsV0FBVCxDQUFxQixNQUFyQjtBQUNBZ0ssYUFBUyxDQUFDOVIsTUFBVjtBQUVBalEsWUFBUSxDQUFDMkosYUFBVCxDQUF1QixVQUF2QixFQUFtQzRDLFNBQW5DLEdBQStDLFNBQS9DO0FBRUF5TCxjQUFVLENBQUMsWUFBTTtBQUFBOztBQUNmaFksY0FBUSxDQUFDMkosYUFBVCxDQUF1QixVQUF2QixFQUFtQzRDLFNBQW5DLEdBQStDLGVBQS9DO0FBQ0QsS0FGUyxhQUVQLElBRk8sQ0FBVjtBQUdELEdBZkQ7QUFpQkQsQ0E1RnFCLGdCQUF0Qjs7QUE4RkEsSUFBTXlWLG9CQUFvQixHQUFHLGdDQUFNO0FBQUE7O0FBQUE7O0FBRWpDLE1BQU1qTCxLQUFLLEdBQUcvVyxRQUFRLENBQUN3SyxnQkFBVCxDQUEwQixzQkFBMUIsQ0FBZDtBQUVBdU0sT0FBSyxDQUFDN0ksT0FBTixDQUFjLFVBQUFRLEVBQUUsRUFBSTtBQUFBOztBQUNsQkEsTUFBRSxDQUFDRixnQkFBSCxDQUFvQixRQUFwQixFQUE4QjJSLGdCQUE5QixFQUFnRCxLQUFoRDtBQUNELEdBRkQ7QUFJRCxDQVJ5QixnQkFBMUI7O0FBVWU2QixtRkFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzFJQTtBQUNBO0FBQ0E7QUFFQS9kLHlDQUFJLENBQUNDLGNBQUwsQ0FBb0IrZCxrRUFBcEIsRUFBb0NqUSxnRUFBcEM7O0FBRUEsSUFBTWtRLGNBQWMsR0FBRywwQkFBTTtBQUFBOztBQUFBOztBQUUzQixNQUFNQyxPQUFPLEdBQUduaUIsUUFBUSxDQUFDd0ssZ0JBQVQsQ0FBMEIsbUJBQTFCLENBQWhCO0FBQ0EsTUFBTTRYLGFBQWEsR0FBR3BpQixRQUFRLENBQUN3SyxnQkFBVCxDQUEwQixrQkFBMUIsQ0FBdEI7QUFDQSxNQUFNNlgsV0FBVyxHQUFHcmlCLFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsV0FBdkIsQ0FBcEI7QUFFQSxNQUFNa00sR0FBRyxHQUFHN1IsTUFBTSxDQUFDOE4sUUFBUCxDQUFnQjZLLE1BQTVCO0FBQ0EsTUFBTTJGLFNBQVMsR0FBRyxJQUFJQyxlQUFKLENBQW9CMU0sR0FBcEIsQ0FBbEI7O0FBRUEsTUFBSXlNLFNBQVMsQ0FBQ0UsR0FBVixDQUFjLE1BQWQsQ0FBSixFQUEyQjtBQUFFeEssY0FBVSxDQUFDLFlBQU07QUFBQTs7QUFDNUMvVCwrQ0FBSSxDQUFDNkwsRUFBTCxDQUFROUwsTUFBUixFQUFnQjtBQUFFeWUsZ0JBQVEsRUFBRTtBQUFFampCLFdBQUMsRUFBRVEsUUFBUSxDQUFDMkosYUFBVCxDQUF1QixPQUF2QixDQUFMO0FBQXNDcUcsY0FBSSxFQUFFO0FBQTVDO0FBQVosT0FBaEI7QUFDRCxLQUZzQyxhQUVwQyxJQUZvQyxDQUFWO0FBRW5COztBQUVWLE1BQUlxUyxXQUFKLEVBQWlCO0FBQ2ZBLGVBQVcsQ0FBQzdULGdCQUFaLENBQTZCLE9BQTdCLEVBQXNDLFVBQUNqUyxDQUFELEVBQU87QUFBQTs7QUFDM0NBLE9BQUMsQ0FBQ2tWLGNBQUY7QUFDQSxVQUFNbUMsT0FBTyxHQUFHNVQsUUFBUSxDQUFDMkosYUFBVCxDQUF1QixNQUFNcE4sQ0FBQyxDQUFDb1QsYUFBRixDQUFnQlQsT0FBaEIsQ0FBd0JZLEVBQXJELENBQWhCO0FBQ0E3TCwrQ0FBSSxDQUFDNkwsRUFBTCxDQUFROUwsTUFBUixFQUFnQjtBQUFFeWUsZ0JBQVEsRUFBRTtBQUFFampCLFdBQUMsRUFBRW9VLE9BQUw7QUFBYzVELGNBQUksRUFBRTtBQUFwQjtBQUFaLE9BQWhCO0FBQ0QsS0FKRDtBQUtEOztBQUVELE1BQUksQ0FBQ21TLE9BQUQsSUFBWW5lLE1BQU0sQ0FBQytLLFVBQVAsR0FBb0IsR0FBcEMsRUFBeUM7QUFFekNvVCxTQUFPLENBQUNqVSxPQUFSLENBQWdCLFVBQUFRLEVBQUUsRUFBSTtBQUFBOztBQUVwQnNELG9FQUFhLENBQUM3RCxNQUFkLENBQXFCO0FBQ25CaUYsYUFBTyxFQUFFMUUsRUFEVTtBQUVuQjJFLFNBQUcsRUFBRSxJQUZjO0FBR25CQyxtQkFBYSxFQUFFLENBSEk7QUFJbkJtTSxnQkFBVSxFQUFFLEtBSk87QUFLbkJsTSxXQUFLLEVBQUUsV0FMWTtBQU1uQmpLLFNBQUcsRUFBRSxzQkFOYztBQU9uQm9XLGdCQUFVLEVBQUUsY0FQTyxDQVFuQjs7QUFSbUIsS0FBckI7QUFXRCxHQWJEO0FBZUEwQyxlQUFhLENBQUNsVSxPQUFkLENBQXNCLFVBQUFRLEVBQUUsRUFBSTtBQUFBOztBQUFBOztBQUMxQkEsTUFBRSxDQUFDRixnQkFBSCxDQUFvQixPQUFwQixFQUE2QixVQUFDalMsQ0FBRCxFQUFPO0FBQUE7O0FBQ2xDQSxPQUFDLENBQUNrVixjQUFGO0FBQ0EsVUFBTW1DLE9BQU8sR0FBRzVULFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsTUFBTXBOLENBQUMsQ0FBQ29ULGFBQUYsQ0FBZ0JULE9BQWhCLENBQXdCWSxFQUFyRCxDQUFoQjtBQUNBN0wsK0NBQUksQ0FBQzZMLEVBQUwsQ0FBUTlMLE1BQVIsRUFBZ0I7QUFBRXllLGdCQUFRLEVBQUU7QUFBRWpqQixXQUFDLEVBQUVvVSxPQUFMO0FBQWM1RCxjQUFJLEVBQUU7QUFBcEI7QUFBWixPQUFoQjtBQUNELEtBSkQ7QUFLRCxHQU5EO0FBUUQsQ0E5Q21CLGdCQUFwQjs7QUFnRGVrUyw2RUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3REQTtBQUNBO0FBQ0E7QUFFQWplLHlDQUFJLENBQUNDLGNBQUwsQ0FBb0I4TixnRUFBcEIsRUFBbUNpUSxrRUFBbkM7O0lBRU1TLFU7QUFDSixzQkFBWWhVLEVBQVosRUFBZ0I7QUFBQTs7QUFBQTs7QUFFZCxTQUFLQSxFQUFMLEdBQVVBLEVBQVY7QUFDQSxTQUFLaVUsSUFBTCxHQUFZalUsRUFBRSxDQUFDL0UsYUFBSCxDQUFpQixtQkFBakIsQ0FBWjtBQUNBLFNBQUtpWixLQUFMLEdBQWFsVSxFQUFFLENBQUNsRSxnQkFBSCxDQUFvQixrQkFBcEIsQ0FBYjtBQUNBLFNBQUtpUixRQUFMLEdBQWdCemIsUUFBUSxDQUFDd0ssZ0JBQVQsQ0FBMEIsZ0JBQTFCLENBQWhCO0FBRUEsU0FBS29ZLEtBQUwsQ0FBVzFVLE9BQVgsQ0FBbUIsVUFBQVEsRUFBRSxFQUFJO0FBQUE7O0FBQ3ZCQSxRQUFFLENBQUNGLGdCQUFILENBQW9CLE9BQXBCLEVBQTZCLEtBQUtxVSxlQUFMLENBQXFCNVQsSUFBckIsQ0FBMEIsSUFBMUIsQ0FBN0I7QUFDRCxLQUZEO0FBSUEsU0FBSzZULFFBQUw7QUFDQSxTQUFLQyxZQUFMO0FBRUEvZSxVQUFNLENBQUN3SyxnQkFBUCxDQUF3QixPQUF4QixFQUFpQyxLQUFLd1UsY0FBTCxDQUFvQi9ULElBQXBCLENBQXlCLElBQXpCLENBQWpDO0FBRUQ7Ozs7V0FDRCxvQkFBVyxDQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVEOzs7V0FDRCx3QkFBZTtBQUFBOztBQUViLFdBQUt3TSxRQUFMLENBQWN2TixPQUFkLENBQXNCLFVBQUFRLEVBQUUsRUFBSTtBQUFBOztBQUFBOztBQUUxQnNELHdFQUFhLENBQUM3RCxNQUFkLENBQXFCO0FBQ25CaUYsaUJBQU8sRUFBRTFFLEVBRFU7QUFFbkI2RSxlQUFLLEVBQUUsU0FGWTtBQUduQmpLLGFBQUcsRUFBRSxlQUhjO0FBSW5CO0FBQ0F1UyxpQkFBTyxFQUFFLG1CQUFNO0FBQUE7O0FBQUE7O0FBQ2IsaUJBQUsrRyxLQUFMLENBQVcxVSxPQUFYLENBQW1CLFVBQUFRLEVBQUUsRUFBSTtBQUFBOztBQUFFQSxnQkFBRSxDQUFDa0IsU0FBSCxDQUFhSyxNQUFiLENBQW9CLFFBQXBCO0FBQStCLGFBQTFEO0FBQ0FqUSxvQkFBUSxDQUFDMkosYUFBVCxDQUF1QixzQkFBc0IrRSxFQUFFLENBQUNRLE9BQUgsQ0FBVzBFLE9BQWpDLEdBQTJDLElBQWxFLEVBQXdFaEUsU0FBeEUsQ0FBa0Z4RyxHQUFsRixDQUFzRixRQUF0RjtBQUNELFdBSE0sV0FMWTtBQVNuQjZaLHFCQUFXLEVBQUUsdUJBQU07QUFBQTs7QUFBQTs7QUFDakIsaUJBQUtMLEtBQUwsQ0FBVzFVLE9BQVgsQ0FBbUIsVUFBQVEsRUFBRSxFQUFJO0FBQUE7O0FBQUVBLGdCQUFFLENBQUNrQixTQUFILENBQWFLLE1BQWIsQ0FBb0IsUUFBcEI7QUFBK0IsYUFBMUQ7QUFDQWpRLG9CQUFRLENBQUMySixhQUFULENBQXVCLHNCQUFzQitFLEVBQUUsQ0FBQ1EsT0FBSCxDQUFXMEUsT0FBakMsR0FBMkMsSUFBbEUsRUFBd0VoRSxTQUF4RSxDQUFrRnhHLEdBQWxGLENBQXNGLFFBQXRGO0FBQ0QsV0FIVTtBQVRRLFNBQXJCO0FBZUQsT0FqQkQ7QUFtQkQ7OztXQUNELHlCQUFnQjdNLENBQWhCLEVBQW1CO0FBRWpCLFVBQU1xWCxPQUFPLEdBQUc1VCxRQUFRLENBQUMySixhQUFULENBQXVCLG9CQUFvQnBOLENBQUMsQ0FBQ29ULGFBQUYsQ0FBZ0JULE9BQWhCLENBQXdCZ1UsU0FBNUMsR0FBd0QsSUFBL0UsQ0FBaEI7QUFFQWpmLCtDQUFJLENBQUM2TCxFQUFMLENBQVE5TCxNQUFSLEVBQWdCO0FBQUV5ZSxnQkFBUSxFQUFFO0FBQUVqakIsV0FBQyxFQUFFb1UsT0FBTDtBQUFjdVAsaUJBQU8sRUFBRSxDQUFDLEVBQXhCO0FBQTRCblQsY0FBSSxFQUFFO0FBQWxDO0FBQVosT0FBaEI7QUFFRDs7O1dBQ0Qsd0JBQWV6VCxDQUFmLEVBQWtCLENBQ2hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDRDs7Ozs7O0FBR0gsSUFBTTZtQixjQUFjLEdBQUcsMEJBQU07QUFBQTs7QUFFM0IsTUFBTUMsVUFBVSxHQUFHcmpCLFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsbUJBQXZCLENBQW5CO0FBRUEsTUFBSSxDQUFDMFosVUFBTCxFQUFpQjtBQUVqQixNQUFJWCxVQUFKLENBQWVXLFVBQWY7QUFFRCxDQVJtQixnQkFBcEI7O0FBVWVELDZFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNyRkE7QUFDQTtBQUVBbmYsNENBQUksQ0FBQ0MsY0FBTCxDQUFvQitkLGtFQUFwQjs7QUFFQSxJQUFNWSxlQUFlLEdBQUcsMkJBQU07QUFBQTs7QUFBQTs7QUFFNUIsTUFBTXpQLE9BQU8sR0FBR3BULFFBQVEsQ0FBQ3dLLGdCQUFULENBQTBCLFdBQTFCLENBQWhCO0FBRUE0SSxTQUFPLENBQUNsRixPQUFSLENBQWdCLFVBQUFRLEVBQUUsRUFBSTtBQUFBOztBQUFBOztBQUNwQkEsTUFBRSxDQUFDRixnQkFBSCxDQUFvQixPQUFwQixFQUE2QixVQUFDalMsQ0FBRCxFQUFPO0FBQUE7O0FBQ2xDQSxPQUFDLENBQUNrVixjQUFGO0FBRUF4TixrREFBSSxDQUFDNkwsRUFBTCxDQUFROUwsTUFBUixFQUFnQjtBQUFFK0wsZ0JBQVEsRUFBRSxDQUFaO0FBQWUwUyxnQkFBUSxFQUFFLE1BQU0vVCxFQUFFLENBQUNRLE9BQUgsQ0FBV1k7QUFBMUMsT0FBaEI7QUFFRCxLQUxEO0FBTUQsR0FQRDtBQVNELENBYm9CLGdCQUFyQjs7QUFlZStTLDhFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3BCQTs7SUFFTVMsVztBQUNKLHVCQUFZNVUsRUFBWixFQUFnQjtBQUFBOztBQUFBOztBQUVkLFNBQUs2VSxXQUFMLEdBQW1CLENBQW5CO0FBQ0EsU0FBS0MsSUFBTCxHQUFZOVUsRUFBRSxDQUFDbEUsZ0JBQUgsQ0FBb0IsbUJBQXBCLENBQVo7QUFDQSxTQUFLMEgsTUFBTCxHQUFjeEQsRUFBRSxDQUFDbEUsZ0JBQUgsQ0FBb0IscUJBQXBCLENBQWQ7QUFDQSxTQUFLaVosV0FBTCxHQUFtQi9VLEVBQUUsQ0FBQy9FLGFBQUgsQ0FBaUIsc0JBQWpCLENBQW5CO0FBQ0EsU0FBS2tGLE1BQUwsR0FBY3hCLEtBQUssQ0FBQ3VCLElBQU4sQ0FBV0YsRUFBRSxDQUFDbEUsZ0JBQUgsQ0FBb0IscUJBQXBCLENBQVgsQ0FBZDtBQUVBLFNBQUtnWixJQUFMLENBQVUsQ0FBVixFQUFhNVQsU0FBYixDQUF1QnhHLEdBQXZCLENBQTJCLFFBQTNCO0FBQ0EsU0FBSzhJLE1BQUwsQ0FBWSxDQUFaLEVBQWV0QyxTQUFmLENBQXlCeEcsR0FBekIsQ0FBNkIsUUFBN0I7QUFFQSxTQUFLb2EsSUFBTCxDQUFVdFYsT0FBVixDQUFrQixVQUFBUSxFQUFFLEVBQUk7QUFBQTs7QUFDdEJBLFFBQUUsQ0FBQ0YsZ0JBQUgsQ0FBb0IsT0FBcEIsRUFBNkIsS0FBS2tWLFNBQUwsQ0FBZXpVLElBQWYsQ0FBb0IsSUFBcEIsQ0FBN0I7QUFDRCxLQUZELGFBWGMsQ0FlZDs7QUFFQSxRQUFJakwsTUFBTSxDQUFDK0ssVUFBUCxHQUFvQixHQUF4QixFQUE2QjtBQUMzQixXQUFLNFUsV0FBTDtBQUNEO0FBRUY7Ozs7V0FDRCx1QkFBYztBQUFBOztBQUVaLFdBQUs5VSxNQUFMLENBQVlYLE9BQVosQ0FBb0IsVUFBQ1EsRUFBRCxFQUFLOVAsQ0FBTCxFQUFXO0FBQUE7O0FBQzdCLGFBQUtzVCxNQUFMLENBQVl0VCxDQUFaLEVBQWVzTixLQUFmLENBQXFCbkwsTUFBckIsR0FBOEIyTixFQUFFLENBQUMxQyxZQUFILEdBQWtCLElBQWhEO0FBQ0EwQyxVQUFFLENBQUNGLGdCQUFILENBQW9CLE9BQXBCLEVBQTZCLEtBQUtvVixTQUFMLENBQWUzVSxJQUFmLENBQW9CLElBQXBCLENBQTdCO0FBQ0QsT0FIRDtBQUtEOzs7V0FDRCxtQkFBVTFTLENBQVYsRUFBYTtBQUFBOztBQUNYQSxPQUFDLENBQUNrVixjQUFGO0FBRUEsV0FBSytSLElBQUwsQ0FBVXRWLE9BQVYsQ0FBa0IsVUFBQVEsRUFBRSxFQUFJO0FBQUE7O0FBQUVBLFVBQUUsQ0FBQ2tCLFNBQUgsQ0FBYUssTUFBYixDQUFvQixRQUFwQjtBQUErQixPQUF6RDtBQUNBLFdBQUtpQyxNQUFMLENBQVloRSxPQUFaLENBQW9CLFVBQUFRLEVBQUUsRUFBSTtBQUFBOztBQUFFQSxVQUFFLENBQUNrQixTQUFILENBQWFLLE1BQWIsQ0FBb0IsUUFBcEI7QUFBK0IsT0FBM0Q7QUFFQTFULE9BQUMsQ0FBQ29ULGFBQUYsQ0FBZ0JDLFNBQWhCLENBQTBCeEcsR0FBMUIsQ0FBOEIsUUFBOUI7QUFFQSxXQUFLb2EsSUFBTCxDQUFVdFYsT0FBVixDQUFrQixVQUFDUSxFQUFELEVBQUs5UCxDQUFMLEVBQVc7QUFBQTs7QUFDM0IsWUFBSThQLEVBQUUsS0FBS25TLENBQUMsQ0FBQ29ULGFBQWIsRUFBNEI7QUFDMUIsZUFBS3VDLE1BQUwsQ0FBWXRULENBQVosRUFBZWdSLFNBQWYsQ0FBeUJ4RyxHQUF6QixDQUE2QixRQUE3QjtBQUNBLGVBQUttYSxXQUFMLEdBQW1CLEtBQUtyUixNQUFMLENBQVl0VCxDQUFaLEVBQWVvTixZQUFsQztBQUNEO0FBQ0YsT0FMRDtBQU9BL0gsa0RBQUksQ0FBQzZMLEVBQUwsQ0FBUSxLQUFLMlQsV0FBYixFQUEwQjtBQUFFMVQsZ0JBQVEsRUFBRSxHQUFaO0FBQWlCaFAsY0FBTSxFQUFFLEtBQUt3aUIsV0FBTCxHQUFtQjtBQUE1QyxPQUExQjtBQUVEOzs7V0FDRCxtQkFBVWhuQixDQUFWLEVBQWE7QUFFWCxVQUFNbVQsS0FBSyxHQUFHLEtBQUtiLE1BQUwsQ0FBWXZPLE9BQVosQ0FBb0IvRCxDQUFDLENBQUNvVCxhQUF0QixDQUFkOztBQUVBLFVBQUlwVCxDQUFDLENBQUNvVCxhQUFGLENBQWdCQyxTQUFoQixDQUEwQkMsUUFBMUIsQ0FBbUMsTUFBbkMsQ0FBSixFQUFnRDtBQUU5QzVMLG9EQUFJLENBQUM2TCxFQUFMLENBQVEsS0FBS29DLE1BQUwsQ0FBWXhDLEtBQVosQ0FBUixFQUE0QjtBQUFFSyxrQkFBUSxFQUFFLEdBQVo7QUFBaUJoUCxnQkFBTSxFQUFFeEUsQ0FBQyxDQUFDb1QsYUFBRixDQUFnQjNELFlBQWhCLEdBQStCLElBQXhEO0FBQThEZ0UsY0FBSSxFQUFFO0FBQXBFLFNBQTVCO0FBQ0F6VCxTQUFDLENBQUNvVCxhQUFGLENBQWdCQyxTQUFoQixDQUEwQkssTUFBMUIsQ0FBaUMsTUFBakM7QUFFRCxPQUxELE1BS087QUFFTGhNLG9EQUFJLENBQUM2TCxFQUFMLENBQVEsS0FBS29DLE1BQUwsQ0FBWXhDLEtBQVosQ0FBUixFQUE0QjtBQUFFSyxrQkFBUSxFQUFFLEdBQVo7QUFBaUJoUCxnQkFBTSxFQUFFLE1BQXpCO0FBQWlDaVAsY0FBSSxFQUFFO0FBQXZDLFNBQTVCO0FBQ0F6VCxTQUFDLENBQUNvVCxhQUFGLENBQWdCQyxTQUFoQixDQUEwQnhHLEdBQTFCLENBQThCLE1BQTlCO0FBRUQ7QUFDRixLLENBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBR0YsSUFBTXlhLGVBQWUsR0FBRywyQkFBTTtBQUFBOztBQUU1QixNQUFNalEsT0FBTyxHQUFHNVQsUUFBUSxDQUFDMkosYUFBVCxDQUF1QixjQUF2QixDQUFoQjtBQUVBLE1BQUlpSyxPQUFKLEVBQWEsSUFBSTBQLFdBQUosQ0FBZ0IxUCxPQUFoQjtBQUVkLENBTm9CLGdCQUFyQjs7QUFRZWlRLDhFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdEZBO0FBQ0E7QUFDQTtBQUVBNWYsNENBQUksQ0FBQ0MsY0FBTCxDQUFvQjhOLGdFQUFwQjs7QUFFQSxJQUFNOFIsU0FBUyxHQUFHLHFCQUFNO0FBQUE7O0FBQUE7O0FBRXRCLE1BQU1BLFNBQVMsR0FBRzlqQixRQUFRLENBQUMySixhQUFULENBQXVCLCtCQUF2QixDQUFsQjtBQUVBLE1BQUksQ0FBQ21hLFNBQUwsRUFBZ0I7QUFFaEIsTUFBTUMsS0FBSyxHQUFHLElBQUlDLGtEQUFKLENBQVlGLFNBQVosRUFBdUJBLFNBQVMsQ0FBQzVVLE9BQVYsQ0FBa0I2VSxLQUF6QyxDQUFkO0FBRUEvUixrRUFBYSxDQUFDN0QsTUFBZCxDQUFxQjtBQUNuQmlGLFdBQU8sRUFBRSwwQkFEVTtBQUVuQkcsU0FBSyxFQUFFLFNBRlk7QUFHbkJqSyxPQUFHLEVBQUUsU0FIYztBQUluQnNTLGVBQVcsRUFBRSxXQUpNO0FBS25CRCxRQUFJLEVBQUUsSUFMYTtBQU1uQjtBQUNBRSxXQUFPLEVBQUUsbUJBQU07QUFBQTs7QUFBRWtJLFdBQUssQ0FBQ3hRLEtBQU47QUFBZSxLQUF6QjtBQVBZLEdBQXJCO0FBVUQsQ0FsQmMsZ0JBQWY7O0FBb0JldVEsd0VBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzFCQTtBQUNBO0FBRUE3Zix5Q0FBSSxDQUFDQyxjQUFMLENBQW9COE4sZ0VBQXBCOztBQUVBLElBQU1pUyxVQUFVLEdBQUcsc0JBQU07QUFBQTs7QUFFdkIsTUFBTWhJLElBQUksR0FBR2pjLFFBQVEsQ0FBQzJKLGFBQVQsQ0FBdUIsWUFBdkIsQ0FBYjtBQUVBLE1BQUksQ0FBQ3NTLElBQUwsRUFBVztBQUVYakssa0VBQWEsQ0FBQzdELE1BQWQsQ0FBcUI7QUFDbkJpRixXQUFPLEVBQUU2SSxJQURVO0FBRW5CNUksT0FBRyxFQUFFLElBRmM7QUFHbkJDLGlCQUFhLEVBQUUsQ0FISTtBQUluQm1NLGNBQVUsRUFBRSxLQUpPO0FBS25CbE0sU0FBSyxFQUFFLFNBTFk7QUFNbkJqSyxPQUFHLEVBQUUsZUFOYztBQU9uQm9XLGNBQVUsRUFBRSxTQVBPLENBUW5COztBQVJtQixHQUFyQjtBQVdELENBakJlLGdCQUFoQjs7QUFtQmV1RSx5RUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4QkE7QUFFZSxTQUFTQyx1QkFBVCxHQUFtQztBQUFBOztBQUVoRCxNQUFNM0ksU0FBUyxHQUFHdmIsUUFBUSxDQUFDd0ssZ0JBQVQsQ0FBMEIscUJBQTFCLENBQWxCO0FBRUEsTUFBSSxDQUFDK1EsU0FBTCxFQUFnQjtBQUVoQkEsV0FBUyxDQUFDck4sT0FBVixDQUFrQixVQUFBUSxFQUFFLEVBQUk7QUFBQTs7QUFFdEIsUUFBTW9NLFFBQVEsR0FBRyxJQUFJbkksK0NBQUosQ0FBYWpFLEVBQWIsRUFBaUI7QUFDaENrRSxhQUFPLEVBQUUsSUFEdUI7QUFFaENDLGNBQVEsRUFBRSxLQUZzQjtBQUdoQ0MscUJBQWUsRUFBRSxJQUhlO0FBSWhDcVIsa0JBQVksRUFBRSxJQUprQjtBQUtoQ3BSLGVBQVMsRUFBRSxJQUxxQjtBQU1oQ0MsZUFBUyxFQUFFO0FBTnFCLEtBQWpCLENBQWpCO0FBU0QsR0FYRDtBQWFELEM7Ozs7Ozs7Ozs7O0FDckJELHVDIiwiZmlsZSI6Im1haW4uYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gSW1wb3J0IFNjc3MgRE8gTk8gREVMRVRFXG5pbXBvcnQgJy4uL3Njc3MvbWFpbi5zY3NzJztcblxuLy8gSW1wb3J0IGV4dGVybmFsIGRlcGVuZGVuY2llc1xuaW1wb3J0IFwiYmFiZWwtcG9seWZpbGxcIjtcbmltcG9ydCAnY3VzdG9tLWV2ZW50LXBvbHlmaWxsL3BvbHlmaWxsLmpzJztcblxuLy8gQnVuZGxlIG1vZHVsZSBmaWxlcyBvbiBidWlsZFxuLy8gaHR0cHM6Ly9tZWRpdW0uY29tL0Bzdmlua2xlL2dldHRpbmctc3RhcnRlZC13aXRoLXdlYnBhY2stYW5kLWVzNi1tb2R1bGVzLWM0NjVkMDUzZDk4OFxuLy8gSXNzdWUgd2l0aCBiZWxvdyBpcyBvcmRlciBvZiBsb2FkIGlzIHNldCBieSBhbHBoYWJldGljYWwgbmFtaW5nIG9mIGZpbGVzLi4uP1xuY29uc3QgcmVxdWlyZWRNb2R1bGVzID0gcmVxdWlyZS5jb250ZXh0KFwiLi9tb2R1bGVzL1wiLCB0cnVlLCAvXFwuKGpzKSQvaSk7XG5yZXF1aXJlZE1vZHVsZXMua2V5cygpLm1hcChrZXkgPT4ge1xuICByZXF1aXJlZE1vZHVsZXMoa2V5KS5kZWZhdWx0KCk7XG59KTsiLCIvKiFcbiAqIE1vcnBoU1ZHUGx1Z2luIDMuMy40XG4gKiBodHRwczovL2dyZWVuc29jay5jb21cbiAqIFxuICogQGxpY2Vuc2UgQ29weXJpZ2h0IDIwMjAsIEdyZWVuU29jay4gQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqIFN1YmplY3QgdG8gdGhlIHRlcm1zIGF0IGh0dHBzOi8vZ3JlZW5zb2NrLmNvbS9zdGFuZGFyZC1saWNlbnNlIG9yIGZvciBDbHViIEdyZWVuU29jayBtZW1iZXJzLCB0aGUgYWdyZWVtZW50IGlzc3VlZCB3aXRoIHRoYXQgbWVtYmVyc2hpcC5cbiAqIEBhdXRob3I6IEphY2sgRG95bGUsIGphY2tAZ3JlZW5zb2NrLmNvbVxuICovXG5cbiFmdW5jdGlvbih0LGUpe1wib2JqZWN0XCI9PXR5cGVvZiBleHBvcnRzJiZcInVuZGVmaW5lZFwiIT10eXBlb2YgbW9kdWxlP2UoZXhwb3J0cyk6XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShbXCJleHBvcnRzXCJdLGUpOmUoKHQ9dHx8c2VsZikud2luZG93PXQud2luZG93fHx7fSl9KHRoaXMsZnVuY3Rpb24odCl7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gbSh0KXtyZXR1cm5cInN0cmluZ1wiPT10eXBlb2YgdH12YXIgTT0vW2FjaGxtcXN0dnpdfCgtP1xcZCpcXC4/XFxkKig/OmVbXFwtK10/XFxkKyk/KVswLTldL2dpLEE9Lyg/OigtKT9cXGQqXFwuP1xcZCooPzplW1xcLStdP1xcZCspPylbMC05XS9naSxiPS9bXFwrXFwtXT9cXGQqXFwuP1xcZCtlW1xcK1xcLV0/XFxkKy9naSxuPS8oXlsjXFwuXVthLXpdfFthLXldW2Etel0pL2ksRD1NYXRoLlBJLzE4MCxFPU1hdGguc2luLGs9TWF0aC5jb3MsWj1NYXRoLmFicywkPU1hdGguc3FydCxoPWZ1bmN0aW9uIF9pc051bWJlcih0KXtyZXR1cm5cIm51bWJlclwiPT10eXBlb2YgdH0scz1mdW5jdGlvbiBfcm91bmQodCl7cmV0dXJuIE1hdGgucm91bmQoMWU1KnQpLzFlNXx8MH07ZnVuY3Rpb24gcmV2ZXJzZVNlZ21lbnQodCl7dmFyIGUscj0wO2Zvcih0LnJldmVyc2UoKTtyPHQubGVuZ3RoO3IrPTIpZT10W3JdLHRbcl09dFtyKzFdLHRbcisxXT1lO3QucmV2ZXJzZWQ9IXQucmV2ZXJzZWR9dmFyIHo9e3JlY3Q6XCJyeCxyeSx4LHksd2lkdGgsaGVpZ2h0XCIsY2lyY2xlOlwicixjeCxjeVwiLGVsbGlwc2U6XCJyeCxyeSxjeCxjeVwiLGxpbmU6XCJ4MSx4Mix5MSx5MlwifTtmdW5jdGlvbiBjb252ZXJ0VG9QYXRoKHQsZSl7dmFyIHIsbixvLGksYSxoLHMsbCxnLGMscCx1LGYsZCxfLFAsbSx2LHksdyx4LFQsTT10LnRhZ05hbWUudG9Mb3dlckNhc2UoKSxiPS41NTIyODQ3NDk4MzE7cmV0dXJuXCJwYXRoXCIhPT1NJiZ0LmdldEJCb3g/KGg9ZnVuY3Rpb24gX2NyZWF0ZVBhdGgodCxlKXt2YXIgcixuPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnROUyhcImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCIsXCJwYXRoXCIpLG89W10uc2xpY2UuY2FsbCh0LmF0dHJpYnV0ZXMpLGk9by5sZW5ndGg7Zm9yKGU9XCIsXCIrZStcIixcIjstMTwtLWk7KXI9b1tpXS5ub2RlTmFtZS50b0xvd2VyQ2FzZSgpLGUuaW5kZXhPZihcIixcIityK1wiLFwiKTwwJiZuLnNldEF0dHJpYnV0ZU5TKG51bGwscixvW2ldLm5vZGVWYWx1ZSk7cmV0dXJuIG59KHQsXCJ4LHksd2lkdGgsaGVpZ2h0LGN4LGN5LHJ4LHJ5LHIseDEseDIseTEseTIscG9pbnRzXCIpLFQ9ZnVuY3Rpb24gX2F0dHJUb09iaih0LGUpe2Zvcih2YXIgcj1lP2Uuc3BsaXQoXCIsXCIpOltdLG49e30sbz1yLmxlbmd0aDstMTwtLW87KW5bcltvXV09K3QuZ2V0QXR0cmlidXRlKHJbb10pfHwwO3JldHVybiBufSh0LHpbTV0pLFwicmVjdFwiPT09TT8oaT1ULnJ4LGE9VC5yeXx8aSxuPVQueCxvPVQueSxjPVQud2lkdGgtMippLHA9VC5oZWlnaHQtMiphLHI9aXx8YT9cIk1cIisoUD0oZD0oZj1uK2kpK2MpK2kpK1wiLFwiKyh2PW8rYSkrXCIgVlwiKyh5PXYrcCkrXCIgQ1wiK1tQLHc9eSthKmIsXz1kK2kqYix4PXkrYSxkLHgsZC0oZC1mKS8zLHgsZisoZC1mKS8zLHgsZix4LHU9bitpKigxLWIpLHgsbix3LG4seSxuLHktKHktdikvMyxuLHYrKHktdikvMyxuLHYsbixtPW8rYSooMS1iKSx1LG8sZixvLGYrKGQtZikvMyxvLGQtKGQtZikvMyxvLGQsbyxfLG8sUCxtLFAsdl0uam9pbihcIixcIikrXCJ6XCI6XCJNXCIrKG4rYykrXCIsXCIrbytcIiB2XCIrcCtcIiBoXCIrLWMrXCIgdlwiKy1wK1wiIGhcIitjK1wielwiKTpcImNpcmNsZVwiPT09TXx8XCJlbGxpcHNlXCI9PT1NPyhsPVwiY2lyY2xlXCI9PT1NPyhpPWE9VC5yKSpiOihpPVQucngsKGE9VC5yeSkqYikscj1cIk1cIisoKG49VC5jeCkraSkrXCIsXCIrKG89VC5jeSkrXCIgQ1wiK1tuK2ksbytsLG4rKHM9aSpiKSxvK2EsbixvK2Esbi1zLG8rYSxuLWksbytsLG4taSxvLG4taSxvLWwsbi1zLG8tYSxuLG8tYSxuK3Msby1hLG4raSxvLWwsbitpLG9dLmpvaW4oXCIsXCIpK1wielwiKTpcImxpbmVcIj09PU0/cj1cIk1cIitULngxK1wiLFwiK1QueTErXCIgTFwiK1QueDIrXCIsXCIrVC55MjpcInBvbHlsaW5lXCIhPT1NJiZcInBvbHlnb25cIiE9PU18fChyPVwiTVwiKyhuPShnPSh0LmdldEF0dHJpYnV0ZShcInBvaW50c1wiKStcIlwiKS5tYXRjaChBKXx8W10pLnNoaWZ0KCkpK1wiLFwiKyhvPWcuc2hpZnQoKSkrXCIgTFwiK2cuam9pbihcIixcIiksXCJwb2x5Z29uXCI9PT1NJiYocis9XCIsXCIrbitcIixcIitvK1wielwiKSksaC5zZXRBdHRyaWJ1dGUoXCJkXCIscmF3UGF0aFRvU3RyaW5nKGguX2dzUmF3UGF0aD1zdHJpbmdUb1Jhd1BhdGgocikpKSxlJiZ0LnBhcmVudE5vZGUmJih0LnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKGgsdCksdC5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHQpKSxoKTp0fWZ1bmN0aW9uIGFyY1RvU2VnbWVudCh0LGUscixuLG8saSxhLGgscyl7aWYodCE9PWh8fGUhPT1zKXtyPVoociksbj1aKG4pO3ZhciBsPW8lMzYwKkQsZz1rKGwpLGM9RShsKSxwPU1hdGguUEksdT0yKnAsZj0odC1oKS8yLGQ9KGUtcykvMixfPWcqZitjKmQsUD0tYypmK2cqZCxtPV8qXyx2PVAqUCx5PW0vKHIqcikrdi8obipuKTsxPHkmJihyPSQoeSkqcixuPSQoeSkqbik7dmFyIHc9cipyLHg9bipuLFQ9KHcqeC13KnYteCptKS8odyp2K3gqbSk7VDwwJiYoVD0wKTt2YXIgTT0oaT09PWE/LTE6MSkqJChUKSxiPXIqUC9uKk0sUz0tbipfL3IqTSxOPWcqYi1jKlMrKHQraCkvMixSPWMqYitnKlMrKGUrcykvMixBPShfLWIpL3Isej0oUC1TKS9uLE89KC1fLWIpL3IsTD0oLVAtUykvbixZPUEqQSt6KnosQz0oejwwPy0xOjEpKk1hdGguYWNvcyhBLyQoWSkpLEY9KEEqTC16Kk88MD8tMToxKSpNYXRoLmFjb3MoKEEqTyt6KkwpLyQoWSooTypPK0wqTCkpKTtpc05hTihGKSYmKEY9cCksIWEmJjA8Rj9GLT11OmEmJkY8MCYmKEYrPXUpLEMlPXUsRiU9dTt2YXIgWCxJPU1hdGguY2VpbChaKEYpLyh1LzQpKSxWPVtdLGo9Ri9JLEc9NC8zKkUoai8yKS8oMStrKGovMikpLFU9ZypyLHE9YypyLEg9YyotbixCPWcqbjtmb3IoWD0wO1g8STtYKyspXz1rKG89QytYKmopLFA9RShvKSxBPWsobys9aiksej1FKG8pLFYucHVzaChfLUcqUCxQK0cqXyxBK0cqeix6LUcqQSxBLHopO2ZvcihYPTA7WDxWLmxlbmd0aDtYKz0yKV89VltYXSxQPVZbWCsxXSxWW1hdPV8qVStQKkgrTixWW1grMV09XypxK1AqQitSO3JldHVybiBWW1gtMl09aCxWW1gtMV09cyxWfX1mdW5jdGlvbiBzdHJpbmdUb1Jhd1BhdGgodCl7ZnVuY3Rpb24gdGModCxlLHIsbil7Zz0oci10KS8zLGM9KG4tZSkvMyxoLnB1c2godCtnLGUrYyxyLWcsbi1jLHIsbil9dmFyIGUscixuLG8saSxhLGgscyxsLGcsYyxwLHUsZixkLF89KHQrXCJcIikucmVwbGFjZShiLGZ1bmN0aW9uKHQpe3ZhciBlPSt0O3JldHVybiBlPDFlLTQmJi0xZS00PGU/MDplfSkubWF0Y2goTSl8fFtdLFA9W10sbT0wLHY9MCx5PV8ubGVuZ3RoLHc9MCx4PVwiRVJST1I6IG1hbGZvcm1lZCBwYXRoOiBcIit0O2lmKCF0fHwhaXNOYU4oX1swXSl8fGlzTmFOKF9bMV0pKXJldHVybiBjb25zb2xlLmxvZyh4KSxQO2ZvcihlPTA7ZTx5O2UrKylpZih1PWksaXNOYU4oX1tlXSk/YT0oaT1fW2VdLnRvVXBwZXJDYXNlKCkpIT09X1tlXTplLS0sbj0rX1tlKzFdLG89K19bZSsyXSxhJiYobis9bSxvKz12KSxlfHwocz1uLGw9byksXCJNXCI9PT1pKWgmJihoLmxlbmd0aDw4Py0tUC5sZW5ndGg6dys9aC5sZW5ndGgpLG09cz1uLHY9bD1vLGg9W24sb10sUC5wdXNoKGgpLGUrPTIsaT1cIkxcIjtlbHNlIGlmKFwiQ1wiPT09aSlhfHwobT12PTApLChoPWh8fFswLDBdKS5wdXNoKG4sbyxtKzEqX1tlKzNdLHYrMSpfW2UrNF0sbSs9MSpfW2UrNV0sdis9MSpfW2UrNl0pLGUrPTY7ZWxzZSBpZihcIlNcIj09PWkpZz1tLGM9dixcIkNcIiE9PXUmJlwiU1wiIT09dXx8KGcrPW0taFtoLmxlbmd0aC00XSxjKz12LWhbaC5sZW5ndGgtM10pLGF8fChtPXY9MCksaC5wdXNoKGcsYyxuLG8sbSs9MSpfW2UrM10sdis9MSpfW2UrNF0pLGUrPTQ7ZWxzZSBpZihcIlFcIj09PWkpZz1tKzIvMyoobi1tKSxjPXYrMi8zKihvLXYpLGF8fChtPXY9MCksbSs9MSpfW2UrM10sdis9MSpfW2UrNF0saC5wdXNoKGcsYyxtKzIvMyoobi1tKSx2KzIvMyooby12KSxtLHYpLGUrPTQ7ZWxzZSBpZihcIlRcIj09PWkpZz1tLWhbaC5sZW5ndGgtNF0sYz12LWhbaC5sZW5ndGgtM10saC5wdXNoKG0rZyx2K2MsbisyLzMqKG0rMS41KmctbiksbysyLzMqKHYrMS41KmMtbyksbT1uLHY9byksZSs9MjtlbHNlIGlmKFwiSFwiPT09aSl0YyhtLHYsbT1uLHYpLGUrPTE7ZWxzZSBpZihcIlZcIj09PWkpdGMobSx2LG0sdj1uKyhhP3YtbTowKSksZSs9MTtlbHNlIGlmKFwiTFwiPT09aXx8XCJaXCI9PT1pKVwiWlwiPT09aSYmKG49cyxvPWwsaC5jbG9zZWQ9ITApLChcIkxcIj09PWl8fC41PFoobS1uKXx8LjU8Wih2LW8pKSYmKHRjKG0sdixuLG8pLFwiTFwiPT09aSYmKGUrPTIpKSxtPW4sdj1vO2Vsc2UgaWYoXCJBXCI9PT1pKXtpZihmPV9bZSs0XSxkPV9bZSs1XSxnPV9bZSs2XSxjPV9bZSs3XSxyPTcsMTxmLmxlbmd0aCYmKGYubGVuZ3RoPDM/KGM9ZyxnPWQsci0tKTooYz1kLGc9Zi5zdWJzdHIoMiksci09MiksZD1mLmNoYXJBdCgxKSxmPWYuY2hhckF0KDApKSxwPWFyY1RvU2VnbWVudChtLHYsK19bZSsxXSwrX1tlKzJdLCtfW2UrM10sK2YsK2QsKGE/bTowKSsxKmcsKGE/djowKSsxKmMpLGUrPXIscClmb3Iocj0wO3I8cC5sZW5ndGg7cisrKWgucHVzaChwW3JdKTttPWhbaC5sZW5ndGgtMl0sdj1oW2gubGVuZ3RoLTFdfWVsc2UgY29uc29sZS5sb2coeCk7cmV0dXJuKGU9aC5sZW5ndGgpPDY/KFAucG9wKCksZT0wKTpoWzBdPT09aFtlLTJdJiZoWzFdPT09aFtlLTFdJiYoaC5jbG9zZWQ9ITApLFAudG90YWxQb2ludHM9dytlLFB9ZnVuY3Rpb24gcmF3UGF0aFRvU3RyaW5nKHQpe2godFswXSkmJih0PVt0XSk7dmFyIGUscixuLG8saT1cIlwiLGE9dC5sZW5ndGg7Zm9yKHI9MDtyPGE7cisrKXtmb3Iobz10W3JdLGkrPVwiTVwiK3Mob1swXSkrXCIsXCIrcyhvWzFdKStcIiBDXCIsZT1vLmxlbmd0aCxuPTI7bjxlO24rKylpKz1zKG9bbisrXSkrXCIsXCIrcyhvW24rK10pK1wiIFwiK3Mob1tuKytdKStcIixcIitzKG9bbisrXSkrXCIgXCIrcyhvW24rK10pK1wiLFwiK3Mob1tuXSkrXCIgXCI7by5jbG9zZWQmJihpKz1cInpcIil9cmV0dXJuIGl9ZnVuY3Rpb24geSgpe3JldHVybiByfHxcInVuZGVmaW5lZFwiIT10eXBlb2Ygd2luZG93JiYocj13aW5kb3cuZ3NhcCkmJnIucmVnaXN0ZXJQbHVnaW4mJnJ9ZnVuY3Rpb24gTCh0KXtyZXR1cm4gY29uc29sZSYmY29uc29sZS53YXJuKHQpfWZ1bmN0aW9uIE4odCl7dmFyIGUscj10Lmxlbmd0aCxuPTAsbz0wO2ZvcihlPTA7ZTxyO2UrKyluKz10W2UrK10sbys9dFtlXTtyZXR1cm5bbi8oci8yKSxvLyhyLzIpXX1mdW5jdGlvbiBPKHQpe3ZhciBlLHIsbixvPXQubGVuZ3RoLGk9dFswXSxhPWksaD10WzFdLHM9aDtmb3Iobj02O248bztuKz02KWk8KGU9dFtuXSk/aT1lOmU8YSYmKGE9ZSksaDwocj10W24rMV0pP2g9cjpyPHMmJihzPXIpO3JldHVybiB0LmNlbnRlclg9KGkrYSkvMix0LmNlbnRlclk9KGgrcykvMix0LnNpemU9KGktYSkqKGgtcyl9ZnVuY3Rpb24gUCh0LGUpe3ZvaWQgMD09PWUmJihlPTMpO2Zvcih2YXIgcixuLG8saSxhLGgscyxsLGcsYyxwLHUsZixkLF8sUCxtPXQubGVuZ3RoLHY9dFswXVswXSx5PXYsdz10WzBdWzFdLHg9dyxUPTEvZTstMTwtLW07KWZvcihyPShhPXRbbV0pLmxlbmd0aCxpPTY7aTxyO2krPTYpZm9yKGc9YVtpXSxjPWFbaSsxXSxwPWFbaSsyXS1nLGQ9YVtpKzNdLWMsdT1hW2krNF0tZyxfPWFbaSs1XS1jLGY9YVtpKzZdLWcsUD1hW2krN10tYyxoPWU7LTE8LS1oOyl2PChuPSgocz1UKmgpKnMqZiszKihsPTEtcykqKHMqdStsKnApKSpzK2cpP3Y9bjpuPHkmJih5PW4pLHc8KG89KHMqcypQKzMqbCoocypfK2wqZCkpKnMrYyk/dz1vOm88eCYmKHg9byk7cmV0dXJuIHQuY2VudGVyWD0odit5KS8yLHQuY2VudGVyWT0odyt4KS8yLHQubGVmdD15LHQud2lkdGg9di15LHQudG9wPXgsdC5oZWlnaHQ9dy14LHQuc2l6ZT0odi15KSoody14KX1mdW5jdGlvbiBRKHQsZSl7cmV0dXJuIGUubGVuZ3RoLXQubGVuZ3RofWZ1bmN0aW9uIFIodCxlKXt2YXIgcj10LnNpemV8fE8odCksbj1lLnNpemV8fE8oZSk7cmV0dXJuIE1hdGguYWJzKG4tcik8KHIrbikvMjA/ZS5jZW50ZXJYLXQuY2VudGVyWHx8ZS5jZW50ZXJZLXQuY2VudGVyWTpuLXJ9ZnVuY3Rpb24gUyh0LGUpe3ZhciByLG4sbz10LnNsaWNlKDApLGk9dC5sZW5ndGgsYT1pLTI7Zm9yKGV8PTAscj0wO3I8aTtyKyspbj0ocitlKSVhLHRbcisrXT1vW25dLHRbcl09b1sxK25dfWZ1bmN0aW9uIFQodCxlLHIsbixvKXt2YXIgaSxhLGgscyxsPXQubGVuZ3RoLGc9MCxjPWwtMjtmb3Iocio9NixhPTA7YTxsO2ErPTYpcz10W2k9KGErciklY10tKGVbYV0tbiksaD10WzEraV0tKGVbYSsxXS1vKSxnKz12KGgqaCtzKnMpO3JldHVybiBnfWZ1bmN0aW9uIFUodCxlLHIpe3ZhciBuLG8saSxhPXQubGVuZ3RoLGg9Tih0KSxzPU4oZSksbD1zWzBdLWhbMF0sZz1zWzFdLWhbMV0sYz1UKHQsZSwwLGwsZykscD0wO2ZvcihpPTY7aTxhO2krPTYpKG89VCh0LGUsaS82LGwsZykpPGMmJihjPW8scD1pKTtpZihyKWZvcihyZXZlcnNlU2VnbWVudChuPXQuc2xpY2UoMCkpLGk9NjtpPGE7aSs9Nikobz1UKG4sZSxpLzYsbCxnKSk8YyYmKGM9byxwPS1pKTtyZXR1cm4gcC82fWZ1bmN0aW9uIFYodCxlLHIpe2Zvcih2YXIgbixvLGksYSxoLHMsbD10Lmxlbmd0aCxnPTFlMjAsYz0wLHA9MDstMTwtLWw7KWZvcihzPShuPXRbbF0pLmxlbmd0aCxoPTA7aDxzO2grPTYpbz1uW2hdLWUsaT1uW2grMV0tciwoYT12KG8qbytpKmkpKTxnJiYoZz1hLGM9bltoXSxwPW5baCsxXSk7cmV0dXJuW2MscF19ZnVuY3Rpb24gVyh0LGUscixuLG8saSl7dmFyIGEsaCxzLGwsZz1lLmxlbmd0aCxjPTAscD1NYXRoLm1pbih0LnNpemV8fE8odCksZVtyXS5zaXplfHxPKGVbcl0pKSpuLHU9MWUyMCxmPXQuY2VudGVyWCtvLGQ9dC5jZW50ZXJZK2k7Zm9yKGE9cjthPGcmJiEoKGVbYV0uc2l6ZXx8TyhlW2FdKSk8cCk7YSsrKWg9ZVthXS5jZW50ZXJYLWYscz1lW2FdLmNlbnRlclktZCwobD12KGgqaCtzKnMpKTx1JiYoYz1hLHU9bCk7cmV0dXJuIGw9ZVtjXSxlLnNwbGljZShjLDEpLGx9ZnVuY3Rpb24gWCh0LGUpe3ZhciByLG4sbyxpLGEsaCxzLGwsZyxjLHAsdSxmLGQsXz0wLFA9dC5sZW5ndGgsbT1lLygoUC0yKS82KTtmb3IoZj0yO2Y8UDtmKz02KWZvcihfKz1tOy45OTk5OTk8Xzspcj10W2YtMl0sbj10W2YtMV0sbz10W2ZdLGk9dFtmKzFdLGE9dFtmKzJdLGg9dFtmKzNdLHM9dFtmKzRdLGw9dFtmKzVdLGc9cisoby1yKSooZD0xLygoTWF0aC5mbG9vcihfKXx8MSkrMSkpLGcrPSgocD1vKyhhLW8pKmQpLWcpKmQscCs9KGErKHMtYSkqZC1wKSpkLGM9bisoaS1uKSpkLGMrPSgodT1pKyhoLWkpKmQpLWMpKmQsdSs9KGgrKGwtaCkqZC11KSpkLHQuc3BsaWNlKGYsNCxyKyhvLXIpKmQsbisoaS1uKSpkLGcsYyxnKyhwLWcpKmQsYysodS1jKSpkLHAsdSxhKyhzLWEpKmQsaCsobC1oKSpkKSxmKz02LFArPTYsXy0tO3JldHVybiB0fWZ1bmN0aW9uIFkodCxlLHIsbixvKXt2YXIgaSxhLGgscyxsLGcsYyxwPWUubGVuZ3RoLXQubGVuZ3RoLHU9MDxwP2U6dCxmPTA8cD90OmUsZD0wLF89XCJjb21wbGV4aXR5XCI9PT1uP1E6UixtPVwicG9zaXRpb25cIj09PW4/MDpcIm51bWJlclwiPT10eXBlb2Ygbj9uOi44LHY9Zi5sZW5ndGgseT1cIm9iamVjdFwiPT10eXBlb2YgciYmci5wdXNoP3Iuc2xpY2UoMCk6W3JdLHc9XCJyZXZlcnNlXCI9PT15WzBdfHx5WzBdPDAseD1cImxvZ1wiPT09cjtpZihmWzBdKXtpZigxPHUubGVuZ3RoJiYodC5zb3J0KF8pLGUuc29ydChfKSx1LnNpemV8fFAodSksZi5zaXplfHxQKGYpLGc9dS5jZW50ZXJYLWYuY2VudGVyWCxjPXUuY2VudGVyWS1mLmNlbnRlclksXz09PVIpKWZvcih2PTA7djxmLmxlbmd0aDt2KyspdS5zcGxpY2UodiwwLFcoZlt2XSx1LHYsbSxnLGMpKTtpZihwKWZvcihwPDAmJihwPS1wKSx1WzBdLmxlbmd0aD5mWzBdLmxlbmd0aCYmWChmWzBdLCh1WzBdLmxlbmd0aC1mWzBdLmxlbmd0aCkvNnwwKSx2PWYubGVuZ3RoO2Q8cDspdVt2XS5zaXplfHxPKHVbdl0pLHM9KGg9VihmLHVbdl0uY2VudGVyWCx1W3ZdLmNlbnRlclkpKVswXSxsPWhbMV0sZlt2KytdPVtzLGwscyxsLHMsbCxzLGxdLGYudG90YWxQb2ludHMrPTgsZCsrO2Zvcih2PTA7djx0Lmxlbmd0aDt2KyspaT1lW3ZdLGE9dFt2XSwocD1pLmxlbmd0aC1hLmxlbmd0aCk8MD9YKGksLXAvNnwwKTowPHAmJlgoYSxwLzZ8MCksdyYmITEhPT1vJiYhYS5yZXZlcnNlZCYmcmV2ZXJzZVNlZ21lbnQoYSksKHI9eVt2XXx8MD09PXlbdl0/eVt2XTpcImF1dG9cIikmJihhLmNsb3NlZHx8TWF0aC5hYnMoYVswXS1hW2EubGVuZ3RoLTJdKTwuNSYmTWF0aC5hYnMoYVsxXS1hW2EubGVuZ3RoLTFdKTwuNT9cImF1dG9cIj09PXJ8fFwibG9nXCI9PT1yPyh5W3ZdPXI9VShhLGksIXZ8fCExPT09bykscjwwJiYodz0hMCxyZXZlcnNlU2VnbWVudChhKSxyPS1yKSxTKGEsNipyKSk6XCJyZXZlcnNlXCIhPT1yJiYodiYmcjwwJiZyZXZlcnNlU2VnbWVudChhKSxTKGEsNioocjwwPy1yOnIpKSk6IXcmJihcImF1dG9cIj09PXImJk1hdGguYWJzKGlbMF0tYVswXSkrTWF0aC5hYnMoaVsxXS1hWzFdKStNYXRoLmFicyhpW2kubGVuZ3RoLTJdLWFbYS5sZW5ndGgtMl0pK01hdGguYWJzKGlbaS5sZW5ndGgtMV0tYVthLmxlbmd0aC0xXSk+TWF0aC5hYnMoaVswXS1hW2EubGVuZ3RoLTJdKStNYXRoLmFicyhpWzFdLWFbYS5sZW5ndGgtMV0pK01hdGguYWJzKGlbaS5sZW5ndGgtMl0tYVswXSkrTWF0aC5hYnMoaVtpLmxlbmd0aC0xXS1hWzFdKXx8ciUyKT8ocmV2ZXJzZVNlZ21lbnQoYSkseVt2XT0tMSx3PSEwKTpcImF1dG9cIj09PXI/eVt2XT0wOlwicmV2ZXJzZVwiPT09ciYmKHlbdl09LTEpLGEuY2xvc2VkIT09aS5jbG9zZWQmJihhLmNsb3NlZD1pLmNsb3NlZD0hMSkpO3JldHVybiB4JiZMKFwic2hhcGVJbmRleDpbXCIreS5qb2luKFwiLFwiKStcIl1cIiksdC5zaGFwZUluZGV4PXl9fWZ1bmN0aW9uIF8odCxlKXt2YXIgcixuLG8saSxhLGgscyxsPTAsZz1wYXJzZUZsb2F0KHRbMF0pLGM9cGFyc2VGbG9hdCh0WzFdKSxwPWcrXCIsXCIrYytcIiBcIjtmb3Iocj0uNSplLyguNSoobz10Lmxlbmd0aCktMSksbj0wO248by0yO24rPTIpe2lmKGwrPXIsaD1wYXJzZUZsb2F0KHRbbisyXSkscz1wYXJzZUZsb2F0KHRbbiszXSksLjk5OTk5OTxsKWZvcihhPTEvKE1hdGguZmxvb3IobCkrMSksaT0xOy45OTk5OTk8bDspcCs9KGcrKGgtZykqYSppKS50b0ZpeGVkKDIpK1wiLFwiKyhjKyhzLWMpKmEqaSkudG9GaXhlZCgyKStcIiBcIixsLS0saSsrO3ArPWgrXCIsXCIrcytcIiBcIixnPWgsYz1zfXJldHVybiBwfWZ1bmN0aW9uIGFhKHQpe3ZhciBlPXRbMF0ubWF0Y2goRyl8fFtdLHI9dFsxXS5tYXRjaChHKXx8W10sbj1yLmxlbmd0aC1lLmxlbmd0aDswPG4/dFswXT1fKGUsbik6dFsxXT1fKHIsLW4pfWZ1bmN0aW9uIGJhKGUpe3JldHVybiBpc05hTihlKT9hYTpmdW5jdGlvbih0KXthYSh0KSx0WzFdPWZ1bmN0aW9uIF9vZmZzZXRQb2ludHModCxlKXtpZighZSlyZXR1cm4gdDt2YXIgcixuLG8saT10Lm1hdGNoKEcpfHxbXSxhPWkubGVuZ3RoLGg9XCJcIjtmb3Iocj1cInJldmVyc2VcIj09PWU/KG49YS0xLC0yKToobj0oMioocGFyc2VJbnQoZSwxMCl8fDApKzErMTAwKmEpJWEsMiksbz0wO288YTtvKz0yKWgrPWlbbi0xXStcIixcIitpW25dK1wiIFwiLG49KG4rciklYTtyZXR1cm4gaH0odFsxXSxwYXJzZUludChlLDEwKSl9fWZ1bmN0aW9uIGRhKHQsZSl7Zm9yKHZhciByLG4sbyxpLGEsaCxzLGwsZyxjLHAsdSxmPXQubGVuZ3RoLGQ9LjIqKGV8fDEpOy0xPC0tZjspe2ZvcihwPShuPXRbZl0pLmlzU21vb3RoPW4uaXNTbW9vdGh8fFswLDAsMCwwXSx1PW4uc21vb3RoRGF0YT1uLnNtb290aERhdGF8fFswLDAsMCwwXSxwLmxlbmd0aD00LGw9bi5sZW5ndGgtMixzPTY7czxsO3MrPTYpbz1uW3NdLW5bcy0yXSxpPW5bcysxXS1uW3MtMV0sYT1uW3MrMl0tbltzXSxoPW5bcyszXS1uW3MrMV0sZz13KGksbyksYz13KGgsYSksKHI9TWF0aC5hYnMoZy1jKTxkKSYmKHVbcy0yXT1nLHVbcysyXT1jLHVbcy0xXT12KG8qbytpKmkpLHVbcyszXT12KGEqYStoKmgpKSxwLnB1c2gocixyLDAsMCxyLHIpO25bbF09PT1uWzBdJiZuWzErbF09PT1uWzFdJiYobz1uWzBdLW5bbC0yXSxpPW5bMV0tbltsLTFdLGE9blsyXS1uWzBdLGg9blszXS1uWzFdLGc9dyhpLG8pLGM9dyhoLGEpLE1hdGguYWJzKGctYyk8ZCYmKHVbbC0yXT1nLHVbMl09Yyx1W2wtMV09dihvKm8raSppKSx1WzNdPXYoYSphK2gqaCkscFtsLTJdPXBbbC0xXT0hMCkpfXJldHVybiB0fWZ1bmN0aW9uIGVhKHQpe3ZhciBlPXQudHJpbSgpLnNwbGl0KFwiIFwiKTtyZXR1cm57eDoofnQuaW5kZXhPZihcImxlZnRcIik/MDp+dC5pbmRleE9mKFwicmlnaHRcIik/MTAwOmlzTmFOKHBhcnNlRmxvYXQoZVswXSkpPzUwOnBhcnNlRmxvYXQoZVswXSkpLzEwMCx5Oih+dC5pbmRleE9mKFwidG9wXCIpPzA6fnQuaW5kZXhPZihcImJvdHRvbVwiKT8xMDA6aXNOYU4ocGFyc2VGbG9hdChlWzFdKSk/NTA6cGFyc2VGbG9hdChlWzFdKSkvMTAwfX1mdW5jdGlvbiBoYSh0LGUscixuKXt2YXIgbyxpLGE9dGhpcy5fb3JpZ2luLGg9dGhpcy5fZU9yaWdpbixzPXRbcl0tYS54LGw9dFtyKzFdLWEueSxnPXYocypzK2wqbCksYz13KGwscyk7cmV0dXJuIHM9ZVtyXS1oLngsbD1lW3IrMV0taC55LGk9ZnVuY3Rpb24gX3Nob3J0QW5nbGUodCl7cmV0dXJuIHQhPT10JXA/dCsodDwwP3U6LXUpOnR9KG89dyhsLHMpLWMpLCFuJiZGJiZNYXRoLmFicyhpK0YuY2EpPGYmJihuPUYpLHRoaXMuX2FuY2hvclBUPUY9e19uZXh0OnRoaXMuX2FuY2hvclBULHQ6dCxzYTpjLGNhOm4mJmkqbi5jYTwwJiZNYXRoLmFicyhpKT5kP286aSxzbDpnLGNsOnYocypzK2wqbCktZyxpOnJ9fWZ1bmN0aW9uIGlhKHQpe3I9eSgpLG89b3x8ciYmci5wbHVnaW5zLm1vcnBoU1ZHLHImJm8/KEM9ci51dGlscy50b0FycmF5LG8ucHJvdG90eXBlLl90d2VlblJvdGF0aW9uPWhhLEk9MSk6dCYmTChcIlBsZWFzZSBnc2FwLnJlZ2lzdGVyUGx1Z2luKE1vcnBoU1ZHUGx1Z2luKVwiKX12YXIgcixDLEYsSSxvLHc9TWF0aC5hdGFuMix4PU1hdGguY29zLGo9TWF0aC5zaW4sdj1NYXRoLnNxcnQscD1NYXRoLlBJLHU9MipwLGY9LjMqcCxkPS43KnAsRz0vWy0rPVxcLl0qXFxkK1tcXC5lXFwtXFwrXSpcXGQqW2VcXC1cXCtdKlxcZCovZ2kscT0vKF5bI1xcLl1bYS16XXxbYS15XVthLXpdKS9naSxIPS9bYWNobG1xc3R2el0vZ2ksQj1cIlVzZSBNb3JwaFNWR1BsdWdpbi5jb252ZXJ0VG9QYXRoKCkgdG8gY29udmVydCB0byBhIHBhdGggYmVmb3JlIG1vcnBoaW5nLlwiLEo9e3ZlcnNpb246XCIzLjMuNFwiLG5hbWU6XCJtb3JwaFNWR1wiLHJlZ2lzdGVyOmZ1bmN0aW9uIHJlZ2lzdGVyKHQsZSl7cj10LG89ZSxpYSgpfSxpbml0OmZ1bmN0aW9uIGluaXQodCxlLHIsbixvKXtpZihJfHxpYSgxKSwhZSlyZXR1cm4gTChcImludmFsaWQgc2hhcGVcIiksITE7KFwic3RyaW5nXCI9PXR5cGVvZiBlfHxlLmdldEJCb3h8fGVbMF0pJiYoZT17c2hhcGU6ZX0pO3ZhciBpLGEsaCxzLGwsZyxjLHAsdSxmLGQsXyxtLHYseSx3LHgsVCxNLGIsUyxOLFI9dC5ub2RlVHlwZT93aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZSh0KTp7fSxBPVIuZmlsbCtcIlwiLHo9IShcIm5vbmVcIj09PUF8fFwiMFwiPT09KEEubWF0Y2goRyl8fFtdKVszXXx8XCJldmVub2RkXCI9PT1SLmZpbGxSdWxlKSxPPShlLm9yaWdpbnx8XCI1MCA1MFwiKS5zcGxpdChcIixcIik7aWYobD1cIlBPTFlMSU5FXCI9PT0oaT0odC5ub2RlTmFtZStcIlwiKS50b1VwcGVyQ2FzZSgpKXx8XCJQT0xZR09OXCI9PT1pLFwiUEFUSFwiIT09aSYmIWwmJiFlLnByb3ApcmV0dXJuIEwoXCJDYW5ub3QgbW9ycGggYSA8XCIraStcIj4gZWxlbWVudC4gXCIrQiksITE7aWYoYT1cIlBBVEhcIj09PWk/XCJkXCI6XCJwb2ludHNcIiwhZS5wcm9wJiZcImZ1bmN0aW9uXCIhPXR5cGVvZiB0LnNldEF0dHJpYnV0ZSlyZXR1cm4hMTtpZihzPWZ1bmN0aW9uIF9wYXJzZVNoYXBlKHQsZSxyKXt2YXIgbixvO3JldHVybighKFwic3RyaW5nXCI9PXR5cGVvZiB0KXx8cS50ZXN0KHQpfHwodC5tYXRjaChHKXx8W10pLmxlbmd0aDwzKSYmKChuPUModClbMF0pPyhvPShuLm5vZGVOYW1lK1wiXCIpLnRvVXBwZXJDYXNlKCksZSYmXCJQQVRIXCIhPT1vJiYobj1jb252ZXJ0VG9QYXRoKG4sITEpLG89XCJQQVRIXCIpLHQ9bi5nZXRBdHRyaWJ1dGUoXCJQQVRIXCI9PT1vP1wiZFwiOlwicG9pbnRzXCIpfHxcIlwiLG49PT1yJiYodD1uLmdldEF0dHJpYnV0ZU5TKG51bGwsXCJkYXRhLW9yaWdpbmFsXCIpfHx0KSk6KEwoXCJXQVJOSU5HOiBpbnZhbGlkIG1vcnBoIHRvOiBcIit0KSx0PSExKSksdH0oZS5zaGFwZXx8ZS5kfHxlLnBvaW50c3x8XCJcIixcImRcIj09YSx0KSxsJiZILnRlc3QocykpcmV0dXJuIEwoXCJBIDxcIitpK1wiPiBjYW5ub3QgYWNjZXB0IHBhdGggZGF0YS4gXCIrQiksITE7aWYoZz1lLnNoYXBlSW5kZXh8fDA9PT1lLnNoYXBlSW5kZXg/ZS5zaGFwZUluZGV4OlwiYXV0b1wiLGM9ZS5tYXB8fEouZGVmYXVsdE1hcCx0aGlzLl9wcm9wPWUucHJvcCx0aGlzLl9yZW5kZXI9ZS5yZW5kZXJ8fEouZGVmYXVsdFJlbmRlcix0aGlzLl9hcHBseT1cInVwZGF0ZVRhcmdldFwiaW4gZT9lLnVwZGF0ZVRhcmdldDpKLmRlZmF1bHRVcGRhdGVUYXJnZXQsdGhpcy5fcm5kPU1hdGgucG93KDEwLGlzTmFOKGUucHJlY2lzaW9uKT8yOitlLnByZWNpc2lvbiksdGhpcy5fdHdlZW49cixzKXtpZih0aGlzLl90YXJnZXQ9dCx4PVwib2JqZWN0XCI9PXR5cGVvZiBlLnByZWNvbXBpbGUsZj10aGlzLl9wcm9wP3RbdGhpcy5fcHJvcF06dC5nZXRBdHRyaWJ1dGUoYSksdGhpcy5fcHJvcHx8dC5nZXRBdHRyaWJ1dGVOUyhudWxsLFwiZGF0YS1vcmlnaW5hbFwiKXx8dC5zZXRBdHRyaWJ1dGVOUyhudWxsLFwiZGF0YS1vcmlnaW5hbFwiLGYpLFwiZFwiPT1hfHx0aGlzLl9wcm9wKXtpZihmPXN0cmluZ1RvUmF3UGF0aCh4P2UucHJlY29tcGlsZVswXTpmKSxkPXN0cmluZ1RvUmF3UGF0aCh4P2UucHJlY29tcGlsZVsxXTpzKSwheCYmIVkoZixkLGcsYyx6KSlyZXR1cm4hMTtmb3IoXCJsb2dcIiE9PWUucHJlY29tcGlsZSYmITAhPT1lLnByZWNvbXBpbGV8fEwoJ3ByZWNvbXBpbGU6W1wiJytyYXdQYXRoVG9TdHJpbmcoZikrJ1wiLFwiJytyYXdQYXRoVG9TdHJpbmcoZCkrJ1wiXScpLChTPVwibGluZWFyXCIhPT0oZS50eXBlfHxKLmRlZmF1bHRUeXBlKSkmJihmPWRhKGYsZS5zbW9vdGhUb2xlcmFuY2UpLGQ9ZGEoZCxlLnNtb290aFRvbGVyYW5jZSksZi5zaXplfHxQKGYpLGQuc2l6ZXx8UChkKSxiPWVhKE9bMF0pLHRoaXMuX29yaWdpbj1mLm9yaWdpbj17eDpmLmxlZnQrYi54KmYud2lkdGgseTpmLnRvcCtiLnkqZi5oZWlnaHR9LE9bMV0mJihiPWVhKE9bMV0pKSx0aGlzLl9lT3JpZ2luPXt4OmQubGVmdCtiLngqZC53aWR0aCx5OmQudG9wK2IueSpkLmhlaWdodH0pLHRoaXMuX3Jhd1BhdGg9dC5fZ3NSYXdQYXRoPWYsbT1mLmxlbmd0aDstMTwtLW07KWZvcih5PWZbbV0sdz1kW21dLHA9eS5pc1Ntb290aHx8W10sdT13LmlzU21vb3RofHxbXSx2PXkubGVuZ3RoLF89Rj0wO188djtfKz0yKXdbX109PT15W19dJiZ3W18rMV09PT15W18rMV18fChTP3BbX10mJnVbX10/KFQ9eS5zbW9vdGhEYXRhLE09dy5zbW9vdGhEYXRhLE49XysoXz09PXYtND83LXY6NSksdGhpcy5fY29udHJvbFBUPXtfbmV4dDp0aGlzLl9jb250cm9sUFQsaTpfLGo6bSxsMXM6VFtfKzFdLGwxYzpNW18rMV0tVFtfKzFdLGwyczpUW05dLGwyYzpNW05dLVRbTl19LGg9dGhpcy5fdHdlZW5Sb3RhdGlvbih5LHcsXysyKSx0aGlzLl90d2VlblJvdGF0aW9uKHksdyxfLGgpLHRoaXMuX3R3ZWVuUm90YXRpb24oeSx3LE4tMSxoKSxfKz00KTp0aGlzLl90d2VlblJvdGF0aW9uKHksdyxfKTooaD10aGlzLmFkZCh5LF8seVtfXSx3W19dKSxoPXRoaXMuYWRkKHksXysxLHlbXysxXSx3W18rMV0pfHxoKSl9ZWxzZSBoPXRoaXMuYWRkKHQsXCJzZXRBdHRyaWJ1dGVcIix0LmdldEF0dHJpYnV0ZShhKStcIlwiLHMrXCJcIixuLG8sMCxiYShnKSxhKTtTJiYodGhpcy5hZGQodGhpcy5fb3JpZ2luLFwieFwiLHRoaXMuX29yaWdpbi54LHRoaXMuX2VPcmlnaW4ueCksaD10aGlzLmFkZCh0aGlzLl9vcmlnaW4sXCJ5XCIsdGhpcy5fb3JpZ2luLnksdGhpcy5fZU9yaWdpbi55KSksaCYmKHRoaXMuX3Byb3BzLnB1c2goXCJtb3JwaFNWR1wiKSxoLmVuZD1zLGguZW5kUHJvcD1hKX1yZXR1cm4gMX0scmVuZGVyOmZ1bmN0aW9uIHJlbmRlcih0LGUpe2Zvcih2YXIgcixuLG8saSxhLGgscyxsLGcsYyxwLHUsZj1lLl9yYXdQYXRoLGQ9ZS5fY29udHJvbFBULF89ZS5fYW5jaG9yUFQsUD1lLl9ybmQsbT1lLl90YXJnZXQsdj1lLl9wdDt2Oyl2LnIodCx2LmQpLHY9di5fbmV4dDtpZigxPT09dCYmZS5fYXBwbHkpZm9yKHY9ZS5fcHQ7djspdi5lbmQmJihlLl9wcm9wP21bZS5fcHJvcF09di5lbmQ6bS5zZXRBdHRyaWJ1dGUodi5lbmRQcm9wLHYuZW5kKSksdj12Ll9uZXh0O2Vsc2UgaWYoZil7Zm9yKDtfOylhPV8uc2ErdCpfLmNhLGk9Xy5zbCt0Kl8uY2wsXy50W18uaV09ZS5fb3JpZ2luLngreChhKSppLF8udFtfLmkrMV09ZS5fb3JpZ2luLnkraihhKSppLF89Xy5fbmV4dDtmb3Iobj10PC41PzIqdCp0Oig0LTIqdCkqdC0xO2Q7KXU9KGg9ZC5pKSsoaD09PShvPWZbZC5qXSkubGVuZ3RoLTQ/Ny1vLmxlbmd0aDo1KSxhPXcob1t1XS1vW2grMV0sb1t1LTFdLW9baF0pLGM9aihhKSxwPXgoYSksbD1vW2grMl0sZz1vW2grM10saT1kLmwxcytuKmQubDFjLG9baF09bC1wKmksb1toKzFdPWctYyppLGk9ZC5sMnMrbipkLmwyYyxvW3UtMV09bCtwKmksb1t1XT1nK2MqaSxkPWQuX25leHQ7aWYobS5fZ3NSYXdQYXRoPWYsZS5fYXBwbHkpe2ZvcihyPVwiXCIscz0wO3M8Zi5sZW5ndGg7cysrKWZvcihpPShvPWZbc10pLmxlbmd0aCxyKz1cIk1cIisob1swXSpQfDApL1ArXCIgXCIrKG9bMV0qUHwwKS9QK1wiIENcIixoPTI7aDxpO2grKylyKz0ob1toXSpQfDApL1ArXCIgXCI7ZS5fcHJvcD9tW2UuX3Byb3BdPXI6bS5zZXRBdHRyaWJ1dGUoXCJkXCIscil9fWUuX3JlbmRlciYmZiYmZS5fcmVuZGVyLmNhbGwoZS5fdHdlZW4sZixtKX0sa2lsbDpmdW5jdGlvbiBraWxsKCl7dGhpcy5fcHQ9dGhpcy5fcmF3UGF0aD0wfSxnZXRSYXdQYXRoOmZ1bmN0aW9uIGdldFJhd1BhdGgodCl7dmFyIGUscj0odD1tKHQpJiZuLnRlc3QodCkmJmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IodCl8fHQpLmdldEF0dHJpYnV0ZT90OjA7cmV0dXJuIHImJih0PXQuZ2V0QXR0cmlidXRlKFwiZFwiKSk/KHIuX2dzUGF0aHx8KHIuX2dzUGF0aD17fSksKGU9ci5fZ3NQYXRoW3RdKSYmIWUuX2RpcnR5P2U6ci5fZ3NQYXRoW3RdPXN0cmluZ1RvUmF3UGF0aCh0KSk6dD9tKHQpP3N0cmluZ1RvUmF3UGF0aCh0KTpoKHRbMF0pP1t0XTp0OmNvbnNvbGUud2FybihcIkV4cGVjdGluZyBhIDxwYXRoPiBlbGVtZW50IG9yIGFuIFNWRyBwYXRoIGRhdGEgc3RyaW5nXCIpfSxzdHJpbmdUb1Jhd1BhdGg6c3RyaW5nVG9SYXdQYXRoLHJhd1BhdGhUb1N0cmluZzpyYXdQYXRoVG9TdHJpbmcscGF0aEZpbHRlcjpmdW5jdGlvbiBfcGF0aEZpbHRlcih0LGUscixuLG8pe3ZhciBpPXN0cmluZ1RvUmF3UGF0aCh0WzBdKSxhPXN0cmluZ1RvUmF3UGF0aCh0WzFdKTtZKGksYSxlfHwwPT09ZT9lOlwiYXV0b1wiLHIsbykmJih0WzBdPXJhd1BhdGhUb1N0cmluZyhpKSx0WzFdPXJhd1BhdGhUb1N0cmluZyhhKSxcImxvZ1wiIT09biYmITAhPT1ufHxMKCdwcmVjb21waWxlOltcIicrdFswXSsnXCIsXCInK3RbMV0rJ1wiXScpKX0scG9pbnRzRmlsdGVyOmFhLGdldFRvdGFsU2l6ZTpQLGVxdWFsaXplU2VnbWVudFF1YW50aXR5OlksY29udmVydFRvUGF0aDpmdW5jdGlvbiBjb252ZXJ0VG9QYXRoJDEodCxlKXtyZXR1cm4gQyh0KS5tYXAoZnVuY3Rpb24odCl7cmV0dXJuIGNvbnZlcnRUb1BhdGgodCwhMSE9PWUpfSl9LGRlZmF1bHRUeXBlOlwibGluZWFyXCIsZGVmYXVsdFVwZGF0ZVRhcmdldDohMCxkZWZhdWx0TWFwOlwic2l6ZVwifTt5KCkmJnIucmVnaXN0ZXJQbHVnaW4oSiksdC5Nb3JwaFNWR1BsdWdpbj1KLHQuZGVmYXVsdD1KO2lmICh0eXBlb2Yod2luZG93KT09PVwidW5kZWZpbmVkXCJ8fHdpbmRvdyE9PXQpe09iamVjdC5kZWZpbmVQcm9wZXJ0eSh0LFwiX19lc01vZHVsZVwiLHt2YWx1ZTohMH0pfSBlbHNlIHtkZWxldGUgdC5kZWZhdWx0fX0pO1xuXG4iLCIvKiFcbiAqIFNwbGl0VGV4dCAzLjMuNFxuICogaHR0cHM6Ly9ncmVlbnNvY2suY29tXG4gKiBcbiAqIEBsaWNlbnNlIENvcHlyaWdodCAyMDIwLCBHcmVlblNvY2suIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKiBTdWJqZWN0IHRvIHRoZSB0ZXJtcyBhdCBodHRwczovL2dyZWVuc29jay5jb20vc3RhbmRhcmQtbGljZW5zZSBvciBmb3IgQ2x1YiBHcmVlblNvY2sgbWVtYmVycywgdGhlIGFncmVlbWVudCBpc3N1ZWQgd2l0aCB0aGF0IG1lbWJlcnNoaXAuXG4gKiBAYXV0aG9yOiBKYWNrIERveWxlLCBqYWNrQGdyZWVuc29jay5jb21cbiAqL1xuXG4hZnVuY3Rpb24oRCx1KXtcIm9iamVjdFwiPT10eXBlb2YgZXhwb3J0cyYmXCJ1bmRlZmluZWRcIiE9dHlwZW9mIG1vZHVsZT91KGV4cG9ydHMpOlwiZnVuY3Rpb25cIj09dHlwZW9mIGRlZmluZSYmZGVmaW5lLmFtZD9kZWZpbmUoW1wiZXhwb3J0c1wiXSx1KTp1KChEPUR8fHNlbGYpLndpbmRvdz1ELndpbmRvd3x8e30pfSh0aGlzLGZ1bmN0aW9uKEQpe1widXNlIHN0cmljdFwiO3ZhciBiPS8oW1xcdUQ4MDAtXFx1REJGRl1bXFx1REMwMC1cXHVERkZGXSg/OltcXHUyMDBEXFx1RkUwRl1bXFx1RDgwMC1cXHVEQkZGXVtcXHVEQzAwLVxcdURGRkZdKXsyLH18XFx1RDgzRFxcdURDNjkoPzpcXHUyMDBEKD86KD86XFx1RDgzRFxcdURDNjlcXHUyMDBEKT9cXHVEODNEXFx1REM2N3woPzpcXHVEODNEXFx1REM2OVxcdTIwMEQpP1xcdUQ4M0RcXHVEQzY2KXxcXHVEODNDW1xcdURGRkItXFx1REZGRl0pfFxcdUQ4M0RcXHVEQzY5XFx1MjAwRCg/OlxcdUQ4M0RcXHVEQzY5XFx1MjAwRCk/XFx1RDgzRFxcdURDNjZcXHUyMDBEXFx1RDgzRFxcdURDNjZ8XFx1RDgzRFxcdURDNjlcXHUyMDBEKD86XFx1RDgzRFxcdURDNjlcXHUyMDBEKT9cXHVEODNEXFx1REM2N1xcdTIwMEQoPzpcXHVEODNEW1xcdURDNjZcXHVEQzY3XSl8XFx1RDgzQ1xcdURGRjNcXHVGRTBGXFx1MjAwRFxcdUQ4M0NcXHVERjA4fCg/OlxcdUQ4M0NbXFx1REZDM1xcdURGQzRcXHVERkNBXXxcXHVEODNEW1xcdURDNkVcXHVEQzcxXFx1REM3M1xcdURDNzdcXHVEQzgxXFx1REM4MlxcdURDODZcXHVEQzg3XFx1REU0NS1cXHVERTQ3XFx1REU0QlxcdURFNERcXHVERTRFXFx1REVBM1xcdURFQjQtXFx1REVCNl18XFx1RDgzRVtcXHVERDI2XFx1REQzNy1cXHVERDM5XFx1REQzRFxcdUREM0VcXHVEREQ2LVxcdURERERdKSg/OlxcdUQ4M0NbXFx1REZGQi1cXHVERkZGXSlcXHUyMDBEW1xcdTI2NDBcXHUyNjQyXVxcdUZFMEZ8XFx1RDgzRFxcdURDNjkoPzpcXHVEODNDW1xcdURGRkItXFx1REZGRl0pXFx1MjAwRCg/OlxcdUQ4M0NbXFx1REYzRVxcdURGNzNcXHVERjkzXFx1REZBNFxcdURGQThcXHVERkVCXFx1REZFRF18XFx1RDgzRFtcXHVEQ0JCXFx1RENCQ1xcdUREMjdcXHVERDJDXFx1REU4MFxcdURFOTJdKXwoPzpcXHVEODNDW1xcdURGQzNcXHVERkM0XFx1REZDQV18XFx1RDgzRFtcXHVEQzZFXFx1REM2RlxcdURDNzFcXHVEQzczXFx1REM3N1xcdURDODFcXHVEQzgyXFx1REM4NlxcdURDODdcXHVERTQ1LVxcdURFNDdcXHVERTRCXFx1REU0RFxcdURFNEVcXHVERUEzXFx1REVCNC1cXHVERUI2XXxcXHVEODNFW1xcdUREMjZcXHVERDM3LVxcdUREMzlcXHVERDNDLVxcdUREM0VcXHVEREQ2LVxcdUREREZdKVxcdTIwMERbXFx1MjY0MFxcdTI2NDJdXFx1RkUwRnxcXHVEODNDXFx1RERGRFxcdUQ4M0NcXHVEREYwfFxcdUQ4M0NcXHVEREY2XFx1RDgzQ1xcdURERTZ8XFx1RDgzQ1xcdURERjRcXHVEODNDXFx1RERGMnxcXHVEODNDXFx1RERFOSg/OlxcdUQ4M0NbXFx1RERFQVxcdURERUNcXHVEREVGXFx1RERGMFxcdURERjJcXHVEREY0XFx1RERGRl0pfFxcdUQ4M0NcXHVEREY3KD86XFx1RDgzQ1tcXHVEREVBXFx1RERGNFxcdURERjhcXHVEREZBXFx1RERGQ10pfFxcdUQ4M0NcXHVEREU4KD86XFx1RDgzQ1tcXHVEREU2XFx1RERFOFxcdURERTlcXHVEREVCLVxcdURERUVcXHVEREYwLVxcdURERjVcXHVEREY3XFx1RERGQS1cXHVEREZGXSl8KD86XFx1MjZGOXxcXHVEODNDW1xcdURGQ0JcXHVERkNDXXxcXHVEODNEXFx1REQ3NSkoPzpcXHVGRTBGXFx1MjAwRFtcXHUyNjQwXFx1MjY0Ml18KD86XFx1RDgzQ1tcXHVERkZCLVxcdURGRkZdKVxcdTIwMERbXFx1MjY0MFxcdTI2NDJdKVxcdUZFMEZ8KD86XFx1RDgzRFxcdURDNDFcXHVGRTBGXFx1MjAwRFxcdUQ4M0RcXHVEREU4fFxcdUQ4M0RcXHVEQzY5KD86XFx1RDgzQ1tcXHVERkZCLVxcdURGRkZdKVxcdTIwMERbXFx1MjY5NVxcdTI2OTZcXHUyNzA4XXxcXHVEODNEXFx1REM2OVxcdTIwMERbXFx1MjY5NVxcdTI2OTZcXHUyNzA4XXxcXHVEODNEXFx1REM2OCg/Oig/OlxcdUQ4M0NbXFx1REZGQi1cXHVERkZGXSlcXHUyMDBEW1xcdTI2OTVcXHUyNjk2XFx1MjcwOF18XFx1MjAwRFtcXHUyNjk1XFx1MjY5NlxcdTI3MDhdKSlcXHVGRTBGfFxcdUQ4M0NcXHVEREYyKD86XFx1RDgzQ1tcXHVEREU2XFx1RERFOC1cXHVEREVEXFx1RERGMC1cXHVEREZGXSl8XFx1RDgzRFxcdURDNjlcXHUyMDBEKD86XFx1RDgzQ1tcXHVERjNFXFx1REY3M1xcdURGOTNcXHVERkE0XFx1REZBOFxcdURGRUJcXHVERkVEXXxcXHVEODNEW1xcdURDQkJcXHVEQ0JDXFx1REQyN1xcdUREMkNcXHVERTgwXFx1REU5Ml18XFx1Mjc2NFxcdUZFMEZcXHUyMDBEKD86XFx1RDgzRFxcdURDOEJcXHUyMDBEKD86XFx1RDgzRFtcXHVEQzY4XFx1REM2OV0pfFxcdUQ4M0RbXFx1REM2OFxcdURDNjldKSl8XFx1RDgzQ1xcdURERjEoPzpcXHVEODNDW1xcdURERTYtXFx1RERFOFxcdURERUVcXHVEREYwXFx1RERGNy1cXHVEREZCXFx1RERGRV0pfFxcdUQ4M0NcXHVEREVGKD86XFx1RDgzQ1tcXHVEREVBXFx1RERGMlxcdURERjRcXHVEREY1XSl8XFx1RDgzQ1xcdURERUQoPzpcXHVEODNDW1xcdURERjBcXHVEREYyXFx1RERGM1xcdURERjdcXHVEREY5XFx1RERGQV0pfFxcdUQ4M0NcXHVEREVCKD86XFx1RDgzQ1tcXHVEREVFLVxcdURERjBcXHVEREYyXFx1RERGNFxcdURERjddKXxbI1xcKjAtOV1cXHVGRTBGXFx1MjBFM3xcXHVEODNDXFx1RERFNyg/OlxcdUQ4M0NbXFx1RERFNlxcdURERTdcXHVEREU5LVxcdURERUZcXHVEREYxLVxcdURERjRcXHVEREY2LVxcdURERjlcXHVEREZCXFx1RERGQ1xcdURERkVcXHVEREZGXSl8XFx1RDgzQ1xcdURERTYoPzpcXHVEODNDW1xcdURERTgtXFx1RERFQ1xcdURERUVcXHVEREYxXFx1RERGMlxcdURERjRcXHVEREY2LVxcdURERkFcXHVEREZDXFx1RERGRFxcdURERkZdKXxcXHVEODNDXFx1RERGRig/OlxcdUQ4M0NbXFx1RERFNlxcdURERjJcXHVEREZDXSl8XFx1RDgzQ1xcdURERjUoPzpcXHVEODNDW1xcdURERTZcXHVEREVBLVxcdURERURcXHVEREYwLVxcdURERjNcXHVEREY3LVxcdURERjlcXHVEREZDXFx1RERGRV0pfFxcdUQ4M0NcXHVEREZCKD86XFx1RDgzQ1tcXHVEREU2XFx1RERFOFxcdURERUFcXHVEREVDXFx1RERFRVxcdURERjNcXHVEREZBXSl8XFx1RDgzQ1xcdURERjMoPzpcXHVEODNDW1xcdURERTZcXHVEREU4XFx1RERFQS1cXHVEREVDXFx1RERFRVxcdURERjFcXHVEREY0XFx1RERGNVxcdURERjdcXHVEREZBXFx1RERGRl0pfFxcdUQ4M0NcXHVERkY0XFx1REI0MFxcdURDNjdcXHVEQjQwXFx1REM2Mig/OlxcdURCNDBcXHVEQzc3XFx1REI0MFxcdURDNkNcXHVEQjQwXFx1REM3M3xcXHVEQjQwXFx1REM3M1xcdURCNDBcXHVEQzYzXFx1REI0MFxcdURDNzR8XFx1REI0MFxcdURDNjVcXHVEQjQwXFx1REM2RVxcdURCNDBcXHVEQzY3KVxcdURCNDBcXHVEQzdGfFxcdUQ4M0RcXHVEQzY4KD86XFx1MjAwRCg/OlxcdTI3NjRcXHVGRTBGXFx1MjAwRCg/OlxcdUQ4M0RcXHVEQzhCXFx1MjAwRCk/XFx1RDgzRFxcdURDNjh8KD86KD86XFx1RDgzRFtcXHVEQzY4XFx1REM2OV0pXFx1MjAwRCk/XFx1RDgzRFxcdURDNjZcXHUyMDBEXFx1RDgzRFxcdURDNjZ8KD86KD86XFx1RDgzRFtcXHVEQzY4XFx1REM2OV0pXFx1MjAwRCk/XFx1RDgzRFxcdURDNjdcXHUyMDBEKD86XFx1RDgzRFtcXHVEQzY2XFx1REM2N10pfFxcdUQ4M0NbXFx1REYzRVxcdURGNzNcXHVERjkzXFx1REZBNFxcdURGQThcXHVERkVCXFx1REZFRF18XFx1RDgzRFtcXHVEQ0JCXFx1RENCQ1xcdUREMjdcXHVERDJDXFx1REU4MFxcdURFOTJdKXwoPzpcXHVEODNDW1xcdURGRkItXFx1REZGRl0pXFx1MjAwRCg/OlxcdUQ4M0NbXFx1REYzRVxcdURGNzNcXHVERjkzXFx1REZBNFxcdURGQThcXHVERkVCXFx1REZFRF18XFx1RDgzRFtcXHVEQ0JCXFx1RENCQ1xcdUREMjdcXHVERDJDXFx1REU4MFxcdURFOTJdKSl8XFx1RDgzQ1xcdURERjgoPzpcXHVEODNDW1xcdURERTYtXFx1RERFQVxcdURERUMtXFx1RERGNFxcdURERjctXFx1RERGOVxcdURERkJcXHVEREZELVxcdURERkZdKXxcXHVEODNDXFx1RERGMCg/OlxcdUQ4M0NbXFx1RERFQVxcdURERUMtXFx1RERFRVxcdURERjJcXHVEREYzXFx1RERGNVxcdURERjdcXHVEREZDXFx1RERGRVxcdURERkZdKXxcXHVEODNDXFx1RERGRSg/OlxcdUQ4M0NbXFx1RERFQVxcdURERjldKXxcXHVEODNDXFx1RERFRSg/OlxcdUQ4M0NbXFx1RERFOC1cXHVEREVBXFx1RERGMS1cXHVEREY0XFx1RERGNi1cXHVEREY5XSl8XFx1RDgzQ1xcdURERjkoPzpcXHVEODNDW1xcdURERTZcXHVEREU4XFx1RERFOVxcdURERUItXFx1RERFRFxcdURERUYtXFx1RERGNFxcdURERjdcXHVEREY5XFx1RERGQlxcdURERkNcXHVEREZGXSl8XFx1RDgzQ1xcdURERUMoPzpcXHVEODNDW1xcdURERTZcXHVEREU3XFx1RERFOS1cXHVEREVFXFx1RERGMS1cXHVEREYzXFx1RERGNS1cXHVEREZBXFx1RERGQ1xcdURERkVdKXxcXHVEODNDXFx1RERGQSg/OlxcdUQ4M0NbXFx1RERFNlxcdURERUNcXHVEREYyXFx1RERGM1xcdURERjhcXHVEREZFXFx1RERGRl0pfFxcdUQ4M0NcXHVEREVBKD86XFx1RDgzQ1tcXHVEREU2XFx1RERFOFxcdURERUFcXHVEREVDXFx1RERFRFxcdURERjctXFx1RERGQV0pfFxcdUQ4M0NcXHVEREZDKD86XFx1RDgzQ1tcXHVEREVCXFx1RERGOF0pfCg/OlxcdTI2Rjl8XFx1RDgzQ1tcXHVERkNCXFx1REZDQ118XFx1RDgzRFxcdURENzUpKD86XFx1RDgzQ1tcXHVERkZCLVxcdURGRkZdKXwoPzpcXHVEODNDW1xcdURGQzNcXHVERkM0XFx1REZDQV18XFx1RDgzRFtcXHVEQzZFXFx1REM3MVxcdURDNzNcXHVEQzc3XFx1REM4MVxcdURDODJcXHVEQzg2XFx1REM4N1xcdURFNDUtXFx1REU0N1xcdURFNEJcXHVERTREXFx1REU0RVxcdURFQTNcXHVERUI0LVxcdURFQjZdfFxcdUQ4M0VbXFx1REQyNlxcdUREMzctXFx1REQzOVxcdUREM0RcXHVERDNFXFx1RERENi1cXHVEREREXSkoPzpcXHVEODNDW1xcdURGRkItXFx1REZGRl0pfCg/OltcXHUyNjFEXFx1MjcwQS1cXHUyNzBEXXxcXHVEODNDW1xcdURGODVcXHVERkMyXFx1REZDN118XFx1RDgzRFtcXHVEQzQyXFx1REM0M1xcdURDNDYtXFx1REM1MFxcdURDNjZcXHVEQzY3XFx1REM3MFxcdURDNzJcXHVEQzc0LVxcdURDNzZcXHVEQzc4XFx1REM3Q1xcdURDODNcXHVEQzg1XFx1RENBQVxcdURENzRcXHVERDdBXFx1REQ5MFxcdUREOTVcXHVERDk2XFx1REU0Q1xcdURFNEZcXHVERUMwXFx1REVDQ118XFx1RDgzRVtcXHVERDE4LVxcdUREMUNcXHVERDFFXFx1REQxRlxcdUREMzAtXFx1REQzNlxcdURERDEtXFx1RERENV0pKD86XFx1RDgzQ1tcXHVERkZCLVxcdURGRkZdKXxcXHVEODNEXFx1REM2OCg/OlxcdTIwMEQoPzooPzooPzpcXHVEODNEW1xcdURDNjhcXHVEQzY5XSlcXHUyMDBEKT9cXHVEODNEXFx1REM2N3woPzooPzpcXHVEODNEW1xcdURDNjhcXHVEQzY5XSlcXHUyMDBEKT9cXHVEODNEXFx1REM2Nil8XFx1RDgzQ1tcXHVERkZCLVxcdURGRkZdKXwoPzpbXFx1MjYxRFxcdTI2RjlcXHUyNzBBLVxcdTI3MERdfFxcdUQ4M0NbXFx1REY4NVxcdURGQzItXFx1REZDNFxcdURGQzdcXHVERkNBLVxcdURGQ0NdfFxcdUQ4M0RbXFx1REM0MlxcdURDNDNcXHVEQzQ2LVxcdURDNTBcXHVEQzY2LVxcdURDNjlcXHVEQzZFXFx1REM3MC1cXHVEQzc4XFx1REM3Q1xcdURDODEtXFx1REM4M1xcdURDODUtXFx1REM4N1xcdURDQUFcXHVERDc0XFx1REQ3NVxcdUREN0FcXHVERDkwXFx1REQ5NVxcdUREOTZcXHVERTQ1LVxcdURFNDdcXHVERTRCLVxcdURFNEZcXHVERUEzXFx1REVCNC1cXHVERUI2XFx1REVDMFxcdURFQ0NdfFxcdUQ4M0VbXFx1REQxOC1cXHVERDFDXFx1REQxRVxcdUREMUZcXHVERDI2XFx1REQzMC1cXHVERDM5XFx1REQzRFxcdUREM0VcXHVEREQxLVxcdURERERdKSg/OlxcdUQ4M0NbXFx1REZGQi1cXHVERkZGXSk/fCg/OltcXHUyMzFBXFx1MjMxQlxcdTIzRTktXFx1MjNFQ1xcdTIzRjBcXHUyM0YzXFx1MjVGRFxcdTI1RkVcXHUyNjE0XFx1MjYxNVxcdTI2NDgtXFx1MjY1M1xcdTI2N0ZcXHUyNjkzXFx1MjZBMVxcdTI2QUFcXHUyNkFCXFx1MjZCRFxcdTI2QkVcXHUyNkM0XFx1MjZDNVxcdTI2Q0VcXHUyNkQ0XFx1MjZFQVxcdTI2RjJcXHUyNkYzXFx1MjZGNVxcdTI2RkFcXHUyNkZEXFx1MjcwNVxcdTI3MEFcXHUyNzBCXFx1MjcyOFxcdTI3NENcXHUyNzRFXFx1Mjc1My1cXHUyNzU1XFx1Mjc1N1xcdTI3OTUtXFx1Mjc5N1xcdTI3QjBcXHUyN0JGXFx1MkIxQlxcdTJCMUNcXHUyQjUwXFx1MkI1NV18XFx1RDgzQ1tcXHVEQzA0XFx1RENDRlxcdUREOEVcXHVERDkxLVxcdUREOUFcXHVEREU2LVxcdURERkZcXHVERTAxXFx1REUxQVxcdURFMkZcXHVERTMyLVxcdURFMzZcXHVERTM4LVxcdURFM0FcXHVERTUwXFx1REU1MVxcdURGMDAtXFx1REYyMFxcdURGMkQtXFx1REYzNVxcdURGMzctXFx1REY3Q1xcdURGN0UtXFx1REY5M1xcdURGQTAtXFx1REZDQVxcdURGQ0YtXFx1REZEM1xcdURGRTAtXFx1REZGMFxcdURGRjRcXHVERkY4LVxcdURGRkZdfFxcdUQ4M0RbXFx1REMwMC1cXHVEQzNFXFx1REM0MFxcdURDNDItXFx1RENGQ1xcdURDRkYtXFx1REQzRFxcdURENEItXFx1REQ0RVxcdURENTAtXFx1REQ2N1xcdUREN0FcXHVERDk1XFx1REQ5NlxcdUREQTRcXHVEREZCLVxcdURFNEZcXHVERTgwLVxcdURFQzVcXHVERUNDXFx1REVEMC1cXHVERUQyXFx1REVFQlxcdURFRUNcXHVERUY0LVxcdURFRjhdfFxcdUQ4M0VbXFx1REQxMC1cXHVERDNBXFx1REQzQy1cXHVERDNFXFx1REQ0MC1cXHVERDQ1XFx1REQ0Ny1cXHVERDRDXFx1REQ1MC1cXHVERDZCXFx1REQ4MC1cXHVERDk3XFx1RERDMFxcdURERDAtXFx1RERFNl0pfCg/OlsjXFwqMC05XFx4QTlcXHhBRVxcdTIwM0NcXHUyMDQ5XFx1MjEyMlxcdTIxMzlcXHUyMTk0LVxcdTIxOTlcXHUyMUE5XFx1MjFBQVxcdTIzMUFcXHUyMzFCXFx1MjMyOFxcdTIzQ0ZcXHUyM0U5LVxcdTIzRjNcXHUyM0Y4LVxcdTIzRkFcXHUyNEMyXFx1MjVBQVxcdTI1QUJcXHUyNUI2XFx1MjVDMFxcdTI1RkItXFx1MjVGRVxcdTI2MDAtXFx1MjYwNFxcdTI2MEVcXHUyNjExXFx1MjYxNFxcdTI2MTVcXHUyNjE4XFx1MjYxRFxcdTI2MjBcXHUyNjIyXFx1MjYyM1xcdTI2MjZcXHUyNjJBXFx1MjYyRVxcdTI2MkZcXHUyNjM4LVxcdTI2M0FcXHUyNjQwXFx1MjY0MlxcdTI2NDgtXFx1MjY1M1xcdTI2NjBcXHUyNjYzXFx1MjY2NVxcdTI2NjZcXHUyNjY4XFx1MjY3QlxcdTI2N0ZcXHUyNjkyLVxcdTI2OTdcXHUyNjk5XFx1MjY5QlxcdTI2OUNcXHUyNkEwXFx1MjZBMVxcdTI2QUFcXHUyNkFCXFx1MjZCMFxcdTI2QjFcXHUyNkJEXFx1MjZCRVxcdTI2QzRcXHUyNkM1XFx1MjZDOFxcdTI2Q0VcXHUyNkNGXFx1MjZEMVxcdTI2RDNcXHUyNkQ0XFx1MjZFOVxcdTI2RUFcXHUyNkYwLVxcdTI2RjVcXHUyNkY3LVxcdTI2RkFcXHUyNkZEXFx1MjcwMlxcdTI3MDVcXHUyNzA4LVxcdTI3MERcXHUyNzBGXFx1MjcxMlxcdTI3MTRcXHUyNzE2XFx1MjcxRFxcdTI3MjFcXHUyNzI4XFx1MjczM1xcdTI3MzRcXHUyNzQ0XFx1Mjc0N1xcdTI3NENcXHUyNzRFXFx1Mjc1My1cXHUyNzU1XFx1Mjc1N1xcdTI3NjNcXHUyNzY0XFx1Mjc5NS1cXHUyNzk3XFx1MjdBMVxcdTI3QjBcXHUyN0JGXFx1MjkzNFxcdTI5MzVcXHUyQjA1LVxcdTJCMDdcXHUyQjFCXFx1MkIxQ1xcdTJCNTBcXHUyQjU1XFx1MzAzMFxcdTMwM0RcXHUzMjk3XFx1MzI5OV18XFx1RDgzQ1tcXHVEQzA0XFx1RENDRlxcdURENzBcXHVERDcxXFx1REQ3RVxcdUREN0ZcXHVERDhFXFx1REQ5MS1cXHVERDlBXFx1RERFNi1cXHVEREZGXFx1REUwMVxcdURFMDJcXHVERTFBXFx1REUyRlxcdURFMzItXFx1REUzQVxcdURFNTBcXHVERTUxXFx1REYwMC1cXHVERjIxXFx1REYyNC1cXHVERjkzXFx1REY5NlxcdURGOTdcXHVERjk5LVxcdURGOUJcXHVERjlFLVxcdURGRjBcXHVERkYzLVxcdURGRjVcXHVERkY3LVxcdURGRkZdfFxcdUQ4M0RbXFx1REMwMC1cXHVEQ0ZEXFx1RENGRi1cXHVERDNEXFx1REQ0OS1cXHVERDRFXFx1REQ1MC1cXHVERDY3XFx1REQ2RlxcdURENzBcXHVERDczLVxcdUREN0FcXHVERDg3XFx1REQ4QS1cXHVERDhEXFx1REQ5MFxcdUREOTVcXHVERDk2XFx1RERBNFxcdUREQTVcXHVEREE4XFx1RERCMVxcdUREQjJcXHVEREJDXFx1RERDMi1cXHVEREM0XFx1REREMS1cXHVEREQzXFx1REREQy1cXHVERERFXFx1RERFMVxcdURERTNcXHVEREU4XFx1RERFRlxcdURERjNcXHVEREZBLVxcdURFNEZcXHVERTgwLVxcdURFQzVcXHVERUNCLVxcdURFRDJcXHVERUUwLVxcdURFRTVcXHVERUU5XFx1REVFQlxcdURFRUNcXHVERUYwXFx1REVGMy1cXHVERUY4XXxcXHVEODNFW1xcdUREMTAtXFx1REQzQVxcdUREM0MtXFx1REQzRVxcdURENDAtXFx1REQ0NVxcdURENDctXFx1REQ0Q1xcdURENTAtXFx1REQ2QlxcdUREODAtXFx1REQ5N1xcdUREQzBcXHVEREQwLVxcdURERTZdKVxcdUZFMEYpLztmdW5jdGlvbiBrKEQpe3JldHVybiBlLmdldENvbXB1dGVkU3R5bGUoRCl9ZnVuY3Rpb24gbihELHUpe3ZhciBlO3JldHVybiBpKEQpP0Q6XCJzdHJpbmdcIj09KGU9dHlwZW9mIEQpJiYhdSYmRD9FLmNhbGwoUS5xdWVyeVNlbGVjdG9yQWxsKEQpLDApOkQmJlwib2JqZWN0XCI9PWUmJlwibGVuZ3RoXCJpbiBEP0UuY2FsbChELDApOkQ/W0RdOltdfWZ1bmN0aW9uIG8oRCl7cmV0dXJuXCJhYnNvbHV0ZVwiPT09RC5wb3NpdGlvbnx8ITA9PT1ELmFic29sdXRlfWZ1bmN0aW9uIHAoRCx1KXtmb3IodmFyIGUsRj11Lmxlbmd0aDstMTwtLUY7KWlmKGU9dVtGXSxELnN1YnN0cigwLGUubGVuZ3RoKT09PWUpcmV0dXJuIGUubGVuZ3RofWZ1bmN0aW9uIHIoRCx1KXt2b2lkIDA9PT1EJiYoRD1cIlwiKTt2YXIgZT1+RC5pbmRleE9mKFwiKytcIiksRj0xO3JldHVybiBlJiYoRD1ELnNwbGl0KFwiKytcIikuam9pbihcIlwiKSksZnVuY3Rpb24oKXtyZXR1cm5cIjxcIit1K1wiIHN0eWxlPSdwb3NpdGlvbjpyZWxhdGl2ZTtkaXNwbGF5OmlubGluZS1ibG9jazsnXCIrKEQ/XCIgY2xhc3M9J1wiK0QrKGU/RisrOlwiXCIpK1wiJz5cIjpcIj5cIil9fWZ1bmN0aW9uIHMoRCx1LGUpe3ZhciBGPUQubm9kZVR5cGU7aWYoMT09PUZ8fDk9PT1GfHwxMT09PUYpZm9yKEQ9RC5maXJzdENoaWxkO0Q7RD1ELm5leHRTaWJsaW5nKXMoRCx1LGUpO2Vsc2UgMyE9PUYmJjQhPT1GfHwoRC5ub2RlVmFsdWU9RC5ub2RlVmFsdWUuc3BsaXQodSkuam9pbihlKSl9ZnVuY3Rpb24gdChELHUpe2Zvcih2YXIgZT11Lmxlbmd0aDstMTwtLWU7KUQucHVzaCh1W2VdKX1mdW5jdGlvbiB1KEQsdSxlKXtmb3IodmFyIEY7RCYmRCE9PXU7KXtpZihGPUQuX25leHR8fEQubmV4dFNpYmxpbmcpcmV0dXJuIEYudGV4dENvbnRlbnQuY2hhckF0KDApPT09ZTtEPUQucGFyZW50Tm9kZXx8RC5fcGFyZW50fX1mdW5jdGlvbiB2KEQpe3ZhciB1LGUsRj1uKEQuY2hpbGROb2RlcyksdD1GLmxlbmd0aDtmb3IodT0wO3U8dDt1KyspKGU9Rlt1XSkuX2lzU3BsaXQ/dihlKToodSYmMz09PWUucHJldmlvdXNTaWJsaW5nLm5vZGVUeXBlP2UucHJldmlvdXNTaWJsaW5nLm5vZGVWYWx1ZSs9Mz09PWUubm9kZVR5cGU/ZS5ub2RlVmFsdWU6ZS5maXJzdENoaWxkLm5vZGVWYWx1ZTozIT09ZS5ub2RlVHlwZSYmRC5pbnNlcnRCZWZvcmUoZS5maXJzdENoaWxkLGUpLEQucmVtb3ZlQ2hpbGQoZSkpfWZ1bmN0aW9uIHcoRCx1KXtyZXR1cm4gcGFyc2VGbG9hdCh1W0RdKXx8MH1mdW5jdGlvbiB4KEQsZSxGLEMsaSxuLEUpe3ZhciByLGwsYSxwLGQsaCxCLGYsQSxjLGcseCx5PWsoRCksYj13KFwicGFkZGluZ0xlZnRcIix5KSxfPS05OTksUz13KFwiYm9yZGVyQm90dG9tV2lkdGhcIix5KSt3KFwiYm9yZGVyVG9wV2lkdGhcIix5KSxUPXcoXCJib3JkZXJMZWZ0V2lkdGhcIix5KSt3KFwiYm9yZGVyUmlnaHRXaWR0aFwiLHkpLE49dyhcInBhZGRpbmdUb3BcIix5KSt3KFwicGFkZGluZ0JvdHRvbVwiLHkpLG09dyhcInBhZGRpbmdMZWZ0XCIseSkrdyhcInBhZGRpbmdSaWdodFwiLHkpLEw9LjIqdyhcImZvbnRTaXplXCIseSksVz15LnRleHRBbGlnbixIPVtdLE89W10sVj1bXSxqPWUud29yZERlbGltaXRlcnx8XCIgXCIsTT1lLnRhZz9lLnRhZzplLnNwYW4/XCJzcGFuXCI6XCJkaXZcIixSPWUudHlwZXx8ZS5zcGxpdHx8XCJjaGFycyx3b3JkcyxsaW5lc1wiLHo9aSYmflIuaW5kZXhPZihcImxpbmVzXCIpP1tdOm51bGwsUD1+Ui5pbmRleE9mKFwid29yZHNcIikscT1+Ui5pbmRleE9mKFwiY2hhcnNcIiksRz1vKGUpLEk9ZS5saW5lc0NsYXNzLEo9fihJfHxcIlwiKS5pbmRleE9mKFwiKytcIiksSz1bXTtmb3IoSiYmKEk9SS5zcGxpdChcIisrXCIpLmpvaW4oXCJcIikpLGE9KGw9RC5nZXRFbGVtZW50c0J5VGFnTmFtZShcIipcIikpLmxlbmd0aCxkPVtdLHI9MDtyPGE7cisrKWRbcl09bFtyXTtpZih6fHxHKWZvcihyPTA7cjxhO3IrKykoKGg9KHA9ZFtyXSkucGFyZW50Tm9kZT09PUQpfHxHfHxxJiYhUCkmJih4PXAub2Zmc2V0VG9wLHomJmgmJk1hdGguYWJzKHgtXyk+TCYmKFwiQlJcIiE9PXAubm9kZU5hbWV8fDA9PT1yKSYmKEI9W10sei5wdXNoKEIpLF89eCksRyYmKHAuX3g9cC5vZmZzZXRMZWZ0LHAuX3k9eCxwLl93PXAub2Zmc2V0V2lkdGgscC5faD1wLm9mZnNldEhlaWdodCkseiYmKChwLl9pc1NwbGl0JiZofHwhcSYmaHx8UCYmaHx8IVAmJnAucGFyZW50Tm9kZS5wYXJlbnROb2RlPT09RCYmIXAucGFyZW50Tm9kZS5faXNTcGxpdCkmJihCLnB1c2gocCkscC5feC09Yix1KHAsRCxqKSYmKHAuX3dvcmRFbmQ9ITApKSxcIkJSXCI9PT1wLm5vZGVOYW1lJiYocC5uZXh0U2libGluZyYmXCJCUlwiPT09cC5uZXh0U2libGluZy5ub2RlTmFtZXx8MD09PXIpJiZ6LnB1c2goW10pKSk7Zm9yKHI9MDtyPGE7cisrKWg9KHA9ZFtyXSkucGFyZW50Tm9kZT09PUQsXCJCUlwiIT09cC5ub2RlTmFtZT8oRyYmKEE9cC5zdHlsZSxQfHxofHwocC5feCs9cC5wYXJlbnROb2RlLl94LHAuX3krPXAucGFyZW50Tm9kZS5feSksQS5sZWZ0PXAuX3grXCJweFwiLEEudG9wPXAuX3krXCJweFwiLEEucG9zaXRpb249XCJhYnNvbHV0ZVwiLEEuZGlzcGxheT1cImJsb2NrXCIsQS53aWR0aD1wLl93KzErXCJweFwiLEEuaGVpZ2h0PXAuX2grXCJweFwiKSwhUCYmcT9wLl9pc1NwbGl0PyhwLl9uZXh0PXAubmV4dFNpYmxpbmcscC5wYXJlbnROb2RlLmFwcGVuZENoaWxkKHApKTpwLnBhcmVudE5vZGUuX2lzU3BsaXQ/KHAuX3BhcmVudD1wLnBhcmVudE5vZGUsIXAucHJldmlvdXNTaWJsaW5nJiZwLmZpcnN0Q2hpbGQmJihwLmZpcnN0Q2hpbGQuX2lzRmlyc3Q9ITApLHAubmV4dFNpYmxpbmcmJlwiIFwiPT09cC5uZXh0U2libGluZy50ZXh0Q29udGVudCYmIXAubmV4dFNpYmxpbmcubmV4dFNpYmxpbmcmJksucHVzaChwLm5leHRTaWJsaW5nKSxwLl9uZXh0PXAubmV4dFNpYmxpbmcmJnAubmV4dFNpYmxpbmcuX2lzRmlyc3Q/bnVsbDpwLm5leHRTaWJsaW5nLHAucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChwKSxkLnNwbGljZShyLS0sMSksYS0tKTpofHwoeD0hcC5uZXh0U2libGluZyYmdShwLnBhcmVudE5vZGUsRCxqKSxwLnBhcmVudE5vZGUuX3BhcmVudCYmcC5wYXJlbnROb2RlLl9wYXJlbnQuYXBwZW5kQ2hpbGQocCkseCYmcC5wYXJlbnROb2RlLmFwcGVuZENoaWxkKFEuY3JlYXRlVGV4dE5vZGUoXCIgXCIpKSxcInNwYW5cIj09PU0mJihwLnN0eWxlLmRpc3BsYXk9XCJpbmxpbmVcIiksSC5wdXNoKHApKTpwLnBhcmVudE5vZGUuX2lzU3BsaXQmJiFwLl9pc1NwbGl0JiZcIlwiIT09cC5pbm5lckhUTUw/Ty5wdXNoKHApOnEmJiFwLl9pc1NwbGl0JiYoXCJzcGFuXCI9PT1NJiYocC5zdHlsZS5kaXNwbGF5PVwiaW5saW5lXCIpLEgucHVzaChwKSkpOnp8fEc/KHAucGFyZW50Tm9kZSYmcC5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHApLGQuc3BsaWNlKHItLSwxKSxhLS0pOlB8fEQuYXBwZW5kQ2hpbGQocCk7Zm9yKHI9Sy5sZW5ndGg7LTE8LS1yOylLW3JdLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoS1tyXSk7aWYoeil7Zm9yKEcmJihjPVEuY3JlYXRlRWxlbWVudChNKSxELmFwcGVuZENoaWxkKGMpLGc9Yy5vZmZzZXRXaWR0aCtcInB4XCIseD1jLm9mZnNldFBhcmVudD09PUQ/MDpELm9mZnNldExlZnQsRC5yZW1vdmVDaGlsZChjKSksQT1ELnN0eWxlLmNzc1RleHQsRC5zdHlsZS5jc3NUZXh0PVwiZGlzcGxheTpub25lO1wiO0QuZmlyc3RDaGlsZDspRC5yZW1vdmVDaGlsZChELmZpcnN0Q2hpbGQpO2ZvcihmPVwiIFwiPT09aiYmKCFHfHwhUCYmIXEpLHI9MDtyPHoubGVuZ3RoO3IrKyl7Zm9yKEI9eltyXSwoYz1RLmNyZWF0ZUVsZW1lbnQoTSkpLnN0eWxlLmNzc1RleHQ9XCJkaXNwbGF5OmJsb2NrO3RleHQtYWxpZ246XCIrVytcIjtwb3NpdGlvbjpcIisoRz9cImFic29sdXRlO1wiOlwicmVsYXRpdmU7XCIpLEkmJihjLmNsYXNzTmFtZT1JKyhKP3IrMTpcIlwiKSksVi5wdXNoKGMpLGE9Qi5sZW5ndGgsbD0wO2w8YTtsKyspXCJCUlwiIT09QltsXS5ub2RlTmFtZSYmKHA9QltsXSxjLmFwcGVuZENoaWxkKHApLGYmJnAuX3dvcmRFbmQmJmMuYXBwZW5kQ2hpbGQoUS5jcmVhdGVUZXh0Tm9kZShcIiBcIikpLEcmJigwPT09bCYmKGMuc3R5bGUudG9wPXAuX3krXCJweFwiLGMuc3R5bGUubGVmdD1iK3grXCJweFwiKSxwLnN0eWxlLnRvcD1cIjBweFwiLHgmJihwLnN0eWxlLmxlZnQ9cC5feC14K1wicHhcIikpKTswPT09YT9jLmlubmVySFRNTD1cIiZuYnNwO1wiOlB8fHF8fCh2KGMpLHMoYyxTdHJpbmcuZnJvbUNoYXJDb2RlKDE2MCksXCIgXCIpKSxHJiYoYy5zdHlsZS53aWR0aD1nLGMuc3R5bGUuaGVpZ2h0PXAuX2grXCJweFwiKSxELmFwcGVuZENoaWxkKGMpfUQuc3R5bGUuY3NzVGV4dD1BfUcmJihFPkQuY2xpZW50SGVpZ2h0JiYoRC5zdHlsZS5oZWlnaHQ9RS1OK1wicHhcIixELmNsaWVudEhlaWdodDxFJiYoRC5zdHlsZS5oZWlnaHQ9RStTK1wicHhcIikpLG4+RC5jbGllbnRXaWR0aCYmKEQuc3R5bGUud2lkdGg9bi1tK1wicHhcIixELmNsaWVudFdpZHRoPG4mJihELnN0eWxlLndpZHRoPW4rVCtcInB4XCIpKSksdChGLEgpLFAmJnQoQyxPKSx0KGksVil9ZnVuY3Rpb24geShELHUsZSxGKXt2YXIgdCxDLGksbixFLHIsbCxhLGQ9dS50YWc/dS50YWc6dS5zcGFuP1wic3BhblwiOlwiZGl2XCIsaD1+KHUudHlwZXx8dS5zcGxpdHx8XCJjaGFycyx3b3JkcyxsaW5lc1wiKS5pbmRleE9mKFwiY2hhcnNcIiksQj1vKHUpLGY9dS53b3JkRGVsaW1pdGVyfHxcIiBcIixBPVwiIFwiIT09Zj9cIlwiOkI/XCImIzE3MzsgXCI6XCIgXCIsYz1cIjwvXCIrZCtcIj5cIixnPTEseD11LnNwZWNpYWxDaGFycz9cImZ1bmN0aW9uXCI9PXR5cGVvZiB1LnNwZWNpYWxDaGFycz91LnNwZWNpYWxDaGFyczpwOm51bGwseT1RLmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiksdj1ELnBhcmVudE5vZGU7Zm9yKHYuaW5zZXJ0QmVmb3JlKHksRCkseS50ZXh0Q29udGVudD1ELm5vZGVWYWx1ZSx2LnJlbW92ZUNoaWxkKEQpLGw9LTEhPT0odD1mdW5jdGlvbiBnZXRUZXh0KEQpe3ZhciB1PUQubm9kZVR5cGUsZT1cIlwiO2lmKDE9PT11fHw5PT09dXx8MTE9PT11KXtpZihcInN0cmluZ1wiPT10eXBlb2YgRC50ZXh0Q29udGVudClyZXR1cm4gRC50ZXh0Q29udGVudDtmb3IoRD1ELmZpcnN0Q2hpbGQ7RDtEPUQubmV4dFNpYmxpbmcpZSs9Z2V0VGV4dChEKX1lbHNlIGlmKDM9PT11fHw0PT09dSlyZXR1cm4gRC5ub2RlVmFsdWU7cmV0dXJuIGV9KEQ9eSkpLmluZGV4T2YoXCI8XCIpLCExIT09dS5yZWR1Y2VXaGl0ZVNwYWNlJiYodD10LnJlcGxhY2UoUyxcIiBcIikucmVwbGFjZShfLFwiXCIpKSxsJiYodD10LnNwbGl0KFwiPFwiKS5qb2luKFwie3tMVH19XCIpKSxFPXQubGVuZ3RoLEM9KFwiIFwiPT09dC5jaGFyQXQoMCk/QTpcIlwiKStlKCksaT0wO2k8RTtpKyspaWYocj10LmNoYXJBdChpKSx4JiYoYT14KHQuc3Vic3RyKGkpLHUuc3BlY2lhbENoYXJzKSkpcj10LnN1YnN0cihpLGF8fDEpLEMrPWgmJlwiIFwiIT09cj9GKCkrcitcIjwvXCIrZCtcIj5cIjpyLGkrPWEtMTtlbHNlIGlmKHI9PT1mJiZ0LmNoYXJBdChpLTEpIT09ZiYmaSl7Zm9yKEMrPWc/YzpcIlwiLGc9MDt0LmNoYXJBdChpKzEpPT09ZjspQys9QSxpKys7aT09PUUtMT9DKz1BOlwiKVwiIT09dC5jaGFyQXQoaSsxKSYmKEMrPUErZSgpLGc9MSl9ZWxzZVwie1wiPT09ciYmXCJ7e0xUfX1cIj09PXQuc3Vic3RyKGksNik/KEMrPWg/RigpK1wie3tMVH19PC9cIitkK1wiPlwiOlwie3tMVH19XCIsaSs9NSk6NTUyOTY8PXIuY2hhckNvZGVBdCgwKSYmci5jaGFyQ29kZUF0KDApPD01NjMxOXx8NjUwMjQ8PXQuY2hhckNvZGVBdChpKzEpJiZ0LmNoYXJDb2RlQXQoaSsxKTw9NjUwMzk/KG49KCh0LnN1YnN0cihpLDEyKS5zcGxpdChiKXx8W10pWzFdfHxcIlwiKS5sZW5ndGh8fDIsQys9aCYmXCIgXCIhPT1yP0YoKSt0LnN1YnN0cihpLG4pK1wiPC9cIitkK1wiPlwiOnQuc3Vic3RyKGksbiksaSs9bi0xKTpDKz1oJiZcIiBcIiE9PXI/RigpK3IrXCI8L1wiK2QrXCI+XCI6cjtELm91dGVySFRNTD1DKyhnP2M6XCJcIiksbCYmcyh2LFwie3tMVH19XCIsXCI8XCIpfWZ1bmN0aW9uIHooRCx1LGUsRil7dmFyIHQsQyxpPW4oRC5jaGlsZE5vZGVzKSxFPWkubGVuZ3RoLHM9byh1KTtpZigzIT09RC5ub2RlVHlwZXx8MTxFKXtmb3IodS5hYnNvbHV0ZT0hMSx0PTA7dDxFO3QrKykzPT09KEM9aVt0XSkubm9kZVR5cGUmJiEvXFxTKy8udGVzdChDLm5vZGVWYWx1ZSl8fChzJiYzIT09Qy5ub2RlVHlwZSYmXCJpbmxpbmVcIj09PWsoQykuZGlzcGxheSYmKEMuc3R5bGUuZGlzcGxheT1cImlubGluZS1ibG9ja1wiLEMuc3R5bGUucG9zaXRpb249XCJyZWxhdGl2ZVwiKSxDLl9pc1NwbGl0PSEwLHooQyx1LGUsRikpO3JldHVybiB1LmFic29sdXRlPXMsdm9pZChELl9pc1NwbGl0PSEwKX15KEQsdSxlLEYpfXZhciBRLGUsRixDLF89Lyg/OlxccnxcXG58XFx0XFx0KS9nLFM9Lyg/Olxcc1xccyspL2csaT1BcnJheS5pc0FycmF5LEU9W10uc2xpY2UsbD0oKEM9U3BsaXRUZXh0LnByb3RvdHlwZSkuc3BsaXQ9ZnVuY3Rpb24gc3BsaXQoRCl7dGhpcy5pc1NwbGl0JiZ0aGlzLnJldmVydCgpLHRoaXMudmFycz1EPUR8fHRoaXMudmFycyx0aGlzLl9vcmlnaW5hbHMubGVuZ3RoPXRoaXMuY2hhcnMubGVuZ3RoPXRoaXMud29yZHMubGVuZ3RoPXRoaXMubGluZXMubGVuZ3RoPTA7Zm9yKHZhciB1LGUsRix0PXRoaXMuZWxlbWVudHMubGVuZ3RoLEM9RC50YWc/RC50YWc6RC5zcGFuP1wic3BhblwiOlwiZGl2XCIsaT1yKEQud29yZHNDbGFzcyxDKSxuPXIoRC5jaGFyc0NsYXNzLEMpOy0xPC0tdDspRj10aGlzLmVsZW1lbnRzW3RdLHRoaXMuX29yaWdpbmFsc1t0XT1GLmlubmVySFRNTCx1PUYuY2xpZW50SGVpZ2h0LGU9Ri5jbGllbnRXaWR0aCx6KEYsRCxpLG4pLHgoRixELHRoaXMuY2hhcnMsdGhpcy53b3Jkcyx0aGlzLmxpbmVzLGUsdSk7cmV0dXJuIHRoaXMuY2hhcnMucmV2ZXJzZSgpLHRoaXMud29yZHMucmV2ZXJzZSgpLHRoaXMubGluZXMucmV2ZXJzZSgpLHRoaXMuaXNTcGxpdD0hMCx0aGlzfSxDLnJldmVydD1mdW5jdGlvbiByZXZlcnQoKXt2YXIgZT10aGlzLl9vcmlnaW5hbHM7aWYoIWUpdGhyb3dcInJldmVydCgpIGNhbGwgd2Fzbid0IHNjb3BlZCBwcm9wZXJseS5cIjtyZXR1cm4gdGhpcy5lbGVtZW50cy5mb3JFYWNoKGZ1bmN0aW9uKEQsdSl7cmV0dXJuIEQuaW5uZXJIVE1MPWVbdV19KSx0aGlzLmNoYXJzPVtdLHRoaXMud29yZHM9W10sdGhpcy5saW5lcz1bXSx0aGlzLmlzU3BsaXQ9ITEsdGhpc30sU3BsaXRUZXh0LmNyZWF0ZT1mdW5jdGlvbiBjcmVhdGUoRCx1KXtyZXR1cm4gbmV3IFNwbGl0VGV4dChELHUpfSxTcGxpdFRleHQpO2Z1bmN0aW9uIFNwbGl0VGV4dChELHUpe0Z8fGZ1bmN0aW9uIF9pbml0Q29yZSgpe1E9ZG9jdW1lbnQsZT13aW5kb3csRj0xfSgpLHRoaXMuZWxlbWVudHM9bihEKSx0aGlzLmNoYXJzPVtdLHRoaXMud29yZHM9W10sdGhpcy5saW5lcz1bXSx0aGlzLl9vcmlnaW5hbHM9W10sdGhpcy52YXJzPXV8fHt9LHRoaXMuc3BsaXQodSl9bC52ZXJzaW9uPVwiMy4zLjRcIixELlNwbGl0VGV4dD1sLEQuZGVmYXVsdD1sO2lmICh0eXBlb2Yod2luZG93KT09PVwidW5kZWZpbmVkXCJ8fHdpbmRvdyE9PUQpe09iamVjdC5kZWZpbmVQcm9wZXJ0eShELFwiX19lc01vZHVsZVwiLHt2YWx1ZTohMH0pfSBlbHNlIHtkZWxldGUgRC5kZWZhdWx0fX0pO1xuXG4iLCJ2YXIgbWFwID0ge1xuXHRcIi4vX3NwbGl0VGV4dC5qc1wiOiBcIi4vc3JjL2pzL21vZHVsZXMvX3NwbGl0VGV4dC5qc1wiLFxuXHRcIi4vYWNjb3JkaWFuLmpzXCI6IFwiLi9zcmMvanMvbW9kdWxlcy9hY2NvcmRpYW4uanNcIixcblx0XCIuL2FuaW1hdGlvbnMuanNcIjogXCIuL3NyYy9qcy9tb2R1bGVzL2FuaW1hdGlvbnMuanNcIixcblx0XCIuL2JhY2tCdXR0b24uanNcIjogXCIuL3NyYy9qcy9tb2R1bGVzL2JhY2tCdXR0b24uanNcIixcblx0XCIuL2ZlYXR1cmVkUHJvZHVjdHMuanNcIjogXCIuL3NyYy9qcy9tb2R1bGVzL2ZlYXR1cmVkUHJvZHVjdHMuanNcIixcblx0XCIuL2Zvcm1zRGVtby5qc1wiOiBcIi4vc3JjL2pzL21vZHVsZXMvZm9ybXNEZW1vLmpzXCIsXG5cdFwiLi9mb3Jtc0VtYWlsLmpzXCI6IFwiLi9zcmMvanMvbW9kdWxlcy9mb3Jtc0VtYWlsLmpzXCIsXG5cdFwiLi9mb3Jtc1RyaWFsLmpzXCI6IFwiLi9zcmMvanMvbW9kdWxlcy9mb3Jtc1RyaWFsLmpzXCIsXG5cdFwiLi9nb29nbGVNYXAuanNcIjogXCIuL3NyYy9qcy9tb2R1bGVzL2dvb2dsZU1hcC5qc1wiLFxuXHRcIi4vaGVhZGVyLmpzXCI6IFwiLi9zcmMvanMvbW9kdWxlcy9oZWFkZXIuanNcIixcblx0XCIuL2hlbGxvd29ybGQuanNcIjogXCIuL3NyYy9qcy9tb2R1bGVzL2hlbGxvd29ybGQuanNcIixcblx0XCIuL2ltYWdlTW9kYWwuanNcIjogXCIuL3NyYy9qcy9tb2R1bGVzL2ltYWdlTW9kYWwuanNcIixcblx0XCIuL2ludHJvU2xpZGVyLmpzXCI6IFwiLi9zcmMvanMvbW9kdWxlcy9pbnRyb1NsaWRlci5qc1wiLFxuXHRcIi4vaW52aWV3LmpzXCI6IFwiLi9zcmMvanMvbW9kdWxlcy9pbnZpZXcuanNcIixcblx0XCIuL2xvZ29Db2xvdXIuanNcIjogXCIuL3NyYy9qcy9tb2R1bGVzL2xvZ29Db2xvdXIuanNcIixcblx0XCIuL25ld3NMaWdodGJveC5qc1wiOiBcIi4vc3JjL2pzL21vZHVsZXMvbmV3c0xpZ2h0Ym94LmpzXCIsXG5cdFwiLi9wYXJhbVN0b3JhZ2UuanNcIjogXCIuL3NyYy9qcy9tb2R1bGVzL3BhcmFtU3RvcmFnZS5qc1wiLFxuXHRcIi4vcGluSW1hZ2UuanNcIjogXCIuL3NyYy9qcy9tb2R1bGVzL3BpbkltYWdlLmpzXCIsXG5cdFwiLi9wcmljaW5nLmpzXCI6IFwiLi9zcmMvanMvbW9kdWxlcy9wcmljaW5nLmpzXCIsXG5cdFwiLi9wcm9jZXNzSW54ZHJGb3JtLmpzXCI6IFwiLi9zcmMvanMvbW9kdWxlcy9wcm9jZXNzSW54ZHJGb3JtLmpzXCIsXG5cdFwiLi9wcm9kdWN0U2lkZWJhci5qc1wiOiBcIi4vc3JjL2pzL21vZHVsZXMvcHJvZHVjdFNpZGViYXIuanNcIixcblx0XCIuL3F1aWNrbGlua3MuanNcIjogXCIuL3NyYy9qcy9tb2R1bGVzL3F1aWNrbGlua3MuanNcIixcblx0XCIuL3Njcm9sbFRvLmpzXCI6IFwiLi9zcmMvanMvbW9kdWxlcy9zY3JvbGxUby5qc1wiLFxuXHRcIi4vc2VydmljZVRhYnMuanNcIjogXCIuL3NyYy9qcy9tb2R1bGVzL3NlcnZpY2VUYWJzLmpzXCIsXG5cdFwiLi9zdGF0aXN0aWMuanNcIjogXCIuL3NyYy9qcy9tb2R1bGVzL3N0YXRpc3RpYy5qc1wiLFxuXHRcIi4vc3RpY2t5TG9nby5qc1wiOiBcIi4vc3JjL2pzL21vZHVsZXMvc3RpY2t5TG9nby5qc1wiLFxuXHRcIi4vdGVzdGltb25pYWxzLmpzXCI6IFwiLi9zcmMvanMvbW9kdWxlcy90ZXN0aW1vbmlhbHMuanNcIlxufTtcblxuXG5mdW5jdGlvbiB3ZWJwYWNrQ29udGV4dChyZXEpIHtcblx0dmFyIGlkID0gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSk7XG5cdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKGlkKTtcbn1cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0UmVzb2x2ZShyZXEpIHtcblx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhtYXAsIHJlcSkpIHtcblx0XHR2YXIgZSA9IG5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIgKyByZXEgKyBcIidcIik7XG5cdFx0ZS5jb2RlID0gJ01PRFVMRV9OT1RfRk9VTkQnO1xuXHRcdHRocm93IGU7XG5cdH1cblx0cmV0dXJuIG1hcFtyZXFdO1xufVxud2VicGFja0NvbnRleHQua2V5cyA9IGZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0S2V5cygpIHtcblx0cmV0dXJuIE9iamVjdC5rZXlzKG1hcCk7XG59O1xud2VicGFja0NvbnRleHQucmVzb2x2ZSA9IHdlYnBhY2tDb250ZXh0UmVzb2x2ZTtcbm1vZHVsZS5leHBvcnRzID0gd2VicGFja0NvbnRleHQ7XG53ZWJwYWNrQ29udGV4dC5pZCA9IFwiLi9zcmMvanMvbW9kdWxlcyBzeW5jIHJlY3Vyc2l2ZSBcXFxcLihqcykkL1wiOyIsImltcG9ydCBnc2FwIGZyb20gJ2dzYXAnXG5pbXBvcnQgU3BsaXRUZXh0IGZyb20gJy4uL2xpYnJhcmllcy9TcGxpdFRleHQubWluLmpzJ1xuXG5nc2FwLnJlZ2lzdGVyUGx1Z2luKFNwbGl0VGV4dClcblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gaW5pdFNwbGl0VGV4dCgpIHtcblxuICBjb25zdCBsaW5lc1RvU3BsaXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCdbZGF0YS1zcGxpdC1saW5lc10nKVxuXG4gIGNvbnN0IGxpbmVTcGxpdCA9IG5ldyBTcGxpdFRleHQobGluZXNUb1NwbGl0LCB7IHR5cGU6ICdsaW5lcycsIGxpbmVzQ2xhc3M6ICdsaW5lJyB9KVxuXG4gIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLCAoKSA9PiB7XG4gICAgbGluZVNwbGl0LnNwbGl0KClcbiAgfSlcblxufVxuIiwiaW1wb3J0IGdzYXAgZnJvbSAnZ3NhcCdcblxuY2xhc3MgQWNjb3JkaWFuIHtcbiAgY29uc3RydWN0b3IoZWwpIHtcbiAgICBcbiAgICB0aGlzLmVsID0gZWxcbiAgICB0aGlzLml0ZW1zID0gQXJyYXkuZnJvbShlbC5xdWVyeVNlbGVjdG9yQWxsKCdbZGF0YS1hY2NvcmRpYW4taXRlbV0nKSlcbiAgICB0aGlzLnRpdGxlcyA9IEFycmF5LmZyb20oZWwucXVlcnlTZWxlY3RvckFsbCgnW2RhdGEtYWNjb3JkaWFuLXRpdGxlXScpKVxuICAgIHRoaXMud2luZG93V2lkdGggPSB3aW5kb3cuaW5uZXJXaWR0aFxuXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRoaXMub25SZXNpemUuYmluZCh0aGlzKSlcblxuICAgIGlmIChlbC5kYXRhc2V0LmFjY29yZGlhbiA9PT0gJ21vYmlsZScpIHtcbiAgICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8PSA3NTApIHRoaXMuc2V0dXAoKVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNldHVwKClcbiAgICB9XG5cbiAgfVxuICBzZXR1cCgpIHtcblxuICAgIHRoaXMuaXRlbUNsaWNrTGlzdGVuZXIgPSB0aGlzLnRvZ2dsZUl0ZW0uYmluZCh0aGlzKVxuXG4gICAgZG9jdW1lbnQuZm9udHMucmVhZHkudGhlbigoKSA9PiB7XG4gICAgICB0aGlzLml0ZW1zLmZvckVhY2goKGVsLCBpKSA9PiB7XG4gICAgICAgIGVsLnN0eWxlLmhlaWdodCA9IHRoaXMudGl0bGVzW2ldLm9mZnNldEhlaWdodCArICdweCdcbiAgICAgICAgZWwuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCB0aGlzLml0ZW1DbGlja0xpc3RlbmVyKVxuICAgICAgfSlcbiAgICB9KVxuXG4gIH1cbiAgdG9nZ2xlSXRlbShlKSB7XG5cbiAgICBjb25zdCBpbmRleCA9IHRoaXMuaXRlbXMuaW5kZXhPZihlLmN1cnJlbnRUYXJnZXQpXG5cbiAgICBpZiAoZS5jdXJyZW50VGFyZ2V0LmNsYXNzTGlzdC5jb250YWlucygnanMtb3BlbicpKSB7XG5cbiAgICAgIGdzYXAudG8odGhpcy5pdGVtc1tpbmRleF0sIHsgZHVyYXRpb246IDAuNCwgaGVpZ2h0OiB0aGlzLnRpdGxlc1tpbmRleF0ub2Zmc2V0SGVpZ2h0ICsgJ3B4JywgZWFzZTogJ3Bvd2VyMS5pbk91dCcgfSlcbiAgICAgIGUuY3VycmVudFRhcmdldC5jbGFzc0xpc3QucmVtb3ZlKCdqcy1vcGVuJylcblxuICAgIH0gZWxzZSB7XG5cbiAgICAgIGdzYXAudG8odGhpcy5pdGVtc1tpbmRleF0sIHsgZHVyYXRpb246IDAuNCwgaGVpZ2h0OiAnYXV0bycsIGVhc2U6ICdwb3dlcjEuaW5PdXQnIH0pXG4gICAgICBlLmN1cnJlbnRUYXJnZXQuY2xhc3NMaXN0LmFkZCgnanMtb3BlbicpXG5cbiAgICB9XG4gIH1cbiAgb25SZXNpemUoZSkge1xuXG4gICAgaWYgKGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRXaWR0aCA9PT0gdGhpcy53aW5kb3dXaWR0aCkgcmV0dXJuXG5cbiAgICBpZiAodGhpcy5lbC5kYXRhc2V0LmFjY29yZGlhbiA9PT0gJ21vYmlsZScgJiYgd2luZG93LmlubmVyV2lkdGggPD0gNzUwKSB7XG5cbiAgICAgIHRoaXMuc2V0dXAoKVxuXG4gICAgfSBlbHNlIGlmICh0aGlzLmVsLmRhdGFzZXQuYWNjb3JkaWFuID09PSAnbW9iaWxlJykge1xuXG4gICAgICB0aGlzLmRlc3Ryb3koKVxuXG4gICAgfSBlbHNlIHtcblxuICAgICAgdGhpcy5pdGVtcy5mb3JFYWNoKChlbCwgaSkgPT4ge1xuICAgICAgICBnc2FwLnRvKGVsLCB7IGR1cmF0aW9uOiAwLjQsIGhlaWdodDogdGhpcy50aXRsZXNbaV0ub2Zmc2V0SGVpZ2h0ICsgJ3B4JywgZWFzZTogJ3Bvd2VyMS5pbk91dCcgfSlcbiAgICAgICAgZWwuY2xhc3NMaXN0LnJlbW92ZSgnb3BlbicpXG4gICAgICB9KVxuXG4gICAgfVxuICB9XG4gIGRlc3Ryb3koKSB7XG5cbiAgICB0aGlzLml0ZW1zLmZvckVhY2goZWwgPT4ge1xuICAgICAgZWwuc3R5bGUuaGVpZ2h0ID0gJ2F1dG8nXG4gICAgICBlbC5yZW1vdmVFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMuaXRlbUNsaWNrTGlzdGVuZXIpXG4gICAgfSlcblxuICB9XG59XG5cbmNvbnN0IGluaXRBY2NvcmRpYW4gPSAoKSA9PiB7XG5cbiAgY29uc3QgYWNjb3JkaWFucyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJ1tkYXRhLWFjY29yZGlhbl0nKVxuICBcbiAgaWYgKCFhY2NvcmRpYW5zKSByZXR1cm5cblxuICBhY2NvcmRpYW5zLmZvckVhY2goZWwgPT4ge1xuICAgIG5ldyBBY2NvcmRpYW4oZWwpXG4gIH0pXG5cbn1cblxuZXhwb3J0IGRlZmF1bHQgaW5pdEFjY29yZGlhbiIsImltcG9ydCBnc2FwIGZyb20gJ2dzYXAnXG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGFuaW1hdGlvbnMoKSB7IH1cblxuY29uc3Qgc2V0dXBMb2dvcyA9ICgpID0+IHtcblxuICBnc2FwLnNldCgnLmNsaWVudExvZ29zX19pdGVtIGltZycsIHsgc2NhbGU6IDAgfSlcblxufVxuXG5zZXR1cExvZ29zKCkgXG5cbmV4cG9ydCBmdW5jdGlvbiBoZXJvQW5pbShlbCkge1xuIFxuICBpZiAoIWVsKSByZXR1cm5cblxuICBjb25zdCB0bCA9IGdzYXAudGltZWxpbmUoeyBvbkNvbXBsZXRlOiAoKSA9PiB7XG4gICAgZWwuY2xhc3NMaXN0LmFkZCgnY29tcGxldGUnKVxuICB9fSlcblxuICBpZiAod2luZG93LmlubmVyV2lkdGggPiA5NjApIHtcbiAgICB0bC50byhbJy5oZWFkZXJfX25hdiAubWVudS1pdGVtJywgJy5oZWFkZXJfX2FjY291bnQnXSwgeyBkdXJhdGlvbjogMSwgb3BhY2l0eTogMSwgeTogJzAlJywgc3RhZ2dlcjogMC4wNCwgZWFzZTogJ3Bvd2VyMi5vdXQnIH0pXG4gICAgdGwudG8oWycuZml4ZWRMb2dvX19sb2dvJ10sIHsgZHVyYXRpb246IDAuOCwgeTogJzAlJywgb3BhY2l0eTogMSwgc3RhZ2dlcjogMC4wOCwgZWFzZTogJ3Bvd2VyMi5vdXQnIH0sICctPTEuMicpXG4gICAgdGwudG8oJy5oZXJvX19iZycsIHsgZHVyYXRpb246IDEuMCwgaGVpZ2h0OiAnYXV0bycsIGVhc2U6ICdwb3dlcjIub3V0JyB9LCAnLT0xLjAnKVxuICB9XG5cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGNsaWVudHNBbmltKGVsKSB7XG5cbiAgaWYgKCFlbCkgcmV0dXJuXG5cbiAgY29uc3QgbGluZXMgPSBlbC5xdWVyeVNlbGVjdG9yQWxsKCcubGluZScpXG4gIGNvbnN0IGxvZ29zID0gQXJyYXkuZnJvbShlbC5xdWVyeVNlbGVjdG9yQWxsKCdpbWcnKSlcbiAgY29uc3QgcmFuZG9taXNlTG9nb3MgPSBsb2dvcy5zb3J0KCgpID0+IE1hdGgucmFuZG9tKCkgLSAwLjUpXG5cbiAgY29uc3QgdGwgPSBnc2FwLnRpbWVsaW5lKHsgb25Db21wbGV0ZTogKCkgPT4ge1xuICAgIGVsLmNsYXNzTGlzdC5hZGQoJ2NvbXBsZXRlJylcbiAgfX0pXG5cbiAgdGwudG8obGluZXMsIHsgZHVyYXRpb246IDAuOCwgeTogJzAlJywgb3BhY2l0eTogMSwgc3RhZ2dlcjogMC4wOCwgZWFzZTogJ3Bvd2VyMi5vdXQnIH0pXG4gIHRsLnRvKHJhbmRvbWlzZUxvZ29zLCB7IGR1cmF0aW9uOiAxLCBzY2FsZTogMSwgc3RhZ2dlcjogMC4xLCBlYXNlOiAnYmFjay5vdXQoMS40KScgfSwgJy09MC43JylcblxufVxuXG5leHBvcnQgZnVuY3Rpb24gdGVzdGltb25pYWxzQW5pbShlbCkge1xuXG4gIGlmICghZWwpIHJldHVyblxuXG4gIGNvbnN0IGl0ZW1zID0gZWwucXVlcnlTZWxlY3RvckFsbCgnLnRlc3RpbW9uaWFsc19faXRlbScpXG5cbiAgZ3NhcC50byhpdGVtcywgeyBkdXJhdGlvbjogMS42LCBkZWxheTogMC40LCB5OiAnMCUnLCBvcGFjaXR5OiAxLCBzY2FsZTogMSwgc3RhZ2dlcjogMC4wOCwgZWFzZTogJ2VsYXN0aWMub3V0KDEuMiwgMSknIH0pXG5cbn0iLCJjb25zdCBiYWNrQnV0dG9uID0gKCkgPT4ge1xuXG4gIGNvbnN0IGJ1dHRvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5pbnRyb1Bvc3RfX2JhY2snKVxuXG4gIGlmICghYnV0dG9uKSByZXR1cm5cblxuICBidXR0b24uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoZSkgPT4ge1xuICAgIGUucHJldmVudERlZmF1bHQoKVxuXG4gICAgaWYgKHdpbmRvdy5oaXN0b3J5Lmxlbmd0aCA+IDEgJiYgZG9jdW1lbnQucmVmZXJyZXIuaW5jbHVkZXMoJ2VuY29kaWFuJykpIHtcbiAgICAgIGhpc3RvcnkuYmFjaygpXG4gICAgfSBlbHNlIHtcbiAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gJy9ibG9nJztcbiAgICB9XG5cbiAgfSlcblxufVxuXG5leHBvcnQgZGVmYXVsdCBiYWNrQnV0dG9uIiwiaW1wb3J0IGdzYXAgZnJvbSAnZ3NhcCdcbmltcG9ydCBGbGlja2l0eSBmcm9tICdmbGlja2l0eSdcbmltcG9ydCB7IFNjcm9sbFRyaWdnZXIgfSBmcm9tICdnc2FwL1Njcm9sbFRyaWdnZXInXG5pbXBvcnQgeyBNb3JwaFNWR1BsdWdpbiB9IGZyb20gJy4uL2xpYnJhcmllcy9Nb3JwaFNWR1BsdWdpbi5taW4uanMnXG5cbmdzYXAucmVnaXN0ZXJQbHVnaW4oU2Nyb2xsVHJpZ2dlciwgTW9ycGhTVkdQbHVnaW4pXG5cbmNsYXNzIEZlYXR1cmVkUHJvZHVjdHMge1xuICBjb25zdHJ1Y3RvcihlbCkge1xuXG4gICAgdGhpcy5lbCA9IGVsXG4gICAgdGhpcy5zbGlkZXMgPSBlbC5xdWVyeVNlbGVjdG9yQWxsKCcuZmVhdHVyZWRQcm9kdWN0c19fc2xpZGUnKVxuICAgIHRoaXMuY2Fyb3VzZWwgPSBlbC5xdWVyeVNlbGVjdG9yKCcuZmVhdHVyZWRQcm9kdWN0c19fY2Fyb3VzZWwnKVxuICAgIHRoaXMucHJvZHVjdFNoYXBlID0gZWwucXVlcnlTZWxlY3RvcignLnByb2R1Y3Qtc2hhcGVzJylcbiAgICB0aGlzLnByb2R1Y3RTaGFwZU1vYmlsZSA9IGVsLnF1ZXJ5U2VsZWN0b3IoJy5wcm9kdWN0LXNoYXBlcy1tb2JpbGUnKVxuICAgIHRoaXMucGFnaW5hdGlvbkl0ZW1zID0gZWwucXVlcnlTZWxlY3RvckFsbCgnLmZlYXR1cmVkUHJvZHVjdHNfX3BhZ2luYXRpb25JdGVtJylcbiAgICB0aGlzLnNrZXRjaGVzID0gZWwucXVlcnlTZWxlY3RvckFsbCgnLmZlYXR1cmVkUHJvZHVjdHNfX3NrZXRjaCcpXG5cbiAgICAvLyBTZXQgdGhlIGZpcnN0IHNsaWRlIGFzIGFjdGl2ZS5cbiAgICB0aGlzLnBhZ2luYXRpb25JdGVtc1swXS5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKVxuICAgIHRoaXMuc2tldGNoZXNbMF0uY2xhc3NMaXN0LmFkZCgnYWN0aXZlJylcbiAgICB0aGlzLnNsaWRlc1swXS5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKVxuICAgIHRoaXMuYWN0aXZlU2xpZGUgPSB0aGlzLnNsaWRlc1swXS5kYXRhc2V0LnNoYXBlXG5cbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPj0gNzUwKSB7XG4gICAgICB0aGlzLnBpblNlY3Rpb24oKVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLm1vYmlsZUNhcm91c2VsKClcbiAgICB9XG5cbiAgfVxuICBtb2JpbGVDYXJvdXNlbCgpIHtcblxuICAgIGNvbnN0IGNhcm91c2VsID0gbmV3IEZsaWNraXR5KHRoaXMuY2Fyb3VzZWwsIHtcbiAgICAgIGNvbnRhaW46IGZhbHNlLFxuICAgICAgcGFnZURvdHM6IHRydWUsXG4gICAgICBwcmV2TmV4dEJ1dHRvbnM6IHRydWUsXG4gICAgICBkcmFnZ2FibGU6IHRydWUsXG4gICAgICBjZWxsQWxpZ246ICdsZWZ0J1xuICAgIH0pXG5cbiAgICBjYXJvdXNlbC5vbignY2hhbmdlJywgKGluZGV4KSA9PiB7XG4gICAgICBnc2FwLnRvKCcuc2hhcGVNMCcsIHsgbW9ycGhTVkc6ICcuc2hhcGVNJyArIGluZGV4ICsgJycsIGR1cmF0aW9uOiAwLjYsIGVhc2U6ICdwb3dlcjMuaW5PdXQnIH0pXG4gICAgICB0aGlzLnByb2R1Y3RTaGFwZU1vYmlsZS5zdHlsZS5maWxsID0gdGhpcy5zbGlkZXNbaW5kZXhdLmRhdGFzZXQuY29sb3VyXG4gICAgfSlcblxuICB9XG4gIHBpblNlY3Rpb24oKSB7XG5cbiAgICB0aGlzLmxlbmd0aE51bWJlciA9IDQwMFxuXG4gICAgU2Nyb2xsVHJpZ2dlci5jcmVhdGUoe1xuICAgICAgdHJpZ2dlcjogdGhpcy5lbCxcbiAgICAgIHBpbjogdHJ1ZSxcbiAgICAgIGFudGljaXBhdGVQaW46IDEsXG4gICAgICBzdGFydDogJ3RvcCB0b3AtPTM1cHgnLFxuICAgICAgZW5kOiAnYm90dG9tIGJvdHRvbScsXG4gICAgICBlbmQ6ICh0aGlzLmxlbmd0aE51bWJlciAqIHRoaXMuc2xpZGVzLmxlbmd0aCkgKyAncHgnLFxuICAgICAgLy8gbWFya2VyczogdHJ1ZSxcbiAgICAgIG9uVXBkYXRlOiAoeyBwcm9ncmVzcyB9KSA9PiB7XG4gICAgICAgIHRoaXMuc2xpZGVzLmZvckVhY2goKGVsLCBpKSA9PiB7XG4gICAgICAgICAgaWYgKHByb2dyZXNzID49ICgxIC8gKHRoaXMuc2xpZGVzLmxlbmd0aCkgKiBpKSAmJiBwcm9ncmVzcyA8ICgxIC8gKHRoaXMuc2xpZGVzLmxlbmd0aCkgKiAoaSArIDEpKSkge1xuICAgICAgICAgICAgdGhpcy5jaGFuZ2VTbGlkZShlbClcbiAgICAgICAgICB9XG4gICAgICAgIH0pXG4gICAgICB9XG4gICAgfSlcblxuICB9XG4gIGNoYW5nZVNsaWRlKGVsKSB7XG5cbiAgICBpZiAoZWwuZGF0YXNldC5zaGFwZSA9PT0gdGhpcy5hY3RpdmVTbGlkZSkgcmV0dXJuXG5cbiAgICB0aGlzLmFjdGl2ZVNsaWRlID0gZWwuZGF0YXNldC5zaGFwZVxuXG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID49IDc1MCkge1xuICAgICAgZ3NhcC50bygnLnNoYXBlMCcsIHsgbW9ycGhTVkc6ICcuc2hhcGUnICsgZWwuZGF0YXNldC5zaGFwZSArICcnLCBkdXJhdGlvbjogMC42LCBlYXNlOiAncG93ZXIzLmluT3V0JyB9KVxuICAgICAgdGhpcy5wcm9kdWN0U2hhcGUuc3R5bGUuZmlsbCA9IGVsLmRhdGFzZXQuY29sb3VyXG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIGdzYXAudG8oJyNWZXJ0ci1NJywgeyBtb3JwaFNWRzogJyMnICsgZWwuZGF0YXNldC5zaGFwZSArICctTScgLCBkdXJhdGlvbjogMC42LCBlYXNlOiAncG93ZXIzLmluT3V0JyB9KVxuICAgICAgLy8gdGhpcy5wcm9kdWN0U2hhcGVNb2JpbGUuc3R5bGUuZmlsbCA9IGVsLmRhdGFzZXQuY29sb3VyXG4gICAgfVxuXG4gICAgdGhpcy5wYWdpbmF0aW9uSXRlbXMuZm9yRWFjaChlbCA9PiB7IGVsLmNsYXNzTGlzdC5yZW1vdmUoJ2FjdGl2ZScpIH0pXG4gICAgdGhpcy5za2V0Y2hlcy5mb3JFYWNoKGVsID0+IHsgZWwuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJykgfSlcbiAgICB0aGlzLnNsaWRlcy5mb3JFYWNoKGVsID0+IHsgZWwuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJykgfSlcblxuICAgIHRoaXMuc2xpZGVzLmZvckVhY2goKHNsaWRlLCBpKSA9PiB7XG4gICAgICBpZiAoZWwgPT09IHRoaXMuc2xpZGVzW2ldKSB7XG4gICAgICAgIHRoaXMucGFnaW5hdGlvbkl0ZW1zW2ldLmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpXG4gICAgICAgIHRoaXMuc2tldGNoZXNbaV0uY2xhc3NMaXN0LmFkZCgnYWN0aXZlJylcbiAgICAgICAgZWwuY2xhc3NMaXN0LmFkZCgnYWN0aXZlJylcbiAgICAgIH1cbiAgICB9KVxuXG4gIH1cbn1cblxuY29uc3QgaW5pdEZlYXR1cmVkUHJvZHVjdHMgPSAoKSA9PiB7XG5cbiAgY29uc3Qgc2VjdGlvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5mZWF0dXJlZFByb2R1Y3RzJylcblxuICBpZiAoc2VjdGlvbikgbmV3IEZlYXR1cmVkUHJvZHVjdHMoc2VjdGlvbilcblxufVxuXG5leHBvcnQgZGVmYXVsdCBpbml0RmVhdHVyZWRQcm9kdWN0cyAiLCJpbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnXG5pbXBvcnQgQm91bmNlciBmcm9tICdmb3JtYm91bmNlcmpzJ1xuXG4vLyBHZXQgdmFsdWVzIGZyb20gdGhlIGZvcm0gYW5kIHRoZW4gcmVtb3ZlIHRoZSBkYXRhIGF0dHJpYnV0ZXMuXG5cbmNvbnN0IGZvcm0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdbZGF0YS1kZW1vXScpXG5cbmxldCBhcGksIHJlc3RyaWN0ZWREb21haW5zLCBzdWJqZWN0LCByZWNpcGllbnQsIGZvb3RlclxuXG5pZiAoZm9ybSkge1xuICBcbiAgYXBpID0gZm9ybS5kYXRhc2V0LmFwaVxuICBmb290ZXIgPSBmb3JtLmRhdGFzZXQuZm9vdGVyIFxuICBzdWJqZWN0ID0gZm9ybS5kYXRhc2V0LnN1YmplY3QgXG4gIHJlY2lwaWVudCA9IGZvcm0uZGF0YXNldC5yZWNpcGllbnQgXG4gIHJlc3RyaWN0ZWREb21haW5zID0gZm9ybS5kYXRhc2V0LnJlc3RyaWN0ZWQuc3BsaXQoJywgJykgXG5cbiAgZm9ybS5yZW1vdmVBdHRyaWJ1dGUoJ2RhdGEtYXBpJylcbiAgZm9ybS5yZW1vdmVBdHRyaWJ1dGUoJ2RhdGEtZm9vdGVyJylcbiAgZm9ybS5yZW1vdmVBdHRyaWJ1dGUoJ2RhdGEtc3ViamVjdCcpXG4gIGZvcm0ucmVtb3ZlQXR0cmlidXRlKCdkYXRhLXJlY2lwaWVudCcpXG4gIGZvcm0ucmVtb3ZlQXR0cmlidXRlKCdkYXRhLXJlc3RyaWN0ZWQnKVxuICBcbn1cblxubGV0IGZvcm1IYXNTdWJtaXR0ZWQgPSBmYWxzZSBcblxuY29uc3QgdmFsaWRhdGlvbiA9IG5ldyBCb3VuY2VyKCdbZGF0YS12YWxpZGF0ZWRlbW9dJywge1xuICBkaXNhYmxlU3VibWl0OiB0cnVlLFxuICBjdXN0b21WYWxpZGF0aW9uczoge1xuICAgIHZhbGlkYXRlRG9tYWluOiAoZmllbGQpID0+IHtcbiAgICAgIGlmIChmaWVsZC50eXBlID09ICdlbWFpbCcpIHtcbiAgICAgICAgbGV0IGRvbWFpbiA9ICcnXG4gICAgICAgIGNvbnN0IGlucHV0dGVkRG9tYWluID0gZmllbGQudmFsdWUuc3BsaXQoXCJAXCIpXG4gICAgICAgIGlmIChpbnB1dHRlZERvbWFpblsxXSkgZG9tYWluID0gaW5wdXR0ZWREb21haW5bMV0uc3BsaXQoJy4nKVswXVxuICAgICAgICBpZiAocmVzdHJpY3RlZERvbWFpbnMuaW5kZXhPZihkb21haW4pID49IDApXG4gICAgICAgICAgcmV0dXJuIHRydWVcbiAgICAgICAgZWxzZVxuICAgICAgICAgIHJldHVybiBmYWxzZVxuICAgICAgfVxuICAgIH1cbiAgfSxcbiAgbWVzc2FnZXM6IHtcblx0XHR2YWxpZGF0ZURvbWFpbjogJ1BsZWFzZSBwcm92aWRlIGEgYnVzaW5lc3MgYW5kIG5vdCBhIHBlcnNvbmFsIGVtYWlsIGFkZHJlc3MuJ1xuXHR9XG59KVxuXG5jb25zdCBwcm9jZXNzRGVtb0Zvcm0gPSAoZSkgPT4ge1xuICBlLnByZXZlbnREZWZhdWx0KClcblxuICBjb25zdCBmb3JtID0gZS50YXJnZXQsXG4gICAgICAgIGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKGZvcm0pLFxuICAgICAgICBzdWNjZXNzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmNvbnRhY3RGb3JtX19zdWNjZXNzJyksXG4gICAgICAgIGVycm9yRWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuY29udGFjdEZvcm1fX2Vycm9yJylcblxuICBmb3JtRGF0YS5hcHBlbmQoJ25vbmNlJywgV1Aubm9uY2UpXG4gIGZvcm1EYXRhLmFwcGVuZCgnYWN0aW9uJywgJ3Byb2Nlc3NfZm9ybScpXG4gIGZvcm1EYXRhLmFwcGVuZCgnZm9vdGVyJywgZm9vdGVyKVxuICBmb3JtRGF0YS5hcHBlbmQoJ3N1YmplY3QnLCBzdWJqZWN0KVxuICBmb3JtRGF0YS5hcHBlbmQoJ3JlY2lwaWVudCcsIHJlY2lwaWVudClcblxuICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdib3VuY2VyRm9ybUludmFsaWQnLCAoZXZlbnQpID0+IHtcbiAgICBjb25zb2xlLmxvZyhldmVudC5kZXRhaWwuZXJyb3JzKVxuICB9LCBmYWxzZSlcblxuICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdib3VuY2VyRm9ybVZhbGlkJywgKGUpID0+IHtcblxuICAgIGlmIChmb3JtSGFzU3VibWl0dGVkKSByZXR1cm4gZmFsc2VcblxuICAgIGZvcm1IYXNTdWJtaXR0ZWQgPSB0cnVlXG5cbiAgICAvLyBTZW5kIHN1Ym1pc3Npb24gdG8gZW1haWwgdmlhIEFKQVhcbiAgICBheGlvcyh7XG4gICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgIHVybDogV1AuYWpheCxcbiAgICAgIGRhdGE6IGZvcm1EYXRhXG4gICAgfSlcbiAgICAudGhlbihmdW5jdGlvbiAocmVzcG9uc2UpIHtcbiAgICAgIGNvbnNvbGUubG9nKCdkYXRhOiAnICsgcmVzcG9uc2UuZGF0YSlcbiAgICAgIGNvbnNvbGUubG9nKCdzdGF0dXM6ICcgKyByZXNwb25zZS5zdGF0dXMpXG4gICAgfSxcbiAgICAoZXJyb3IpID0+IHsgXG4gICAgICBjb25zb2xlLmxvZyhlcnJvcilcbiAgICB9KVxuXG4gICAgLy8gQ3JlYXRlIG9iamVjdCB3aXRoIGZvcm0gZGF0YSBhbmQgcG9zdCB0byBBUEkuXG4gICAgY29uc3QgcG9zdERhdGEgPSB7IH1cbiAgICBjb25zdCBhbGxGaWVsZHMgPSBmb3JtLnF1ZXJ5U2VsZWN0b3JBbGwoJ2lucHV0JylcblxuICAgIC8vIEFkZCBhbGwgb2YgdGhlIGZvcm1zIGZpZWxkcyB0byB0aGUgcG9zdERhdGEgb2JqZWN0LlxuICAgIGFsbEZpZWxkcy5mb3JFYWNoKGZpZWxkID0+IHtcbiAgICAgIGlmIChmaWVsZC5uYW1lICE9ICdwcml2YWN5X2NoZWNrJyB8fCBmaWVsZC5uYW1lICE9ICdNYWlsTGlzdEFwcHJvdmVkJykge1xuICAgICAgICBwb3N0RGF0YVtmaWVsZC5uYW1lXSA9IGZpZWxkLnZhbHVlXG4gICAgICB9XG4gICAgICBpZiAoZmllbGQubmFtZSA9PSAnTWFpbExpc3RBcHByb3ZlZCcgJiYgZmllbGQudmFsdWUgPT0gJ29uJykge1xuICAgICAgICBwb3N0RGF0YVsnTWFpbExpc3RBcHByb3ZlZCddID0gdHJ1ZVxuICAgICAgfSBlbHNlIGlmIChmaWVsZC5uYW1lID09ICdNYWlsTGlzdEFwcHJvdmVkJyAmJiBmaWVsZC52YWx1ZSA9PSAnb2ZmJykge1xuICAgICAgICBwb3N0RGF0YVsnTWFpbExpc3RBcHByb3ZlZCddID0gZmFsc2VcbiAgICAgIH1cbiAgICB9KVxuXG4gICAgY29uc3Qgb3B0aW9ucyA9IHsgXG4gICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KHBvc3REYXRhKSxcbiAgICAgIG1vZGU6ICdjb3JzJyxcbiAgICAgIGhlYWRlcnM6IHsgXG4gICAgICAgICdBY2Nlc3MtQ29udHJvbC1BbGxvdy1DcmVkZW50aWFscyc6ICd0cnVlJywgXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdBY2Nlc3MtQ29udHJvbC1BbGxvdy1IZWFkZXJzJzogJ09yaWdpbiwgWC1SZXF1ZXN0ZWQtV2l0aCwgQ29udGVudC1UeXBlLCBBY2NlcHQnXG4gICAgICB9XG4gICAgfVxuXG4gICAgZmV0Y2goYXBpLCBvcHRpb25zKVxuICAgICAgLnRoZW4ocmVzID0+IHtcbiAgICAgICAgZm9ybS5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnXG4gICAgICAgIHN1Y2Nlc3Muc3R5bGUuZGlzcGxheSA9ICdibG9jaydcbiAgICAgIH0pXG4gICAgICAuY2F0Y2goZXJyID0+IHsgXG4gICAgICAgIGNvbnNvbGUubG9nKGBFcnJvciB3aXRoIG1lc3NhZ2U6ICR7ZXJyfWApXG4gICAgICAgIGZvcm0uc3R5bGUuZGlzcGxheSA9ICdub25lJ1xuICAgICAgICBlcnJvckVsLnN0eWxlLmRpc3BsYXkgPSAnYmxvY2snXG4gICAgICB9KVxuXG4gICAgICBmb3JtLmNsYXNzTGlzdC5hZGQoJ3N1Ym1pdHRlZCcpXG5cbiAgfSwgZmFsc2UpXG4gIFxufVxuXG5jb25zdCBpbml0UHJvY2Vzc0RlbW9Gb3JtID0gKCkgPT4ge1xuXG4gIGNvbnN0IGZvcm1zID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnW2RhdGEtZGVtb10nKVxuXG4gIGZvciAobGV0IGkgPSAwOyBpIDwgZm9ybXMubGVuZ3RoOyBpKyspIHtcbiAgICBmb3Jtc1tpXS5hZGRFdmVudExpc3RlbmVyKCdzdWJtaXQnLCBwcm9jZXNzRGVtb0Zvcm0sIGZhbHNlKVxuICB9XG5cbn1cblxuZXhwb3J0IGRlZmF1bHQgaW5pdFByb2Nlc3NEZW1vRm9ybSIsImltcG9ydCBheGlvcyBmcm9tICdheGlvcydcbmltcG9ydCBCb3VuY2VyIGZyb20gJ2Zvcm1ib3VuY2VyanMnXG5cbi8vIEdldCB2YWx1ZXMgZnJvbSB0aGUgZm9ybSBhbmQgdGhlbiByZW1vdmUgdGhlIGRhdGEgYXR0cmlidXRlcy5cblxuY29uc3QgZm9ybSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ1tkYXRhLXByb2Nlc3NdJylcblxubGV0IHJlc3RyaWN0ZWREb21haW5zLCBzdWJqZWN0LCByZWNpcGllbnQsIGZvb3RlclxuXG5pZiAoZm9ybSkge1xuICBcbiAgZm9vdGVyID0gZm9ybS5kYXRhc2V0LmZvb3RlciBcbiAgc3ViamVjdCA9IGZvcm0uZGF0YXNldC5zdWJqZWN0IFxuICByZWNpcGllbnQgPSBmb3JtLmRhdGFzZXQucmVjaXBpZW50IFxuICByZXN0cmljdGVkRG9tYWlucyA9IGZvcm0uZGF0YXNldC5yZXN0cmljdGVkLnNwbGl0KCcsICcpIFxuXG4gIGZvcm0ucmVtb3ZlQXR0cmlidXRlKCdkYXRhLWZvb3RlcicpXG4gIGZvcm0ucmVtb3ZlQXR0cmlidXRlKCdkYXRhLXN1YmplY3QnKVxuICBmb3JtLnJlbW92ZUF0dHJpYnV0ZSgnZGF0YS1yZWNpcGllbnQnKVxuICBmb3JtLnJlbW92ZUF0dHJpYnV0ZSgnZGF0YS1yZXN0cmljdGVkJylcbiAgXG59XG5cbmxldCBmb3JtSGFzU3VibWl0dGVkID0gZmFsc2UgXG5cbmNvbnN0IHZhbGlkYXRpb24gPSBuZXcgQm91bmNlcignW2RhdGEtdmFsaWRhdGVdJywge1xuICBkaXNhYmxlU3VibWl0OiB0cnVlLFxuICBjdXN0b21WYWxpZGF0aW9uczoge1xuICAgIHZhbGlkYXRlRG9tYWluOiAoZmllbGQpID0+IHtcbiAgICAgIGlmIChmaWVsZC50eXBlID09ICdlbWFpbCcpIHtcbiAgICAgICAgbGV0IGRvbWFpbiA9ICcnXG4gICAgICAgIGNvbnN0IGlucHV0dGVkRG9tYWluID0gZmllbGQudmFsdWUuc3BsaXQoXCJAXCIpXG4gICAgICAgIGlmIChpbnB1dHRlZERvbWFpblsxXSkgZG9tYWluID0gaW5wdXR0ZWREb21haW5bMV0uc3BsaXQoJy4nKVswXVxuICAgICAgICBpZiAocmVzdHJpY3RlZERvbWFpbnMuaW5kZXhPZihkb21haW4pID49IDApXG4gICAgICAgICAgcmV0dXJuIHRydWVcbiAgICAgICAgZWxzZVxuICAgICAgICAgIHJldHVybiBmYWxzZVxuICAgICAgfVxuICAgIH1cbiAgfSxcbiAgbWVzc2FnZXM6IHtcblx0XHR2YWxpZGF0ZURvbWFpbjogJ1BsZWFzZSBwcm92aWRlIGEgYnVzaW5lc3MgYW5kIG5vdCBhIHBlcnNvbmFsIGVtYWlsIGFkZHJlc3MuJ1xuXHR9XG59KVxuXG5jb25zdCBwcm9jZXNzRm9ybSA9IChlKSA9PiB7XG4gIGUucHJldmVudERlZmF1bHQoKTtcbiAgY29uc3QgZm9ybSA9IGUudGFyZ2V0O1xuXG4gIGNvbnN0IGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKGZvcm0pO1xuXG4gIGZvcm1EYXRhLmFwcGVuZCgnYWN0aW9uJywgJ3Byb2Nlc3NfZm9ybScpXG4gIGZvcm1EYXRhLmFwcGVuZCgnbm9uY2UnLCBXUC5ub25jZSlcbiAgZm9ybURhdGEuYXBwZW5kKCdmb3JtX2lkJywgZm9ybS5nZXRBdHRyaWJ1dGUoJ2RhdGEtZm9ybS1pZCcpKVxuICBmb3JtRGF0YS5hcHBlbmQoJ2Zvb3RlcicsIGZvb3RlcilcbiAgZm9ybURhdGEuYXBwZW5kKCdzdWJqZWN0Jywgc3ViamVjdClcbiAgZm9ybURhdGEuYXBwZW5kKCdyZWNpcGllbnQnLCByZWNpcGllbnQpXG5cbiAgLy8gZm9yICh2YXIgdmFsdWUgb2YgZm9ybURhdGEudmFsdWVzKCkpIHtcbiAgLy8gICBjb25zb2xlLmxvZyh2YWx1ZSk7XG4gIC8vIH1cblxuICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdib3VuY2VyRm9ybUludmFsaWQnLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICBjb25zb2xlLmxvZyhldmVudC5kZXRhaWwuZXJyb3JzKTtcbiAgfSwgZmFsc2UpO1xuXG5cbiAgLyoqIElmIHZhbGlkYXRpb24gcGFzc2VzLCBmaXJlIEFKQVggc3VibWlzc2lvbiAqL1xuICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdib3VuY2VyRm9ybVZhbGlkJywgZnVuY3Rpb24gKGUpIHtcblxuICAgIGlmIChmb3JtSGFzU3VibWl0dGVkKSByZXR1cm4gZmFsc2VcblxuICAgIGZvcm1IYXNTdWJtaXR0ZWQgPSB0cnVlXG5cbiAgICBmb3JtLmNsYXNzTGlzdC5hZGQoJ3N1Ym1pdHRlZCcpXG5cbiAgICBheGlvcyh7XG4gICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgIHVybDogV1AuYWpheCxcbiAgICAgIGRhdGE6IGZvcm1EYXRhXG4gICAgfSlcbiAgICAudGhlbihmdW5jdGlvbiAocmVzcG9uc2UpIHtcbiAgICAgIGNvbnNvbGUubG9nKCdkYXRhOiAnICsgcmVzcG9uc2UuZGF0YSk7XG4gICAgICBjb25zb2xlLmxvZygnc3RhdHVzOiAnICsgcmVzcG9uc2Uuc3RhdHVzKTtcbiAgICAgIGZvcm0uaW5uZXJIVE1MID0gJzxkaXYgY2xhc3M9XCJzdWNjZXNzLW1zZ1wiPicgKyBmb3JtLmdldEF0dHJpYnV0ZSgnZGF0YS1zdWNjZXNzJykgKyAnPC9kaXY+J1xuICAgIH0sXG4gICAgKGVycm9yKSA9PiB7XG4gICAgICBjb25zb2xlLmxvZyhlcnJvcilcbiAgICB9KTtcblxuICB9LCBmYWxzZSk7XG5cbn07XG5cbmNvbnN0IGluaXRQcm9jZXNzRm9ybSA9ICgpID0+IHtcblxuICBjb25zdCBmb3JtcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJ1tkYXRhLXByb2Nlc3NdJyk7XG5cbiAgZm9yIChsZXQgaSA9IDA7IGkgPCBmb3Jtcy5sZW5ndGg7IGkrKykge1xuICAgIGZvcm1zW2ldLmFkZEV2ZW50TGlzdGVuZXIoJ3N1Ym1pdCcsIHByb2Nlc3NGb3JtLCBmYWxzZSk7XG4gIH0gLy8gRU5EIGxvb3BcblxufTtcblxuZXhwb3J0IGRlZmF1bHQgaW5pdFByb2Nlc3NGb3JtOyIsImltcG9ydCBheGlvcyBmcm9tICdheGlvcydcbmltcG9ydCBCb3VuY2VyIGZyb20gJ2Zvcm1ib3VuY2VyanMnXG5cbi8vIEdldCB2YWx1ZXMgZnJvbSB0aGUgZm9ybSBhbmQgdGhlbiByZW1vdmUgdGhlIGRhdGEgYXR0cmlidXRlcy5cblxuY29uc3QgZm9ybSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ1tkYXRhLXByb2Nlc3N0cmlhbF0nKVxuXG5sZXQgYXBpLCB0eXBlLCByZXN0cmljdGVkRG9tYWluc1xuXG5pZiAoZm9ybSkge1xuXG4gIGFwaSA9IGZvcm0uZGF0YXNldC5hcGlcbiAgdHlwZSA9IGZvcm0uZGF0YXNldC50eXBlXG4gIHJlc3RyaWN0ZWREb21haW5zID0gZm9ybS5kYXRhc2V0LnJlc3RyaWN0ZWQuc3BsaXQoJywgJylcbiAgXG4gIGZvcm0ucmVtb3ZlQXR0cmlidXRlKCdkYXRhLWFwaScpXG4gIGZvcm0ucmVtb3ZlQXR0cmlidXRlKCdkYXRhLXR5cGUnKVxuICBmb3JtLnJlbW92ZUF0dHJpYnV0ZSgnZGF0YS1yZXN0cmljdGVkJylcbiAgXG59XG4gXG5sZXQgZm9ybUhhc1N1Ym1pdHRlZCA9IGZhbHNlIFxuXG5jb25zdCB2YWxpZGF0aW9uID0gbmV3IEJvdW5jZXIoJ1tkYXRhLXZhbGlkYXRldHJpYWxdJywge1xuICBkaXNhYmxlU3VibWl0OiB0cnVlLFxuICBjdXN0b21WYWxpZGF0aW9uczoge1xuICAgIHZhbGlkYXRlRG9tYWluOiAoZmllbGQpID0+IHtcbiAgICAgIGlmIChmaWVsZC50eXBlID09ICdlbWFpbCcpIHtcbiAgICAgICAgbGV0IGRvbWFpbiA9ICcnXG4gICAgICAgIGNvbnN0IGlucHV0dGVkRG9tYWluID0gZmllbGQudmFsdWUuc3BsaXQoXCJAXCIpXG4gICAgICAgIGlmIChpbnB1dHRlZERvbWFpblsxXSkgZG9tYWluID0gaW5wdXR0ZWREb21haW5bMV0uc3BsaXQoJy4nKVswXVxuICAgICAgICBpZiAocmVzdHJpY3RlZERvbWFpbnMuaW5kZXhPZihkb21haW4pID49IDApXG4gICAgICAgICAgcmV0dXJuIHRydWVcbiAgICAgICAgZWxzZVxuICAgICAgICAgIHJldHVybiBmYWxzZVxuICAgICAgfVxuICAgIH1cbiAgfSxcbiAgbWVzc2FnZXM6IHtcblx0XHR2YWxpZGF0ZURvbWFpbjogJ1BsZWFzZSBwcm92aWRlIGEgYnVzaW5lc3MgYW5kIG5vdCBhIHBlcnNvbmFsIGVtYWlsIGFkZHJlc3MuJ1xuXHR9XG59KVxuXG5jb25zdCBwcm9jZXNzVHJpYWxGb3JtID0gKGUpID0+IHtcblxuICBlLnByZXZlbnREZWZhdWx0KClcbiAgXG4gIGNvbnN0IGZvcm0gPSBlLnRhcmdldCxcbiAgICAgICAgc3VjY2VzcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jb250YWN0Rm9ybV9fc3VjY2VzcycpLFxuICAgICAgICBlcnJvckVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmNvbnRhY3RGb3JtX19lcnJvcicpLFxuICAgICAgICBleGlzdHNFbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jb250YWN0Rm9ybV9fZXhpc3RzJyksXG4gICAgICAgIHJlc3BvbnNlRWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuY29udGFjdEZvcm1fX3Jlc3BvbnNlJyksXG4gICAgICAgIGFwaUtleUVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI1RyaWFsQXBpS2V5JylcbiAgXG4gIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2JvdW5jZXJGb3JtSW52YWxpZCcsIChldmVudCkgPT4gY29uc29sZS5sb2coZXZlbnQuZGV0YWlsLmVycm9ycyksIGZhbHNlKVxuICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdib3VuY2VyRm9ybVZhbGlkJywgZnVuY3Rpb24gKGUpIHtcblxuICAgIGlmIChmb3JtSGFzU3VibWl0dGVkKSByZXR1cm4gZmFsc2VcblxuICAgIGZvcm1IYXNTdWJtaXR0ZWQgPSB0cnVlXG5cbiAgICBjb25zdCBwb3N0RGF0YSA9IHsgVHlwZTogdHlwZSB9XG4gICAgY29uc3QgYWxsRmllbGRzID0gZm9ybS5xdWVyeVNlbGVjdG9yQWxsKCdpbnB1dCcpXG5cbiAgICAvLyBBZGQgYWxsIG9mIHRoZSBmb3JtcyBmaWVsZHMgdG8gdGhlIHBvc3REYXRhIG9iamVjdC5cbiAgICBhbGxGaWVsZHMuZm9yRWFjaChmaWVsZCA9PiB7XG4gICAgICBpZiAoZmllbGQubmFtZSAhPSAncHJpdmFjeV9jaGVjaycgfHwgZmllbGQubmFtZSAhPSAnTWFpbExpc3RBcHByb3ZlZCcpIHtcbiAgICAgICAgcG9zdERhdGFbZmllbGQubmFtZV0gPSBmaWVsZC52YWx1ZVxuICAgICAgfVxuICAgICAgaWYgKGZpZWxkLm5hbWUgPT0gJ01haWxMaXN0QXBwcm92ZWQnICYmIGZpZWxkLnZhbHVlID09ICdvbicpIHtcbiAgICAgICAgcG9zdERhdGFbJ01haWxMaXN0QXBwcm92ZWQnXSA9IHRydWVcbiAgICAgIH0gZWxzZSBpZiAoZmllbGQubmFtZSA9PSAnTWFpbExpc3RBcHByb3ZlZCcgJiYgZmllbGQudmFsdWUgPT0gJ29mZicpIHtcbiAgICAgICAgcG9zdERhdGFbJ01haWxMaXN0QXBwcm92ZWQnXSA9IGZhbHNlXG4gICAgICB9XG4gICAgfSlcblxuICAgIGNvbnNvbGUubG9nKHBvc3REYXRhKVxuXG4gICAgY29uc3Qgb3B0aW9ucyA9IHtcbiAgICAgIG1ldGhvZDogJ1BPU1QnLCAgXG4gICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShwb3N0RGF0YSksXG4gICAgICBtb2RlOiAnY29ycycsXG4gICAgICBoZWFkZXJzOiB7IFxuICAgICAgICAnQWNjZXNzLUNvbnRyb2wtQWxsb3ctQ3JlZGVudGlhbHMnOiAndHJ1ZScsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdBY2Nlc3MtQ29udHJvbC1BbGxvdy1IZWFkZXJzJzogJ09yaWdpbiwgWC1SZXF1ZXN0ZWQtV2l0aCwgQ29udGVudC1UeXBlLCBBY2NlcHQnXG4gICAgICB9XG4gICAgfVxuICAgIGZldGNoKGFwaSwgb3B0aW9ucylcbiAgICAgIC50aGVuKHJlcyA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdDb2RlOiAnICsgcmVzLnN0YXR1cylcbiAgICAgICAgaWYgKHJlcy5zdGF0dXMgPT0gNTAwKSB0aHJvdyBFcnJvcihyZXNwb25zZS5zdGF0dXNUZXh0KVxuICAgICAgICBlbHNlIHJldHVybiByZXMudGV4dCgpXG4gICAgICB9KVxuICAgICAgLnRoZW4ocmVzID0+IHtcblxuICAgICAgICBjb25zb2xlLmxvZyhyZXMpXG4gICAgICAgIGNvbnNvbGUubG9nKCdBcGlLZXk6ICcgKyByZXMuQXBpS2V5KVxuICAgICAgICBjb25zb2xlLmxvZygnRXJyb3I6ICcgKyByZXMuRXJyb3IpXG5cbiAgICAgICAgaWYgKHJlcyA9PSAnVGVuYW50IGFscmVhZHkgZXhpc3RzJykge1xuICAgICAgICAgIGZvcm0uc3R5bGUuZGlzcGxheSA9ICdub25lJ1xuICAgICAgICAgIGV4aXN0c0VsLnN0eWxlLmRpc3BsYXkgPSAnYmxvY2snXG4gICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICBjb25zdCByZXNwb25zZSA9IEpTT04ucGFyc2UocmVzKVxuXG4gICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpXG5cbiAgICAgICAgICBpZiAocmVzcG9uc2UuRXJyb3IgIT0gbnVsbCkge1xuICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmNvbnRhY3RGb3JtX19yZXNwb25zZSBoMycpLmlubmVySFRNTCA9IHJlc3BvbnNlLkVycm9yXG4gICAgICAgICAgICBmb3JtLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSdcbiAgICAgICAgICAgIHJlc3BvbnNlRWwuc3R5bGUuZGlzcGxheSA9ICdibG9jaydcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgYXBpS2V5RWwuaW5uZXJIVE1MID0gcmVzcG9uc2UuQXBpS2V5XG4gICAgICAgICAgICBmb3JtLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSdcbiAgICAgICAgICAgIHN1Y2Nlc3Muc3R5bGUuZGlzcGxheSA9ICdibG9jaydcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgfSlcbiAgICAgIC5jYXRjaChlcnIgPT4ge1xuICAgICAgICBjb25zb2xlLmxvZyhgRXJyb3Igd2l0aCBtZXNzYWdlOiAke2Vycn1gKSBcbiAgICAgICAgZm9ybS5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnXG4gICAgICAgIGVycm9yRWwuc3R5bGUuZGlzcGxheSA9ICdibG9jaydcbiAgICAgIH0pXG5cbiAgICBmb3JtLmNsYXNzTGlzdC5hZGQoJ3N1Ym1pdHRlZCcpXG5cbiAgfSwgZmFsc2UpXG4gIFxuICAvLyBDb3B5IHRoZSBBUEkgdG8gdGhlIGNsaXBib2FyZFxuICBhcGlLZXlFbC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IHtcbiAgXG4gICAgY29uc3QgYXBpS2V5ID0gYXBpS2V5RWwuaW5uZXJIVE1MLFxuICAgICAgICAgIHRlbXBvcmFyeUZpZWxkID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaW5wdXQnKVxuXG4gICAgdGVtcG9yYXJ5RmllbGQudmFsdWUgPSBhcGlLZXlcbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdib2R5JykuYXBwZW5kQ2hpbGQodGVtcG9yYXJ5RmllbGQpXG4gICAgdGVtcG9yYXJ5RmllbGQuc2VsZWN0KClcbiAgICBkb2N1bWVudC5leGVjQ29tbWFuZCgnY29weScpXG4gICAgdGVtcG9yYXJ5RmllbGQucmVtb3ZlKClcbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcudG9vbHRpcCcpLmlubmVySFRNTCA9ICdDb3BpZWQhJ1xuICAgIHNldFRpbWVvdXQoKCkgPT4gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnRvb2x0aXAnKS5pbm5lckhUTUwgPSAnQ2xpY2sgdG8gQ29weScsIDMwMDApXG4gIH0pXG5cbn1cblxuY29uc3QgaW5pdFByb2Nlc3NUcmlhbEZvcm0gPSAoKSA9PiB7XG5cbiAgY29uc3QgZm9ybXMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCdbZGF0YS1wcm9jZXNzdHJpYWxdJylcblxuICBmb3Jtcy5mb3JFYWNoKGVsID0+IGVsLmFkZEV2ZW50TGlzdGVuZXIoJ3N1Ym1pdCcsIHByb2Nlc3NUcmlhbEZvcm0sIGZhbHNlKSApXG5cbn1cblxuZXhwb3J0IGRlZmF1bHQgaW5pdFByb2Nlc3NUcmlhbEZvcm0iLCJpbXBvcnQgeyBMb2FkZXIgfSBmcm9tICdAZ29vZ2xlbWFwcy9qcy1hcGktbG9hZGVyJ1xuXG5jb25zdCBpbml0R29vZ2xlTWFwcyA9ICgpID0+IHtcblxuICBpZiAoIWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNtYXAnKSkgcmV0dXJuXG5cbiAgY29uc3QgbG9hZGVyID0gbmV3IExvYWRlcih7XG4gICAgYXBpS2V5OiAnQUl6YVN5Q2lzMmJpNmJPM29RR2IyMXFqWFM0Z2sxTGFNaWNib0k0JyxcbiAgICB2ZXJzaW9uOiAnd2Vla2x5J1xuICB9KVxuXG4gIGxvYWRlci5sb2FkKCkudGhlbigoKSA9PiB7XG4gICAgbWFwID0gbmV3IGdvb2dsZS5tYXBzLk1hcChkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbWFwJyksIHtcbiAgICAgIGNlbnRlcjogeyBsYXQ6IDUyLjM3MjA4MiwgbG5nOiAtMS43OTc5NjcgfSxcbiAgICAgIHpvb206IDE0LFxuICAgICAgZGlzYWJsZURlZmF1bHRVSTogdHJ1ZVxuICAgIH0pXG4gIH0pXG5cblxufVxuXG5leHBvcnQgZGVmYXVsdCBpbml0R29vZ2xlTWFwcyAgIiwiY2xhc3MgSGVhZGVyIHtcbiAgY29uc3RydWN0b3IoZWwpIHtcblxuICAgIHRoaXMuZWwgPSBlbFxuICAgIHRoaXMubmF2ID0gZWwucXVlcnlTZWxlY3RvcignLmhlYWRlcl9fbmF2JylcbiAgICB0aGlzLm1lbnVCdXR0b24gPSBlbC5xdWVyeVNlbGVjdG9yKCcuaGVhZGVyX19tZW51QnV0dG9uJylcbiAgICB0aGlzLnByb2R1Y3RzSXRlbSA9IGVsLnF1ZXJ5U2VsZWN0b3IoJy5tZW51LWl0ZW0tMzcgYScpXG4gICAgdGhpcy5pbmR1c3RyeUl0ZW0gPSBlbC5xdWVyeVNlbGVjdG9yKCcubWVudS1pdGVtLTI1MTMgYScpXG4gICAgdGhpcy5hYm91dEl0ZW0gPSBlbC5xdWVyeVNlbGVjdG9yKCcubWVudS1pdGVtLTUyIGEnKVxuXG4gICAgdGhpcy5wcm9kdWN0c0l0ZW0uY2xhc3NMaXN0LmFkZCgnbWVudS1hY2NvcmRpYW4nKVxuICAgIHRoaXMuaW5kdXN0cnlJdGVtLmNsYXNzTGlzdC5hZGQoJ21lbnUtYWNjb3JkaWFuJylcbiAgICB0aGlzLmFib3V0SXRlbS5jbGFzc0xpc3QuYWRkKCdtZW51LWFjY29yZGlhbicpXG5cbiAgICB0aGlzLm1lbnVCdXR0b24uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCB0aGlzLnRvZ2dsZU5hdi5iaW5kKHRoaXMpKVxuXG4gICAgdGhpcy5wcm9kdWN0c0l0ZW0uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCB0aGlzLnRvZ2dsZVN1Ym1lbnUuYmluZCh0aGlzKSlcbiAgICB0aGlzLmluZHVzdHJ5SXRlbS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMudG9nZ2xlU3VibWVudS5iaW5kKHRoaXMpKVxuICAgIHRoaXMuYWJvdXRJdGVtLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgdGhpcy50b2dnbGVTdWJtZW51LmJpbmQodGhpcykpXG5cbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPCA5NjApIHNldFRpbWVvdXQoKCkgPT4gdGhpcy5wcm9kdWN0c0l0ZW0ucGFyZW50Tm9kZS5jbGFzc0xpc3QuYWRkKCd0b2dnbGUnKSwgNTAwKVxuICAgIFxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLCB0aGlzLmNhbGNTdWJtZW51SGVpZ2h0LmJpbmQodGhpcykpXG5cbiAgICB0aGlzLmNhbGNTdWJtZW51SGVpZ2h0KClcblxuICAgIHdpbmRvdy5vbnNjcm9sbCA9IGZ1bmN0aW9uKGUpIHtcbiAgICAgIGlmICh0aGlzLm9sZFNjcm9sbCA+IHRoaXMuc2Nyb2xsWSB8fCB3aW5kb3cucGFnZVlPZmZzZXQgPT09IDApIHtcbiAgICAgICAgZWwuY2xhc3NMaXN0LnJlbW92ZSgnaGlkZGVuJylcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGVsLmNsYXNzTGlzdC5hZGQoJ2hpZGRlbicpXG4gICAgICB9XG4gICAgICBpZiAodGhpcy5zY3JvbGxZID49IDIwMCkge1xuICAgICAgICBlbC5jbGFzc0xpc3QuYWRkKCd3aGl0ZScpXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBlbC5jbGFzc0xpc3QucmVtb3ZlKCd3aGl0ZScpIFxuICAgICAgfVxuICAgICAgdGhpcy5vbGRTY3JvbGwgPSB0aGlzLnNjcm9sbFk7XG4gICAgfSBcblxuICB9XG4gIHRvZ2dsZU5hdihlKSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpXG4gICAgdGhpcy5uYXYuY2xhc3NMaXN0LnRvZ2dsZSgnanMtb3BlbicpXG4gICAgdGhpcy5tZW51QnV0dG9uLmNsYXNzTGlzdC50b2dnbGUoJ2pzLW9wZW4nKVxuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2h0bWwnKS5jbGFzc0xpc3QudG9nZ2xlKCduby1zY3JvbGwnKVxuICB9XG4gIHRvZ2dsZVN1Ym1lbnUoZSkge1xuICAgIGUucHJldmVudERlZmF1bHQoKVxuICAgIGUuY3VycmVudFRhcmdldC5wYXJlbnROb2RlLmNsYXNzTGlzdC50b2dnbGUoJ3RvZ2dsZScpXG4gIH1cbiAgY2FsY1N1Ym1lbnVIZWlnaHQoZSkge1xuICAgIGNvbnN0IHByb2R1Y3RNZW51ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLm1lbnUtaXRlbS0zNycpXG4gICAgcHJvZHVjdE1lbnUuc3R5bGUuaGVpZ2h0ID0gJ2F1dG8nXG4gICAgLy8gcHJvZHVjdE1lbnUuc3R5bGUuaGVpZ2h0ID0gcHJvZHVjdE1lbnUub2Zmc2V0SGVpZ2h0ICsgJ3B4J1xuICB9XG59XG5cbmNvbnN0IGluaXRIZWFkZXIgPSAoKSA9PiB7XG5cbiAgbmV3IEhlYWRlcihkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuaGVhZGVyJykpXG5cbn1cblxuZXhwb3J0IGRlZmF1bHQgaW5pdEhlYWRlciIsImNvbnN0IGhlbGxvV29ybGQgPSAoKSA9PiB7XG5cbiAgY29uc29sZS5sb2coJyVjQkdOXFxuJyArICclY0J1aWx0IGJ5IEJHTiBBZ2VuY3k6XFxuJyArICclY2h0dHBzOi8vd3d3LmJnbi5hZ2VuY3kvJywgJ2ZvbnQtZmFtaWx5OiBHZW9yZ2lhO2ZvbnQtc2l6ZTogM2VtO2ZvbnQtd2VpZ2h0OiBib2xkO2NvbG9yOiAjZjhiZGQ3OycsICdmb250LXNpemU6IDFlbTtjb2xvcjogI2Y4YmRkNzsnLCAnZm9udC1zaXplOiAxZW07Jyk7XG5cbn07XG5cbmV4cG9ydCBkZWZhdWx0IGhlbGxvV29ybGQ7IiwiY2xhc3MgSW1hZ2VNb2RhbCB7XG4gIGNvbnN0cnVjdG9yKGVsKSB7XG5cbiAgICB0aGlzLml0ZW1zID0gQXJyYXkuZnJvbShlbC5xdWVyeVNlbGVjdG9yQWxsKCcuanMtbW9kYWwtaXRlbScpKVxuICAgIHRoaXMubW9kYWxzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmZlYXR1cmVzQWx0X19tb2RhbCcpXG4gICAgdGhpcy5jbG9zZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5mZWF0dXJlc0FsdF9fY2xvc2UnKVxuXG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID4gNzUwKSB7XG5cbiAgICAgIHRoaXMuaXRlbXMuZm9yRWFjaChlbCA9PiB7XG4gICAgICAgIGVsLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgdGhpcy5vcGVuTW9kYWwuYmluZCh0aGlzKSlcbiAgICAgIH0pXG4gIFxuICAgICAgdGhpcy5jbG9zZS5mb3JFYWNoKGVsID0+IHtcbiAgICAgICAgZWwuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCB0aGlzLmNsb3NlTW9kYWwuYmluZCh0aGlzKSlcbiAgICAgIH0pXG5cbiAgICB9XG5cbiAgfVxuICBvcGVuTW9kYWwoZSkge1xuICAgIGUucHJldmVudERlZmF1bHQoKVxuXG4gICAgY29uc3QgaW5kZXggPSB0aGlzLml0ZW1zLmluZGV4T2YoZS5jdXJyZW50VGFyZ2V0KVxuXG4gICAgdGhpcy5tb2RhbHNbaW5kZXhdLmNsYXNzTGlzdC5hZGQoJ2pzLW9wZW4nKVxuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2h0bWwnKS5jbGFzc0xpc3QuYWRkKCduby1zY3JvbGwnKVxuXG4gIH1cbiAgY2xvc2VNb2RhbChlKSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpXG4gICAgXG4gICAgdGhpcy5tb2RhbHMuZm9yRWFjaChlbCA9PiB7XG4gICAgICBlbC5jbGFzc0xpc3QucmVtb3ZlKCdqcy1vcGVuJylcbiAgICB9KVxuXG4gICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignaHRtbCcpLmNsYXNzTGlzdC5yZW1vdmUoJ25vLXNjcm9sbCcpXG5cbiAgfVxufVxuXG5jb25zdCBpbml0SW1hZ2VNb2RhbCA9ICgpID0+IHtcblxuICBjb25zdCBmZWF0dXJlcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ1tkYXRhLWZlYXR1cmVzXScpXG4gIFxuICBpZiAoIWZlYXR1cmVzKSByZXR1cm5cblxuICBuZXcgSW1hZ2VNb2RhbChmZWF0dXJlcylcblxufVxuXG5leHBvcnQgZGVmYXVsdCBpbml0SW1hZ2VNb2RhbCIsInZhciBGbGlja2l0eSA9IHJlcXVpcmUoJ2ZsaWNraXR5LWZhZGUnKVxuXG5jbGFzcyBJbnRyb1NsaWRlciB7XG4gIGNvbnN0cnVjdG9yKGVsKSB7XG4gICAgXG4gICAgdGhpcy5lbCA9IGVsXG4gICAgdGhpcy5hcnJvd05leHQgPSBlbC5wYXJlbnROb2RlLnF1ZXJ5U2VsZWN0b3IoJy5uZXh0JylcbiAgICB0aGlzLmFycm93UHJldiA9IGVsLnBhcmVudE5vZGUucXVlcnlTZWxlY3RvcignLnByZXZpb3VzJylcbiAgICB0aGlzLmRvdHMgPSBlbC5wYXJlbnROb2RlLnF1ZXJ5U2VsZWN0b3JBbGwoJy5pbnRyb1NsaWRlcl9fZG90JylcblxuICAgIHRoaXMuYXJyb3dOZXh0LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgdGhpcy5uZXh0U2xpZGUuYmluZCh0aGlzKSlcbiAgICB0aGlzLmFycm93UHJldi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMucHJldlNsaWRlLmJpbmQodGhpcykpXG5cbiAgICB0aGlzLmRvdHNbMF0uY2xhc3NMaXN0LmFkZCgnYWN0aXZlJylcblxuICAgIHRoaXMuaW5pdFNsaWRlcigpXG5cbiAgfVxuICBpbml0U2xpZGVyKCkge1xuXG4gICAgdGhpcy5mbGlja2l0eSA9IG5ldyBGbGlja2l0eSh0aGlzLmVsLCB7XG4gICAgICBjb250YWluOiB0cnVlLFxuICAgICAgcGFnZURvdHM6IGZhbHNlLFxuICAgICAgcHJldk5leHRCdXR0b25zOiBmYWxzZSxcbiAgICAgIGRyYWdnYWJsZTogdHJ1ZSxcbiAgICAgIGZhZGU6IHRydWUsXG4gICAgICBhZGFwdGl2ZUhlaWdodDogdHJ1ZVxuICAgIH0pXG4gXG4gICAgdGhpcy5mbGlja2l0eS5vbiggJ2NoYW5nZScsIHRoaXMuc2xpZGVDaGFuZ2VkLmJpbmQodGhpcykpXG5cbiAgfVxuICBzbGlkZUNoYW5nZWQoZSkge1xuXG4gICAgaWYgKHRoaXMuZmxpY2tpdHkuc2VsZWN0ZWRJbmRleCA9PT0gMCkge1xuICAgICAgdGhpcy5hcnJvd1ByZXYuZGlzYWJsZWQgPSB0cnVlXG4gICAgICB0aGlzLmFycm93TmV4dC5kaXNhYmxlZCA9IGZhbHNlXG4gICAgfSBlbHNlIGlmICh0aGlzLmZsaWNraXR5LnNlbGVjdGVkSW5kZXggPT09IHRoaXMuZmxpY2tpdHkuc2xpZGVzLmxlbmd0aCAtIDEpIHtcbiAgICAgIHRoaXMuYXJyb3dOZXh0LmRpc2FibGVkID0gdHJ1ZVxuICAgICAgdGhpcy5hcnJvd1ByZXYuZGlzYWJsZWQgPSBmYWxzZVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmFycm93UHJldi5kaXNhYmxlZCA9IGZhbHNlXG4gICAgICB0aGlzLmFycm93TmV4dC5kaXNhYmxlZCA9IGZhbHNlXG4gICAgfVxuXG4gICAgdGhpcy5kb3RzLmZvckVhY2goZWwgPT4geyBlbC5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKSB9KVxuICAgIHRoaXMuZG90c1t0aGlzLmZsaWNraXR5LnNlbGVjdGVkSW5kZXhdLmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpXG5cbiAgfVxuICBuZXh0U2xpZGUoZSkge1xuXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpXG4gICAgdGhpcy5mbGlja2l0eS5uZXh0KClcblxuICB9XG4gIHByZXZTbGlkZShlKSB7XG5cbiAgICBlLnByZXZlbnREZWZhdWx0KClcbiAgICB0aGlzLmZsaWNraXR5LnByZXZpb3VzKClcblxuICB9XG59XG5cbmNvbnN0IGluaXRJbnRyb1NsaWRlciA9ICgpID0+IHtcblxuICBjb25zdCBjYXJvdXNlbHMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuaW50cm9TbGlkZXJfX3NsaWRlcicpXG5cbiAgaWYgKGNhcm91c2VscykgbmV3IEludHJvU2xpZGVyKGNhcm91c2VscylcblxufVxuXG5leHBvcnQgZGVmYXVsdCBpbml0SW50cm9TbGlkZXIiLCJpbXBvcnQgZ3NhcCBmcm9tICdnc2FwJ1xuaW1wb3J0IHsgU2Nyb2xsVHJpZ2dlciB9IGZyb20gXCJnc2FwL1Njcm9sbFRyaWdnZXJcIlxuaW1wb3J0ICogYXMgYW5pbWF0aW9ucyBmcm9tICcuL2FuaW1hdGlvbnMuanMnXG5cbmdzYXAucmVnaXN0ZXJQbHVnaW4oU2Nyb2xsVHJpZ2dlcilcblxuY29uc3QgaXNJbnZpZXcgPSAoKSA9PiB7XG5cbiAgY29uc3Qgc2VjdGlvbnMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCdbZGF0YS1pbnZpZXddJylcblxuICBpZiAoIXNlY3Rpb25zKSByZXR1cm5cblxuICBzZWN0aW9ucy5mb3JFYWNoKHNlY3Rpb24gPT4ge1xuXG4gICAgU2Nyb2xsVHJpZ2dlci5jcmVhdGUoe1xuICAgICAgdHJpZ2dlcjogc2VjdGlvbiwgXG4gICAgICBzdGFydDogKHNlY3Rpb24uZGF0YXNldC5pbnZpZXcgPT09ICdzdGFydCcpID8gJ3RvcCA5NSUnIDogJ3RvcCA4MCUnLFxuICAgICAgZW5kOiAnYm90dG9tIGJvdHRvbScsXG4gICAgICBvbmNlOiB0cnVlLFxuICAgICAgdG9nZ2xlQ2xhc3M6ICdpcy1pbnZpZXcnLFxuICAgICAgLy8gbWFya2VyczogdHJ1ZSxcbiAgICAgIG9uRW50ZXI6ICgpID0+IHtcbiAgICAgICAgc3dpdGNoIChzZWN0aW9uLmRhdGFzZXQuaW52aWV3KSB7IFxuICAgICAgICAgIGNhc2UgJ2hlcm8nOlxuICAgICAgICAgICAgYW5pbWF0aW9ucy5oZXJvQW5pbShzZWN0aW9uKSBcbiAgICAgICAgICBicmVha1xuICAgICAgICAgIGNhc2UgJ2NsaWVudCc6XG4gICAgICAgICAgICBhbmltYXRpb25zLmNsaWVudHNBbmltKHNlY3Rpb24pXG4gICAgICAgICAgYnJlYWtcbiAgICAgICAgICBjYXNlICd0ZXN0aW1vbmlhbHMnOlxuICAgICAgICAgICAgYW5pbWF0aW9ucy50ZXN0aW1vbmlhbHNBbmltKHNlY3Rpb24pXG4gICAgICAgICAgYnJlYWtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pXG4gICAgXG4gIH0pXG5cbiAgZG9jdW1lbnQuZm9udHMucmVhZHkudGhlbigoKSA9PiB7XG4gICAgU2Nyb2xsVHJpZ2dlci5yZWZyZXNoKClcbiAgfSlcblxufVxuXG5leHBvcnQgZGVmYXVsdCBpc0ludmlldyIsImltcG9ydCBnc2FwIGZyb20gJ2dzYXAnO1xuaW1wb3J0IHsgU2Nyb2xsVHJpZ2dlciB9IGZyb20gXCJnc2FwL1Njcm9sbFRyaWdnZXJcIjtcbmdzYXAucmVnaXN0ZXJQbHVnaW4oU2Nyb2xsVHJpZ2dlcik7XG5cbmNvbnN0IGxvZ29Db2xvdXIgPSAoKSA9PiB7XG5cbiAgY29uc3Qgc2VjdGlvbnNMaWdodCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJ1tkYXRhLWxpZ2h0XScpXG4gIGNvbnN0IGxvZ28gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuZml4ZWRMb2dvJylcblxuICBpZiAoc2VjdGlvbnNMaWdodCkge1xuXG4gICAgc2VjdGlvbnNMaWdodC5mb3JFYWNoKGVsID0+IHtcblxuICAgICAgU2Nyb2xsVHJpZ2dlci5jcmVhdGUoe1xuICAgICAgICB0cmlnZ2VyOiBlbCxcbiAgICAgICAgc3RhcnQ6ICd0b3AgdG9wKz0xMDBweCcsXG4gICAgICAgIGJvdHRvbTogJ2JvdHRvbSBib3R0b20nLFxuICAgICAgICBvblRvZ2dsZTogKHtwcm9ncmVzcywgZGlyZWN0aW9uLCBpc0FjdGl2ZX0pID0+IHtcbiAgICAgICAgICBpZiAoaXNBY3RpdmUpIGxvZ28uY2xhc3NMaXN0LmFkZCgnbG9nby15ZWxsb3cnKVxuICAgICAgICAgIGVsc2UgbG9nby5jbGFzc0xpc3QucmVtb3ZlKCdsb2dvLXllbGxvdycpXG4gICAgICAgIH0sXG4gICAgICB9KVxuICAgICAgXG4gICAgfSlcblxuICB9XG4gIFxuXG59XG5cbmV4cG9ydCBkZWZhdWx0IGxvZ29Db2xvdXIiLCJpbXBvcnQgTGlnaHRlbnNlIGZyb20gJ2xpZ2h0ZW5zZS1pbWFnZXMnXG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIG5ld3NMaWdodGJveCgpIHtcblxuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignbG9hZCcsIGZ1bmN0aW9uICgpIHtcbiAgICBMaWdodGVuc2UoJy5zaW5nbGUtcG9zdCAucG9zdCBpbWcnKVxuICB9LCBmYWxzZSlcbiAgXG59IiwiZnVuY3Rpb24gZ2V0UGFyYW0ocCkge1xuICB2YXIgbWF0Y2ggPSBSZWdFeHAoJ1s/Jl0nICsgcCArICc9KFteJl0qKScpLmV4ZWMod2luZG93LmxvY2F0aW9uLnNlYXJjaCk7XG4gIHJldHVybiBtYXRjaCAmJiBkZWNvZGVVUklDb21wb25lbnQobWF0Y2hbMV0ucmVwbGFjZSgvXFwrL2csICcgJykpO1xufVxuXG5mdW5jdGlvbiBnZXRFeHBpcnlSZWNvcmQodmFsdWUpIHtcbiAgdmFyIGV4cGlyeVBlcmlvZCA9IDkwICogMjQgKiA2MCAqIDYwICogMTAwMDsgLy8gOTAgZGF5IGV4cGlyeSBpbiBtaWxsaXNlY29uZHNcbiAgdmFyIGV4cGlyeURhdGUgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKSArIGV4cGlyeVBlcmlvZDtcbiAgcmV0dXJuIHtcbiAgICB2YWx1ZTogdmFsdWUsXG4gICAgZXhwaXJ5RGF0ZTogZXhwaXJ5RGF0ZVxuICB9O1xufVxuXG5mdW5jdGlvbiBhZGRQYXJhbXMoKSB7XG4gIHZhciBnY2xpZFBhcmFtID0gZ2V0UGFyYW0oJ2djbGlkJyk7XG4gIHZhciBnY2xpZEZvcm1GaWVsZHMgPSBbJ2djbGlkX2ZpZWxkJ107XG4gIHZhciBnY2xpZFJlY29yZCA9IG51bGw7XG4gIHZhciBjdXJyR2NsaWRGb3JtRmllbGQ7XG5cbiAgdmFyIHV0bVNvdXJjZVBhcmFtID0gZ2V0UGFyYW0oJ3V0bV9zb3VyY2UnKTtcbiAgdmFyIHV0Y1NvdXJjZUZvcm1GaWVsZHMgPSBbJ3V0bV9zb3VyY2UnXTtcbiAgdmFyIHV0bVNvdXJjZVJlY29yZCA9IG51bGw7XG4gIHZhciBjdXJyVXRtU291cmNlRm9ybUZpZWxkO1xuICBcbiAgdmFyIHV0bU1lZGl1bVBhcmFtID0gZ2V0UGFyYW0oJ3V0bV9tZWRpdW0nKTtcbiAgdmFyIHV0Y01lZGl1bUZvcm1GaWVsZHMgPSBbJ3V0bV9tZWRpdW0nXTtcbiAgdmFyIHV0bU1lZGl1bVJlY29yZCA9IG51bGw7XG4gIHZhciBjdXJyVXRtTWVkaXVtRm9ybUZpZWxkO1xuICBcbiAgdmFyIHV0bUNhbXBhaWduUGFyYW0gPSBnZXRQYXJhbSgndXRtX2NhbXBhaWduJyk7XG4gIHZhciB1dGNDYW1wYWlnbkZvcm1GaWVsZHMgPSBbJ3V0bV9jYW1wYWlnbiddO1xuICB2YXIgdXRtQ2FtcGFpZ25SZWNvcmQgPSBudWxsO1xuICB2YXIgY3VyclV0bUNhbXBhaWduRm9ybUZpZWxkO1xuICBcbiAgdmFyIHV0bVRlcm1QYXJhbSA9IGdldFBhcmFtKCd1dG1fdGVybScpO1xuICB2YXIgdXRjVGVybUZvcm1GaWVsZHMgPSBbJ3V0bV90ZXJtJ107XG4gIHZhciB1dG1UZXJtUmVjb3JkID0gbnVsbDtcbiAgdmFyIGN1cnJVdG1UZXJtRm9ybUZpZWxkO1xuXG4gIHZhciBnY2xzcmNQYXJhbSA9IGdldFBhcmFtKCdnY2xzcmMnKTtcbiAgdmFyIGlzR2Nsc3JjVmFsaWQgPSAhZ2Nsc3JjUGFyYW0gfHwgZ2Nsc3JjUGFyYW0uaW5kZXhPZignYXcnKSAhPT0gLTE7XG5cbiAgZ2NsaWRGb3JtRmllbGRzLmZvckVhY2goZnVuY3Rpb24gKGZpZWxkKSB7XG4gICAgaWYgKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGZpZWxkKSkge1xuICAgICAgY3VyckdjbGlkRm9ybUZpZWxkID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoZmllbGQpO1xuICAgIH1cbiAgfSk7XG4gIHV0Y1NvdXJjZUZvcm1GaWVsZHMuZm9yRWFjaChmdW5jdGlvbiAoZmllbGQpIHtcbiAgICBpZiAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoZmllbGQpKSB7XG4gICAgICBjdXJyVXRtU291cmNlRm9ybUZpZWxkID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoZmllbGQpO1xuICAgIH1cbiAgfSk7XG4gIHV0Y01lZGl1bUZvcm1GaWVsZHMuZm9yRWFjaChmdW5jdGlvbiAoZmllbGQpIHtcbiAgICBpZiAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoZmllbGQpKSB7XG4gICAgICBjdXJyVXRtTWVkaXVtRm9ybUZpZWxkID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoZmllbGQpO1xuICAgIH1cbiAgfSk7XG4gIHV0Y0NhbXBhaWduRm9ybUZpZWxkcy5mb3JFYWNoKGZ1bmN0aW9uIChmaWVsZCkge1xuICAgIGlmIChkb2N1bWVudC5nZXRFbGVtZW50QnlJZChmaWVsZCkpIHtcbiAgICAgIGN1cnJVdG1DYW1wYWlnbkZvcm1GaWVsZCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGZpZWxkKTtcbiAgICB9XG4gIH0pO1xuICB1dGNUZXJtRm9ybUZpZWxkcy5mb3JFYWNoKGZ1bmN0aW9uIChmaWVsZCkge1xuICAgIGlmIChkb2N1bWVudC5nZXRFbGVtZW50QnlJZChmaWVsZCkpIHtcbiAgICAgIGN1cnJVdG1UZXJtRm9ybUZpZWxkID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoZmllbGQpO1xuICAgIH1cbiAgfSk7XG5cbiAgaWYgKGdjbGlkUGFyYW0gJiYgaXNHY2xzcmNWYWxpZCkge1xuICAgIGdjbGlkUmVjb3JkID0gZ2V0RXhwaXJ5UmVjb3JkKGdjbGlkUGFyYW0pO1xuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdnY2xpZCcsIEpTT04uc3RyaW5naWZ5KGdjbGlkUmVjb3JkKSk7XG4gIH1cbiAgaWYgKHV0bVNvdXJjZVBhcmFtKSB7XG4gICAgdXRtU291cmNlUmVjb3JkID0gZ2V0RXhwaXJ5UmVjb3JkKHV0bVNvdXJjZVBhcmFtKTtcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgndXRtX3NvdXJjZScsIEpTT04uc3RyaW5naWZ5KHV0bVNvdXJjZVJlY29yZCkpO1xuICB9XG4gIGlmICh1dG1NZWRpdW1QYXJhbSkge1xuICAgIHV0bU1lZGl1bVJlY29yZCA9IGdldEV4cGlyeVJlY29yZCh1dG1NZWRpdW1QYXJhbSk7XG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ3V0bV9tZWRpdW0nLCBKU09OLnN0cmluZ2lmeSh1dG1NZWRpdW1SZWNvcmQpKTtcbiAgfVxuICBpZiAodXRtQ2FtcGFpZ25QYXJhbSkge1xuICAgIHV0bUNhbXBhaWduUmVjb3JkID0gZ2V0RXhwaXJ5UmVjb3JkKHV0bUNhbXBhaWduUGFyYW0pO1xuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCd1dG1fY2FtcGFpZ24nLCBKU09OLnN0cmluZ2lmeSh1dG1DYW1wYWlnblJlY29yZCkpO1xuICB9XG4gIGlmICh1dG1UZXJtUGFyYW0pIHtcbiAgICB1dG1UZXJtUmVjb3JkID0gZ2V0RXhwaXJ5UmVjb3JkKHV0bVRlcm1QYXJhbSk7XG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ3V0bV90ZXJtJywgSlNPTi5zdHJpbmdpZnkodXRtVGVybVJlY29yZCkpO1xuICB9XG5cbiAgdmFyIGdjbGlkID0gZ2NsaWRSZWNvcmQgfHwgSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnZ2NsaWQnKSk7XG4gIHZhciB1dG1fc291cmNlID0gdXRtU291cmNlUmVjb3JkIHx8IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3V0bV9zb3VyY2UnKSk7XG4gIHZhciB1dG1fbWVkaXVtID0gdXRtTWVkaXVtUmVjb3JkIHx8IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3V0bV9tZWRpdW0nKSk7XG4gIHZhciB1dG1fY2FtcGFpZ24gPSB1dG1DYW1wYWlnblJlY29yZCB8fCBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1dG1fY2FtcGFpZ24nKSk7XG4gIHZhciB1dG1fdGVybSA9IHV0bVRlcm1SZWNvcmQgfHwgSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXRtX3Rlcm0nKSk7XG4gIHZhciBpc0djbGlkVmFsaWQgPSBnY2xpZCAmJiBuZXcgRGF0ZSgpLmdldFRpbWUoKSA8IGdjbGlkLmV4cGlyeURhdGU7XG4gIHZhciBpc1V0bVNvdXJjZVZhbGlkID0gdXRtX3NvdXJjZSAmJiBuZXcgRGF0ZSgpLmdldFRpbWUoKSA8IHV0bV9zb3VyY2UuZXhwaXJ5RGF0ZTtcbiAgdmFyIGlzVXRtTWVkaXVtVmFsaWQgPSB1dG1fbWVkaXVtICYmIG5ldyBEYXRlKCkuZ2V0VGltZSgpIDwgdXRtX21lZGl1bS5leHBpcnlEYXRlO1xuICB2YXIgaXNVdG1DYW1wYWlnblZhbGlkID0gdXRtX2NhbXBhaWduICYmIG5ldyBEYXRlKCkuZ2V0VGltZSgpIDwgdXRtX2NhbXBhaWduLmV4cGlyeURhdGU7XG4gIHZhciBpc1V0bVRlcm1WYWxpZCA9IHV0bV90ZXJtICYmIG5ldyBEYXRlKCkuZ2V0VGltZSgpIDwgdXRtX3Rlcm0uZXhwaXJ5RGF0ZTtcblxuICBpZiAoY3VyckdjbGlkRm9ybUZpZWxkICYmIGlzR2NsaWRWYWxpZCkge1xuICAgIGN1cnJHY2xpZEZvcm1GaWVsZC52YWx1ZSA9IGdjbGlkLnZhbHVlO1xuICB9XG4gIGlmIChjdXJyVXRtU291cmNlRm9ybUZpZWxkICYmIGlzVXRtU291cmNlVmFsaWQpIHtcbiAgICBjdXJyVXRtU291cmNlRm9ybUZpZWxkLnZhbHVlID0gdXRtX3NvdXJjZS52YWx1ZTtcbiAgfVxuICBpZiAoY3VyclV0bU1lZGl1bUZvcm1GaWVsZCAmJiBpc1V0bU1lZGl1bVZhbGlkKSB7XG4gICAgY3VyclV0bU1lZGl1bUZvcm1GaWVsZC52YWx1ZSA9IHV0bV9tZWRpdW0udmFsdWU7XG4gIH1cbiAgaWYgKGN1cnJVdG1DYW1wYWlnbkZvcm1GaWVsZCAmJiBpc1V0bUNhbXBhaWduVmFsaWQpIHtcbiAgICBjdXJyVXRtQ2FtcGFpZ25Gb3JtRmllbGQudmFsdWUgPSB1dG1fY2FtcGFpZ24udmFsdWU7XG4gIH1cbiAgaWYgKGN1cnJVdG1UZXJtRm9ybUZpZWxkICYmIGlzVXRtVGVybVZhbGlkKSB7XG4gICAgY3VyclV0bVRlcm1Gb3JtRmllbGQudmFsdWUgPSB1dG1fdGVybS52YWx1ZTtcbiAgfVxufVxuXG53aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignbG9hZCcsIGFkZFBhcmFtcykgXG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIHBhcmFtU3RvcmFnZSgpIHsgfSIsImltcG9ydCB7IGdzYXAgfSBmcm9tICdnc2FwJ1xuaW1wb3J0IHsgU2Nyb2xsVHJpZ2dlciB9IGZyb20gJ2dzYXAvU2Nyb2xsVHJpZ2dlcidcblxuZ3NhcC5yZWdpc3RlclBsdWdpbihTY3JvbGxUcmlnZ2VyKVxuXG5jb25zdCBwaW5JbWFnZSA9ICgpID0+IHtcblxuICBjb25zdCBpbWFnZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zb2x1dGlvbnNfX2ltYWdlSG9sZGVyJylcblxuICBpZiAoIWltYWdlKSByZXR1cm5cblxuICBpZiAod2luZG93LmlubmVyV2lkdGggPiA5NjApIHtcblxuICAgIFNjcm9sbFRyaWdnZXIuY3JlYXRlKHtcbiAgICAgIHRyaWdnZXI6IGltYWdlLFxuICAgICAgcGluOiB0cnVlLFxuICAgICAgYW50aWNpcGF0ZVBpbjogMSxcbiAgICAgIHBpblNwYWNpbmc6IGZhbHNlLFxuICAgICAgc3RhcnQ6ICd0b3AgNTBweCcsXG4gICAgICBlbmQ6ICdib3R0b20gYm90dG9tLT0yNTBweCcsXG4gICAgICBlbmRUcmlnZ2VyOiAnLmVuZC10cmlnZ2VyJyxcbiAgICAgIC8vIG1hcmtlcnM6IHRydWVcbiAgICB9KVxuXG4gIH1cbiAgXG59XG5cbmV4cG9ydCBkZWZhdWx0IHBpbkltYWdlIiwiaW1wb3J0IEZsaWNraXR5IGZyb20gJ2ZsaWNraXR5J1xuXG5jbGFzcyBQcmljaW5nIHtcbiAgY29uc3RydWN0b3IoZWwpIHtcblxuICAgIHRoaXMuc2xpZGVyID0gZWwucXVlcnlTZWxlY3RvcignLnByaWNpbmdfX3NsaWRlcicpXG4gICAgdGhpcy5wcmljZXMgPSBlbC5xdWVyeVNlbGVjdG9yQWxsKCcucHJpY2luZ19fcHJpY2VzJylcbiAgICB0aGlzLmRvdHMgPSBlbC5xdWVyeVNlbGVjdG9yQWxsKCcucHJpY2luZ19fZG90JylcbiAgICB0aGlzLnNlbGVjdCA9IGVsLnF1ZXJ5U2VsZWN0b3IoJy5wcmljaW5nX19jb2x1bW4gc2VsZWN0JylcbiAgICB0aGlzLnNlbGVjdC5hZGRFdmVudExpc3RlbmVyKCdjaGFuZ2UnLCB0aGlzLnVwZGF0ZUN1cnJlbmN5LmJpbmQodGhpcykpXG4gICAgdGhpcy5hcnJvd05leHQgPSBlbC5wYXJlbnROb2RlLnF1ZXJ5U2VsZWN0b3IoJy5uZXh0JylcbiAgICB0aGlzLmFycm93UHJldiA9IGVsLnBhcmVudE5vZGUucXVlcnlTZWxlY3RvcignLnByZXZpb3VzJylcblxuICAgIGlmIChlbC5jbGFzc0xpc3QuY29udGFpbnMoJ3ByaWNpbmctLXNsaWRlcicpKSB0aGlzLmluaXRTbGlkZXIoKVxuXG4gIH1cbiAgaW5pdFNsaWRlcigpIHtcblxuICAgIHRoaXMuZmxpY2tpdHkgPSBuZXcgRmxpY2tpdHkodGhpcy5zbGlkZXIsIHtcbiAgICAgIGNvbnRhaW46IGZhbHNlLFxuICAgICAgcGFnZURvdHM6IGZhbHNlLFxuICAgICAgcHJldk5leHRCdXR0b25zOiBmYWxzZSxcbiAgICAgIGRyYWdnYWJsZTogdHJ1ZSxcbiAgICAgIGNlbGxBbGlnbjogJ2xlZnQnXG4gICAgfSlcblxuICAgIHRoaXMuZG90c1swXS5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKVxuICAgIHRoaXMuYXJyb3dOZXh0LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgdGhpcy5uZXh0U2xpZGUuYmluZCh0aGlzKSlcbiAgICB0aGlzLmFycm93UHJldi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMucHJldlNsaWRlLmJpbmQodGhpcykpXG5cbiAgICB0aGlzLmZsaWNraXR5Lm9uKCAnY2hhbmdlJywgdGhpcy5zbGlkZUNoYW5nZWQuYmluZCh0aGlzKSlcblxuICB9XG4gIHNsaWRlQ2hhbmdlZChlKSB7XG5cbiAgICBpZiAodGhpcy5mbGlja2l0eS5zZWxlY3RlZEluZGV4ID09PSAwKSB7XG4gICAgICB0aGlzLmFycm93UHJldi5kaXNhYmxlZCA9IHRydWVcbiAgICB9IGVsc2UgaWYgKHRoaXMuZmxpY2tpdHkuc2VsZWN0ZWRJbmRleCA9PT0gdGhpcy5mbGlja2l0eS5zbGlkZXMubGVuZ3RoIC0gMSkge1xuICAgICAgdGhpcy5hcnJvd05leHQuZGlzYWJsZWQgPSB0cnVlXG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuYXJyb3dQcmV2LmRpc2FibGVkID0gZmFsc2VcbiAgICAgIHRoaXMuYXJyb3dOZXh0LmRpc2FibGVkID0gZmFsc2VcbiAgICB9XG5cbiAgICB0aGlzLmRvdHMuZm9yRWFjaChlbCA9PiB7IGVsLmNsYXNzTGlzdC5yZW1vdmUoJ2FjdGl2ZScpIH0pXG4gICAgdGhpcy5kb3RzW3RoaXMuZmxpY2tpdHkuc2VsZWN0ZWRJbmRleF0uY2xhc3NMaXN0LmFkZCgnYWN0aXZlJylcblxuICB9XG4gIHVwZGF0ZUN1cnJlbmN5KGUpIHtcblxuICAgIHRoaXMucHJpY2VzLmZvckVhY2goZWwgPT4ge1xuICAgICAgZWwuc2V0QXR0cmlidXRlKCdkYXRhLWN1cnJlbmN5JywgZS50YXJnZXQudmFsdWUpXG4gICAgfSlcblxuICB9XG4gIG5leHRTbGlkZShlKSB7XG5cbiAgICBlLnByZXZlbnREZWZhdWx0KClcbiAgICB0aGlzLmZsaWNraXR5Lm5leHQoKVxuXG4gIH1cbiAgcHJldlNsaWRlKGUpIHtcblxuICAgIGUucHJldmVudERlZmF1bHQoKVxuICAgIHRoaXMuZmxpY2tpdHkucHJldmlvdXMoKVxuXG4gIH1cbn1cblxuY29uc3QgaW5pdFByaWNpbmcgPSAoKSA9PiB7XG5cbiAgY29uc3Qgc2VjdGlvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5wcmljaW5nJylcblxuICBpZiAoc2VjdGlvbikgbmV3IFByaWNpbmcoc2VjdGlvbilcblxufVxuXG5leHBvcnQgZGVmYXVsdCBpbml0UHJpY2luZyIsImltcG9ydCBheGlvcyBmcm9tICdheGlvcyc7XG5pbXBvcnQgQm91bmNlciBmcm9tICdmb3JtYm91bmNlcmpzJztcbiBcbmxldCBmb3JtSGFzU3VibWl0dGVkID0gZmFsc2UgXG5cbnZhciBib3VuY2VyID0gbmV3IEJvdW5jZXIoJ1tkYXRhLXZhbGlkYXRlLWluZHhyXScsIHtcbiAgZGlzYWJsZVN1Ym1pdDogdHJ1ZSxcbiAgY3VzdG9tVmFsaWRhdGlvbnM6IHtcbiAgICB2YWxpZGF0ZURvbWFpbjogKGZpZWxkKSA9PiB7XG4gICAgICBpZiAoZmllbGQuaWQgPT09ICdlbWFpbF9hZGRyZXNzJykgeyBcblxuICAgICAgICAvLyBDaGVjayBpZiBpbnB1dHRlZCBkb21haW4gY29udGFpbnMgYW55IG9mIHRob3NlIGxpc3RlZCBpbiByZWplY3RlZERvbWFpbnMuXG4gICAgICAgIGNvbnN0IHJlamVjdGVkRG9tYWlucyA9IFsneWFob28nLCAnZ21haWwnLCAnYW9sJywgJ2hvdG1haWwnLCAnb3V0bG9vaycsICdtYWMnLCAnbGl2ZScsICdnb29nbGVtYWlsJywgJ2ljbG91ZCddXG4gICAgICAgIGNvbnN0IGlucHV0dGVkRG9tYWluID0gZmllbGQudmFsdWUuc3BsaXQoXCJAXCIpXG4gICAgICAgIGxldCBkb21haW4gPSAnJ1xuXG4gICAgICAgIGlmIChpbnB1dHRlZERvbWFpblsxXSkge1xuICAgICAgICAgIGRvbWFpbiA9IGlucHV0dGVkRG9tYWluWzFdLnNwbGl0KCcuJylbMF1cbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKHJlamVjdGVkRG9tYWlucy5pbmRleE9mKGRvbWFpbikgPj0gMCkge1xuICAgICAgICAgIHJldHVybiB0cnVlXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICAgIH1cblxuICAgICAgfVxuICAgIH1cbiAgfSxcbiAgbWVzc2FnZXM6IHtcblx0XHR2YWxpZGF0ZURvbWFpbjogJ1BsZWFzZSBwcm92aWRlIGEgYnVzaW5lc3MgYW5kIG5vdCBhIHBlcnNvbmFsIGVtYWlsIGFkZHJlc3MuJ1xuXHR9XG59KVxuXG5jb25zdCBwcm9jZXNzSW5keHJGb3JtID0gKGUpID0+IHtcbiAgZS5wcmV2ZW50RGVmYXVsdCgpXG4gIGNvbnN0IGZvcm0gPSBlLnRhcmdldFxuICBjb25zdCBlbmRQb2ludCA9ICdodHRwczovL2Z1bmN0aW9uc2FkbWlucGFhbHdheXNvbmNvcmU1LmF6dXJld2Vic2l0ZXMubmV0L2FwaS9XZWJUcmlhbFJlcXVlc3RJbmR4cj9jb2RlPXpCQmVRbVBsWUNSMDUvYVI1SHNFR24yak1VOXdJeXYwdEZ5M1VSRlNpNzNDNXNYME55MHY5dz09J1xuICBjb25zdCBzdWNjZXNzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmNvbnRhY3RGb3JtX19zdWNjZXNzJylcbiAgY29uc3QgZXJyb3JFbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jb250YWN0Rm9ybV9fZXJyb3InKVxuICBjb25zdCBleGlzdHNFbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jb250YWN0Rm9ybV9fZXhpc3RzJylcbiAgY29uc3QgcmVzcG9uc2VFbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jb250YWN0Rm9ybV9fcmVzcG9uc2UnKVxuICBjb25zdCBhcGlLZXlFbGVtID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI1RyaWFsQXBpS2V5JylcbiAgXG4gIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2JvdW5jZXJGb3JtSW52YWxpZCcsIGZ1bmN0aW9uIChldmVudCkge1xuICAgIGNvbnNvbGUubG9nKGV2ZW50LmRldGFpbC5lcnJvcnMpXG4gIH0sIGZhbHNlKVxuXG4gIC8qKiBJZiB2YWxpZGF0aW9uIHBhc3NlcywgZmlyZSBBSkFYIHN1Ym1pc3Npb24gKi9cbiAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignYm91bmNlckZvcm1WYWxpZCcsIGZ1bmN0aW9uIChlKSB7XG5cbiAgICBpZiAoZm9ybUhhc1N1Ym1pdHRlZCkgcmV0dXJuIGZhbHNlXG5cbiAgICBmb3JtSGFzU3VibWl0dGVkID0gdHJ1ZVxuXG4gICAgdmFyIGZpcnN0TmFtZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZmlyc3RfbmFtZVwiKS52YWx1ZVxuICAgIHZhciBsYXN0TmFtZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibGFzdF9uYW1lXCIpLnZhbHVlXG4gICAgdmFyIGNvbXBhbnlOYW1lID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjb21wYW55XCIpLnZhbHVlXG4gICAgdmFyIGpvYlJvbGUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInJvbGVcIikudmFsdWVcbiAgICB2YXIgY29udGFjdE51bWJlciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGhvbmVfbnVtYmVyXCIpLnZhbHVlXG4gICAgdmFyIGVtYWlsQWRkcmVzcyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZW1haWxfYWRkcmVzc1wiKS52YWx1ZVxuICAgIHZhciBtYWlsTGlzdEFwcHJvdmVkID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJtYWlsX2NoZWNrXCIpLmNoZWNrZWRcbiAgICB2YXIgZ2NsaWQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImdjbGlkX2ZpZWxkXCIpLnZhbHVlXG4gICAgdmFyIHNvdXJjZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwidXRtX3NvdXJjZVwiKS52YWx1ZVxuICAgIHZhciBtZWRpdW0gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInV0bV9tZWRpdW1cIikudmFsdWVcbiAgICB2YXIgY2FtcGFpZ24gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInV0bV9jYW1wYWlnblwiKS52YWx1ZVxuICAgIHZhciB0ZXJtID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ1dG1fdGVybVwiKS52YWx1ZVxuXG4gICAgdmFyIHJlcXVlc3REYXRhID0geyBUeXBlOiAnSW5keHJBcHAnLCBGaXJzdE5hbWU6IGZpcnN0TmFtZSwgTGFzdE5hbWU6IGxhc3ROYW1lLCBDb21wYW55TmFtZTogY29tcGFueU5hbWUsIEpvYlJvbGU6IGpvYlJvbGUsIENvbnRhY3ROdW1iZXI6IGNvbnRhY3ROdW1iZXIsIEVtYWlsQWRkcmVzczogZW1haWxBZGRyZXNzLCBNYWlsTGlzdEFwcHJvdmVkOiBtYWlsTGlzdEFwcHJvdmVkLCB1dG1DYW1wYWlnbjogY2FtcGFpZ24sIHV0bVNvdXJjZTogc291cmNlLCB1dG1NZWRpdW06IG1lZGl1bSwgdXRtVGVybTogdGVybSwgZ2NsaWQ6IGdjbGlkIH1cblxuICAgIGNvbnN0IG9wdGlvbnMgPSB7XG4gICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KHJlcXVlc3REYXRhKSxcbiAgICAgIG1vZGU6ICdjb3JzJyxcbiAgICAgIGhlYWRlcnM6IHsgXG4gICAgICAgICdBY2Nlc3MtQ29udHJvbC1BbGxvdy1DcmVkZW50aWFscyc6ICd0cnVlJyxcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgJ0FjY2Vzcy1Db250cm9sLUFsbG93LUhlYWRlcnMnOiAnT3JpZ2luLCBYLVJlcXVlc3RlZC1XaXRoLCBDb250ZW50LVR5cGUsIEFjY2VwdCdcbiAgICAgIH1cbiAgICB9XG4gIFxuICAgIGZldGNoKGVuZFBvaW50LCBvcHRpb25zKVxuICAgICAgLnRoZW4ocmVzID0+IHJlcy5qc29uKCkpXG4gICAgICAudGhlbihyZXMgPT4ge1xuICAgICAgICBjb25zb2xlLmxvZyhyZXMpXG4gICAgICAgIGNvbnNvbGUubG9nKCdBcGlLZXk6ICcgKyByZXMuQXBpS2V5KVxuICAgICAgICBjb25zb2xlLmxvZygnRXJyb3I6ICcgKyByZXMuRXJyb3IpXG4gICAgICAgIGlmIChyZXMuRXJyb3IgIT0gbnVsbCkge1xuICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jb250YWN0Rm9ybV9fcmVzcG9uc2UgaDMnKS5pbm5lckhUTUwgPSByZXMuRXJyb3JcbiAgICAgICAgICBmb3JtLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSdcbiAgICAgICAgICByZXNwb25zZUVsLnN0eWxlLmRpc3BsYXkgPSAnYmxvY2snXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgYXBpS2V5RWxlbS5pbm5lckhUTUwgPSByZXMuQXBpS2V5XG4gICAgICAgICAgZm9ybS5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnXG4gICAgICAgICAgc3VjY2Vzcy5zdHlsZS5kaXNwbGF5ID0gJ2Jsb2NrJ1xuICAgICAgICB9XG4gICAgICB9KVxuICAgICAgLmNhdGNoKGVyciA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKGVycilcbiAgICAgICAgY29uc29sZS5sb2coYEVycm9yIHdpdGggbWVzc2FnZTogJHtlcnJ9YClcbiAgICAgICAgZm9ybS5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnXG4gICAgICAgIGVycm9yRWwuc3R5bGUuZGlzcGxheSA9ICdibG9jaydcbiAgICAgIH0pXG5cbiAgICBmb3JtLmNsYXNzTGlzdC5hZGQoJ3N1Ym1pdHRlZCcpXG5cbiAgfSwgZmFsc2UpXG4gIFxuICAvLyBDb3B5IHRoZSBBUEkgdG8gdGhlIGNsaXBib2FyZFxuICBhcGlLZXlFbGVtLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4ge1xuICBcbiAgICBjb25zdCBhcGlLZXkgPSBhcGlLZXlFbGVtLmlubmVySFRNTFxuICAgIGNvbnN0IHRlbXBJbnB1dCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2lucHV0JylcbiAgICB0ZW1wSW5wdXQudmFsdWUgPSBhcGlLZXlcbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdib2R5JykuYXBwZW5kQ2hpbGQodGVtcElucHV0KVxuICAgIHRlbXBJbnB1dC5zZWxlY3QoKVxuICAgIGRvY3VtZW50LmV4ZWNDb21tYW5kKCdjb3B5JylcbiAgICB0ZW1wSW5wdXQucmVtb3ZlKClcblxuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy50b29sdGlwJykuaW5uZXJIVE1MID0gJ0NvcGllZCEnXG5cbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy50b29sdGlwJykuaW5uZXJIVE1MID0gJ0NsaWNrIHRvIENvcHknXG4gICAgfSwgMzAwMClcbiAgfSlcblxufTsgXG5cbmNvbnN0IGluaXRQcm9jZXNzSW5keHJGb3JtID0gKCkgPT4ge1xuXG4gIGNvbnN0IGZvcm1zID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnW2RhdGEtcHJvY2Vzcy1pbmR4cl0nKVxuXG4gIGZvcm1zLmZvckVhY2goZWwgPT4ge1xuICAgIGVsLmFkZEV2ZW50TGlzdGVuZXIoJ3N1Ym1pdCcsIHByb2Nlc3NJbmR4ckZvcm0sIGZhbHNlKVxuICB9KVxuXG59XG5cbmV4cG9ydCBkZWZhdWx0IGluaXRQcm9jZXNzSW5keHJGb3JtOyIsImltcG9ydCB7IGdzYXAgfSBmcm9tICdnc2FwJ1xuaW1wb3J0IHsgU2Nyb2xsVHJpZ2dlciB9IGZyb20gJ2dzYXAvU2Nyb2xsVHJpZ2dlcidcbmltcG9ydCB7IFNjcm9sbFRvUGx1Z2luIH0gZnJvbSAnZ3NhcC9TY3JvbGxUb1BsdWdpbidcblxuZ3NhcC5yZWdpc3RlclBsdWdpbihTY3JvbGxUb1BsdWdpbiwgU2Nyb2xsVHJpZ2dlcilcblxuY29uc3QgcHJvZHVjdFNpZGViYXIgPSAoKSA9PiB7XG5cbiAgY29uc3Qgc2lkZWJhciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5zaWRlYmFyX19jb250ZW50JylcbiAgY29uc3Qgc2lkZWJhckJ1dHRvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5zaWRlYmFyX19idXR0b24nKVxuICBjb25zdCBmaXhlZEJ1dHRvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5maXhlZEJ0bicpXG5cbiAgY29uc3QgdXJsID0gd2luZG93LmxvY2F0aW9uLnNlYXJjaFxuICBjb25zdCB1cmxQYXJhbXMgPSBuZXcgVVJMU2VhcmNoUGFyYW1zKHVybClcblxuICBpZiAodXJsUGFyYW1zLmhhcygnZm9ybScpKSB7IHNldFRpbWVvdXQoKCkgPT4ge1xuICAgIGdzYXAudG8od2luZG93LCB7IHNjcm9sbFRvOiB7IHk6IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNmb3JtJyksIGVhc2U6ICdwb3dlcjEuaW5PdXQnIH0gfSlcbiAgfSwgMTUwMCkgfVxuXG4gIGlmIChmaXhlZEJ1dHRvbikge1xuICAgIGZpeGVkQnV0dG9uLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKGUpID0+IHtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKVxuICAgICAgY29uc3Qgc2VjdGlvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyMnICsgZS5jdXJyZW50VGFyZ2V0LmRhdGFzZXQudG8pXG4gICAgICBnc2FwLnRvKHdpbmRvdywgeyBzY3JvbGxUbzogeyB5OiBzZWN0aW9uLCBlYXNlOiAncG93ZXIxLmluT3V0JyB9IH0pXG4gICAgfSlcbiAgfVxuXG4gIGlmICghc2lkZWJhciB8fCB3aW5kb3cuaW5uZXJXaWR0aCA8IDk2MCkgcmV0dXJuXG4gIFxuICBzaWRlYmFyLmZvckVhY2goZWwgPT4ge1xuXG4gICAgU2Nyb2xsVHJpZ2dlci5jcmVhdGUoe1xuICAgICAgdHJpZ2dlcjogZWwsXG4gICAgICBwaW46IHRydWUsXG4gICAgICBhbnRpY2lwYXRlUGluOiAxLFxuICAgICAgcGluU3BhY2luZzogZmFsc2UsXG4gICAgICBzdGFydDogJ3RvcCAxNTBweCcsXG4gICAgICBlbmQ6ICdib3R0b20gYm90dG9tLT0yNThweCcsXG4gICAgICBlbmRUcmlnZ2VyOiAnLmVuZC10cmlnZ2VyJyxcbiAgICAgIC8vIG1hcmtlcnM6IHRydWVcbiAgICB9KVxuXG4gIH0pXG5cbiAgc2lkZWJhckJ1dHRvbi5mb3JFYWNoKGVsID0+IHtcbiAgICBlbC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIChlKSA9PiB7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KClcbiAgICAgIGNvbnN0IHNlY3Rpb24gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjJyArIGUuY3VycmVudFRhcmdldC5kYXRhc2V0LnRvKVxuICAgICAgZ3NhcC50byh3aW5kb3csIHsgc2Nyb2xsVG86IHsgeTogc2VjdGlvbiwgZWFzZTogJ3Bvd2VyMS5pbk91dCcgfSB9KVxuICAgIH0pXG4gIH0pXG4gIFxufVxuXG5leHBvcnQgZGVmYXVsdCBwcm9kdWN0U2lkZWJhciIsImltcG9ydCB7IGdzYXAgfSBmcm9tICdnc2FwJ1xuaW1wb3J0IHsgU2Nyb2xsVG9QbHVnaW4gfSBmcm9tICdnc2FwL1Njcm9sbFRvUGx1Z2luJ1xuaW1wb3J0IHsgU2Nyb2xsVHJpZ2dlciB9IGZyb20gJ2dzYXAvU2Nyb2xsVHJpZ2dlcidcblxuZ3NhcC5yZWdpc3RlclBsdWdpbihTY3JvbGxUcmlnZ2VyLCBTY3JvbGxUb1BsdWdpbilcblxuY2xhc3MgUXVpY2tsaW5rcyB7XG4gIGNvbnN0cnVjdG9yKGVsKSB7XG5cbiAgICB0aGlzLmVsID0gZWxcbiAgICB0aGlzLmxpc3QgPSBlbC5xdWVyeVNlbGVjdG9yKCcucXVpY2tsaW5rc19fbGlzdCcpXG4gICAgdGhpcy5saW5rcyA9IGVsLnF1ZXJ5U2VsZWN0b3JBbGwoJ1tkYXRhLXF1aWNrbGlua10nKVxuICAgIHRoaXMuc2VjdGlvbnMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCdbZGF0YS1zZWN0aW9uXScpXG5cbiAgICB0aGlzLmxpbmtzLmZvckVhY2goZWwgPT4ge1xuICAgICAgZWwuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCB0aGlzLnNjcm9sbFRvU2VjdGlvbi5iaW5kKHRoaXMpKVxuICAgIH0pXG5cbiAgICB0aGlzLnBpbkxpbmtzKClcbiAgICB0aGlzLmFjdGl2ZVN0YXRlcygpXG5cbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignd2hlZWwnLCB0aGlzLm1vdmVRdWlja2xpbmtzLmJpbmQodGhpcykpXG5cbiAgfVxuICBwaW5MaW5rcygpIHtcblxuICAgIC8vIFNjcm9sbFRyaWdnZXIuY3JlYXRlKHtcbiAgICAvLyAgIHRyaWdnZXI6IHRoaXMuZWwsXG4gICAgLy8gICBwaW46IHRydWUsXG4gICAgLy8gICBhbnRpY2lwYXRlUGluOiAxLFxuICAgIC8vICAgcGluU3BhY2luZzogZmFsc2UsXG4gICAgLy8gICBzdGFydDogJ3RvcCAyNXB4JyxcbiAgICAvLyAgIGVuZDogJ2JvdHRvbSBib3R0b20tPTE1MHB4JyxcbiAgICAvLyAgIGVuZFRyaWdnZXI6ICcuZmFxcydcbiAgICAvLyB9KVxuXG4gIH1cbiAgYWN0aXZlU3RhdGVzKCkge1xuXG4gICAgdGhpcy5zZWN0aW9ucy5mb3JFYWNoKGVsID0+IHtcbiAgICAgIFxuICAgICAgU2Nyb2xsVHJpZ2dlci5jcmVhdGUoe1xuICAgICAgICB0cmlnZ2VyOiBlbCxcbiAgICAgICAgc3RhcnQ6ICd0b3AgdG9wJyxcbiAgICAgICAgZW5kOiAnYm90dG9tIGJvdHRvbScsXG4gICAgICAgIC8vIG1hcmtlcnM6IHRydWUsXG4gICAgICAgIG9uRW50ZXI6ICgpID0+IHtcbiAgICAgICAgICB0aGlzLmxpbmtzLmZvckVhY2goZWwgPT4geyBlbC5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKSB9KVxuICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ1tkYXRhLXF1aWNrbGluaz1cIicgKyBlbC5kYXRhc2V0LnNlY3Rpb24gKyAnXCJdJykuY2xhc3NMaXN0LmFkZCgnYWN0aXZlJylcbiAgICAgICAgfSxcbiAgICAgICAgb25FbnRlckJhY2s6ICgpID0+IHtcbiAgICAgICAgICB0aGlzLmxpbmtzLmZvckVhY2goZWwgPT4geyBlbC5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKSB9KVxuICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ1tkYXRhLXF1aWNrbGluaz1cIicgKyBlbC5kYXRhc2V0LnNlY3Rpb24gKyAnXCJdJykuY2xhc3NMaXN0LmFkZCgnYWN0aXZlJylcbiAgICAgICAgfVxuICAgICAgfSlcblxuICAgIH0pXG5cbiAgfVxuICBzY3JvbGxUb1NlY3Rpb24oZSkge1xuXG4gICAgY29uc3Qgc2VjdGlvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ1tkYXRhLXNlY3Rpb249XCInICsgZS5jdXJyZW50VGFyZ2V0LmRhdGFzZXQucXVpY2tsaW5rICsgJ1wiXScpXG5cbiAgICBnc2FwLnRvKHdpbmRvdywgeyBzY3JvbGxUbzogeyB5OiBzZWN0aW9uLCBvZmZzZXRZOiAtMTAsIGVhc2U6ICdwb3dlcjEuaW5PdXQnIH0gfSlcblxuICB9XG4gIG1vdmVRdWlja2xpbmtzKGUpIHtcbiAgICAvLyBpZiAoZS5kZWx0YVkgPiAwICYmIHdpbmRvdy5pbm5lcldpZHRoID4gOTYwKSB7XG4gICAgLy8gICB0aGlzLmxpc3QuY2xhc3NMaXN0LnJlbW92ZSgnbmF2LXJldmVhbGVkJylcbiAgICAvLyB9IGVsc2Uge1xuICAgIC8vICAgdGhpcy5saXN0LmNsYXNzTGlzdC5hZGQoJ25hdi1yZXZlYWxlZCcpXG4gICAgLy8gfVxuICB9XG59XG5cbmNvbnN0IGluaXRRdWlja2xpbmtzID0gKCkgPT4ge1xuXG4gIGNvbnN0IHF1aWNrbGlua3MgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdbZGF0YS1xdWlja2xpbmtzXScpXG4gIFxuICBpZiAoIXF1aWNrbGlua3MpIHJldHVyblxuXG4gIG5ldyBRdWlja2xpbmtzKHF1aWNrbGlua3MpXG5cbn1cblxuZXhwb3J0IGRlZmF1bHQgaW5pdFF1aWNrbGlua3MiLCJpbXBvcnQgZ3NhcCBmcm9tICdnc2FwJ1xuaW1wb3J0IHsgU2Nyb2xsVG9QbHVnaW4gfSBmcm9tICdnc2FwL1Njcm9sbFRvUGx1Z2luJ1xuXG5nc2FwLnJlZ2lzdGVyUGx1Z2luKFNjcm9sbFRvUGx1Z2luKVxuXG5jb25zdCBzY3JvbGxUb1NlY3Rpb24gPSAoKSA9PiB7XG5cbiAgY29uc3QgdHJpZ2dlciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJ1tkYXRhLXRvXScpXG5cbiAgdHJpZ2dlci5mb3JFYWNoKGVsID0+IHtcbiAgICBlbC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIChlKSA9PiB7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KClcblxuICAgICAgZ3NhcC50byh3aW5kb3csIHsgZHVyYXRpb246IDEsIHNjcm9sbFRvOiAnIycgKyBlbC5kYXRhc2V0LnRvIH0pXG5cbiAgICB9KVxuICB9KVxuXG59XG5cbmV4cG9ydCBkZWZhdWx0IHNjcm9sbFRvU2VjdGlvbiIsImltcG9ydCBnc2FwIGZyb20gJ2dzYXAnXG5cbmNsYXNzIFNlcnZpY2VUYWJzIHtcbiAgY29uc3RydWN0b3IoZWwpIHtcblxuICAgIHRoaXMuc2xpZGVIZWlnaHQgPSAwXG4gICAgdGhpcy50YWJzID0gZWwucXVlcnlTZWxlY3RvckFsbCgnLnNlcnZpY2VUYWJzX190YWInKVxuICAgIHRoaXMuc2xpZGVzID0gZWwucXVlcnlTZWxlY3RvckFsbCgnLnNlcnZpY2VUYWJzX19zbGlkZScpXG4gICAgdGhpcy5zbGlkZUhvbGRlciA9IGVsLnF1ZXJ5U2VsZWN0b3IoJy5zZXJ2aWNlVGFic19fc2xpZGVzJylcbiAgICB0aGlzLnRpdGxlcyA9IEFycmF5LmZyb20oZWwucXVlcnlTZWxlY3RvckFsbCgnLnNlcnZpY2VUYWJzX190aXRsZScpKVxuXG4gICAgdGhpcy50YWJzWzBdLmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpXG4gICAgdGhpcy5zbGlkZXNbMF0uY2xhc3NMaXN0LmFkZCgnYWN0aXZlJylcblxuICAgIHRoaXMudGFicy5mb3JFYWNoKGVsID0+IHtcbiAgICAgIGVsLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgdGhpcy5jaGFuZ2VUYWIuYmluZCh0aGlzKSlcbiAgICB9KVxuXG4gICAgLy8gd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRoaXMucmVzaXplLmJpbmQodGhpcykpXG5cbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPCA5NjApIHtcbiAgICAgIHRoaXMuc2V0dXBNb2JpbGUoKVxuICAgIH1cblxuICB9XG4gIHNldHVwTW9iaWxlKCkge1xuXG4gICAgdGhpcy50aXRsZXMuZm9yRWFjaCgoZWwsIGkpID0+IHtcbiAgICAgIHRoaXMuc2xpZGVzW2ldLnN0eWxlLmhlaWdodCA9IGVsLm9mZnNldEhlaWdodCArICdweCdcbiAgICAgIGVsLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgdGhpcy5vcGVuU2xpZGUuYmluZCh0aGlzKSlcbiAgICB9KVxuXG4gIH1cbiAgY2hhbmdlVGFiKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KClcblxuICAgIHRoaXMudGFicy5mb3JFYWNoKGVsID0+IHsgZWwuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJykgfSlcbiAgICB0aGlzLnNsaWRlcy5mb3JFYWNoKGVsID0+IHsgZWwuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJykgfSlcblxuICAgIGUuY3VycmVudFRhcmdldC5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKVxuXG4gICAgdGhpcy50YWJzLmZvckVhY2goKGVsLCBpKSA9PiB7XG4gICAgICBpZiAoZWwgPT09IGUuY3VycmVudFRhcmdldCkge1xuICAgICAgICB0aGlzLnNsaWRlc1tpXS5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKVxuICAgICAgICB0aGlzLnNsaWRlSGVpZ2h0ID0gdGhpcy5zbGlkZXNbaV0ub2Zmc2V0SGVpZ2h0XG4gICAgICB9XG4gICAgfSlcblxuICAgIGdzYXAudG8odGhpcy5zbGlkZUhvbGRlciwgeyBkdXJhdGlvbjogMC4zLCBoZWlnaHQ6IHRoaXMuc2xpZGVIZWlnaHQgKyAncHgnIH0pXG5cbiAgfVxuICBvcGVuU2xpZGUoZSkge1xuXG4gICAgY29uc3QgaW5kZXggPSB0aGlzLnRpdGxlcy5pbmRleE9mKGUuY3VycmVudFRhcmdldClcblxuICAgIGlmIChlLmN1cnJlbnRUYXJnZXQuY2xhc3NMaXN0LmNvbnRhaW5zKCdvcGVuJykpIHtcblxuICAgICAgZ3NhcC50byh0aGlzLnNsaWRlc1tpbmRleF0sIHsgZHVyYXRpb246IDAuNCwgaGVpZ2h0OiBlLmN1cnJlbnRUYXJnZXQub2Zmc2V0SGVpZ2h0ICsgJ3B4JywgZWFzZTogJ3Bvd2VyMS5pbk91dCcgfSlcbiAgICAgIGUuY3VycmVudFRhcmdldC5jbGFzc0xpc3QucmVtb3ZlKCdvcGVuJylcblxuICAgIH0gZWxzZSB7XG5cbiAgICAgIGdzYXAudG8odGhpcy5zbGlkZXNbaW5kZXhdLCB7IGR1cmF0aW9uOiAwLjQsIGhlaWdodDogJ2F1dG8nLCBlYXNlOiAncG93ZXIxLmluT3V0JyB9KVxuICAgICAgZS5jdXJyZW50VGFyZ2V0LmNsYXNzTGlzdC5hZGQoJ29wZW4nKVxuXG4gICAgfVxuICB9XG4gIC8vIHJlc2l6ZSgpIHtcbiAgLy8gICBpZiAod2luZG93LmlubmVyV2lkdGggPCA5NjApIHtcbiAgLy8gICAgIHRoaXMudGl0bGVzLmZvckVhY2goKGVsLCBpKSA9PiB7XG4gIC8vICAgICAgIHRoaXMuc2xpZGVzW2ldLnN0eWxlLmhlaWdodCA9IGVsLm9mZnNldEhlaWdodCArICdweCdcbiAgLy8gICAgIH0pXG4gIC8vICAgfSBlbHNlIHtcbiAgLy8gICAgIGdzYXAudG8odGhpcy5zbGlkZXMsIHsgZHVyYXRpb246IDAuMSwgaGVpZ2h0OiAnYXV0bycsIGVhc2U6ICdwb3dlcjEuaW5PdXQnIH0pXG4gIC8vICAgfVxuICAvLyB9XG59XG5cbmNvbnN0IGluaXRTZXJ2aWNlVGFicyA9ICgpID0+IHtcblxuICBjb25zdCBzZWN0aW9uID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnNlcnZpY2VUYWJzJylcbiAgXG4gIGlmIChzZWN0aW9uKSBuZXcgU2VydmljZVRhYnMoc2VjdGlvbilcblxufVxuXG5leHBvcnQgZGVmYXVsdCBpbml0U2VydmljZVRhYnMiLCJpbXBvcnQgZ3NhcCBmcm9tICdnc2FwJ1xuaW1wb3J0IHsgU2Nyb2xsVHJpZ2dlciB9IGZyb20gJ2dzYXAvU2Nyb2xsVHJpZ2dlcidcbmltcG9ydCB7IENvdW50VXAgfSBmcm9tICdjb3VudHVwLmpzJ1xuXG5nc2FwLnJlZ2lzdGVyUGx1Z2luKFNjcm9sbFRyaWdnZXIpXG5cbmNvbnN0IHN0YXRpc3RpYyA9ICgpID0+IHtcblxuICBjb25zdCBzdGF0aXN0aWMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcucHJvZHVjdEludHJvX19zdGF0aXN0aWMgc3BhbicpXG4gIFxuICBpZiAoIXN0YXRpc3RpYykgcmV0dXJuXG5cbiAgY29uc3QgY291bnQgPSBuZXcgQ291bnRVcChzdGF0aXN0aWMsIHN0YXRpc3RpYy5kYXRhc2V0LmNvdW50KVxuXG4gIFNjcm9sbFRyaWdnZXIuY3JlYXRlKHtcbiAgICB0cmlnZ2VyOiAnLnByb2R1Y3RJbnRyb19fc3RhdGlzdGljJyxcbiAgICBzdGFydDogJ3RvcCA3NSUnLFxuICAgIGVuZDogJ3RvcCA3NSUnLFxuICAgIHRvZ2dsZUNsYXNzOiAnaXMtaW52aWV3JyxcbiAgICBvbmNlOiB0cnVlLFxuICAgIC8vIG1hcmtlcnM6IHRydWUsXG4gICAgb25FbnRlcjogKCkgPT4geyBjb3VudC5zdGFydCgpIH1cbiAgfSlcblxufVxuXG5leHBvcnQgZGVmYXVsdCBzdGF0aXN0aWMiLCJpbXBvcnQgeyBnc2FwIH0gZnJvbSAnZ3NhcCdcbmltcG9ydCB7IFNjcm9sbFRyaWdnZXIgfSBmcm9tICdnc2FwL1Njcm9sbFRyaWdnZXInXG5cbmdzYXAucmVnaXN0ZXJQbHVnaW4oU2Nyb2xsVHJpZ2dlcilcblxuY29uc3Qgc3RpY2t5TG9nbyA9ICgpID0+IHtcbiAgXG4gIGNvbnN0IGxvZ28gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuZml4ZWRMb2dvJylcblxuICBpZiAoIWxvZ28pIHJldHVyblxuXG4gIFNjcm9sbFRyaWdnZXIuY3JlYXRlKHtcbiAgICB0cmlnZ2VyOiBsb2dvLFxuICAgIHBpbjogdHJ1ZSxcbiAgICBhbnRpY2lwYXRlUGluOiAxLFxuICAgIHBpblNwYWNpbmc6IGZhbHNlLFxuICAgIHN0YXJ0OiAndG9wIHRvcCcsXG4gICAgZW5kOiAnYm90dG9tIGJvdHRvbScsXG4gICAgZW5kVHJpZ2dlcjogJy5ib3R0b20nLFxuICAgIC8vIG1hcmtlcnM6IHRydWVcbiAgfSlcbiAgXG59XG5cbmV4cG9ydCBkZWZhdWx0IHN0aWNreUxvZ28gIiwiaW1wb3J0IEZsaWNraXR5IGZyb20gJ2ZsaWNraXR5J1xuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBpbml0VGVzdGltb25pYWxDYXJvdXNlbCgpIHtcblxuICBjb25zdCBjYXJvdXNlbHMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCdbZGF0YS10ZXN0aW1vbmlhbHNdJylcblxuICBpZiAoIWNhcm91c2VscykgcmV0dXJuXG5cbiAgY2Fyb3VzZWxzLmZvckVhY2goZWwgPT4ge1xuXG4gICAgY29uc3QgZmxpY2tpdHkgPSBuZXcgRmxpY2tpdHkoZWwsIHtcbiAgICAgIGNvbnRhaW46IHRydWUsXG4gICAgICBwYWdlRG90czogZmFsc2UsXG4gICAgICBwcmV2TmV4dEJ1dHRvbnM6IHRydWUsXG4gICAgICBpbWFnZXNMb2FkZWQ6IHRydWUsXG4gICAgICBkcmFnZ2FibGU6IHRydWUsXG4gICAgICBjZWxsQWxpZ246ICdsZWZ0J1xuICAgIH0pXG4gICAgXG4gIH0pXG5cbn1cbiIsIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiJdLCJzb3VyY2VSb290IjoiIn0=