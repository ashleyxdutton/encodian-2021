<?php 
  $heading = get_field('team_heading') ? get_field('team_heading') : get_sub_field('heading');
  $members = get_field('team_members') ? get_field('team_members') : get_sub_field('members');
?>

<section class="team">
  <div class="container container--l">
    <h2 class="team__heading fs--60" data-inview data-split-lines><?= $heading; ?></h2>
    <ul class="team__list flex">
      <?php foreach( $members as $member ): ?>
      <li class="team__member" data-inview>
        <div class="team__image" style="background-image:url('<?= $member['image']['url'] ?>')"></div>
        <p class="team__name fs--24 fw--700"><?= $member['name'] ?></p>
        <p class="team__title fs--24"><?= $member['job_title'] ?></p>
        <?php if ($member['linkedin_url']): ?>
          <a class="team__linkedin" href="<?= $member['linkedin_url'] ?>" target="_blank"></a>
        <?php endif; ?>
      </li>
      <?php endforeach; ?>
    </ul>
  </div>
</section>