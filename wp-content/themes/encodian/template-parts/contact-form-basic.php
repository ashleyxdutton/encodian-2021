<section class="contactForm" style="background-color: <?= the_field('colour'); ?>">
  <div class="container container--m">
    <div class="contactForm__header flex">
      <h2 class="contactForm__heading" data-inview><?= the_field('form_heading'); ?></h2>
      <p class="contactForm__text fs--24" data-inview><?= the_field('form_text'); ?></p>
    </div>

    <div class="contactForm__formHolder" data-inview>
    
      <form
        class="contactForm__form flex flex--x-between flex--y-start"
        data-process
        data-validate
        data-success="<h2>Thanks for contacting Encodian. We will be in touch shortly.</h2>">
        <fieldset>
          <input name="full_name" type="text" placeholder="* Name" autocomplete="off" required>
        </fieldset>
        <fieldset>
          <input name="company" type="text" placeholder="Company" autocomplete="off">
        </fieldset>
        <fieldset>
          <input
            id="email_address"
            name="email_address"
            type="email"
            placeholder="* Email address"
            pattern="^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*(\.\w{2,})+$"
            data-bouncer-message="The domain portion of the email address is invalid (the portion after the @)"
            required
            autocomplete="off">
        </fieldset>
        <fieldset>
          <input id="phone_number" name="phone_number" type="tel" placeholder="Contact number" autocomplete="off">
        </fieldset>

        <div class="contactForm__boxes flex">
          <fieldset class="checkbox">
            <input name="privacy_check" type="checkbox" id="privacy" required>
            <span class="checkbox__box"></span>
            <label for="privacy">* I can confirm I have read and accept the <a href="https://support.encodian.com/hc/en-gb/articles/360010642813-Terms-of-Service" target="_blank">Terms of Service</a> and <a href="https://support.encodian.com/hc/en-gb/articles/360010559134-Data-Processing-Agreement" target="_blank">Data Processing Agreement.</a></label>
          </fieldset>
          <fieldset class="checkbox">
            <input id="mail_check" name="mail_check" type="checkbox" id="marketing">
            <span class="checkbox__box"></span>
            <label for="marketing">Keep me up to date with news and product updates</label>
          </fieldset>
        </div>
        
        <input type="text" name="sfu" class="sfu">

        <button type="submit">Submit <div class="spinner"></div></button>

      </form>
    </div>
    
  </div>
</section>