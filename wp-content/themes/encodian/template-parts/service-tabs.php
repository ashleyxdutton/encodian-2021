<?php 
  $heading = get_sub_field('heading');
  $services = get_sub_field('slides');
?>

<section class="serviceTabs pos--rel" id="services" data-inview>

  <div class="serviceTabs__header flex">
    <h2 class="serviceTabs__heading ff--body fs--18 fw--300 ls--l40 tt--upper"><?= $heading; ?></h2>
    <nav class="serviceTabs__tabs flex flex--x-end flex--r-nowrap">

      <?php foreach( $services as $service): ?>

        <a class="serviceTabs__tab fs--20 fw--700 ls--t10 pos--rel" href="">
          <?= $service['heading'] ?>
          <div class="serviceTabs__arrow pos--abs"><?= load_svg('arrow'); ?></div>
        </a>

      <?php endforeach; ?>
      
    </nav>
  </div>

  <div class="serviceTabs__slides pos--rel z--2">

    <?php foreach( $services as $service): ?>

      <div class="serviceTabs__slide pos--abs flex">
        <h3 class="serviceTabs__title"><?= $service['heading'] ?></h3>
        <div class="serviceTabs__text">
          <?= $service['text'] ?>
          <?php if ( $service['button'] ): ?>
            <a class="serviceTabs__button button" href="<?= $service['button']['url']; ?>" target="<?= $service['button']['target']; ?>"><?= $service['button']['title']; ?></a>
          <?php endif; ?>
        </div>
      </div>

    <?php endforeach; ?>

  </div>

  <?= load_svg('services-clip'); ?>
  <img class="services-sketch" src="<?= '' . get_site_url() . '/wp-content/themes/encodian/src/images/services-sketch.png' ?>" alt="">

</section>