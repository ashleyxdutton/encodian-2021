<?php 
  $heading = get_field('features_heading') ? get_field('features_heading') : get_sub_field('heading');
  $text = get_field('features_text') ? get_field('features_text') : get_sub_field('text');
  $features = get_field('features_items') ? get_field('features_items') : get_sub_field('items');
?>

<section class="featuresAlt" data-features data-section="features">
  <div class="container container--l">
    <div class="featuresAlt__header">
      <h2 class="featuresAlt__heading" data-inview><?= $heading; ?></h2>
      <p class="featuresAlt__intro" data-inview><?= $text; ?></p>
    </div>

    <ul class="featuresAlt__list flex flex--x-between" data-accordian="mobile">
      <?php foreach( $features as $feature ): ?>
        <li class="featuresAlt__item pos--rel <?= ($feature['include_image_modal'] === true) ? "js-modal-item" : ""; ?>" data-inview="start" data-accordian-item>
          <h3 class="featuresAlt__title fs--30" data-accordian-title><?= $feature['title'] ?></h3>
          <div class="featuresAlt__text"><?= $feature['text'] ?></div>
          <div class="featuresAlt__icon pos--abs"></div>
          <div class="featuresAlt__hover pos--abs flex flex--x-center flex--y-center o--0"><p class="ff--head fs--30">View example</p></div>
        </li>
      <?php endforeach; ?>
    </ul>
    <div class="end-trigger"></div>
  </div>

</section>

<?php foreach( $features as $feature ): ?>
  <?php if($feature['include_image_modal'] === false): continue; endif; ?>
  <div class="featuresAlt__modal pos--fix z--max flex flex--x-center flex--y-center">
    <div class="featuresAlt__modalText pos--rel">
      <h3 class="featuresAlt__modalTitle fs--30 fc--white"><?= $feature['title'] ?></h3>
      <a class="featuresAlt__close pos--abs" href=""></a>
      <img class="featuresAlt__image" src="<?= $feature["example_image"]['url'] ?>" alt="">
    </div>
  </div>
<?php endforeach; ?>