<?php 
  $heading = get_field('products_heading') ? get_field('products_heading') : get_sub_field('heading');
  $products = get_field('select_products') ? get_field('select_products') : get_sub_field('products');
?>

<section class="products">
  <div class="container">
    <h2 class="products__heading fs--60"><?= $heading; ?></h2>
    <ul class="products__list products__list--<?= count($products); ?> flex pos--rel">
      <?php foreach( $products as $product): ?>
        <li class="products__item align--center flex">
          <img class="products__icon" src="<?= get_field('icon', $product['product']->ID); ?>" alt="">
          <h2 class="products__title fs--40"><?= get_field('heading', $product['product']->ID); ?></h2>
          <p class="products__text"><?= $product['product']->post_excerpt ?></p>
          <a class="products__button button" href="<?= get_the_permalink($product['product']->ID); ?>">Find out more</a>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
</section>
