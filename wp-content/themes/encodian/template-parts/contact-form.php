<section class="contactForm" style="background-color: <?= the_field('colour'); ?>" data-inview>
  <div class="container container--m">
    <div class="contactForm__header flex">
      <h2 class="contactForm__heading" data-inview><?= the_field('form_heading'); ?></h2>
      <p class="contactForm__text fs--24" data-inview><?= the_field('form_text'); ?></p>
    </div>

    <div class="contactForm__formHolder" data-inview>

      <?php gravity_form(get_field('form'), false, false, false, array(), true, 100, true); ?>
    
      <!-- <form
        class="contactForm__form flex flex--x-between flex--y-start"
        data-process
        data-validate
        data-success="<h2>Thanks for contacting Encodian. We will be in touch shortly.</h2>">
        <fieldset>
          <input name="full_name" type="text" placeholder="* Name" autocomplete="off" required>
        </fieldset>
        <fieldset>
          <input name="company" type="text" placeholder="Company" autocomplete="off">
        </fieldset>
        <fieldset>
          <input id="role" name="role" type="text" placeholder="Role" autocomplete="off">
        </fieldset>
        <fieldset>
          <input id="phone_number" name="phone_number" type="tel" placeholder="Contact number" autocomplete="off">
        </fieldset>
        <fieldset>
          <input
            id="email_address"
            name="email_address"
            type="email"
            placeholder="* Email address"
            pattern="^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*(\.\w{2,})+$"
            data-bouncer-message="The domain portion of the email address is invalid (the portion after the @)"
            required
            autocomplete="off">
        </fieldset>

        <div class="contactForm__boxes flex">
          <fieldset class="checkbox">
            <input name="privacy_check" type="checkbox" id="privacy" required>
            <span class="checkbox__box"></span>
            <label for="privacy">*I agree to the <a href="/privacy-policy">privacy policy</a></label>
          </fieldset>
          <fieldset class="checkbox">
            <input id="mail_check" name="mail_check" type="checkbox" id="marketing">
            <span class="checkbox__box"></span>
            <label for="marketing">Keep me up to date with news and product updates</label>
          </fieldset>
        </div>

        <?php $options = get_field('im_interested_in'); ?>

        <fieldset>
          <select id="interested" name="interested" name="products" id="">
            <option value="">I’m interested in…</option>
            <?php foreach( $options as $option ): ?>
              <option value="<?= $option['option']; ?>"><?= $option['option']; ?></option>
            <?php endforeach; ?>
          </select>
        </fieldset>

        <input type="text" name="sfu" class="sfu">

        <button type="submit">Submit <div class="spinner"></div></button>

      </form> -->

    </div>
    
  </div>
</section>