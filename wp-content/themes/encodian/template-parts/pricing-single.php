<section id="pricing" class="pricing pricing--single pos--rel" data-section="pricing">
  <div class="container container--l pos--rel z--2">
    
    <?php $packages = get_field('pricing_items'); ?>

    <div class="pricing__header pos--rel z--2 flex">
      <div class="pricing__column">
        <h2 class="pricing__heading"><?= the_field('pricing_heading'); ?></h2>
        <select name="currency" id="">
          <option value="gbp">£ GBP</option>
          <option value="usd">$ USD</option>
          <option value="eur">€ EUR</option>
        </select>
      </div>
      <div class="pricing__column fs--24">
        <p><strong><?= the_field('bold_text_column_1'); ?></strong></p>
      </div>
      <div class="pricing__column">
      <p><?= the_field('pricing_text_one'); ?></p>
      </div>
    </div>

    <?php $package = get_field('pricing_item'); ?>

    <div class="pricing__panel <?php if( '' != $package["prices"]["monthly_gbp"] ): ?>update <?php endif; ?>align--center">
      <h3 class="pricing__title fs--30 fc--white pos--rel" style="background-color:#331F45;"><?= $package['title'] ?></h3>

      <div class="pricing__features">
        <?= $package['features'] ?>
      </div>

      <?php if( '' != $package["prices"]["monthly_gbp"] ): ?>

        <div class="pricing__prices pricing__options" data-currency="gbp">
          <div>
            <div class="pricing__price pricing__price--yearly">
              <span data-currency="usd"><?php echo is_numeric($package["prices"]["monthly_usd"]) ? sprintf('$%s', $package["prices"]["monthly_usd"]) : $package["prices"]["monthly_usd"]; ?></span>
              <span data-currency="gbp"><?php echo is_numeric($package["prices"]["monthly_gbp"]) ? sprintf('£%s', $package["prices"]["monthly_gbp"]) : $package["prices"]["monthly_gbp"]; ?></span>
              <span data-currency="eur"><?php echo is_numeric($package["prices"]["monthly_eur"]) ? sprintf('€%s', $package["prices"]["monthly_eur"]) : $package["prices"]["monthly_eur"]; ?></span>
              <p class="fs--18">/month</p>
            </div>
            <a class="pricing__button button-outline button-outline--black" href="#" data-to="form">Activate Free Trial</a>
          </div>
          <div>
            <div class="pricing__price pricing__price--monthly">
              <span data-currency="usd"><?php echo is_numeric($package["prices"]["year_usd"]) ? sprintf('$%s', $package["prices"]["year_usd"]) : $package["prices"]["year_usd"]; ?></span>
              <span data-currency="gbp"><?php echo is_numeric($package["prices"]["year_gbp"]) ? sprintf('£%s', $package["prices"]["year_gbp"]) : $package["prices"]["year_gbp"]; ?></span>
              <span data-currency="eur"><?php echo is_numeric($package["prices"]["year_eur"]) ? sprintf('€%s', $package["prices"]["year_eur"]) : $package["prices"]["year_eur"]; ?></span>
              <p class="fs--18">/year</p>
            </div>
            <a class="pricing__button button-outline button-outline--black" href="#" data-to="form">Activate Free Trial</a>
          </div>
        </div>

      <?php else: ?>

        <div class="pricing__prices flex" data-currency="gbp">
          <div class="pricing__price pricing__price--yearly">
            <span data-currency="usd"><?php echo is_numeric($package["prices"]["year_usd"]) ? sprintf('$%s', $package["prices"]["year_usd"]) : $package["prices"]["year_usd"]; ?></span>
            <span data-currency="gbp"><?php echo is_numeric($package["prices"]["year_gbp"]) ? sprintf('£%s', $package["prices"]["year_gbp"]) : $package["prices"]["year_gbp"]; ?></span>
            <span data-currency="eur"><?php echo is_numeric($package["prices"]["year_eur"]) ? sprintf('€%s', $package["prices"]["year_eur"]) : $package["prices"]["year_eur"]; ?></span>
            <p class="fs--18">/year</p>
          </div>
          <a class="pricing__button button-outline button-outline--black" href="#" data-to="form">Activate Free Trial</a>
        </div>

      <?php endif; ?>

    </div>

  </div>

  <div class="pricing__bg pos--abs z--1" style="background-color: <?= the_field('colour'); ?>"></div>

</section>