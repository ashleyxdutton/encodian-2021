<section class="benefits">
  <div class="container container--l pos--rel">
    <h2 class="benefits__heading" data-inview><?= the_field('benefits_heading'); ?></h2>
    <?php $benefits = get_field('benefits_items'); ?>
    <ul class="benefits__list" data-accordian="mobile">
      <?php foreach( $benefits as $benefit ): ?>
        <li class="benefits__item flex pos--rel" data-inview="start" data-accordian-item>
          <h3 class="benefits__title fs--30" data-accordian-title><?= $benefit['title'] ?></h3>
          <div class="benefits__text"><?= $benefit['text'] ?></div>
          <div class="benefits__icon pos--abs"></div>
        </li>
      <?php endforeach; ?>
    </ul>
    <img class="benefits__sketch pos--abs" src="<?= get_field('benefits_image')['url']; ?>" alt="">
  </div>
</section>