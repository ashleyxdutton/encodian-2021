<section id="form" class="contactForm" style="background-color: <?= the_field('colour'); ?>">
  <div class="container container--m">
    <div class="contactForm__header flex">
      <h2 class="contactForm__heading" data-inview><?= the_field('form_heading'); ?></h2>
      <p class="contactForm__text fs--24" data-inview><?= the_field('form_text'); ?></p>
    </div>

    <div class="contactForm__formHolder" data-inview>

      <div class="contactForm__success align--center">
        <h3>Thank you for requesting a product demonstration with Encodian.</h3>
        <p>A member of our team will contact you shortly, in the meantime if you have any further questions please email <a href="mailto:support@encodian.com?subject=Encodian%20Trial%20Enquiry">support@encodian.com</a></p>
      </div>

      <div class="contactForm__error align--center">
        <h3>We're very sorry, something has gone wrong!</h3>
        <p>Our engineers have been notified, in the meantime please try to contact our team via: <a href="mailto:support@encodian.com?subject=Encodian%20Trial%20Error">hello@encodian.com</a> or <a href="tel:+44808109200">+44 808 109 200</a></p>        
      </div>

      <form
        class="contactForm__form flex flex--x-between flex--y-start"
        data-demo
        data-validatedemo
        data-success="<h2>Thank you for your enquiry. We will be in touch soon.</h2>"
        data-form-id="2"
        data-type="<?= get_the_title(); ?>">
        <fieldset>
          <input id="full_name" name="full_name" type="text" placeholder="* Name" autocomplete="off" required>
        </fieldset>
        <fieldset>
          <input id="company" name="company" id="company" type="text" placeholder="Company" autocomplete="off">
        </fieldset>
        <fieldset>
          <input id="role" name="role" type="text" placeholder="Role" autocomplete="off">
        </fieldset>
        <fieldset>
          <input id="phone_number" name="phone_number" type="tel" placeholder="Contact number" autocomplete="off">
        </fieldset>
        <fieldset>
          <input
            id="email_address"
            name="email_address"
            type="email"
            placeholder="* Email address"
            pattern="^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*(\.\w{2,})+$"
            data-bouncer-message="The domain portion of the email address is invalid (the portion after the @)"
            required
            autocomplete="off">
        </fieldset>

        <div class="contactForm__boxes flex">
          <fieldset class="checkbox">
            <input name="privacy_check" type="checkbox" id="privacy" required>
            <span class="checkbox__box"></span>
            <label for="privacy">* I can confirm I have read and accept the <a href="https://support.encodian.com/hc/en-gb/articles/360010642813-Terms-of-Service" target="_blank">Terms of Service</a> and <a href="https://support.encodian.com/hc/en-gb/articles/360010559134-Data-Processing-Agreement" target="_blank">Data Processing Agreement.</a></label>
          </fieldset>
          <fieldset class="checkbox">
            <input id="mail_check" name="mail_check" type="checkbox" id="marketing">
            <span class="checkbox__box"></span>
            <label for="marketing">Keep me up to date with news and product updates</label>
          </fieldset>
        </div>

        <!-- <?php $options = get_field('im_interested_in'); ?>

        <fieldset>
          <select id="interested" name="interested" name="products" id="">
            <option value="">I’m interested in…</option>
            <?php foreach( $options as $option ): ?>
              <option value="<?= $option['option']; ?>" <?= ($option['default']) ? 'selected' : ''; ?>><?= $option['option']; ?></option>
            <?php endforeach; ?>
          </select>
        </fieldset> -->

        <input type="text" name="sfu" class="sfu">

        <input type="hidden" id="gclid_field" name="gclid_field" value="">
        <input type="hidden" id="utm_source" name="utm_source" value="">
        <input type="hidden" id="utm_medium" name="utm_medium" value="">
        <input type="hidden" id="utm_campaign" name="utm_campaign" value="">
        <input type="hidden" id="utm_term" name="utm_term" value="">

        <button type="submit">Submit <div class="spinner"></div></button>

      </form>
    </div>
    
  </div>
</section>