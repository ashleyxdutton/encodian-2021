<?php 
  $image = get_field('intro_image') ? get_field('intro_image') : get_sub_field('image');
  $heading = get_field('intro_heading') ? get_field('intro_heading') : get_sub_field('heading');
  $text = get_field('intro_text') ? get_field('intro_text') : get_sub_field('text');
?>

<section class="imageIntro <?= $image ? '' : 'imageIntro--noImage'; ?>" data-inview>
  <div class="container container--l flex">

    <?php if ( $image ): ?>
      
      <div class="imageIntro__imageHolder pos--rel">
        <img class="imageIntro__image pos--abs" src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>">
        <?= load_svg('services-mask'); ?>
      </div>

    <?php endif; ?>

    <div class="imageIntro__content">
      <h1 class="imageIntro__heading" data-split-lines><?= $heading; ?></h1>
      <p class="imageIntro__text fs--24" data-inview><?= $text; ?></p>
    </div>

  </div>
</section>