<?php
if ( get_sub_field('video_type') ):
  $video_type = get_sub_field('video_type');
  $video_embed = get_sub_field('video_embed');
  $video_file = get_sub_field('video_file');
  $video_text = get_sub_field('video_header');
  ?>
  <section class="productVideo <?php echo $video_embed ? 'productVideo--module' : '' ?>" data-section="video" data-inview data-inview="client">
    <div class="container container--l">
      <div class="productVideo__content">
        <?php if ( $video_text ): ?>
          <h2 class="productVideo__heading fs--30" data-split-lines>
            <?php echo $video_text; ?>
          </h2>
        <?php endif; ?>
        <div class="productVideo__embed">
          <?php if ( $video_type == 'file' ): ?>
            <video controls>
              <source src="<?php echo $video_file['url']; ?>" type="<?php echo $video_file['mime_type']; ?>">
              Sorry, your browser doesn't support embedded videos.
            </video>
          <?php else: ?>
            <?php echo $video_embed; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>

<?php
if ( get_field('video_type') ):
  $video_type = get_field('video_type');
  $video_embed = get_field('video_embed');
  $video_file = get_field('video_file');
  $video_text = get_field('video_header');
  ?>
  <section class="productVideo <?php echo $video_embed ? 'productVideo--module' : '' ?>" data-section="video" data-inview data-inview="client">
    <div class="container container--l">
      <div class="productVideo__content">
        <?php if ( $video_text ): ?>
          <h2 class="productVideo__heading fs--30" data-split-lines>
            <?php echo $video_text; ?>
          </h2>
        <?php endif; ?>
        <div class="productVideo__embed">
          <?php if ( $video_type == 'file' ): ?>
            <video controls>
              <source src="<?php echo $video_file['url']; ?>" type="<?php echo $video_file['mime_type']; ?>">
              Sorry, your browser doesn't support embedded videos.
            </video>
          <?php else: ?>
            <?php echo $video_embed; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>