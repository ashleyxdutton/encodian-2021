<article <?php post_class() ?>>
  <a href="<?php the_permalink(); ?>">
    <h2><?php the_title(); ?></h2>
  </a>
  <?php if (has_post_thumbnail()): the_post_thumbnail(); endif; ?>
  <?php the_content(); ?>
</article>
