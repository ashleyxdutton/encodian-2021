<?php
  $heading = get_field('text_heading') ? get_field('text_heading') : get_sub_field('heading');
  $blocks = get_field('text_blocks') ? get_field('text_blocks') : get_sub_field('items');
?>

<section class="textBlocks" data-inview>
  <div class="container container--l flex">
    <h2 class="textBlocks__heading" data-split-lines><?= $heading; ?></h2>
    <ul class="textBlocks__list flex flex--x-between">
      <?php foreach( $blocks as $block ): ?>
        <li class="textBlocks__item" data-inview>
          <h3 class="textBlocks__title fs--30"><?= $block['heading']; ?></h3>
          <p class="textBlocks__text fs--24"><?= $block['text']; ?></p>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
</section>