<?php
$heading = get_sub_field('heading');
$items = get_sub_field('logos');
?>
<section class="partners">
  <div class="container container--m">
    <?php if ( $heading ): ?>
      <h2 class="partners__heading fs--60 align--center">
        <?php echo $heading; ?>
      </h2>
    <?php endif; ?>
    <ul class="partners__list flex">
      <?php foreach( $items as $item ): ?>
        <li class="partners__item flex flex--y-center align--center">
          <?php if ( $item['logo'] ): ?>
            <div class="partners__logo flex flex--y-center flex--x-center">
              <img src="<?php echo $item['logo']['url']; ?>" alt="<?php echo $item['logo']['alt']; ?>">
            </div>
          <?php endif; ?>
          <?php if ( $item['text'] ): ?>
            <p class="partners__text"><?php echo $item['text']; ?></p>
          <?php endif; ?>
          <?php if ( $item['button'] ): ?>
            <a class="partners__button button" href="<?php echo $item['button']['url']; ?>" target="<?php echo $item['button']['target']; ?>">
              <?php echo $item['button']['title']; ?>
            </a>
          <?php endif; ?>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
</section>
