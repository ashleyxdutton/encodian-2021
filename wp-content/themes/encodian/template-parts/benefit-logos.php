<section class="benefitLogos" data-section="benefits" data-inview data-inview="client">
  <?php $logos = get_field('client_logos'); ?>
  <div class="container container--l">
    <div class="benefitLogos__content">
      <h2 class="benefitLogos__heading fs--30" data-split-lines><?= the_field('client_heading'); ?></h2>
      <ul class="benefitLogos__list flex">
        <?php foreach( $logos as $logo): ?>
          <li class="benefitLogos__item flex flex--y-center">
            <img src="<?= $logo["add_logo"]["url"]; ?>" alt="<?= $logo["add_logo"]["alt"]; ?>">
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</section>