<?php 
  $heading = get_sub_field('heading');
  $testimonials = get_sub_field('testimonials');
?>

<section id="testimonials" class="testimonials">
  <div class="container container--l">
    <div class="testimonials__header" data-inview>
      <h2 class="testimonials__heading" data-split-lines><?= $heading; ?></h2>
    </div>

    <div class="testimonials__holder pos--rel" data-inview="testimonials">

      <ul class="testimonials__slider pos--rel z--2" data-testimonials>

        <?php foreach( $testimonials as $testimonial): ?>

          <li class="testimonials__item">
            <div class="testimonials__author pos--rel flex flex--x-center">
              <img class="testimonials__image" src="<?= $testimonial['image']['url'] ?>" alt="">
              <p class="testimonials__name fs--16 fw--700"><?= $testimonial['author']['name'] ?></p>
              <p class="testimonials__company fs--16"><?= $testimonial['author']['title'] ?></p>
            </div>
            <p class="testimonials__quote fs--24"><?= $testimonial['quote'] ?></p>
          </li>

        <?php endforeach; ?>

      </ul>

      <div class="testimonials__bg pos--abs">
        <?= load_svg('testimonials-bg'); ?>
        <?= load_svg('testimonials-clip'); ?>
      </div>

    </div>

  </div>
</section>