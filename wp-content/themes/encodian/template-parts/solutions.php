<?php 
  $image = get_field('solutions_image') ? get_field('solutions_image') : get_sub_field('image');
  $heading = get_field('solutions_heading') ? get_field('solutions_heading') : get_sub_field('content')['heading'];
  $subheading = get_field('solutions_subheading') ? get_field('solutions_subheading') : get_sub_field('content')['subheading'];
  $text = get_field('solutions_text') ? get_field('solutions_text') : get_sub_field('content')['text'];
  $solutions = get_field('solutions') ? get_field('solutions') : get_sub_field('solutions');
?>

<section class="solutions">
  <div class="container container--l pos--rel">
    <div class="solutions__imageHolder pos--abs">
      <img class="solutions__image pos--abs" src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>">
      <?= load_svg('solutions-mask'); ?>
    </div>
    <div class="solutions__main">
      <div class="solutions__header">
        <h2 class="solutions__heading" data-inview><?= $heading; ?></h2>
        <h3 class="solutions__subheading fs--30" data-inview><?= $subheading; ?></h3>
        <p class="solutions__intro fs--24" data-inview><?= $text; ?></p>
      </div>

      <ul class="solutions__list">
        <?php foreach( $solutions as $solution ): ?>
          <li class="solutions__item flex" data-inview>

            <div class="solutions__cta align--center pos--rel">
              <img class="solutions__ctaIcon" src="<?= get_field('icon', $solution["related_product"]); ?>" alt="">
              <p class="solutions__ctaTitle fs--18 fw--700"><?= the_field('sidebar_title', $solution["related_product"]); ?></p>
              <p class="solutions__ctaText fs--18"><?= the_field('sidebar_text', $solution["related_product"]); ?></p>
              <a class="solutions__ctaButton button-outline button-outline--black" href="<?= get_the_permalink($solution["related_product"]); ?>?form"><?= get_field('sidebar_button', $solution["related_product"])['title']; ?></a>
            </div>

            <div class="solutions__content">
              <img class="solutions__icon" src="<?= get_field('icon', $solution["related_product"]); ?>" alt="">
              <h4 class="solutions__title fs--30"><?= $solution['heading'] ?></h4>
              <p class="solutions__text fs--24"><?= $solution['text'] ?></p>
              <a class="solutions__button button-outline button-outline--black" href="<?= get_the_permalink($solution["related_product"]); ?>?form"><?= get_field('sidebar_button', $solution["related_product"])['title']; ?></a>
            </div>
            
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
  <div class="end-trigger"></div>
</section>