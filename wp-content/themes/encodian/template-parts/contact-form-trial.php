<section id="form" class="contactForm" style="background-color: <?= the_field('colour'); ?>">
  <div class="container container--m">
    <div class="contactForm__header flex">
      <h2 class="contactForm__heading" data-inview><?= the_field('form_heading'); ?></h2>
      <p class="contactForm__text fs--24" data-inview><?= the_field('form_text'); ?></p>
    </div>

    <div class="contactForm__formHolder" data-inview>

      <div class="contactForm__success align--center">
        <h3>Thank you for setting up a trial subscription with Encodian.</h3>
        <p>Please copy your API Key below, your new trial details have also been emailed to you</p>
        <p><span id="TrialApiKey">7499336e-9a6a-4669-8c3d-32e35ceae1f2</span><span class="tooltip">Click to Copy</span></p>
      </div>

      <div class="contactForm__error align--center">
        <h3>We're very sorry, something has gone wrong!</h3>
        <p>Our engineers have been notified, in the meantime please try to contact our team via: <a href="mailto:support@encodian.com?subject=Encodian%20Trial%20Error">hello@encodian.com</a> or <a href="tel:+44808109200">+44 808 109 200</a></p>        
      </div>

      <div class="contactForm__exists align--center">
        <h3>An existing Encodian subscription has been found.</h3>
        <p>An email containing the details of your existing Encodian subscription has been sent to your email address.</p>
      </div>

      <?php $formId = get_field('form'); ?>
      <?php $form = get_field('form', $formId); ?>

      <form
        class="contactForm__form flex flex--x-between flex--y-start"
        data-processtrial
        data-validatetrial
        data-type="<?= $form['type'] ?>"
        data-api="<?= $form['api_endpoint'] ?>"
        data-restricted="<?= $form['restricted_domains'] ?>"
        data-success="<h2>Thank you for your enquiry. We will be in touch soon.</h2>">

        <?php foreach( $form['fields'] as $field ): ?>

          <?php if ( $field['type'] == 'select'): ?>

            <select id="<?= $field['key']; ?>" name="<?= $field['key']; ?>" <?= ($field['required']) ? "required" : ""; ?>>
              <option value="" selected disabled><?= $field['placeholder']; ?></option>
              <?php foreach( $field['options'] as $option ): ?>
                <option value="<?= strtolower($option['value']) ?>"><?= $option['value'] ?></option>
              <?php endforeach; ?>
            </select>

          <?php else: ?>

            <fieldset>
              <input
                id="<?= $field['key']; ?>"
                name="<?= $field['key']; ?>"
                type="<?= $field['type']; ?>" 
                placeholder="<?= $field['placeholder']; ?>"
                autocomplete="off"
                <?= ($field['required']) ? "required" : ""; ?>
                <?= ($field['type'] == 'email') ? 'pattern="^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*(\.\w{2,})+$"' : ""; ?>
              >
            </fieldset>

          <?php endif; ?>
          
        <?php endforeach; ?>

        <div class="contactForm__boxes flex">
          <fieldset class="checkbox">
            <input name="privacy_check" type="checkbox" id="privacy" autocomplete="off" required>
            <span class="checkbox__box"></span>
            <label for="privacy">* I can confirm I have read and accept the <a href="https://support.encodian.com/hc/en-gb/articles/360010642813-Terms-of-Service" target="_blank">Terms of Service</a> and <a href="https://support.encodian.com/hc/en-gb/articles/360010559134-Data-Processing-Agreement" target="_blank">Data Processing Agreement.</a></label>
          </fieldset>
          <fieldset class="checkbox">
            <input id="mail_check" name="mail_check" type="checkbox" autocomplete="off" id="marketing">
            <span class="checkbox__box"></span>
            <label for="marketing">Keep me up to date with news and product updates</label>
          </fieldset>
        </div>

        <input type="hidden" id="gclid_field" name="gclid_field" value="">
        <input type="hidden" id="utm_source" name="utm_source" value="">
        <input type="hidden" id="utm_medium" name="utm_medium" value="">
        <input type="hidden" id="utm_campaign" name="utm_campaign" value="">
        <input type="hidden" id="utm_term" name="utm_term" value="">
        <input type="hidden" id="PageName" name="PageName" value="<?= get_the_title(); ?>">

        <button type="submit">Submit <div class="spinner"></div></button>

      </form>
      
      <?php if ($form['booking_link']): ?>

        <a class="contactForm__booking button-outline" target="_blank" href="<?= $form['booking_link']['url']; ?>"><?= $form['booking_link']['title']; ?></a>

      <?php endif; ?>

    </div>
    
  </div>
</section>