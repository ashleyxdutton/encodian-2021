<section id="pricing" class="pricing pricing--slider pos--rel" data-section="pricing" data-pricing-peroid="yearly">
  <div class="container container--l pos--rel">
    
    <?php $packages = get_field('pricing_items'); ?>

    <div class="pricing__header pos--rel z--2 flex">
      <div class="pricing__column">
        <h2 class="pricing__heading"><?= the_field('pricing_heading'); ?></h2>
        <select name="currency" id="">
          <option value="usd">$ USD</option>
          <option value="gbp">£ GBP</option>
          <option value="eur">€ EUR</option>
        </select>
      </div>
      <div class="pricing__column fs--24">
        <div class="pricing-toggle">
          <div class="item">
            <input type="radio" id="yearly" name="period" value="Yearly" checked="checked" />
            <label for="yearly" data-pricing-toggle="yearly">Yearly</label>
          </div>
          <div class="item">
            <input type="radio" id="monthly" name="period" value="Monthly">
            <label for="monthly" data-pricing-toggle="monthly">Monthly</label>
          </div>
        </div>
      </div>
      <div class="pricing__column fs--24">
        <p class="flex-end"><strong><?= the_field('bold_text_column_1'); ?></strong></p>
      </div>
    </div>

    <div class="pricing__main">

      <?php $rows = get_field('pricing_rows'); ?>

      <div class="pricing__products">
        <?php foreach ( $rows as $row ): ?>
          <div class="pricing__product">
            <div class="product-price-inner">
              <?php if ( $row['features'] ): ?>
                <h2 class="pricing__productTitle"><?= $row['title'] ?></h2>
                <p class="pricing__productText open-tooltip"><?= $row['subtitle'] ?><span class="icon"></span></p>
                <div class="pricing__tooltip">
                  <?= $row['features']; ?>
                  <?php if ( $row['link'] ): ?>
                    <a href="<?php echo $row['link']['url']; ?>" class="button"><?php echo $row['link']['title']; ?></a>
                  <?php endif; ?>
                </div>
              <?php else: ?>
                <h2 class="pricing__productTitle"><?= $row['title'] ?></h2>
                <p class="pricing__productText"><?= $row['subtitle'] ?></p>
              <?php endif; ?>
            </div>
          </div>
        <?php endforeach; ?>
      </div>

      <div class="pricing__noslide pos--rel z--2">
        <?php foreach( $packages as $package ): ?>
          <div class="pricing__itemWrap <?= (isset($package['most_popular'][0])) ? 'most-popular' : ''; ?>">
            <div class="pricing__item align--center <?= (isset($package['most_popular'][0])) ? 'most-popular' : ''; ?>" style="background-color: <?= the_field('colour'); ?>">
              
              <?php if ($package['most_popular']): ?>
                <p class="pricing__popular ff--head fs--26">Most Popular</p>
                <?php endif; ?>

              <h3 class="pricing__title fs--30 fc--white pos--rel" ><?= $package['title'] ?></h3>
        
              <?php if ($package["prices"]['tbc']): ?>
                
                <div class="pricing__prices" data-currency="usd">
                  <div class="pricing__price pricing__price--yearly">
                    <span data-currency="usd">TBC</span>
                    <span data-currency="gbp">TBC</span>
                    <span data-currency="eur">TBC</span>
                    <p class="fs--18">/year</p>
                  </div>
    
                  <div class="pricing__price pricing__price--monthly">
                    <span data-currency="usd">TBC</span>
                    <span data-currency="gbp">TBC</span>
                    <span data-currency="eur">TBC</span>
                    <p class="fs--18">/month</p>
                  </div>
                </div>
    
              <?php else: ?>

                <?php if ( $package['prices']['year_gbp'] ): ?>
                
                  <div class="pricing__prices" data-currency="usd">
                    <div class="pricing__price pricing__price--yearly">
                      <span data-currency="usd"><?php echo is_numeric($package["prices"]["year_usd"]) ? sprintf('$%s', number_format($package["prices"]["year_usd"], 2)) : $package["prices"]["year_usd"]; ?></span>
                      <span data-currency="gbp"><?php echo is_numeric($package["prices"]["year_gbp"]) ? sprintf('£%s', number_format($package["prices"]["year_gbp"], 2)) : $package["prices"]["year_gbp"]; ?></span>
                      <span data-currency="eur"><?php echo is_numeric($package["prices"]["year_eur"]) ? sprintf('€%s', number_format($package["prices"]["year_eur"], 2)) : $package["prices"]["year_eur"]; ?></span>
                      <p class="fs--18">/year</p>
                    </div>

                    <?php if ($package["prices"]["monthly_usd"] && $package["prices"]["monthly_gbp"] && $package["prices"]["monthly_eur"]): ?>
                      <div class="pricing__price pricing__price--monthly">
                        <span data-currency="usd"><?php echo is_numeric($package["prices"]["monthly_usd"]) ? sprintf('$%s', number_format($package["prices"]["monthly_usd"], 2)) : $package["prices"]["monthly_usd"]; ?></span>
                        <span data-currency="gbp"><?php echo is_numeric($package["prices"]["monthly_gbp"]) ? sprintf('£%s', number_format($package["prices"]["monthly_gbp"], 2)) : $package["prices"]["monthly_gbp"]; ?></span>
                        <span data-currency="eur"><?php echo is_numeric($package["prices"]["monthly_eur"]) ? sprintf('€%s', number_format($package["prices"]["monthly_eur"], 2)) : $package["prices"]["monthly_eur"]; ?></span>
                        <p class="fs--18">/month</p>
                      </div>
                    <?php endif; ?>
                  </div>

                <?php endif; ?>
    
              <?php endif; ?>

            </div>
            
            <div class="pricing__rows">
              <?php foreach ( $package['rows'] as $row ): ?>

                <div class="pricing__rowsItem" style="color: <?= the_field('colour'); ?>">
                  <?php if ( $row['type'] == 'text' ): ?>
                    <p><?= $row['text']; ?></p>
                  <?php else: ?>
                    <?php if ( $row['icon'] == 'true' ): ?>
                      <svg height="20.92" viewBox="0 0 27.407 20.92" width="27.407" xmlns="http://www.w3.org/2000/svg"><path d="m2671.087 4764.063-18.176 17.735-7.12-7.161" fill="none" stroke-width="3" transform="translate(-2644.728 -4762.989)"/></svg>
                    <?php else: ?>
                      <svg xmlns="http://www.w3.org/2000/svg" width="20.299" height="20.297" viewBox="0 0 20.299 20.297"><g transform="translate(1.075 1.074)"><path d="M2671.087,4764.063l-7.952,7.759-10.224,9.976" transform="translate(-2652.91 -4764.063)" fill="none" stroke-width="3"/><path d="M18.176,0,10.224,7.759,0,17.735" transform="translate(17.734) rotate(90)" fill="none" stroke-width="3"/></g></svg>
                    <?php endif; ?>
                  <?php endif; ?>
                  <p class="mobile-text"><?= $row['mobile_text']; ?></p>
                </div>

                
              <?php endforeach; ?>

              <a class="pricing__button button-outline button-outline--black" href="#" data-to="form">Activate Free Trial</a>

            </div>

          </div>
        <?php endforeach; ?>
      </div>
    </div>

    <nav class="pricing__controls flex flex--y-center pos--rel z--2">
      <button class="flickity-button flickity-prev-next-button previous pos--rel" type="button" disabled="" aria-label="Previous"></button>
      <ul class="pricing__dots flex">
        <?php foreach( $packages as $package ): ?>
          <li class="pricing__dot"></li>
        <?php endforeach; ?>
      </ul>
      <button class="flickity-button flickity-prev-next-button next pos--rel" type="button" aria-label="Next"></button>
    </nav>

    <div class="pricing-lower-info" style="color: <?= the_field('colour'); ?>">
      <?php if ( get_field('pricing_text_one') ): ?>
        <p><?= the_field('pricing_text_one'); ?></p>
      <?php endif; ?>
      <?php if ( get_field('pricing_text_two') ): ?>
        <p><?= the_field('pricing_text_two'); ?></p>
      <?php endif; ?>
      <a class="button" href="#" data-to="form"><span>Activate Free Trial</span></a>
    </div>

  </div>

  <div class="pricing__bg pos--abs z--1" style="background-color: <?= the_field('colour'); ?>"></div>

</section>