<div class="container error-msg">
  <p class="h-2">Nothing Found</p>
  <p>Sorry, but you are looking for something that isn't here.</p>
  <p><a href="<?php echo get_option('home'); ?>">Return to the homepage</a></p>
</div>