<section class="features" data-section="benefits">
  <div class="container container--l">
    <p class="features__intro fs--24" data-inview><?= the_field('features_text'); ?></p>
    <?php $features = get_field('features_items'); ?>
    <ul class="features__list" data-accordian>
      <?php foreach( $features as $feature ): ?>
      <li class="features__item flex flex--y-start pos--rel" data-accordian-item data-inview="start">
        <h3 class="features__title fs--30" data-accordian-title><?= $feature['title'] ?></h3>
        <div class="features__text fs--16"><?= $feature['text'] ?></div>
        <div class="features__icon pos--abs"></div>
      </li>
      <?php endforeach; ?>
    </ul>
    <div class="end-trigger"></div>
  </div>
</section>