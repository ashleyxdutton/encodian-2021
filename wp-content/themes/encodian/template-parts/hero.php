<?php 
  $heading = get_sub_field('heading');
  $text = get_sub_field('text');
?>

<section class="hero pos--rel" data-inview="hero">
  <div class="container container--l flex pos--rel">
    <div class="hero__content z--3 o--0">
      <h1 class="hero__heading" data-split-lines><?= $heading; ?></h1>
      <p class="hero__text fs--24"><?= $text; ?></p>
    </div>
    <div class="hero__imageHolder align--center pos--rel z--2">
      <?= load_svg('logo-icon'); ?>
      <img class="hero-drawing" src="<?= '' . get_site_url() . '/wp-content/themes/encodian/src/images/hero-drawing.png' ?>" alt="">
    </div>
  </div>
  <div class="hero__bg pos--abs"></div>
</section>