<section class="introSlider" data-inview>
  <div class="container container--l flex">

    <div class="introSlider__content">
      <h1 class="introSlider__heading" data-split-lines><?= the_field('intro_heading'); ?></h1>
      <p class="introSlider__text fs--24"><?= the_field('intro_text'); ?></p>
    </div>

    <div class="introSlider__imageHolder pos--rel">
      <img class="introSlider__image pos--abs" src="<?= get_field('intro_image')['url']; ?>" alt="<?= get_field('intro_image')['alt']; ?>">
      <?= file_get_contents('' . get_site_url() . '/wp-content/themes/encodian/src/svgs/mask-industries.svg'); ?>

      <?php $testimonials = get_field('intro_testimonials'); ?>

      <div class="introSlider__sliderHolder pos--abs">
        <ul class="introSlider__slider">
          <?php foreach( $testimonials as $testimonial ): ?>
            <li class="introSlider__item">
              <div class="introSlider__author pos--rel flex flex--x-center">
                <img class="introSlider__logo" src="<?= $testimonial['image']['url'] ?>" alt="">
                <p class="introSlider__name fs--16 fw--700"><?= $testimonial['author'] ?></p>
                <p class="introSlider__company fs--16"><?= $testimonial['company_title'] ?></p>
              </div>
              <p class="introSlider__quote fs--24"><?= $testimonial['quote'] ?></p>
            </li>
          <?php endforeach; ?>
        </ul>

        <nav class="introSlider__controls flex flex--y-center flex--x-between pos--abs z--2">
          <button class="flickity-button flickity-prev-next-button previous pos--rel" type="button" disabled="" aria-label="Previous"></button>
          <ul class="introSlider__dots flex">
            <?php foreach( $testimonials as $testimonial ): ?>
              <li class="introSlider__dot"></li>
            <?php endforeach; ?>
          </ul>
          <button class="flickity-button flickity-prev-next-button next pos--rel" type="button" aria-label="Next"></button>
        </nav>

      </div>
    </div>

  </div>
</section>