<?php 
  $heading = get_field('form_heading') ? get_field('form_heading') : get_sub_field('heading');
  $text = get_field('form_text') ? get_field('form_text') : get_sub_field('text');
  $formId = get_field('form') ? get_field('form') : get_sub_field('form');
  $formType = get_field('form_type');
  $gravityFormID = get_field('gravity_form');
  $form = get_field('form', $formId);
?>
    
<section id="form" class="contactForm" style="background-color: <?= the_field('colour'); ?>" data-inview>
  <div class="container container--m">
    <div class="contactForm__header flex">
      <h2 class="contactForm__heading" data-inview><?= $heading; ?></h2>
      <p class="contactForm__text fs--24" data-inview>
        <?= $text; ?>
        <?php if ($form['booking_link']): ?>
          <a class="contactForm__booking button-outline" target="_blank" href="<?= $form['booking_link']['url']; ?>"><?= $form['booking_link']['title']; ?></a>
        <?php endif; ?>
      </p>
    </div>

    <div class="contactForm__formHolder" data-inview>

      <?php if ( $formType == 'gravity' ):

        gravity_form($gravityFormID, false, false, false, array(), true, 100, true);

      else: ?>

        <?php if ( $form['form_type'] == 'api' ): ?>
  
          <div class="contactForm__success align--center">
            <?= $form['success']; ?>
            <p><span id="TrialApiKey">7499336e-9a6a-4669-8c3d-32e35ceae1f2</span><span class="tooltip">Click to Copy</span></p>
          </div>
  
          <div class="contactForm__error align--center">
            <?= $form['error']; ?>       
          </div>
  
          <div class="contactForm__response align--center">
            <h3>An trial Indxr licence for your email address already exists, please contact support@encodian.com if you require further assistance.</h3>
          </div>
  
          <div class="contactForm__exists align--center">
            <h3>An existing Encodian subscription has been found.</h3>
            <p>An email containing the details of your existing Encodian subscription has been sent to your email address.</p>
          </div>
  
          <form
            class="contactForm__form flex flex--x-between flex--y-start"
            data-processtrial
            data-validatetrial
            data-type="<?= $form['type'] ?>"
            data-api="<?= $form['api_endpoint'] ?>"
            data-restricted="<?= $form['restricted_domains'] ?>"
            data-success="<h2>Thank you for your enquiry. We will be in touch soon.</h2>"
            data-tracking="<?= $form['tracking_script']; ?>">
  
            <?php foreach( $form['fields'] as $field ): ?>
  
              <?php if ( $field['type'] == 'select'): ?>
  
                <select id="<?= $field['key']; ?>" name="<?= $field['key']; ?>" <?= ($field['required']) ? "required" : ""; ?>>
                  <option value="" selected disabled><?= $field['placeholder']; ?></option>
                  <?php foreach( $field['options'] as $option ): ?>
                    <option value="<?= strtolower($option['value']) ?>"><?= $option['value'] ?></option>
                  <?php endforeach; ?>
                </select>
  
              <?php else: ?>
  
                <fieldset>
                  <input
                    id="<?= $field['key']; ?>"
                    name="<?= $field['key']; ?>"
                    type="<?= $field['type']; ?>" 
                    placeholder="<?= $field['placeholder']; ?>"
                    autocomplete="off"
                    <?= ($field['required']) ? "required" : ""; ?>
                    <?= ($field['type'] == 'email') ? 'pattern="^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*(\.\w{2,})+$"' : ""; ?>
                  >
                </fieldset>
  
              <?php endif; ?>
              
            <?php endforeach; ?>
  
            <div class="contactForm__boxes flex">
              <fieldset class="checkbox">
                <input name="privacy_check" type="checkbox" id="privacy" autocomplete="off" required>
                <span class="checkbox__box"></span>
                <label for="privacy">* I can confirm I have read and accept the <a href="https://support.encodian.com/hc/en-gb/articles/360010642813-Terms-of-Service" target="_blank">Terms of Service</a> and <a href="https://support.encodian.com/hc/en-gb/articles/360010559134-Data-Processing-Agreement" target="_blank">Data Processing Agreement.</a></label>
              </fieldset>
              <fieldset class="checkbox">
                <input name="MailListApproved" id="mail_check" type="checkbox" autocomplete="off" id="marketing">
                <span class="checkbox__box"></span>
                <label for="marketing">Keep me up to date with news and product updates</label>
              </fieldset>
            </div>
  
            <input type="hidden" id="gclid_field" name="gclid" value="">
            <input type="hidden" id="utm_source" name="utmSource" value="">
            <input type="hidden" id="utm_medium" name="utmMedium" value="">
            <input type="hidden" id="utm_campaign" name="utmCampaign" value="">
            <input type="hidden" id="utm_term" name="utmTerm" value="">
  
            <button type="submit">Submit <div class="spinner"></div></button>
  
          </form>
  
        <?php elseif ( $form['form_type'] == 'demo' ): ?>
  
          <div class="contactForm__success align--center">
            <?= $form['success']; ?>
          </div>
  
          <div class="contactForm__error align--center">
            <?= $form['error']; ?>       
          </div>
  
          <form
            class="contactForm__form flex flex--x-between flex--y-start"
            data-demo
            data-validatedemo
            data-api="<?= $form['api_endpoint'] ?>"
            data-restricted="<?= $form['restricted_domains'] ?>"
            data-subject="<?= $form['subject'] ?>"
            data-recipient="<?= $form['recipient_email'] ?>"
            data-footer="<?= $form['footer'] ?>"
            data-success="<h2>Thank you for your enquiry. We will be in touch soon.</h2>"
            data-type="<?= get_the_title(); ?>"
            data-tracking="<?= $form['tracking_script']; ?>">
  
            <?php foreach( $form['fields'] as $field ): ?>
  
              <?php if ( $field['type'] == 'select'): ?>
  
                <select id="<?= $field['key']; ?>" name="<?= $field['key']; ?>" <?= ($field['required']) ? "required" : ""; ?>>
                  <option value="" selected disabled><?= $field['placeholder']; ?></option>
                  <?php foreach( $field['options'] as $option ): ?>
                    <option value="<?= strtolower($option['value']) ?>"><?= $option['value'] ?></option>
                  <?php endforeach; ?>
                </select>
  
              <?php else: ?>
  
                <fieldset>
                  <input
                    id="<?= $field['key']; ?>"
                    name="<?= $field['key']; ?>"
                    type="<?= $field['type']; ?>" 
                    placeholder="<?= $field['placeholder']; ?>"
                    autocomplete="off"
                    <?= ($field['required']) ? "required" : ""; ?>
                    <?= ($field['type'] == 'email') ? 'pattern="^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*(\.\w{2,})+$"' : ""; ?>
                  >
                </fieldset>
  
              <?php endif; ?>
  
            <?php endforeach; ?>
  
            <div class="contactForm__boxes flex">
              <fieldset class="checkbox">
                <input name="privacy_check" type="checkbox" id="privacy" required>
                <span class="checkbox__box"></span>
                <label for="privacy">* I can confirm I have read and accept the <a href="https://support.encodian.com/hc/en-gb/articles/360010642813-Terms-of-Service" target="_blank">Terms of Service</a> and <a href="https://support.encodian.com/hc/en-gb/articles/360010559134-Data-Processing-Agreement" target="_blank">Data Processing Agreement.</a></label>
              </fieldset>
              <fieldset class="checkbox">
                <input id="mail_check" name="mail_check" type="checkbox" id="marketing">
                <span class="checkbox__box"></span>
                <label for="marketing">Keep me up to date with news and product updates</label>
              </fieldset>
            </div>
  
            <input type="text" name="sfu" class="sfu">
  
            <input type="hidden" id="PageName" name="PageName" value="<?= get_the_title(); ?>">
            <input type="hidden" id="gclid_field" name="gclid_field" value="">
            <input type="hidden" id="utm_source" name="utm_source" value="">
            <input type="hidden" id="utm_medium" name="utm_medium" value="">
            <input type="hidden" id="utm_campaign" name="utm_campaign" value="">
            <input type="hidden" id="utm_term" name="utm_term" value="">
  
            <button type="submit">Submit <div class="spinner"></div></button>
  
          </form>
  
        <?php else: ?>
  
          <form
            class="contactForm__form flex flex--x-between flex--y-start"
            data-process
            data-validate
            data-subject="<?= $form['subject'] ?>"
            data-recipient="<?= $form['recipient_email'] ?>"
            data-footer="<?= $form['footer'] ?>"
            data-restricted="<?= $form['restricted_domains'] ?>"
            data-success="<?= $form['success'] ?>"
            data-tracking="<?= $form['tracking_script']; ?>">
    
            <?php foreach( $form['fields'] as $field ): ?>
    
              <?php if ( $field['type'] == 'select'): ?>
    
                <select id="<?= $field['key']; ?>" name="<?= $field['key']; ?>" <?= ($field['required']) ? "required" : ""; ?>>
                  <option value="" selected disabled><?= $field['placeholder']; ?></option>
                  <?php foreach( $field['options'] as $option ): ?>
                    <option value="<?= strtolower($option['value']) ?>"><?= $option['value'] ?></option>
                  <?php endforeach; ?>
                </select>
    
              <?php else: ?>
    
                <fieldset>
                  <input
                    id="<?= $field['key']; ?>"
                    name="<?= $field['key']; ?>"
                    type="<?= $field['type']; ?>" 
                    placeholder="<?= $field['placeholder']; ?>"
                    autocomplete="off"
                    <?= ($field['required']) ? "required" : ""; ?>
                    <?= ($field['type'] == 'email') ? 'pattern="^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*(\.\w{2,})+$"' : ""; ?>
                  >
                </fieldset>
    
              <?php endif; ?>
              
            <?php endforeach; ?>
    
            <div class="contactForm__boxes flex">
              <fieldset class="checkbox">
                <input name="privacy_check" type="checkbox" id="privacy" required>
                <span class="checkbox__box"></span>
                <label for="privacy">* I can confirm I have read and accept the <a href="https://support.encodian.com/hc/en-gb/articles/360010642813-Terms-of-Service" target="_blank">Terms of Service</a> and <a href="https://support.encodian.com/hc/en-gb/articles/360010559134-Data-Processing-Agreement" target="_blank">Data Processing Agreement.</a></label>
              </fieldset>
              <fieldset class="checkbox">
                <input id="mail_check" name="mail_check" type="checkbox" id="marketing">
                <span class="checkbox__box"></span>
                <label for="marketing">Keep me up to date with news and product updates</label>
              </fieldset>
            </div>
    
            <input type="hidden" id="PageName" name="PageName" value="<?= get_the_title(); ?>">
            <input type="hidden" id="gclid_field" name="gclid_field" value="">
            <input type="hidden" id="utm_source" name="utm_source" value="">
            <input type="hidden" id="utm_medium" name="utm_medium" value="">
            <input type="hidden" id="utm_campaign" name="utm_campaign" value="">
            <input type="hidden" id="utm_term" name="utm_term" value="">
    
            <input type="text" name="sfu" class="sfu">
    
            <button type="submit">Submit <div class="spinner"></div></button>
    
          </form>
  
        <?php endif; ?>

      <?php endif; ?>


    </div>
    
  </div>
</section>