<?php 
  $heading = get_field('client_heading') ? get_field('client_heading') : get_sub_field('heading');
  $logos = get_field('logos') ? get_field('logos') : get_sub_field('logos');
?>

<section id="clientlogos" class="clientLogos" data-inview="client">
  <div class="container container--l">
    <h2 class="clientLogos__heading fs--40 align--center" data-split-lines><?= $heading; ?></h2>
    <ul class="clientLogos__list flex">
      <?php foreach( $logos as $logo ): ?>
        <li class="clientLogos__item flex flex--x-center flex--y-center">
          <?php if ( $logo['url'] ): ?>
            <img src="<?= $logo['url']; ?>" alt="<?= $logo['alt']; ?>">
          <?php else: ?>
            <img src="<?= $logo['logo']["url"]; ?>" alt="<?= $logo['logo']["alt"]; ?>">
          <?php endif; ?>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
</section>