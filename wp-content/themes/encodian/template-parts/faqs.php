<?php 
  $heading = get_field('faqs_heading') ? get_field('faqs_heading') : get_sub_field('heading');
  $faqs = get_field('faqs_items') ? get_field('faqs_items') : get_sub_field('items');
?>

<section class="faqs pos--rel z--2" data-section="faqs">
  <div class="container container--l">
    <h2 class="faqs__heading" data-split-lines data-inview><?= $heading; ?></h2>

    <?php if ( $faqs ) : ?>

      <ul class="faqs__list" data-accordian>
        <?php foreach( $faqs as $faq ): ?>
          <li class="faqs__item pos--rel" data-accordian-item data-inview="start">
            <h3 class="faqs__title ff--body fs--20 fw--400 lh--140" data-accordian-title><?= $faq['title'] ?></h3>
            <div class="faqs__text"><?= $faq['text'] ?></div>
          </li>
        <?php endforeach; ?>
      </ul>

    <?php endif; ?>
    
  </div>
</section>