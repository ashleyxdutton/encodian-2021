<?php /*
  Template Name: About Us
  Template Post Type: page
*/ ?>

<section class="introBasic" data-inview>
  <div class="introBasic__content o--0 z--3">
    <h1 class="introBasic__heading" data-split-lines><?= the_field('about_heading'); ?></h1>
    <p class="introBasic__text fs--24"><?= the_field('about_text'); ?></p>
  </div>
</section>

<section class="team">
  <div class="container container--l">
    <h2 class="team__heading fs--60" data-inview data-split-lines><?= the_field('team_heading'); ?></h2>

    <?php $members = get_field('team_members'); ?>

    <ul class="team__list flex">
      <?php foreach( $members as $member ): ?>
      <li class="team__member" data-inview>
        <div class="team__image" style="background-image:url('<?= $member['image']['url'] ?>')"></div>
        <p class="team__name fs--24 fw--700"><?= $member['name'] ?></p>
        <p class="team__title fs--24"><?= $member['job_title'] ?></p>

        <?php if ($member['linkedin_url']): ?>
          <a class="team__linkedin" href="<?= $member['linkedin_url'] ?>" target="_blank"></a>
        <?php endif; ?>

      </li>
      <?php endforeach; ?>
    </ul>

  </div>
</section>

<section class="products">
  <div class="container">
    <h2 class="products__heading fs--60"><?= the_field('products_heading'); ?></h2>

    <?php $products = get_field('select_products'); ?>

    <ul class="products__list products__list--<?= count($products); ?> flex pos--rel">
      <?php foreach( $products as $product): ?>
        <li class="products__item align--center flex">
          <img class="products__icon" src="<?= get_field('icon', $product['product']->ID); ?>" alt="">
          <h2 class="products__title fs--40"><?= get_field('heading', $product['product']->ID); ?></h2>
          <p class="products__text"><?= $product['product']->post_excerpt ?></p>
          <a class="products__button button" href="<?= get_the_permalink($product['product']->ID); ?>">Find out more</a>
        </li>
      <?php endforeach; ?>
    </ul>

  </div>
</section>

<section class="serviceTabs serviceTabs--full pos--rel" data-inview>
  <div class="container container--l pos--rel">
  
    <?php $services = get_field('add_services'); ?>

    <div class="serviceTabs__header flex">
      <h2 class="serviceTabs__heading fs--60"><?= the_field('services_heading'); ?></h2>
      <nav class="serviceTabs__tabs flex flex--x-end flex--r-nowrap">
        <?php foreach( $services as $service): ?>
          <a class="serviceTabs__tab fs--20 fw--700 ls--t10 pos--rel" href="">
            <?= $service['heading'] ?>
            <div class="serviceTabs__arrow pos--abs"><?= file_get_contents('' . get_site_url() . '/wp-content/themes/encodian/src/svgs/arrow.svg'); ?></div>
          </a>
        <?php endforeach; ?>
      </nav>
    </div>

    <div class="serviceTabs__slides pos--rel z--2">
      <?php foreach( $services as $service): ?>
        <div class="serviceTabs__slide pos--abs flex">
          <h3 class="serviceTabs__title"><?= $service['heading'] ?></h3>
          <div class="serviceTabs__text">
            <?= $service['text'] ?>

            <?php if ( $service['button'] ): ?>
              <a class="serviceTabs__button button" href="<?= $service['button']['url']; ?>" target="<?= $service['button']['target']; ?>"><?= $service['button']['title']; ?></a>
            <?php endif; ?>
            
          </div>
        </div>
      <?php endforeach; ?>
    </div>

    <?= file_get_contents('' . get_site_url() . '/wp-content/themes/encodian/src/svgs/services-clip.svg'); ?>
    <img class="services-sketch" src="<?= '' . get_site_url() . '/wp-content/themes/encodian/src/images/services-sketch.png' ?>" alt="">

  </div>
</section>

<?= get_template_part( 'template-parts/form' ); ?>