<?php /* Template name: Front Page */ ?>

<div class="fixedLogo z--9 pos--abs" data-inview>
  <div class="container container--l">
    <a class="fixedLogo__logo" href="/">
      <?= load_svg('logo'); ?>
    </a>
  </div>
</div>

<section class="hero pos--rel" data-inview="hero">
  <div class="container container--l flex pos--rel">

    <div class="hero__content z--3 o--0">
      <h1 class="hero__heading" data-split-lines><?= the_field('heading'); ?></h1>
      <p class="hero__text fs--24"><?= the_field('text'); ?></p>
    </div>

    <div class="hero__imageHolder align--center pos--rel z--2">
      <?= load_svg('logo-icon'); ?>

      <img class="hero-drawing" src="<?= '' . get_site_url() . '/wp-content/themes/encodian/src/images/hero-drawing.png' ?>" alt="">
    </div>

    <a class="hero__scroll pos--abs z--2" href="#" data-to="<?= the_field('scroll_to_section'); ?>">
      <?= load_svg('arrow'); ?>
    </a>

  </div>

  <div class="hero__bg pos--abs"></div>

</section>

<div data-light>

<?php get_template_part( 'template-parts/client-logos', 'part' ); ?>

<section id="products" class="featuredProducts pos--rel" data-inview>
  <div class="container container--l flex pos--rel z--2">

    <div class="featuredProducts__slides pos--rel">

      <?php $products = get_field('select_products'); ?>

      <div class="featuredProducts__carousel">

        <?php foreach( $products as $index => $product): ?>

          <div class="featuredProducts__slide pos--rel z--2" data-shape="<?= $index; ?>" data-colour="<?= get_field('colour', $product['product']->ID); ?>">
            <img class="featuredProducts__icon" src="<?= get_field('icon', $product['product']->ID); ?>" alt="">
            <h2 class="featuredProducts__heading"><?= get_field('heading', $product['product']->ID); ?></h2>
            <p class="featuredProducts__text fs--24"><?= $product['product']->post_excerpt ?></p>
            <div class="featuredProducts__links flex flex--y-center">
              <a class="featuredProducts__button button" href="<?= $product["button"]["url"]; ?>" target="<?= $product["button"]["target"]; ?>"><?= $product["button"]["title"]; ?></a>
              <?php if ($product["link"]): ?>
                <a class="featuredProducts__link fs--14" href="<?= $product["link"]["url"]; ?>" target="<?= $product["link"]["target"]; ?>"><?= $product["link"]["title"]; ?></a>
              <?php endif; ?>
            </div>
          </div>

        <?php endforeach; ?>

      </div>

      <div class="featuredProducts__shape pos--abs">
        <?= file_get_contents('' . get_site_url() . '/wp-content/themes/encodian/src/svgs/product-shapes.svg'); ?>
        <?= file_get_contents('' . get_site_url() . '/wp-content/themes/encodian/src/svgs/product-shapes-mobile.svg'); ?>

        <div class="featuredProducts__sketches pos--abs center--xy">
          <?php foreach( $products as $product): ?>
            <img class="featuredProducts__sketch pos--abs center--xy <?= get_the_title($product['product']->ID); ?>" src="<?= get_field('sketch_image', $product['product']->ID); ?>" alt="">
          <?php endforeach; ?>
        </div>

        <div class="featuredProducts__pagination pos--abs center--y">
          <?php foreach( $products as $product): ?>
            <div class="featuredProducts__paginationItem"></div>
          <?php endforeach; ?>
        </div>

      </div>
    
    </div>

  </div>

  <div class="featuredProducts__bg pos--abs"></div>

</section>

</div>

<section id="services" class="serviceTabs pos--rel" data-inview>

  <?php $services = get_field('add_services'); ?>

  <div class="serviceTabs__header flex">
    <h2 class="serviceTabs__heading ff--body fs--18 fw--300 ls--l40 tt--upper"><?= the_field('services_heading'); ?></h2>
    <nav class="serviceTabs__tabs flex flex--x-end flex--r-nowrap">
      <?php foreach( $services as $service): ?>
        <a class="serviceTabs__tab fs--20 fw--700 ls--t10 pos--rel" href="">
          <?= $service['heading'] ?>
          <div class="serviceTabs__arrow pos--abs"><?= file_get_contents('' . get_site_url() . '/wp-content/themes/encodian/src/svgs/arrow.svg'); ?></div>
        </a>
      <?php endforeach; ?>
    </nav>
  </div>

  <div class="serviceTabs__slides pos--rel z--2">
    <?php foreach( $services as $service): ?>
      <div class="serviceTabs__slide pos--abs flex">
        <h3 class="serviceTabs__title"><?= $service['heading'] ?></h3>
        <div class="serviceTabs__text">
          <?= $service['text'] ?>

          <?php if ( $service['button'] ): ?>
            <a class="serviceTabs__button button" href="<?= $service['button']['url']; ?>" target="<?= $service['button']['target']; ?>"><?= $service['button']['title']; ?></a>
          <?php endif; ?>

        </div>
      </div>
    <?php endforeach; ?>
  </div>

  <?= file_get_contents('' . get_site_url() . '/wp-content/themes/encodian/src/svgs/services-clip.svg'); ?>
  <img class="services-sketch" src="<?= '' . get_site_url() . '/wp-content/themes/encodian/src/images/services-sketch.png' ?>" alt="">

</section>

<section id="testimonials" class="testimonials" data-light>
  <div class="container container--l">
    <div class="testimonials__header" data-inview>
      <h2 class="testimonials__heading" data-split-lines><?= the_field('testimonial_heading'); ?></h2>
    </div>

    <?php $testimonials = get_field('add_testimonials'); ?>

    <div class="testimonials__holder pos--rel" data-inview="testimonials">

      <ul class="testimonials__slider pos--rel z--2" data-testimonials>
        <?php foreach( $testimonials as $testimonial): ?>
          <li class="testimonials__item">
            <div class="testimonials__author pos--rel flex flex--x-center">
              <img class="testimonials__image" src="<?= $testimonial['image']['url'] ?>" alt="">
              <p class="testimonials__name fs--16 fw--700"><?= $testimonial['author'] ?></p>
              <p class="testimonials__company fs--16"><?= $testimonial['company_title'] ?></p>
            </div>
            <p class="testimonials__quote fs--24"><?= $testimonial['quote'] ?></p>
          </li>
        <?php endforeach; ?>
      </ul>

      <div class="testimonials__bg pos--abs">
        <?= file_get_contents('' . get_site_url() . '/wp-content/themes/encodian/src/svgs/testimonials-bg.svg'); ?>
        <?= file_get_contents('' . get_site_url() . '/wp-content/themes/encodian/src/svgs/testimonials-clip.svg'); ?>
      </div>

    </div>

  </div>
</section>

<?= get_template_part( 'template-parts/form' ); ?>