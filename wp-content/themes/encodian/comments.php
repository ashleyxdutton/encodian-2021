<section id="comments" class="comments">
	<div class="container container--s">

		<?php if ( have_comments() ) : ?>

			<h2 class="comments__heading fs--30">
				<?php $count = get_comments_number();
					if ( '1' === $count ) {
						echo '' . $count . ' Comment';
					} else {
						echo '' . $count . ' Comments';
					}
				?>
			</h2>

			<?php the_comments_navigation(); ?>

			<ol class="comments__list">
				<?php
					wp_list_comments(
						array(
							'style'      => 'ol',
							'short_ping' => true,
						)
					);
				?>
			</ol>

			<?php the_comments_navigation();

			// If comments are closed and there are comments, let's leave a little note, shall we?
			if ( ! comments_open() ) : ?>

				<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'encodian' ); ?></p>

			<?php endif;

		endif; ?>

		</div>

		<div class="container container--m">
			
			<?php

			$send = 'Submit';
			$reply = 'Leave a comment';
			$reply_to = 'Reply';
			$author = '*Name';
			$email = '*Email address';
			$comment = 'Comment';
			$url = 'Website';
			$comment_cancel = 'Cancel Reply';

			if (is_user_logged_in()) {
				$container_class = 'comment-respond loggedin';
			} else {
				$container_class = 'comment-respond';
			}

			$comments_args = array(
				'fields' => array(
					'author' => '<div class="comment-form-fields"><p class="comment-form-author"><input id="author" type="text" name="author" aria-required="true" placeholder="' . $author .'"></input></p>',
					'email' => '<p class="comment-form-email"><input id="email" type="email" name="email" placeholder="' . $email .'"></input></p>',
					'url' => '<p class="comment-form-url"><input id="url" type="url" name="url" placeholder="' . $url .'"></input></p></div>',
				),
				'class_container' => $container_class,
				'label_submit' => __( $send ),
				'title_reply' => __( $reply ),
				'title_reply_to' => __( $reply_to ),
				'cancel_reply_link' => __( $comment_cancel ),
				'comment_field' => '<p class="comment-form-comment"><textarea id="comment" name="comment" aria-required="true" placeholder="' . $comment .'"></textarea></p>',
				'id_submit' => __( 'comment-submit' ),
			);

			comment_form($comments_args); ?>

		</div>

</section>