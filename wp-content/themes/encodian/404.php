<?php
/*
 * 404 Page Template
 */
?>

<section class="errorPage">
  <div class="container container--m align--center">
    <h1>404</h1>
    <h2>Page not found</h2>
    <p><a class="button" href="<?php echo get_option('home'); ?>">Return to the homepage</a></p>
  </div>
</section>