<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title><?php wp_title(); ?></title>

    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/src/assets/brand/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/src/assets/brand/favicon.ico" type="image/x-icon" />

    <!-- <link rel="preload" as="style" href="<?php bloginfo('template_directory'); ?>/dist/fonts.css"> -->
 
    <!-- <script type="text/javascript" src="https://www.bugherd.com/sidebarv2.js?apikey=o8101cjttsfxq8nvrwaksa" async="true"></script> -->

    <script type="text/javascript" async>
      WebFontConfig = {
        custom: {
          families: [
            'Right Grotesk',
            'Biennale'
          ],
          urls: [
            '<?php bloginfo('template_directory'); ?>/dist/fonts.css',
          ]
        },
      };
      (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
      })();
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KR6N6FK');</script>
    <!-- End Google Tag Manager -->

    <?php wp_head(); ?>

    <?php if ( strpos($_SERVER['HTTP_HOST'], 'bgn.agency') !== false ): ?>
      <meta name="robots" content="noindex, nofollow">
    <?php endif; ?>

  </head>
  <body <?php body_class(); ?>>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KR6N6FK"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <?php get_header( root_template_base() ); ?>

    <main role="main">
      <?php include root_template_path(); ?>
    </main>

    <?php get_footer( root_template_base() ); ?>

  </body>
</html>